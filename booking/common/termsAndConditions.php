<?php
include "common.php";
// Terms and conditions text

function getTermsAndConditions($html)
{
    $nl = "\n";
    $retString = "TERMS AND CONDITIONS: ";
    if($html == true)
    {
        $nl = " ";
        $retString .= "<p>";
    }
    $retString .= " $nl";
    $retString .= "No refunds for cancellation within 7 days of coach tours.$nl";
    $retString .= "No refunds on air tour package once it is confirmed.$nl";
    $retString .= "It is the passenger's responsibility to be at the departure$nl";
    $retString .= "point at the appointed time.  No refunds will be given for a missed$nl";
    $retString .= "departure.  Tours must be booked and paid in advance.  Tours$nl";
    $retString .= "and prices are subject to availability at time of booking.$nl";
    $retString .= "All prices include taxes.  Except when otherwise noted, meals are$nl";
    $retString .= "not included.  Tour prices do not include tips for drivers and/or$nl";
    $retString .= "guides.  Local government may limit sightseeing tours.$nl";
    
    return $retString;
}
?>
