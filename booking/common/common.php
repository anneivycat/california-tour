<?php
// common.php
class DebugLogFile
{
    var $fp;
    var $pathName;
    var $filePath = "C:\\tmp\\phpDebug.txt";
    function open()
    {
        
        $this->fp = fopen($filePath,"a+");
        if(!$this->fp)
        {
            echo("Could not open file " . $inPathName);
        }
        else
        {
           echo("Ok So far<p>");   
        }
        return;
    }
    function log($string)
    {
        if($this->fp)
        {
            $msg = $string . "  time:" . date("D M H:i:s") . "\n";
            fputs($this->fp,$msg);
            print("<p>" . $msg);
        }
    }
    function close()
    {
        fclose($this->fp);
    }
}

function getTermsAndConditions($html)
{
    $nl = "\n";
    $retString = "TERMS AND CONDITIONS: ";
    if($html == true)
    {
        $nl = " ";
        $retString .= "<p>";
    }
    $retString .= " $nl";
    $retString .= "No refunds for cancellation within 7 days of coach tours.$nl";
    $retString .= "No refunds on air tour package once it is confirmed.$nl";
    $retString .= "It is the passengers responsibility to be at the departure$nl";
    $retString .= "point by pick up time.  No refunds will be given for a missed$nl";
    $retString .= "departure.  Tours must be booked and paid in advance.  Tours$nl";
    $retString .= "and prices are subject to availability at time of booking.$nl";
    $retString .= "All prices include taxes.  Except when otherwise noted, meals are$nl";
    $retString .= "not included.  Tour prices do not include tips for drivers and/or$nl";
    $retString .= "guides.  Local government may limit sightseeing tours.$nl";
    
    return $retString;
}

?>
