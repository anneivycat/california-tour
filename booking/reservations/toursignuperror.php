<?php
/**
  * Data Validation is below
  */
 include_once 'validateform.php';

 $query_string = "LocalScheduledTourCode=" . @$_REQUEST['LocalScheduledTourCode'] . "&languageUsed=";
 $error        = 0;
 $error_string = '';
 
 if(!isset($_POST['Occupancy']) || !is_numeric($_POST['Occupancy'])) {
   $_POST['Occupancy'] = 1;	 
 } 
 
 if(!checkFirstname(@$_POST['FirstName'])) {
    $error++;
    $error_string .= 'Please enter a valid First Name' . '<br>';
 }
 if(!checkLastname(@$_POST['LastName'])) {
    $error++;
    $error_string .= 'Please enter a valid Last Name' . '<br>';
 }
 if(!checkEmail(@$_POST['Email'])) {
    $error++;
    $error_string .= 'Please enter a valid Email' . '<br>';
 }

 if($_POST['Occupancy'] == 2) {
   if(!isset($_POST['find_roommate_1'])) {
       if(!checkFirstname(@$_POST['FirstName_1'])) {
	 $error++;
	 $error_string .= 'Please enter a valid first name for your roommate or select "Find me a roommate"' . '<br>';
       }
       if(!checkLastname(@$_POST['LastName_1'])) {
	 $error++;
	 $error_string .= 'Please enter a valid last name for your roommate or select "Find me a roommate"' . '<br>';
       }		 
   }
 }
 
 if($_POST['Occupancy'] == 3) {
   $passenger = 1;	 
   if(!isset($_POST['find_roommate_1'])) {
     $passenger++;  
   }
   if(!isset($_POST['find_roommate_2'])) {
     $passenger++;  
   }
   
   if($passenger == 2) {
      if(!isset($_POST['find_roommate_1'])) { 
	  if(!checkFirstname(@$_POST['FirstName_1'])) {
	     $error++;
	     $error_string .= 'Please enter a valid first name for your first roommate or select "Find me a roommate"' . '<br>';
	  }
	  if(!checkLastname(@$_POST['LastName_1'])) {
	     $error++;
	     $error_string .= 'Please enter a valid last name for your first roommate or select "Find me a roommate"' . '<br>';
	  }    
	  if(@$_POST['Gender'] != @$_POST['Gender_1']) {
	     $error++;
	     $error_string .= 'Triple occupancy passengers must be of the same gender' . '<br>';	  
	  }
      }
      if(!isset($_POST['find_roommate_2'])) { 
	  if(!checkFirstname(@$_POST['FirstName_2'])) {
	     $error++;
	     $error_string .= 'Please enter a valid first name for your second roommate or select "Find me a roommate"' . '<br>';
	  }
	  if(!checkLastname(@$_POST['LastName_2'])) {
	     $error++;
	     $error_string .= 'Please enter a valid last name for your second roommate or select "Find me a roommate"' . '<br>';
	  }    
	  if(@$_POST['Gender'] != @$_POST['Gender_2']) {
	     $error++;
	     $error_string .= 'Triple occupancy passengers must be of the same gender' . '<br>';	  
	  }
      }
   }
   if($passenger == 3) {
      if(!isset($_POST['find_roommate_1'])) { 
	  if(!checkFirstname(@$_POST['FirstName_1'])) {
	     $error++;
	     $error_string .= 'Please enter a valid first name for your first roommate or select "Find me a roommate"' . '<br>';
	  }
	  if(!checkLastname(@$_POST['LastName_1'])) {
	     $error++;
	     $error_string .= 'Please enter a valid last name for your first roommate or select "Find me a roommate"' . '<br>';
	  }    
      }
      if(!isset($_POST['find_roommate_2'])) { 
	  if(!checkFirstname(@$_POST['FirstName_2'])) {
	     $error++;
	     $error_string .= 'Please enter a valid first name for your second roommate or select "Find me a roommate"' . '<br>';
	  }
	  if(!checkLastname(@$_POST['LastName_2'])) {
	     $error++;
	     $error_string .= 'Please enter a valid last name for your second roommate or select "Find me a roommate"' . '<br>';
	  }    
      }	   
   }
 }// end if
 
 //Quad Occupancy has its own special rule
 if($_POST['Occupancy'] == 4) {
   $passenger = 1;	 
   if(!isset($_POST['find_roommate_1'])) {
     $passenger++;  
   }
   if(!isset($_POST['find_roommate_2'])) {
     $passenger++;  
   }
   if(!isset($_POST['find_roommate_3'])) {
     $passenger++;  
   }
   if($passenger != 2 && $passenger != 4) {  
     $error++;
     $error_string .= 'Quad occupancy must have either two or four passengers' . '<br>';
   }
   if($passenger == 2) {
      if(!isset($_POST['find_roommate_1'])) { 
	  if(!checkFirstname(@$_POST['FirstName_1'])) {
	     $error++;
	     $error_string .= 'Please enter a valid first name for your first roommate' . '<br>';
	  }
	  if(!checkLastname(@$_POST['LastName_1'])) {
	     $error++;
	     $error_string .= 'Please enter a valid last name for your first roommate' . '<br>';
	  }    
	  if(@$_POST['Gender'] != @$_POST['Gender_1']) {
	     $error++;
	     $error_string .= 'Quad occupancy passengers must be of the same gender' . '<br>';	  
	  }
      }
      if(!isset($_POST['find_roommate_2'])) { 
	  if(!checkFirstname(@$_POST['FirstName_2'])) {
	     $error++;
	     $error_string .= 'Please enter a valid first name for your second roommate or select "Find me a roommate"' . '<br>';
	  }
	  if(!checkLastname(@$_POST['LastName_2'])) {
	     $error++;
	     $error_string .= 'Please enter a valid last name for your second roommate or select "Find me a roommate"' . '<br>';
	  }    
	  if(@$_POST['Gender'] != @$_POST['Gender_2']) {
	     $error++;
	     $error_string .= 'Quad occupancy passengers must be of the same gender' . '<br>';	  
	  }
      }
      if(!isset($_POST['find_roommate_3'])) { 
	  if(!checkFirstname(@$_POST['FirstName_3'])) {
	     $error++;
	     $error_string .= 'Please enter a valid first name for your third roommate or select "Find me a roommate"' . '<br>';
	  }
	  if(!checkLastname(@$_POST['LastName_3'])) {
	     $error++;
	     $error_string .= 'Please enter a valid last name for your third roommate or select "Find me a roommate"' . '<br>';
	  }    
	  if(@$_POST['Gender'] != @$_POST['Gender_3']) {
	     $error++;
	     $error_string .= 'Quad occupancy passengers must be of the same gender' . '<br>';	  
	  }
      }
   }// end if
   
   if($passenger == 4) {
      if(!isset($_POST['find_roommate_1'])) { 
	  if(!checkFirstname(@$_POST['FirstName_1'])) {
	     $error++;
	     $error_string .= 'Please enter a valid first name for your first roommate' . '<br>';
	  }
	  if(!checkLastname(@$_POST['LastName_1'])) {
	     $error++;
	     $error_string .= 'Please enter a valid last name for your first roommate' . '<br>';
	  }    
      }   
      if(!isset($_POST['find_roommate_2'])) { 
	  if(!checkFirstname(@$_POST['FirstName_2'])) {
	     $error++;
	     $error_string .= 'Please enter a valid first name for your second roommate or select "Find me a roommate"' . '<br>';
	  }
	  if(!checkLastname(@$_POST['LastName_2'])) {
	     $error++;
	     $error_string .= 'Please enter a valid last name for your second roommate or select "Find me a roommate"' . '<br>';
	  }    
      } 
      if(!isset($_POST['find_roommate_3'])) { 
	  if(!checkFirstname(@$_POST['FirstName_3'])) {
	     $error++;
	     $error_string .= 'Please enter a valid first name for your third roommate or select "Find me a roommate' . '<br>';
	  }
	  if(!checkLastname(@$_POST['LastName_3'])) {
	     $error++;
	     $error_string .= 'Please enter a valid last name for your third roommate or select "Find me a roommmate"' . '<br>';
	  }    
      } 
   }// end if
   
 }
 
 if($error > 0) {
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
   "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
   <html xmlns="http://www.w3.org/1999/xhtml">
   <head>
   <title>California Tour: Error</title>
   <meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
   <script type="text/javascript" src="/scripts/emails.js"></script>
   <?php
   include "../header.php";
   ?>
   <h2>Book a Tour</h2>
    <h4>Error</h4>

	<p><b>Please fill out all the required information</b></p>
	<p style="color:#CF0000;"><?php echo $error_string; ?></p>
	
	<p>Please <a href="javascript:history.go(-1)"  onMouseOver="self.status=document.referrer;return true">go back</a> to correct the above error before you proceed</p>

<?php
include "../footer.php";
?>
<?php
  die();
 }

?>
