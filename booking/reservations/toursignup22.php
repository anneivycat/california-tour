<?php
include_once '../common/common.php';
include_once '../sqlfuncs.php';


   if(isset($_REQUEST["LocalScheduledTourCode"]))
   		$LocalScheduledTourCode = $_REQUEST["LocalScheduledTourCode"];
   		
   if(isset($_REQUEST["languageUsed"]))
   		$languageUsed = $_REQUEST["languageUsed"];


   if(isset($_REQUEST["TourName"]))
   		$TourName = $_REQUEST["TourName"];
   		
   if(isset($_REQUEST["TourType"]))
   		$TourType = $_REQUEST["TourType"];
   		
   if(isset($_REQUEST["OPTIONALTOUR_ID_1"]))
   		$OPTIONALTOUR_ID_1 = $_REQUEST["OPTIONALTOUR_ID_1"];
   		
   if(isset($_REQUEST["SchoolCode"]))
   		$SchoolCode = $_REQUEST["SchoolCode"];

session_start();
// get the optional_tour_info
//get the optional tour checkbox(s) info - either from previous session (because just got bounced back here b/c or error on the toursignuptotal) or from post
if ( @$_POST['OPTIONALTOUR_ID_1'] != ''   ){
	$OPTIONALTOUR_ID_1 = $_POST['OPTIONALTOUR_ID_1'];
	$_SESSION['OPTIONALTOUR_ID_1'] = $OPTIONALTOUR_ID_1;
}
else {
	$OPTIONALTOUR_ID_1 = $_SESSION['OPTIONALTOUR_ID_1'];
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>California Tour: Book a Tour</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<?php
include "../header.php";
?>
<?php
   $mysql_link = connect_to_db();
   $mysql_result = select_db($mysql_link);

   $one_day_tour = false;
   $TourCodeLen = strlen($LocalScheduledTourCode);
   if (($TourCodeLen > 0) and (strcmp($LocalScheduledTourCode, "BLANK") != 0))   { 
      $query = "SELECT S.TOUR_ID, S.SCHEDULEDTOUR_ID, T.TOUR_NAME, T.TOUR_NAME_JP, S.SCHEDULEDTOUR_DEPART_DATE, S.SCHEDULEDTOUR_RETURN_DATE, T.TOUR_TYPE, T.CITY_ID, C.CITY_CODE, C.CITY_DESC FROM SCHEDULEDTOUR AS S, TOUR AS T, CITY AS C WHERE S.SCHEDULEDTOUR_CODE like '$LocalScheduledTourCode%' AND S.TOUR_ID = T.TOUR_ID AND T.CITY_ID = C.CITY_ID";
      $result = mysql_query($query, $mysql_link);
      $row    = mysql_fetch_array($result);
      $TourId   = $row['TOUR_ID'];
      $ScheduledTourId = $row['SCHEDULEDTOUR_ID'];
      $TourName = $row['TOUR_NAME'];
      $TourNameJP = $row['TOUR_NAME_JP'];
      $TourType = $row['TOUR_TYPE'];
      $CityId   = $row['CITY_ID'];
      $City     = $row['CITY_DESC'];
      $DepartDate = $row['SCHEDULEDTOUR_DEPART_DATE'];
      $ReturnDate = $row['SCHEDULEDTOUR_RETURN_DATE'];
      if($DepartDate == $ReturnDate) {
         $one_day_tour = true;
      }
   }
?>
<h2>Book a Tour</h2>
<h4>Personal Information &gt; <span style="color:#999;">Agree to Terms</span> &gt; <span style="color:#999;">Payment Information</span> &gt; <span style="color:#999;">Tour Confirmation</span></h4> 

<div id="sidebar">
<div class="sidebox">
<h4>Tour Summary <? echo "option: $OPTIONALTOUR_ID_1"; ?></h4>
<p>Tour Name: <?php echo $TourName; ?><br />
<?
if ( $OPTIONALTOUR_ID_1 != '0' ){
	$query2 = "SELECT OPTIONALTOUR_NAME, OPTIONALTOUR_PRICE FROM  OPTIONALTOUR WHERE OPTIONALTOUR_ID = '$OPTIONALTOUR_ID_1'";
	$mysql_result2 = mysql_query($query2,$mysql_link);
	$row2 = mysql_fetch_array($mysql_result2);
	$OPTIONALTOUR_NAME = $row2['OPTIONALTOUR_NAME'];
	$OPTIONALTOUR_PRICE = $row2['OPTIONALTOUR_PRICE'];
	echo "<p>Optional Tour: " . $OPTIONALTOUR_NAME . "</p>";
}
?>
Leaving From: <?php echo $City; ?> <br />
Departure Date: <?php echo date("F j, Y" , strtotime($DepartDate)); ?><br />
Return Date: <?php echo date("F j, Y" , strtotime($ReturnDate)); ?></p>
</div>
</div>

<form name="signup" method="POST" action="https://california-tour.com/booking/reservations/toursignuptotal2.php">
        <input type='hidden' name='languageUsed' value='<?php echo $languageUsed; ?>'>
        <input type='hidden' name='ScheduledTourCode' value='<?php echo $LocalScheduledTourCode; ?>'>
        <input type='hidden' name='SchoolCode' value='<?php echo $SchoolCode; ?>'>
		<input type='hidden' name='OPTIONALTOUR_ID_1' value='<?php echo $OPTIONALTOUR_ID_1; ?>'>
<?php
if (!$SchoolCode) {
?>
<p>Where do you want to be picked up?</p>
<div id="pickuplocation">
       <select name="PULocationId" size="1">
	<?php
	$query1 = "SELECT PULOCATION_SCHEDULEDTOUR.PULOCATION_ID,PULOCATION_SCHEDULEDTOUR.PUTIME,PULOCATION.PULOCATION_CODE,PULOCATION.PULOCATION_LOCATION FROM PULOCATION_SCHEDULEDTOUR,PULOCATION WHERE PULOCATION_SCHEDULEDTOUR.SCHEDULEDTOUR_ID = '$ScheduledTourId' AND PULOCATION_SCHEDULEDTOUR.PULOCATION_ID = PULOCATION.PULOCATION_ID ORDER BY PULOCATION_SCHEDULEDTOUR.PUTIME";
	$mysql_result_pu = mysql_query($query1, $mysql_link);
	$numrows_pu = mysql_num_rows($mysql_result_pu);
	if ($numrows_pu > 0)
	{
		while ($row1 = mysql_fetch_array($mysql_result_pu))
		{
		$TempPULocationId = $row1['PULOCATION_ID'];
		$TempPULocationTime = $row1['PUTIME'];
		$TempPULocationCode = $row1['PULOCATION_CODE'];
		$TempPULocationLocation = $row1['PULOCATION_LOCATION'];
	?>
	        <OPTION value='<?php echo $TempPULocationId; ?>'><?php echo $TempPULocationTime . ' ' . $TempPULocationLocation ?></OPTION>
		<?php
          	}
	}
	?>
	</select>
</div>
<?php
} else {
	$query1 = "SELECT AGENT_PULOCATION.PULOCATION_ID FROM AGENT_PULOCATION,AGENT,PULOCATION_SCHEDULEDTOUR WHERE PULOCATION_SCHEDULEDTOUR.SCHEDULEDTOUR_ID = '$ScheduledTourId' AND AGENT.AGENT_ID = AGENT_PULOCATION.AGENT_ID AND PULOCATION_SCHEDULEDTOUR.PULOCATION_ID = AGENT_PULOCATION.PULOCATION_ID AND AGENT.AGENT_CODE = '$SchoolCode'";
	$mysql_result_pu = mysql_query($query1, $mysql_link);
	$numrows_pu = mysql_num_rows($mysql_result_pu);
	if ($numrows_pu > 0)
	{
		$row1 = mysql_fetch_array($mysql_result_pu);
		$TempPULocationId = $row1['PULOCATION_ID'];
?>	
	<input type="hidden" name="PULocationId" value="<?php echo $TempPULocationId; ?>" />
<?php
	}
}
?>
	<!-- each table below has a unique id: 'occupant#' -->
	<!--occupant 1-->
	<h4>Contact Information</h4><?php
		if ($SchoolCode) {?>
			<span style="color:#FF0000; font-size:12px;">If you are booking this tour for yourself only but you would like to share a room <br />
			please choose Double or Triple occupancy below and then select "Find me a roommate".<br />
			Quad occupancy requires you to book this tour for at least two people.</span><br /><?php
		} ?>

		<table cellpadding="2" cellspacing="0" id="occupant1">
					<tr>
						<td>First Name: </td>
						<td><INPUT TYPE="TEXT" NAME="FirstName" SIZE="25" class="text"></td>
					</tr>
					<tr>
						<td>Last Name: </td>
						<td><INPUT TYPE="TEXT" NAME="LastName" SIZE="25" class="text"></td>
					</tr>
					<?php
					   if(!$one_day_tour) {
					?>
					<tr>
						<td>Occupancy: </td>
						<td>
						    <SELECT name = 'Occupancy' size='1' onchange='checkOccupancy();'>
							<OPTION value = '1'>Single</OPTION>
							<OPTION value = '2'>Double</OPTION>
							<OPTION value = '3'>Triple</OPTION>
							<OPTION value = '4'>Quad</OPTION>
						    </SELECT>
						</td>
					</tr>
					<?php
					   }
					   else {
					?>
					       <INPUT TYPE='hidden' NAME='Occupancy' value='1'>
					<?php
					   }
					?>
					<tr>
						<td>Gender: </td>
						<td><SELECT name = 'Gender' size='1'>
							   <OPTION value='M'>Male</OPTION>
							   <OPTION value='F'>Female</OPTION>
							   </SELECT></td>
					</tr>
					<tr>
						<td>Email: </td>
						<td><INPUT TYPE="TEXT" NAME="Email" SIZE="15" class="text">
						</td>
					</tr>
  </table>

				<div id='ROOMMATE_1'>
				<h4>Roommate 1</h4>
				<table cellpadding="2" cellspacing="0" id="occupant2">
					<tr>
						<td>First Name: </td>
						<td><INPUT TYPE="TEXT" NAME="FirstName_1" SIZE="25" class="text"></td>
					</tr>
					<tr>
						<td>Last Name: </td>
						<td><INPUT TYPE="TEXT" NAME="LastName_1" SIZE="25" class="text"></td>
					</tr>
					<tr>
						<td>Gender: </td>
						<td><SELECT name = 'Gender_1' size='1'>
							   <OPTION value='M'>Male</OPTION>
							   <OPTION value='F'>Female</OPTION>
							   </SELECT></td>
					</tr>
					<!--tr>
						<td>Email:</td>
						<td><INPUT TYPE="TEXT" NAME="Email" SIZE="15" class="text">
						</td>
					</tr-->
				</table>
				<?php 
				if ($SchoolCode) {
				?>
				<span id="roommate1_check">
				or <input type='checkbox' value='yes' name='find_roommate_1'> Find me a roommate</span>
				<?php
				}
				?>
				</div>
				<!-- occupant 3-->
				<div id='ROOMMATE_2'>
				<h4>Roommate 2</h4>
				<table cellpadding="2" cellspacing="0" id="occupant1">
					<tr>
						<td>First Name:</td>
						<td><INPUT TYPE="TEXT" NAME="FirstName_2" SIZE="25" class="text"></td>
					</tr>
					<tr>
						<td>Last Name:</td>
						<td><INPUT TYPE="TEXT" NAME="LastName_2" SIZE="25" class="text"></td>
					</tr>
					<tr>
						<td>Gender:</td>
						<td><SELECT name = 'Gender_2' size='1'>
							   <OPTION value='M'>Male</OPTION>
							   <OPTION value='F'>Female</OPTION>
							   </SELECT></td>
					</tr>
					<!--tr>
						<td>Email:</td>
						<td><INPUT TYPE="TEXT" NAME="Email" SIZE="15" class="text">
						</td>
					</tr-->
				</table>
				<?php 
				if ($SchoolCode) {
				?>
				<span id="roommate2_check">or <input type='checkbox' value='yes' name='find_roommate_2'> Find me a roommate</span>
			<?php } ?>
			</div>
				<div id='ROOMMATE_3'>
				<h4>Roommate 3</h4>
				<table cellpadding="2" cellspacing="0" id="occupant1">
					<tr>
						<td>First Name:</td>
						<td><INPUT TYPE="TEXT" NAME="FirstName_3" SIZE="25" class="text"></td>
					</tr>
					<tr>
						<td>Last Name:</td>
						<td><INPUT TYPE="TEXT" NAME="LastName_3" SIZE="25" class="text"></td>
					</tr>
					<tr>
						<td>Gender:</td>
						<td><SELECT name = 'Gender_3' size='1'>
							   <OPTION value='M'>Male</OPTION>
							   <OPTION value='F'>Female</OPTION>
							   </SELECT></td>
					</tr>
					<!--tr>
						<td>Email:</td>
						<td><INPUT TYPE="TEXT" NAME="Email" SIZE="15" class="text">
						</td>
					</tr-->
				</table>
				<?php
				    if ($SchoolCode) {
				?>
				<span id="roommate3_check">
				 or <input type='checkbox' value='yes' name='find_roommate_3'> Find me a roommate</span>
                                <?php
                                     }
                                ?>
  </div>

    <br><input name="Cancel" value="Cancel" class="button" onclick="CancelReservation();" type="button">
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    <input name="WebSignUp" value="Continue" class="button" type="submit">
    <br><br>
</form>
<?php
include "../footer.php";
?>
