<HTML>

<HEAD>
<TITLE>Credit Card Report</TITLE>
<BODY bgcolor="#fffff0">

<SCRIPT LANGUAGE = "JavaScript">

	var strArgString = document.location.search;

		var strArgs = strArgString.substr(6);
		var strArray = strArgs.split(",");

		var strLevel = strArray[0];
		var strUserName = strArray[1];

</SCRIPT>

<?

include ("classes.php");
// connect to server

// open a connection to mysql

include ("../sqlfuncs.php");

$mysql_link = connect_to_db();
$mysql_result = select_db($mysql_link);



$SearchItem1 = new ReportSearchItem;
$SearchItem1->init(	"TOUR",
					"TOUR_ID",
					"TOUR_TYPE",
					"Tour:",
					"TourId",$mysql_link);

$SearchItem2 = new ReportSearchItem;
$SearchItem2->init(	"AGENT",
					"AGENT_ID",
					"AGENT_CODE",
					"Agent Code:",
					"AgentId",$mysql_link);
/*
$SearchItem3 = new ReportSearchItem;
$SearchItem3->init(	"SCHEDULEDTOURBOOK",
					"PAYCCDATE",
					"PAYCCDATE",
					"From Date:",
					"FromDate",$mysql_link);


$SearchItem4 = new ReportSearchItem;
$SearchItem4->init(	"SCHEDULEDTOURBOOK",
					"PAYCCDATE",
					"PAYCCDATE",
					"To Date:",
					"ToDate",$mysql_link);
*/
// date range as text entry

$SearchItem3 = new ReportTextBoxItem;
$SearchItem3->init("From (YYYY-MM-DD):","FromDate");

$SearchItem4 = new ReportTextBoxItem;
$SearchItem4->init("To (YYYY-MM-DD):","ToDate");

// declined or confirmed

$SearchItem5 = new ReportRadioItem;
$SearchItem5->init("Confirmed","Declined","CCStatus");

$SearchForm = new ReportSearchForm;
$SearchForm->init(	"ccrptmain.php",
					"ccrpt.php",
					"ccrpttotext.php",
					"-- Credit Card Report Search --",
					$SearchItem1,$SearchItem2,$SearchItem3,$SearchItem4,$SearchItem5);

if ($ExitBtn)
{
	$SearchForm->mainexitcmd();
}

if ($PrintScreenBtn)
{
	include ($SearchForm->printtoscreen);
}
else
{

	if ($PrintFileBtn)
	{
		include ($SearchForm->printtofile);
	}

	else
	{
		$SearchForm->showmain();
	}
}

?>