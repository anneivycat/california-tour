<?php
error_reporting(0);
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2 Final//EN">
<HTML>
<HEAD>
  <title>Tour Schedule and Reservations for Agents</title>
<?php if($languageUsed == "JP") { ?>
<meta http-equiv=\"Content-Type\" content="text/html; charset=x-sjis">
<?php
} else {
?>
<meta http-equiv=\"Content-Type\" content="text/html; charset=UTF-8">
<?php
}
?>
<script language="JAVASCRIPT" type="TEXT/JAVASCRIPT">

function MM_jumpMenu(selObj){ //vX.X
	var i=selObj.options[selObj.selectedIndex].value;
	if (i!='') {
		location=i;
		selObj.selectedIndex=0;
	}
}
</script>
<STYLE TYPE="text/css">
BODY, TD {
	font: 300 12px Verdana, Arial, sans-serif;
	color: black;
}
.blueType {
	color: #33F;
}
A:link {COLOR: #3366FF; TEXT-DECORATION: none}
A:visited {COLOR: #3366FF; TEXT-DECORATION: none}
A:hover {COLOR: #0000CC; TEXT-DECORATION: none}
.style1 {color: #3300FF}
.style2 {color: #0033FF}
</STYLE>
</head>
<body>
<table width="742" border="0" cellspacing="0" cellpadding="0">
<tr><td width="106">&nbsp;</td>
<td width="261"><img src="/booking/images/california_tours_logo/home153_64.gif" width="153" height="64" alt="California Tours"></td>
<td width="315" align="center"><h2>Reservation Tracking System</h2></td>
<td width="60">&nbsp;</td>
</tr>
</table>
<br>
<table width="768" border="0" cellspacing="0" cellpadding="0">
<tr align="center" valign="middle" bgcolor="#ffba3d">
<td width="31" height="13" bgcolor="#FFCC66"></td>
<td width="293" height="13" bgcolor="#FFCC66" align="left"></td>
<td width="351" height="13" bgcolor="#FFCC66" valign="middle" align="right"><a href="mailto:tours@california-tour.com"><img src="/booking/images/links_position/contact.gif" width="310" height="17" border="0" alt="California Tour"></a></td>
<td width="63" height="13" bgcolor="#FFCC66"></td>
</tr>
<tr align="center" valign="top">
<td colspan="5">
<table width="720" border="0" cellspacing="0" cellpadding="0">
<tr>
<td width="130" align="left" valign="top">
<table width="124" border="0" cellspacing="0" cellpadding="0">
<tr><td height="12" align="left" valign="top"><img src="/booking/images/spacer/120_10.gif" width="120" height="10"></td>
</tr>
<tr><td height="168" align="left" valign="top">
</td></tr></table></td>
<td width="485" align="left" valign="top">
<Table width="80%" align="center">
<tr><td align="center"><br /><br />
<?
/* get the username and from the username get the city for the agent */
	$QueryString = $_SERVER['QUERY_STRING'];
	parse_str($QueryString);
/* get the city from the agent code */
	include("../sqlfuncs.php");
	// connect to database
	$mysql_link = connect_to_db();
	$mysql_query = select_db($mysql_link);
		$query = "SELECT AGENT.AGENT_CODE,PULOCATION.PULOCATION_CODE FROM USER, AGENT, PULOCATION, AGENT_PULOCATION WHERE USER.AGENT_ID = AGENT.AGENT_ID AND PULOCATION.PULOCATION_ID = AGENT_PULOCATION.PULOCATION_ID AND AGENT.AGENT_ID = AGENT_PULOCATION.AGENT_ID AND USER.USER_NAME = '$UserName' LIMIT 1";
		$mysql_result = mysql_query($query);
		if ($mysql_result)
		{
			$row = mysql_fetch_array($mysql_result);
			$AgentCode = $row[0];
			$PULocationCode = $row[1];
			$CityCode = substr($PULocationCode,0,2);
		}
	echo "Level 1 (AGENT)<br><b>$UserName ($AgentCode - $CityCode)\n</b> logged in\n";
?>
</center>
<center>
<br>
Please select a month to make reservation<br><br>
</center>
 <table  border="0" align="center" cellpadding="0" cellspacing="0">
              <tbody>
                <tr>
<?
if ($CityCode == "SF")
{
	echo "<td width=\"171\" height=\"202\" background=\"../calendars_events/calendars/images/calendars_01.gif\">\n";
}
if ($CityCode == "LA")
{
	echo "<td width=\"171\" height=\"202\" background=\"../calendars_events/calendars/images/calendars_02.gif\">\n";
}
if ($CityCode == "SD")
{
	echo "<td width=\"171\" height=\"202\" background=\"../calendars_events/calendars/images/calendars_03.gif\">\n";
}
if ($CityCode == "SB")
{
	echo "<td width=\"171\" height=\"202\" background=\"../calendars_events/calendars/images/calendars_04.gif\">\n";
}
    echo "<div align=\"center\">\n<form name=\"form2004\">\n";
    echo "<select name=\"menu2\" onchange=\"MM_jumpMenu(this)\">\n";
    echo "<option value=\"\" selected>Select A Month</option>\n";
   
    echo "<option value=\"agent_calendar_2010.php?year=2010&month=1&city=$CityCode&languageUsed=ENG&SchoolCode=$AgentCode&UserName=$UserName\">2010 January</option>\n";
    echo "<option value=\"agent_calendar_2010.php?year=2010&month=2&city=$CityCode&languageUsed=ENG&SchoolCode=$AgentCode&UserName=$UserName\">2010 February</option>\n";
    echo "<option value=\"agent_calendar_2010.php?year=2010&month=3&city=$CityCode&languageUsed=ENG&SchoolCode=$AgentCode&UserName=$UserName\">2010 March</option>\n";
    echo "<option value=\"agent_calendar_2010.php?year=2010&month=4&city=$CityCode&languageUsed=ENG&SchoolCode=$AgentCode&UserName=$UserName\">2010 April</option>\n";
    echo "<option value=\"agent_calendar_2010.php?year=2010&month=5&city=$CityCode&languageUsed=ENG&SchoolCode=$AgentCode&UserName=$UserName\">2010 May</option>\n";
    echo "<option value=\"agent_calendar_2010.php?year=2010&month=6&city=$CityCode&languageUsed=ENG&SchoolCode=$AgentCode&UserName=$UserName\">2010 June</option>\n";
    echo "<option value=\"agent_calendar_2010.php?year=2010&month=7&city=$CityCode&languageUsed=ENG&SchoolCode=$AgentCode&UserName=$UserName\">2010 July</option>\n";
    echo "<option value=\"agent_calendar_2010.php?year=2010&month=8&city=$CityCode&languageUsed=ENG&SchoolCode=$AgentCode&UserName=$UserName\">2010 August</option>\n";
    echo "<option value=\"agent_calendar_2010.php?year=2010&month=9&city=$CityCode&languageUsed=ENG&SchoolCode=$AgentCode&UserName=$UserName\">2010 September</option>\n";
    echo "<option value=\"agent_calendar_2010.php?year=2010&month=10&city=$CityCode&languageUsed=ENG&SchoolCode=$AgentCode&UserName=$UserName\">2010 October</option>\n";
    echo "<option value=\"agent_calendar_2010.php?year=2010&month=11&city=$CityCode&languageUsed=ENG&SchoolCode=$AgentCode&UserName=$UserName\">2010 November</option>\n";
    echo "<option value=\"agent_calendar_2010.php?year=2010&month=12&city=$CityCode&languageUsed=ENG&SchoolCode=$AgentCode&UserName=$UserName\">2010 December</option>\n";
    
	echo "<option value=\"agent_calendar_2011.php?year=2011&month=1&city=$CityCode&languageUsed=ENG&SchoolCode=$AgentCode&UserName=$UserName\">2011 January</option>\n";
    echo "<option value=\"agent_calendar_2011.php?year=2011&month=2&city=$CityCode&languageUsed=ENG&SchoolCode=$AgentCode&UserName=$UserName\">2011 February</option>\n";
    echo "<option value=\"agent_calendar_2011.php?year=2011&month=3&city=$CityCode&languageUsed=ENG&SchoolCode=$AgentCode&UserName=$UserName\">2011 March</option>\n";
    echo "<option value=\"agent_calendar_2011.php?year=2011&month=4&city=$CityCode&languageUsed=ENG&SchoolCode=$AgentCode&UserName=$UserName\">2011 April</option>\n";
    echo "<option value=\"agent_calendar_2011.php?year=2011&month=5&city=$CityCode&languageUsed=ENG&SchoolCode=$AgentCode&UserName=$UserName\">2011 May</option>\n";
    echo "<option value=\"agent_calendar_2011.php?year=2011&month=6&city=$CityCode&languageUsed=ENG&SchoolCode=$AgentCode&UserName=$UserName\">2011 June</option>\n";
    echo "<option value=\"agent_calendar_2011.php?year=2011&month=7&city=$CityCode&languageUsed=ENG&SchoolCode=$AgentCode&UserName=$UserName\">2011 July</option>\n";
    echo "<option value=\"agent_calendar_2011.php?year=2011&month=8&city=$CityCode&languageUsed=ENG&SchoolCode=$AgentCode&UserName=$UserName\">2011 August</option>\n";
    echo "<option value=\"agent_calendar_2011.php?year=2011&month=9&city=$CityCode&languageUsed=ENG&SchoolCode=$AgentCode&UserName=$UserName\">2011 September</option>\n";
    echo "<option value=\"agent_calendar_2011.php?year=2011&month=10&city=$CityCode&languageUsed=ENG&SchoolCode=$AgentCode&UserName=$UserName\">2011 October</option>\n";
    echo "<option value=\"agent_calendar_2011.php?year=2011&month=11&city=$CityCode&languageUsed=ENG&SchoolCode=$AgentCode&UserName=$UserName\">2011 November</option>\n";
    echo "<option value=\"agent_calendar_2011.php?year=2011&month=12&city=$CityCode&languageUsed=ENG&SchoolCode=$AgentCode&UserName=$UserName\">2011 December</option>\n";
?>
</select></form></div></td></tr></tbody></table>
<?
	echo "<center><br><a href='mainagent.php?args=menu,$UserLevel,$UserName'>Agent Menus</a>\n";
	echo "&nbsp;&nbsp;<a href='index.html'>Exit/Re-Login</a></center>\n";
?>
</td></tr></table>
</body></html>
