<?php
error_reporting(0);
session_start();

include ("schedtourbookfuncs.php");
include ("schedtourbookholdfuncs.php");
include ("../sqlfuncs.php");
include ("parsefuncs.php");
// connect to database
$mysql_link = connect_to_db();
$result_code = select_db($mysql_link);
//get the optional tour checkbox(s) info - either from previous session (because just got bounced back here b/c or error on the toursignuptotal) or from post
if ( $_POST['OPTIONALTOUR_ID_1'] != ''   ){
	$OPTIONALTOUR_ID_1 = $_POST['OPTIONALTOUR_ID_1'];
	$_SESSION['OPTIONALTOUR_ID_1'] = $OPTIONALTOUR_ID_1;
}
else {
	$OPTIONALTOUR_ID_1 = $_SESSION['OPTIONALTOUR_ID_1'];
}


	


// clean the post of any non numeric characters for security
$OPTIONALTOUR_ID_1 = preg_replace("/[^0-9]/", "", "$OPTIONALTOUR_ID_1" );

$OPTIONALTOUR_PRICE_1 = 0;

$query2 = "SELECT OPTIONALTOUR_NAME, OPTIONALTOUR_PRICE FROM  OPTIONALTOUR WHERE OPTIONALTOUR_ID = '$OPTIONALTOUR_ID_1'";
	$mysql_result2 = mysql_query($query2,$mysql_link);
	$row2 = mysql_fetch_array($mysql_result2);
	$OPTIONALTOUR_NAME_1 = $row2['OPTIONALTOUR_NAME'];
	$OPTIONALTOUR_PRICE_1 = $row2['OPTIONALTOUR_PRICE'];
	
	
// specify table name

if (!$AgentProcRefresh) {
	$table_name = "SCHEDULEDTOURBOOK";
	$FullCode = "FALSE";
// if student signs up, give school discount for that school
// get Agent Id, etc if signup was through reservation program
	if ($SchoolCode) {
		$query2 = "SELECT AGENT_ID, AGENT_CODE FROM AGENT WHERE AGENT_CODE = '$SchoolCode'";
		$mysql_result2 = mysql_query($query2);
		$row2 = mysql_fetch_array($mysql_result2);
		$AgentId = $row2[0];
		$AgentCode = strtoupper($row2[1]);
	}
	if ($ScheduledTourId < 1) {
			$query_check = "SELECT SCHEDULEDTOUR_ID FROM SCHEDULEDTOUR WHERE SCHEDULEDTOUR_CODE = '$ScheduledTourCode'";
			$mysql_result_check = mysql_query($query_check);
			$numrows_check = mysql_num_rows($mysql_result_check);
			$rowcheck = mysql_fetch_array($mysql_result_check);
			$ScheduledTourId = $rowcheck[0];
	}
	if ($AgentId < 1) {
			$query_check = "SELECT AGENT_ID FROM AGENT WHERE AGENT_CODE = '$AgentCode'";
			$mysql_result_check = mysql_query($query_check);
			$numrows_check = mysql_num_rows($mysql_result_check);
			$rowcheck = mysql_fetch_array($mysql_result_check);
			$AgentId = $rowcheck[0];
	}
	if ($PULocationId < 1) {
			$query_check = "SELECT PULOCATION.PULOCATION_ID, PULOCATION.PULOCATION_LOCATION, PULOCATION_SCHEDULEDTOUR.PUTIME FROM PULOCATION,PULOCATION_SCHEDULEDTOUR WHERE PULOCATION.PULOCATION_ID = PULOCATION_SCHEDULEDTOUR.PULOCATION_ID AND PULOCATION_SCHEDULEDTOUR.SCHEDULEDTOUR_ID = '$ScheduledTourId' AND PULOCATION.PULOCATION_CODE = '$PULocationCode'";
			$mysql_result_check = mysql_query($query_check, $mysql_link);
			$numrows_check = mysql_num_rows($mysql_result_check);
			$rowcheck = mysql_fetch_array($mysql_result_check);
			$PULocationId = $rowcheck[0];
			$PULocationLocation = $rowcheck[1];
			$PULocationTime = $rowcheck[2];
	}
	else {
			$query_check = "SELECT PULOCATION.PULOCATION_LOCATION, PULOCATION_SCHEDULEDTOUR.PUTIME FROM PULOCATION,PULOCATION_SCHEDULEDTOUR WHERE PULOCATION.PULOCATION_ID = PULOCATION_SCHEDULEDTOUR.PULOCATION_ID AND PULOCATION.PULOCATION_ID = '$PULocationId'";
			$mysql_result_check = mysql_query($query_check, $mysql_link);
			$numrows_check = mysql_num_rows($mysql_result_check);
			$rowcheck = mysql_fetch_array($mysql_result_check);
			$PULocationLocation = $rowcheck[0];
			$PULocationTime = $rowcheck[1];
	}

// print db size
	$ResultString = print_tb_size($table_name, $mysql_link);
// SEND INFO FROM WEB SIGN UP FORM ...
// get new Tour Book ID (loaded into temporary table first, but read from
// current booking table
	if (!$ReservationId) {
		$ScheduledTourBookId = get_new_id($mysql_link);
		$_SESSION['ReservationId'] = $ScheduledTourBookId + 10000;
	}
	else {
/* added 2/18/04 */
		$ScheduledTourBookId = get_new_id($mysql_link);
		$_SESSION['ReservationId'] = $ScheduledTourBookId + 10000;
	}
// check to make sure we can have space available for the tour
// get Last and first name from full name
// get City Code
	$query2 = "SELECT SCHEDULEDTOUR_CODE,TOUR_ID,SCHEDULEDTOUR_DEPART_DATE,SCHEDULEDTOUR_RETURN_DATE FROM SCHEDULEDTOUR WHERE SCHEDULEDTOUR_ID = '$ScheduledTourId'";
	$mysql_result2 = mysql_query($query2);
	$row2 = mysql_fetch_array($mysql_result2);
	$ScheduledTourCode = $row2[0];
	$TourId = $row2[1];
	$DepartDate = $row2[2];
	$ReturnDate = $row2[3];
	$query2 = "SELECT TOUR_NAME,CITY_ID,TOUR_TYPE FROM TOUR WHERE TOUR_ID = '$TourId'";
	$mysql_result2 = mysql_query($query2);
	$row2 = mysql_fetch_array($mysql_result2);
	$TourName = $row2[0];
	$CityId = $row2[1];
// added 11/29/2003
	$TourType = $row2[2];
	$query2 = "SELECT CITY_CODE,CITY_DESC FROM CITY WHERE CITY_ID = '$CityId'";
	$mysql_result2 = mysql_query($query2);
	$row2 = mysql_fetch_array($mysql_result2);
	$CityCode = $row2[0];
	$City = $row2[1];
// get Agent ID from City (SFWEB, LAWEB, SDWEB,SBWEB)
// these are agents used to indicate sign on from
// the web
	if ($CityCode == "LA") {
		if ($AgentId < 1) {
				$query2 = "SELECT AGENT_ID, AGENT_CODE FROM AGENT WHERE AGENT_CODE = 'LAWEB'";
				$mysql_result2 = mysql_query($query2);
				$row2 = mysql_fetch_array($mysql_result2);
				$AgentId = $row2[0];
				$AgentCode = $row2[1];
        }
		else {
				$query2 = "SELECT AGENT_CODE FROM AGENT WHERE AGENT_ID = '$AgentId'";
				$mysql_result2 = mysql_query($query2);
				$row2 = mysql_fetch_array($mysql_result2);
				$AgentCode = $row2[0];
		}
	}
	if ($CityCode == "SD") {
		if ($AgentId < 1) {
				$query2 = "SELECT AGENT_ID, AGENT_CODE FROM AGENT WHERE AGENT_CODE = 'SDWEB'";
				$mysql_result2 = mysql_query($query2);
				$row2 = mysql_fetch_array($mysql_result2);
				$AgentId = $row2[0];
				$AgentCode = $row2[1];
		}
		else {
				$query2 = "SELECT AGENT_CODE FROM AGENT WHERE AGENT_ID = '$AgentId'";
				$mysql_result2 = mysql_query($query2);
				$row2 = mysql_fetch_array($mysql_result2);
				$AgentCode = $row2[0];
		}
	}
	if ($CityCode == "SF") {
		if ($AgentId < 1) {
				$query2 = "SELECT AGENT_ID, AGENT_CODE FROM AGENT WHERE AGENT_CODE = 'SFWEB'";
				$mysql_result2 = mysql_query($query2);
				$row2 = mysql_fetch_array($mysql_result2);
				$AgentId = $row2[0];
				$AgentCode = $row2[1];
		}
		else {
				$query2 = "SELECT AGENT_CODE FROM AGENT WHERE AGENT_ID = '$AgentId'";
				$mysql_result2 = mysql_query($query2);
				$row2 = mysql_fetch_array($mysql_result2);
				$AgentCode = $row2[0];
		}
	}
	if ($CityCode == "SB") {
		if ($AgentId < 1) {
				$query2 = "SELECT AGENT_ID, AGENT_CODE FROM AGENT WHERE AGENT_CODE = 'SBWEB'";
				$mysql_result2 = mysql_query($query2);
				$row2 = mysql_fetch_array($mysql_result2);
				$AgentId = $row2[0];
				$AgentCode = $row2[1];
		}
		else {
				$query2 = "SELECT AGENT_CODE FROM AGENT WHERE AGENT_ID = '$AgentId'";
				$mysql_result2 = mysql_query($query2);
				$row2 = mysql_fetch_array($mysql_result2);
				$AgentCode = $row2[0];
		}
	}
// get total charge for this tour
// this is going to be paid w/ credit card
// web-based agents have a discount schedule of 1
	$query2 = "SELECT SCHEDULEDTOUR_PRICE FROM SCHEDULEDTOUR WHERE SCHEDULEDTOUR_ID = '$ScheduledTourId'";
	$mysql_result2 = mysql_query($query2);
	$row2 = mysql_fetch_array($mysql_result2);
	$TourPrice = $row2[0];
	$query1 = "SELECT AGENT_COMMISIONTYPE FROM AGENT WHERE AGENT_ID = '$AgentId'";
	$mysql_result1 = mysql_query($query1);
	$row1 = mysql_fetch_array($mysql_result1);
	$CommissionType = $row1[0];
	$query1="SELECT TOUR_SUPP_SNG, TOUR_SUPP_DBL, TOUR_SUPP_TRP,TOUR_DISC".$CommissionType.", TOUR_WEB".$CommissionType
	." FROM TOUR WHERE TOUR_ID = '$TourId'";
	$mysql_result1 		= mysql_query($query1);
	$row1 = mysql_fetch_array($mysql_result1);
	$TourSuppSng = $row1[0];
	$TourSuppDbl = $row1[1];
	$TourSuppTrp = $row1[2];
	$TourDiscount = $row1[3];
	// $TourWebDiscount = $row1[4]; this was casusing errors. no need for webdiscount on agent sign up
	$RoomCharge = 0;

	if ($Occupancy == "1") {
		$RoomCharge = $TourSuppSng;
	}
	if ($Occupancy == "2") {
		$RoomCharge = $TourSuppDbl;
	}
	if ($Occupancy == "3") {
		$RoomCharge = $TourSuppTrp;
	}
	$TotalCharge = $TourPrice + $RoomCharge - $TourDiscount + $ManSuppCharge + $OPTIONALTOUR_PRICE_1 + $OPTIONALTOUR_PRICE_2 + $OPTIONALTOUR_PRICE_3+ $OPTIONALTOUR_PRICE_4+ $OPTIONALTOUR_PRICE_5;
		$current_time = getdate(time());
		$current_hours = $current_time["hours"];
		$current_mins = $current_time["minutes"];
		$current_secs = $current_time["seconds"];
		$current_date = date("Y-m-d");
		$TimeStamp = sprintf("%s %d:%d:%d",$current_date,$current_hours,$current_mins,$current_secs);
	if ($ScheduledTourId <> "") {
		if  (check_if_space_available(1, $ScheduledTourId, $mysql_link) == "TRUE") {
			$FullCode = "FALSE";
		}
		else {
			$FullCode = "TRUE";
		}
	}
	$query2 = "SELECT BOOKTYPE_ID FROM BOOKTYPE WHERE BOOKTYPE_DESC = 'SCHEDULED TOUR'";
	$mysql_result2 = mysql_query($query2);
	$row2 = mysql_fetch_array($mysql_result2);
	$BookTypeId = $row2[0];
	$query2 = "SELECT SCHEDULEDTOUR_CODE,TOUR_ID,SCHEDULEDTOUR_DEPART_DATE,SCHEDULEDTOUR_RETURN_DATE FROM SCHEDULEDTOUR WHERE SCHEDULEDTOUR_ID = '$ScheduledTourId'";
	$mysql_result2 = mysql_query($query2);
	$row2 = mysql_fetch_array($mysql_result2);
	$ScheduledTourCode = $row2[0];
	$TourId = $row2[1];
	$DepartDate = $row2[2];
	$ReturnDate = $row2[3];
	if ($languageUsed == "JP") {
		$query2 = "SELECT TOUR_NAME_JP, CITY_ID FROM TOUR WHERE TOUR_ID = '$TourId'";
	}
	else {
		$query2 = "SELECT TOUR_NAME, CITY_ID FROM TOUR WHERE TOUR_ID = '$TourId'";
	}
	$mysql_result2 = mysql_query($query2);
	$row2 = mysql_fetch_array($mysql_result2);
	$TourName = $row2[0];
	$CityId = $row2[1];
	$query2 = "SELECT CITY_CODE FROM CITY WHERE CITY_ID = '$CityId'";
	$mysql_result2 = mysql_query($query2);
	$row2 = mysql_fetch_array($mysql_result2);
	$CityCode = $row2[0];
// here we have to check if this temporary reservation
// already exists in the hold table - if so, we have to create
// a different one before adding to the holding table
// we should lock both tables briefly while performing

	if (true) {
	//!$ReservationId)
		$ScheduledTourBookId = $ScheduledTourBookId + 10000;
		$_SESSION['ReservationId'] = $ScheduledTourBookId;
		setcookie('ReservationId',$ScheduledTourBookId);
		/* add a new hold record */
		$TempScheduledTourBookId = $ScheduledTourBookId - 10000;
		
		$query_hold_insert = "INSERT INTO SCHEDULEDTOURBOOKHOLD (SCHEDULEDTOURBOOK_ID) VALUES ('$TempScheduledTourBookId')";
		$mysql_result_hold_insert = mysql_query($query_hold_insert);
	} 	else {
		$ScheduledTourBookId = $ScheduledTourBookId + 10000;
		$_SESSION['ReservationId'] = $ScheduledTourBookId;
		setcookie('ReservationId',$ScheduledTourBookId);
		$ScheduledTourBookId = $ReservationId;
	}
	if ($UserName) {
		setcookie('LocalUserName',$UserName);
	} else {
		setcookie('LocalUserName');
	}
	
} else {
	$ScheduledTourBookId = $ReservationId;
	if (!$ScheduledTourBookId) {
		$ScheduledTourBookId = $LocalReservationId;
/* added 2/18/04 */
//		setcookie('ReservationId',$ScheduledTourBookId);
	}
	setcookie('ReservationId',$ScheduledTourBookId);
	

	$FullCode = "FALSE";
/* read variables from cookies */
	$Gender = $SavedGender;
	$Occupancy = $SavedOccupancy;
	$PULocationId = $SavedPULocationId;
	$LastName = $SavedLastName;
	$FirstName = $SavedFirstName;
	$TourName = $SavedTourName;
	$AgentCode = $SavedAgentCode;
	$SchoolCode = $SavedSchoolCode;
	$PULocationLocation = $SavedPULocationLocation;
	$PULocationTime = $SavedPULocationTime;
	$DepartDate = $SavedDepartDate;
	$ReturnDate = $SavedReturnDate;
	$CityCode = $SavedCityCode;
	$RoomCharge = $SavedRoomCharge;
	$Email = $SavedEmail;
	$TourType = $SavedTourType;
	$ManSuppCharge = $SavedManSuppCharge;
	$TourDiscount = $SavedTourDiscount;
	// $TourWebDiscount = $SavedTourWebDiscount; this was casusing errors. no need for webdiscount on agent sign up
	$TourPrice = $SavedTourPrice;
	$TotalCharge = $SavedTotalCharge;
	$ScheduledTourCode = $SavedScheduledTourCode;

}
/* end of additions 2/2/04 */
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2 Final//EN">
<HTML><HEAD><TITLE>Tour Total</TITLE>
<?php if($languageUsed == "JP") { ?>
<meta http-equiv=\"Content-Type\" content="text/html; charset=x-sjis">
<?php
} else {
?>
<meta http-equiv=\"Content-Type\" content="text/html; charset=UTF-8">
<?php
}
?>

<STYLE TYPE="text/css">
BODY, TD {
	font: 300 12px Verdana, Arial, sans-serif;
	color: black;
}
.subTotal {
	font: bold 12px Verdana, Arial, sans-serif;
	color: #333;
	text-align: right;
}
.blueType {
	font: 400 12px Verdana, Arial, sans-serif;
	color: #33F;
	text-align: left;
}
A:link {COLOR: #3366FF; TEXT-DECORATION: none}
A:visited {COLOR: #3366FF; TEXT-DECORATION: none}
A:hover {COLOR: #0000CC; TEXT-DECORATION: none}
.style1 {color: #3300FF}
.style2 {color: #0033FF}
</STYLE>
<SCRIPT LANGUAGE = 'JavaScript'>
function CancelReservation() {
	document.location = "agentclearreservation.php?UserLevel=1&UserName=$SchoolCode&Exit=True";
}

function translate(strText) {
	if (strText == "CHARGE SUMMARY") {
		document.write("æéßä¸è¦§");
		return;
	}
	if (strText == "School Code") {
		 document.write("ã¹ã¯?[ã«ã³?[ãE);
		 return;
	}
	if (strText == "Thank You") {
	document.write("ãããã¨ãEããã¾ãE);
	return;
	}
	if (strText == "Thank You from Tour Total") {
		document.write("æ§E@ãããã¨ãEããã¾ãE);
	return;
	}
	if (strText == "Reservation #") {
		document.write("äºç´Eª?ã»);
		return;
	}
	if (strText == "Tour Name") {
		document.write("ãE¢?[åE);
		return;
	}
	if (strText == "Tour Code") {
		document.write("ãE¢?[ã³?[ãE);
		return;
	}
	if (strText == "Scheduled Tour Code") {
		document.write("ã¹ã±ã¸ã¥?[ã«ãE¢?[ã³?[ãE);
		return;
	}
	if (strText == "Departing") {
		document.write("?oçºæ¥");
		return;
	}
	if (strText == "From") {
		document.write("?oçºå°");
		return;
	}
	if (strText == "Basic Tour Charge") {
		document.write("ãE¢?[åºæ¬æéß");
		return;
	}
	if (strText == "Supp Room Charges") {
		document.write("ã«?[ã è¿½å æéß");
		return;
	}
	if (strText == "Discount") {
		document.write("èª¿?E®è£è¶³éE);
		return;
	}
	if (strText == "Manual Supp Charge") {
		document.write("?ã¹véé¡E);
		return;
	}
	if (strText == "Total Charges") {
		document.write("?ã¹véé¡E);
		return;
	}
	if (strText == "PU Location Map") {
		document.write("PU Location Map (&#26085;&#26412;&#35486;)");
		return;
	}
	if (strText == "Trip Info Link") {
		document.write("Trip Info Link (&#26085;&#26412;&#35486;)");
		return;
	}
	if (strText == "Total Paid") {
		document.write("?ã¹vãæ¯æãéé¡E");
		return;
	}
	if (strText == "LA") {
		document.write("ã»ãµã³ã¼ã«ã¹çº");
		return;
	}
	if (strText == "SF") {
		document.write("ãµã³ãã©ã³ã·ã¹ã³çº");
		return;
	}
	if (strText == "SB") {
		document.write("ãµã³ã¿?EãE[ãã©çº");
		return;
	}
	if (strText == "SD") {
		document.write("ãµã³ãE£ã¨ã´çº");
		return;
	}
	document.write(strText);
	return;
}
</script>
</head>
<body>
<?php
//echo "OPTIONAL TOUR VALUE: " . $_SESSION['OPTIONALTOUR_ID_1'] ;
?>
<table width="742" border="0" cellspacing="0" cellpadding="0">
<tr><td width="106">&nbsp;</td>
<td width="261"><img src="/booking/images/california_tours_logo/home153_64.gif" width="153" height="64" alt="California Tours"></td>
<td width="315" align="center"><h3>Review and Payment</h3></td>
<td width="60">&nbsp;</td>
</tr>
</table>
<br>
<table width="738" border="0" cellspacing="0" cellpadding="0">
<tr align="center" valign="middle" bgcolor="#ffba3d">
<td width="31" height="13" bgcolor="#FFCC66"></td>
<td width="293" height="13" bgcolor="#FFCC66" align="left"></td>
<td width="351" height="13" bgcolor="#FFCC66" valign="middle" align="right"><a href="mailto:tours@california-tour.com"><img src="/booking/images/links_position/contact.gif" width="310" height="17" border="0" alt="California Tour"></a></td>
<td width="63" height="13" bgcolor="#FFCC66"><font color="#FFFFCC"></font><font color="#FFFFCC"></font></td>
</tr>
<tr align="center" valign="top">
<td colspan="5">
<table width="720" border="0" cellspacing="0" cellpadding="0">
<tr>
<td width="130" align="left" valign="top">
<table width="124" border="0" cellspacing="0" cellpadding="0">
<tr><td height="12" align="left" valign="top"><img src="/booking/images/spacer/120_10.gif" width="120" height="10"></td>
</tr>
<tr><td height="168" align="left" valign="top"><img src="/booking/images/photograph/san_francisco/buildings.jpg" width="120" height="172" alt="San Francisco"></td>
</tr>
</table>
</td>
<td width="447" align="left" valign="top">
<table width="603" border="0" cellspacing="0" cellpadding="0">
<tr><td colspan="2" height="4"><img src="/booking/images/spacer/120_10.gif" width="120" height="10"></td>
</tr>
<tr><td width="8">&nbsp;</td>
<td width="395" align="left" valign="top">
<?
	// system returned space available
	if ($FullCode == "FALSE") {
		echo "<center>\n";
		if ($UserName) {
			if ($languageUsed == "JP") {
				print("<font face='arial' size='-1'>$UserName <Script Language='JavaScript'>translate(\"Thank You from Tour Total\");</script></font>\n");
				echo "<font face='arial' size='-1'>For: $FirstName $LastName</font>\n";
			}
			else {
				echo "<font face='arial' size='-1'>Thank You $UserName</font>\n<font face='arial' size='-1'>For: $FirstName $LastName</font>\n";
			}
		}
		else {
			if ($languageUsed == "JP") {
				print("<font face='arial' size='-1'>$FirstName $LastName <Script Language='JavaScript'>translate(\"Thank You from Tour Total\");</script></font>\n");
			}
			else {
				echo "<font face='arial' size='-1'>Thank You $FirstName $LastName</font>\n";
			}
		}
		if ($languageUsed == "JP") {
			echo "&nbsp;&nbsp;<font color='black' size='-1'>** æéßä¸è¦§ **</font>\n";
		}
		else {
			echo "&nbsp;<font color='black' size='-1'>** CHARGE  SUMMARY **</font>\n";
		}
			echo "<br>\n<font color='black' size='-2'>If paying by CC, the next screen prompts you to \"Make a Reservation\"</font><br>\n<font color='black' size='-2'>If paying by Cash, the next screen completes your reservation and generates a receipt.</font>\n</center>\n";
		echo "<form method='POST' action='agenttoursignuptotal2.php'>\n<table align='center' width='80%'>\n<TR>\n<td class='blueType'>\n";
		if ($languageUsed == "JP") {
			print("<script language='JavaScript'>translate(\"Reservation #\")</script> \n");
		}
		else {
			echo "Temporary Reservation # \n";
		}
		echo "</TD>\n<TD class='subTotal'>$ScheduledTourBookId\n</TD>\n</TR>\n<TR>\n<td class='blueType'>\n";
		if ($languageUsed == "JP") {
			print("<script language='JavaScript'>translate(\"Scheduled Tour Code\")</script>: \n");
		}
		else {
			echo "Scheduled Tour Code: \n";
		}
		echo "</TD>\n<TD class='subTotal'>$ScheduledTourCode\n</TD>\n</TR>\n<TR>\n<td class='blueType'>\n";
		if ($languageUsed == "JP") {
			print("<script language='JavaScript'>translate(\"Tour Name\")</script>: \n");
		}
		else {
			echo "Tour Name: \n";
		}
		echo "</TD>\n<TD class='subTotal'>$TourName\n</TD>\n</TR>\n<TR>\n<td class='blueType'>\n";
		if ($languageUsed == "JP") {
			print("<script language='JavaScript'>translate(\"Departing\");</script>: \n");
		}
		else {
			echo "Departing: \n";
		}
		echo "</TD>\n<TD class='subTotal'>$DepartDate\n</TD>\n</TR>\n<TR>\n<td class='blueType'>\n";
		if ($languageUsed == "JP") {
			print("<script language='Javascript'>translate(\"From\");</script>: \n");
		}
		else {
			echo "From: \n";
		}
		echo "</TD>\n<TD ALIGN='left' class='subtotal'>\n";
		if ($languageUsed == "JP") {
			print("<script language='Javascript'>translate(\"$CityCode\");</script>\n");
		}
		else {
			echo "$CityCode\n";
		}
		echo "</TD>\n</TR>\n<TR>\n<td class='blueType'>\n";
		if ($languageUsed == "JP") {
			print("<script language='Javascript'>translate(\"Basic Tour Charge\");</script>: \n");
		}
		else {
			echo "Basic Tour Charge: \n";
		}
		echo "</TD>\n";
		$TourPrice = sprintf("%5.2f", $TourPrice);
		echo "<TD class='subTotal'>$TourPrice</TD>\n</TR>";
		if (  $OPTIONALTOUR_ID_1  != 0  )  {
			echo "<TR>\n<td class='blueType'>\n";
			if ($languageUsed == "JP") {
				print("<script language='Javascript'>translate(\"Optional Tour Charge\");</script>: \n");
			}
			else {
				echo "Optional Tour Charge:<br>&nbsp;&nbsp;&nbsp;($OPTIONALTOUR_NAME_1) ";
			}
			echo "</TD>
			";
			echo "<TD class='subTotal'>$OPTIONALTOUR_PRICE_1</TD>\n</TR>
			";
		}
		if (  $OPTIONALTOUR_ID_2  != 0  )  {
			echo "<TR>\n<td class='blueType'>\n";
			if ($languageUsed == "JP") {
				print("<script language='Javascript'>translate(\"Optional Tour Charge\");</script>: \n");
			}
			else {
				echo "Optional Tour Charge:<br>&nbsp;&nbsp;&nbsp;($OPTIONALTOUR_NAME_2) ";
			}
			echo "</TD>
			";
			echo "<TD class='subTotal'>$OPTIONALTOUR_PRICE_2</TD>\n</TR>
			";
		}
		if (  $OPTIONALTOUR_ID_3  != 0  )  {
			echo "<TR>\n<td class='blueType'>\n";
			if ($languageUsed == "JP") {
				print("<script language='Javascript'>translate(\"Optional Tour Charge\");</script>: \n");
			}
			else {
				echo "Optional Tour Charge:<br>&nbsp;&nbsp;&nbsp;($OPTIONALTOUR_NAME_3) ";
			}
			echo "</TD>
			";
			echo "<TD class='subTotal'>$OPTIONALTOUR_PRICE_3</TD>\n</TR>
			";
		}
		if (  $OPTIONALTOUR_ID_4  != 0  )  {
			echo "<TR>\n<td class='blueType'>\n";
			if ($languageUsed == "JP") {
				print("<script language='Javascript'>translate(\"Optional Tour Charge\");</script>: \n");
			}
			else {
				echo "Optional Tour Charge:<br>&nbsp;&nbsp;&nbsp;($OPTIONALTOUR_NAME_4) ";
			}
			echo "</TD>
			";
			echo "<TD class='subTotal'>$OPTIONALTOUR_PRICE_4</TD>\n</TR>
			";
		}
		if (  $OPTIONALTOUR_ID_5  != 0  )  {
			echo "<TR>\n<td class='blueType'>\n";
			if ($languageUsed == "JP") {
				print("<script language='Javascript'>translate(\"Optional Tour Charge\");</script>: \n");
			}
			else {
				echo "Optional Tour Charge:<br>&nbsp;&nbsp;&nbsp;($OPTIONALTOUR_NAME_5) ";
			}
			echo "</TD>
			";
			echo "<TD class='subTotal'>$OPTIONALTOUR_PRICE_5</TD>\n</TR>
			";
		}
		
		
		
		echo "<TR>\n<td class='blueType'>\n";
		if ($languageUsed == "JP") {
				print("<script language='javascript'>translate(\"Supp Room Charges\");</script>: \n");
		}
		else {
				echo "Supp Room Charges: \n";
		}
		echo "</TD>\n";
		$RoomCharge = sprintf("%5.2f", $RoomCharge);
		echo "<TD class='subTotal'>$RoomCharge</TD>\n</TR>\n";
		if ($TourDiscount > 0) {
			echo "<TR>\n<td class='blueType'>\n";
			if ($languageUsed == "JP") {
				print("<font color='black'><script language='javascript'>translate(\"Discount\");</script> </font>\n");
			}
			else {
				echo "<font color='black'>Student Discount </font>\n";
			}
			echo "</TD>\n";
			$ThisTourDiscount = sprintf("-%5.2f", $TourDiscount);
			echo "<TD class='subTotal'>$ThisTourDiscount</TD>\n</TR>\n";
		}
		if ($ManSuppCharge) {
			echo "<TR>\n<td class='blueType'>\nManual Supp Charge \n</TD>\n";
			$ManSuppCharge = sprintf("%5.2f", $ManSuppCharge);
			echo "<TD class='subTotal'>$ManSuppCharge</TD>\n</TR>\n";
		}
// to calculate student discount ($5.00)
// check if valid school code (this is the same as the agent code)
// direct student sign up is 5.00 discount on the regular price only
// if an agent signs up a student, agent will use the Reservation system
// directly.
	$query2 = "SELECT AGENT_ID FROM AGENT WHERE AGENT_CODE = '$SchoolCode'";
	$mysql_result2 = mysql_query($query2);
	$row2 = mysql_fetch_array($mysql_result2);
	$SchoolSignupID = $row2[0];
// default student discount (if no school code)
	$StudentDiscount = 10.00;
	if ($SchoolSignupID) {
			echo "<TR>\n<td class='blueType'>\n";
		if ($languageUsed == "JP") {
			print("<script language='Javascript'>translate(\"School Code\");</script>: \n");
		}
		else {
			echo "School Code: \n";
		}
			echo "</TD>\n<TD class='subTotal'>$SchoolCode</TD>\n</TR>\n";
			$ThisTotalCharge = $TotalCharge;
	}
	else {
			// if someone signup up on web, without an agent, then
			// default web discount applies
			// BUT, only if not processing a manual credit card job
			/* added 12/19/2003 */
			// removed 6/25/09 - no agent should be getting a web discount!
			
			if (((!$AgentCode) || ($AgentCode == "LAWEB") || ($AgentCode == "SFWEB") || ($AgentCode == "SDWEB")  || ($AgentCode == "SBWEB")) && (!$SchoolCode)) {
				/*
				echo "<TR>\n<td class='blueType'>\nWeb Discount \n</TD>\n";
				$StudentDiscount = sprintf("%5.2f", $StudentDiscount);
				$ThisTotalCharge = $TotalCharge - $StudentDiscount;
				echo "<TD class='subTotal'>-$StudentDiscount</TD>\n</TR>\n";
			    */
				$ThisTotalCharge = $TotalCharge;
			}
			
			else {
						echo "<TR>\n<td class='blueType'>\nAgent Code: \n</TD>\n<TD class='subTotal'>$AgentCode</TD>\n</TR>\n";
			}
	}
/* added 12/19/2003 */
	if ((!$AgentCode) && ($SchoolCode)) {
			$AgentCode = $SchoolCode;
	}
		echo "<TR>\n<td class='blueType'>\n";
		if ($languageUsed == "JP") {
			print("<script language='javascript'>translate(\"Total Charges\");</script>: \n");
		}
		else {
			echo "Total Charges: \n";
		}
		echo "</TD>\n";
		$ThisTotalCharge = sprintf("%5.2f", $ThisTotalCharge);
		echo "<TD class='subTotal'>$ThisTotalCharge</TD>\n</TR>\n";
/* added 2/2/04 */
/* break out portion to pay in Cash/CC */
			echo "<TR>\n<td class='blueType'>\nPay w/cash -TC: \n</TD>\n<td class='blueType'><input type='text' name='PayCash' size='10' class='subTotal'></TD>\n</TR>\n<TR>\n<td class='blueType'>\n";
			echo "Pay W/CC-CLT: \n</TD>\n<td class='blueType'><input type='text' name='PayCC' size='10' class='subTotal'></TD>\n</TR>\n</TABLE>\n";
// send total charge information to Linkpoint secure server for processing
//		print($TourDiscount);
		//echo "<form method='POST' action='toursignuptotal.php'>";
		$TourName = strtoupper($TourName);
		$ScheduledTourCode = strtoupper($ScheduledTourCode);
		$LastName = strtoupper($LastName);
		$FirstName = strtoupper($FirstName);
		echo "<input type='hidden' name='AgentProc' value='Yes'>";
		echo "<input type='hidden' name='OPTIONALTOUR_ID_1'  value='$OPTIONALTOUR_ID_1'>";
		echo "<input type='hidden' name='OPTIONALTOUR_ID_2'  value='$OPTIONALTOUR_ID_2'>";
		echo "<input type='hidden' name='OPTIONALTOUR_ID_3'  value='$OPTIONALTOUR_ID_3'>";
		echo "<input type='hidden' name='OPTIONALTOUR_ID_4'  value='$OPTIONALTOUR_ID_4'>";
		echo "<input type='hidden' name='OPTIONALTOUR_ID_5'  value='$OPTIONALTOUR_ID_5'>";
		echo "<input type='hidden' name='LocalReservationId' value='$ScheduledTourBookId'>";
		echo "<input type='hidden' name='LastName' value='$LastName'>";
		echo "<input type='hidden' name='FirstName' value='$FirstName'>";
		echo "<input type='hidden' name='TourName' value='$TourName'>";
		echo "<input type='hidden' name='ScheduledTourCode' value='$ScheduledTourCode'>";
		echo "<input type='hidden' name='AgentCode' value='$AgentCode'>";
		echo "<input type='hidden' name='SchoolCode' value='$AgentCode'>";
		echo "<input type='hidden' name='UserName' value='$UserName'>";
		echo "<input type='hidden' name='FullCode' value='FALSE'>";
		echo "<input type='hidden' name='PULocationLocation' value='$PULocationLocation'>";
		echo "<input type='hidden' name='PULocationTime' value='$PULocationTime'>";
		echo "<input type='hidden' name='DepartDate' value='$DepartDate'>";
		echo "<input type='hidden' name='ReturnDate' value='$ReturnDate'>";
		echo "<input type='hidden' name='CityCode' value='$CityCode'>";
		echo "<input type='hidden' name='City' value='$City'>";
		echo "<input type='hidden' name='RoomCharge' value='$RoomCharge'>";
		echo "<input type='hidden' name='TotalCharge' value='$TotalCharge'>";
		echo "<input type='hidden' name='Email' value='$Email'>";
		echo "<input type='hidden' name='TourType' value='$TourType'>";
		echo "<input type='hidden' name='ManSuppCharge' value='$ManSuppCharge'>";
		echo "<input type='hidden' name='TourDiscount' value='$TourDiscount'>";
		// echo "<input type='hidden' name='TourWebDiscount' value='$TourWebDiscount'>";  this was casusing errors. no need for webdiscount on agent sign up
		echo "<input type='hidden' name='TourPrice' value='$TourPrice'>";
		echo "<input type='hidden' name='PULocationId' value='$PULocationId'>";
		echo "<input type='hidden' name='Occupancy' value='$Occupancy'>";
		echo "<input type='hidden' name='Gender' value='$Gender'>";
		echo "<center><INPUT TYPE='SUBMIT' name ='WebSignUp' value='Next'>
		<INPUT TYPE='BUTTON' name = 'Cancel' value='Exit' onclick=CancelReservation();></CENTER>";
	//	echo "<center><INPUT TYPE='SUBMIT' name 'WebSignUp' value='Next'></CENTER>";
		echo "</form>";
	}
	else {
		echo "<center>\nThank You $FullName<br>\n<span style='color: red;'>UNFORTUNATELY - THIS TOUR IS FULL</span>\n</center>\n";
		// if called from booking, go back to booking table
		if ($LocalUserName) {
		// get user information if LocalUser is set (called from booking)
		// open a connection to mysql and select the "deltours" database
			$mysql_link = connect_to_db();
			$mysql_query = select_db($mysql_link);
			$query = "SELECT * FROM USER WHERE USER_NAME = '$LocalUserName'";
			$mysql_result = mysql_query($query);
			$numrows = mysql_num_rows($mysql_result);
			if ($numrows > 0) {
				$row = mysql_fetch_array($mysql_result);
				// get row result
				$UserId = $row[0];
				$LocalUserName = strtoupper($row[1]);
				$Access = $row[2];
				$PassWord = strtoupper($row[3]);
				$FullName = strtoupper($row[4]);
				$TimeStamp = $row[5];
			}
			echo "<SCRIPT LANGUAGE = 'JavaScript'>";
			if ($Access == 1) {
				print("document.location = \"schedtourbookforagent.php");
				echo "?args=";
				print($Access);
				echo ",";
				print($LocalUserName);
				print("\";");
			}
			else {
				print("document.location = \"schedtourbook.php");
				echo "?args=";
				print($Access);
				echo ",";
				print($LocalUserName);
				print("\";");
			}
			echo "</SCRIPT>";
		}
	}
?>
</body>
</html>
