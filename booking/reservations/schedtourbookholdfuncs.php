<?


function get_hold_record(	$ScheduledTourBookId,
						$ScheduledTourId,
						$LastName,
						$FirstName,
						$AgentId,
						$PULocationId,
						$mysql_link)
{

	$query = get_hold_query(	$ScheduledTourBookId,
						$ScheduledTourId,
						$LastName,
						$FirstName,
						$AgentId,
						$PULocationId,
						$mysql_link);

	//print($query);

	if ($mysql_link != 0)
	{
		$mysql_result = mysql_query($query, $mysql_link);
		if ($mysql_result)
		{
			$numrows = mysql_num_rows($mysql_result);
			if ($numrows > 0)
			{
				$row = mysql_fetch_array($mysql_result);
			}
			else
			{
				$row[0] = 0;
			}
		}
		else
		{
			$row[0] = 0;
		}
	}
	else
	{
		$row[0] = 0;
	}
	return ($row);
}

function get_hold_query(		$ScheduledTourBookId,
						$ScheduledTourId,
						$LastName,
						$FirstName,
						$AgentId,
						$PULocationId,
						$mysql_link)
{
	$index = 0;


	if ($AgentId)
	{
		$where1 = "AGENT_ID = '$AgentId'";
		$WhereArray[$index] = $where1;
		$index = $index + 1;
	}
	if ($ScheduledTourBookId)
	{
		$where2 = "SCHEDULEDTOURBOOK_ID = '$ScheduledTourBookId'";
		$WhereArray[$index] = $where2;
		$index = $index + 1;
	}
	if ($ScheduledTourId)
	{
		$where3 = "SCHEDULEDTOUR_ID	= '$ScheduledTourId'";
		$WhereArray[$index] = $where3;
		$index = $index + 1;
	}
	if ($LastName)
	{
		$where4 = "LAST_NAME = '$LastName'";
		$WhereArray[$index] = $where4;
		$index = $index + 1;
	}
	if ($FirstName)
	{
		$where5 = "FIRST_NAME = '$FirstName'";
		$WhereArray[$index] = $where5;
		$index = $index + 1;
	}
	if ($PULocationId)
	{
		$where6 = "PULOCATION_ID = '$PULocationId'";
		$WhereArray[$index] = $where6;
		$index = $index + 1;
	}

	$indexlimit = $index;

	if ($indexlimit == 1)
	{
		$query = sprintf("SELECT * FROM SCHEDULEDTOURBOOKHOLD WHERE %s ORDER BY SCHEDULEDTOURBOOK_ID ASC",$WhereArray[0]);
	}
	if ($indexlimit == 2)
	{
		$query = sprintf("SELECT * FROM SCHEDULEDTOURBOOKHOLD WHERE %s AND %s ORDER BY SCHEDULEDTOURBOOK_ID ASC",$WhereArray[0],$WhereArray[1]);
	}
	if ($indexlimit == 3)
	{
		$query = sprintf("SELECT * FROM SCHEDULEDTOURBOOKHOLD WHERE %s AND %s AND %s ORDER BY SCHEDULEDTOURBOOK_ID ASC",$WhereArray[0],$WhereArray[1],$WhereArray[2]);
	}
	if ($indexlimit == 4)
	{
		$query = sprintf("SELECT * FROM SCHEDULEDTOURBOOKHOLD WHERE %s AND %s AND %s AND %s ORDER BY SCHEDULEDTOURBOOK_ID ASC",$WhereArray[0],$WhereArray[1],$WhereArray[2],$WhereArray[3]);
	}
	if ($indexlimit == 5)
	{
		$query = sprintf("SELECT * FROM SCHEDULEDTOURBOOKHOLD WHERE %s AND %s AND %s AND %s AND %s ORDER BY SCHEDULEDTOURBOOK_ID ASC",$WhereArray[0],$WhereArray[1],$WhereArray[2],$WhereArray[3],$WhereArray[4]);
	}
	if ($indexlimit == 6)
	{
		$query = sprintf("SELECT * FROM SCHEDULEDTOURBOOKHOLD WHERE %s AND %s AND %s AND %s AND %s AND %s ORDER BY SCHEDULEDTOURBOOK_ID ASC",$WhereArray[0],$WhereArray[1],$WhereArray[2],$WhereArray[3],$WhereArray[4],$WhereArray[5]);
	}

	//print($query);

	return ($query);
}


function insert_new_hold_record(	$ScheduledTourBookId,
						  	$ScheduledTourId,
						   	$DepartCity,
						   	$DepartDate,
						   	$LastName,
						   	$FirstName,
						   	$Gender,
						   	$Occupancy,
						   	$AgentId,
						   	$TimeStamp,
						   	$LocationId,
						   	$TotalPayToDLT,
						   	$PayToDLTId,
						   	$PayToDLTDate,
							$TotalPayToAgent,
							$PayToAgentId,
							$PayToAgentDate,
							$TotalPayCC,
							$PayByCCId,
							$PayCCDate,
							$Email,
							$ManSuppCharge,
						   	$mysql_link,
							$LastName_1,
							$FirstName_1,
							$LastName_2,
							$FirstName_2,
							$LastName_3,
							$FirstName_3,
							$this_sess)
{ 
	$row = get_hold_record(	$ScheduledTourBookId,
						  	$ScheduledTourId,
						   	$LastName,
						   	$FirstName,
						   	$AgentId,
						   	@$PULocationId,
						   	$mysql_link);

	if ($row[0] == 0)
	{
		$current_time = getdate(time());
		$current_hours = $current_time["hours"];
		$current_mins = $current_time["minutes"];
		$current_secs = $current_time["seconds"];
		$current_date = date("Y-m-d");
		$Gender = strtoupper($Gender);
		if ($Gender == "") $Gender = "M";
		$LastName = strtoupper($LastName);
		$FirstName = strtoupper($FirstName);
		$TimeStamp = sprintf("%s %d:%d:%d",$current_date,$current_hours,$current_mins,$current_secs);

		$query = "INSERT INTO SCHEDULEDTOURBOOKHOLD
					(SCHEDULEDTOURBOOK_ID,
						SCHEDULEDTOUR_ID,
						LAST_NAME,
						FIRST_NAME,
						AGENT_ID,
						GENDER,
						OCCUPANCY,
						SCHEDULEDTOURBOOK_TIMESTAMP,
						PULOCATION_ID,
						TOTALPAYTODLT,
						PAYTODLT_ID,
						PAYTODLTDATE,
						TOTALPAYTOAGENT,
						PAYTOAGENT_ID,
						PAYTOAGENTDATE,
						TOTALPAYCC,
						PAYBYCC_ID,
						PAYCCDATE,
						EMAIL,
						MANSUPPCHARGE,
						LAST_NAME_2,
						FIRST_NAME_2,
						LAST_NAME_3,
						FIRST_NAME_3,
						LAST_NAME_4,
						FIRST_NAME_4,
						session_id)
						VALUES
					('$ScheduledTourBookId',
					'$ScheduledTourId',
					'$LastName',
					'$FirstName',
					'$AgentId',
					'$Gender',
					'$Occupancy',
					'$TimeStamp',
					'$LocationId',
				   	'$TotalPayToDLT',
				   	'$PayToDLTId',
				   	'$PayToDLTDate',
					'$TotalPayToAgent',
					'$PayToAgentId',
					'$PayToAgentDate',
					'$TotalPayCC',
					'$PayByCCId',
					'$PayCCDate',
					'$Email',
					'$ManSuppCharge',
					'$LastName_1',
					'$FirstName_1',
					'$LastName_2',
					'$FirstName_2',
					'$LastName_3',
					'$FirstName_3',
					'$this_sess')";

//				echo "<!-- QRY: $query -->";


		$mysql_result = mysql_query($query, $mysql_link);
	}

	return(@$query);
}

/* added 2/2/04 */

function update_hold_record(	$ScheduledTourBookId,
						  	$ScheduledTourId,
						   	$DepartCity,
						   	$DepartDate,
						   	$LastName,
						   	$FirstName,
						   	$Gender,
						   	$Occupancy,
						   	$AgentId,
						   	$TimeStamp,
						   	$LocationId,
						   	$TotalPayToDLT,
						   	$PayToDLTId,
						   	$PayToDLTDate,
							$TotalPayToAgent,
							$PayToAgentId,
							$PayToAgentDate,
							$TotalPayCC,
							$PayByCCId,
							$PayCCDate,
							$Email,
							$ManSuppCharge,
						   	$mysql_link)
{

/*
	$row = get_hold_record(	$ScheduledTourBookId,
						  	$ScheduledTourId,
						   	$LastName,
						   	$FirstName,
						   	$AgentId,
						   	$PULocationId,
						   	$mysql_link);

	if ($row[0] == 0)
	{
*/

		$current_time = getdate(time());
		$current_hours = $current_time["hours"];
		$current_mins = $current_time["minutes"];
		$current_secs = $current_time["seconds"];
		$current_date = date("Y-m-d");
		$Gender = strtoupper($Gender);
		if ($Gender == "") $Gender = "M";
		$LastName = strtoupper($LastName);
		$FirstName = strtoupper($FirstName);
		$TimeStamp = sprintf("%s %d:%d:%d",$current_date,$current_hours,$current_mins,$current_secs);

		$query = "UPDATE SCHEDULEDTOURBOOKHOLD
						SET SCHEDULEDTOUR_ID='$ScheduledTourId',
						LAST_NAME='$LastName',
						FIRST_NAME='$FirstName',
						AGENT_ID='$AgentId',
						GENDER='$Gender',
						OCCUPANCY='$Occupancy',
						SCHEDULEDTOURBOOK_TIMESTAMP='$TimeStamp',
						PULOCATION_ID='$LocationId',
						TOTALPAYTODLT='$TotalPayToDLT',
						PAYTODLT_ID='$PayToDLTId',
						PAYTODLTDATE='$PayToDLTDate',
						TOTALPAYTOAGENT='$TotalPayToAgent',
						PAYTOAGENT_ID='$PayToAgentId',
						PAYTOAGENTDATE='$PayToAgentDate',
						TOTALPAYCC='$TotalPayCC',
						PAYBYCC_ID='$PayByCCId',
						PAYCCDATE='$PayCCDate',
						EMAIL='$Email',
						MANSUPPCHARGE='$ManSuppCharge'
						WHERE SCHEDULEDTOURBOOK_ID='$ScheduledTourBookId'";
	//	print($query);

		$mysql_result = mysql_query($query, $mysql_link);

//	}

	return($query);

}

?>
