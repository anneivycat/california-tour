<?

// converts the first character of a string to upper case, the rest in
// lowercase

function first_upper($String)
{
	$Rest = strtolower(substr($String,1,strlen($String)));
	$First = strtoupper(substr($String,0,1));
	//print($First);
	$Result = sprintf("%s%s",$First,$Rest);
	return ($Result);
}

?>
