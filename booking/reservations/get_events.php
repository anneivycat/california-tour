<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=x-sjis">


  <title>Today's Schedule</title>
</head>
<body bgcolor="white" text="black">

<?

	include ("../sqlfuncs.php");

	// open a connection to mysql
	$mysql_link = connect_to_db();
	$mysql_result = select_db($mysql_link);

	if ($date)
	{
		$Today = $date;
		$query_sched_tours = "SELECT SCHEDULEDTOUR_CODE, SCHEDULEDTOUR_DEPART_DATE, SCHEDULEDTOUR_AVAILABLE FROM SCHEDULEDTOUR where SCHEDULEDTOUR_DEPART_DATE = '$Today'";
	}
	else
	{
		$Today = sprintf("%s/%s/%s", $month,$day,$year);
		$query_sched_tours = "SELECT SCHEDULEDTOUR_CODE, SCHEDULEDTOUR_DEPART_DATE, SCHEDULEDTOUR_AVAILABLE FROM SCHEDULEDTOUR where SCHEDULEDTOUR_DEPART_DATE = '$Today'";
	}
	$mysql_sched_tours = mysql_query($query_sched_tours, $mysql_link);
	$numrows_sched_tours = mysql_num_rows($mysql_sched_tours);
	if ($numrows_sched_tours > 0)
	{
		while ($rowcheck = mysql_fetch_array($mysql_sched_tours))
		{

			$ScheduledTourCode = $rowcheck[0];
			$ScheduledTourDepartDate = $rowcheck[1];
			$ScheduledTourAvailable = $rowcheck[2];

			$EventId = get_new_id_from_table("EVENTS","event_id", $mysql_link);

			$query_events_check = "SELECT * FROM EVENTS WHERE event_date = '$Today' AND event_name='$ScheduledTourCode'";
			$mysql_result_events_check = mysql_query($query_events_check, $mysql_link);
			$numrows = mysql_num_rows($mysql_result_events_check);
			if ($numrows == 0)
			{
				$query_insert = "INSERT INTO
								EVENTS	(event_id,
										event_name,
										event_desc,
										event_date,
										event_hour_from,
										event_hour_to,
										event_color,
										event_type)
								VALUES ('$EventId',
										'$ScheduledTourCode',
										' ',
										'$ScheduledTourDepartDate',
										' ',
										' ',
										' ',
										'TOUR')";

			//	print($query_insert);
				$mysql_result_insert = mysql_query($query_insert, $mysql_link);
			}
		}
	}

	$query_user = "SELECT user_id from USER where user_name = '$UserName'";
	$mysql_result_user = mysql_query($query_user, $mysql_link);
	$row_user = mysql_fetch_array($mysql_result_user);
	$UserId = strtoupper($row_user[0]);

if ($mode=="individual")
{
	$query_events = "SELECT event_id,event_name,event_hour_from,event_hour_to, event_color,user_id,shared FROM EVENTS WHERE event_date = '$Today' AND user_id = '$UserId' order by event_hour_from asc";
}
else
{
	$query_events = "SELECT A.event_id,A.event_name,A.event_hour_from,A.event_hour_to, A.event_color,A.user_id,A.shared FROM EVENTS A WHERE A.event_date = '$Today'  order by A.event_hour_from asc";
}

//	print($query_events);

	$mysql_result_events = mysql_query($query_events, $mysql_link);
	$numrows = mysql_num_rows($mysql_result_events);
	if ($numrows == 0)
	{
		if ($mode == "individual")
		{
			print("<center>No INDIVIDUAL events scheduled for $Today</center><br>\n");
		}
		else
		{
			print("<center>No GROUP events scheduled for $Today</center><br>\n");
		}
	}
	else
	{
		if ($mode == "individual")
		{
			print("<center><b>INDIVIDUAL Events scheduled for $Today</b></center><br><br>\n");
		}
		else
		{
			print("<center><b>GROUP Events scheduled for $Today</b></center><br><br>\n");

		}
		print("<table>\n");

		while ($rowcheck = mysql_fetch_array($mysql_result_events))
		{

			$EventId = $rowcheck[0];
			$EventName = $rowcheck[1];
			$EventHourFrom = $rowcheck[2];
			$EventHourTo = $rowcheck[3];
			$EventColor = $rowcheck[4];
			$EventUserId = $rowcheck[5];
			$Shared = $rowcheck[6];

	//		print("$UserId,$EventUserId\n");
	//		print("<br>\n");
			if (!$EventColor)
			{
				if ((!$EventHourFrom) and (!$EventHourTo))
				{
					if ($EventUserId <> $UserId)
					{
						if ($EventUserId == 0)
						{
							print("<tr><td align='left'><a href='edit_event.php?EventId=$EventId&mode=$mode&UserName=$UserName&UserLevel=$UserLevel&UserId=$UserId'><font face='arial' size='-1' color='blue'><u>$EventName</u></font></a></td></tr>\n");
						}
						else
						{
							if ($Shared == "Y")
							{
								$SharedCount = $SharedCount+1;
								print("<tr><td align='left'><a href='edit_event.php?EventId=$EventId&mode=$mode&UserName=$UserName&UserLevel=$UserLevel&UserId=$UserId'><font face='arial' size='-1' color='blue'>$EventName</font></a></td></tr>\n");
							}

						}
					}
					else
					{
						print("<tr><td align='left'><a href='edit_event.php?EventId=$EventId&mode=$mode&UserName=$UserName&UserLevel=$UserLevel&UserId=$UserId'><font face='arial' size='-1' color='blue'><u>$EventName</u></font></a></td></tr>\n");
					}
				}
				else
				{
					if ($EventUserId <> $UserId)
					{
						if ($EventUserId == 0)
						{
							print("<tr><td align='left'><font face='arial' size='-1' color='black'>$EventHourFrom - $EventHourTo </font><a href='edit_event.php?EventId=$EventId&mode=$mode&UserName=$UserName&UserLevel=$UserLevel&UserId=$UserId'><font face='arial' size='-1' color='blue'><u>$EventName</u></font></a></td></tr>\n");
						}
						else
						{
							if ($Shared == "Y")
							{
								$SharedCount = $SharedCount+1;
								print("<tr><td align='left'><font face='arial' size='-1' color='black'>$EventHourFrom - $EventHourTo </font><a href='edit_event.php?EventId=$EventId&mode=$mode&UserName=$UserName&UserLevel=$UserLevel&UserId=$UserId'><font face='arial' size='-1' color='blue'>$EventName</font></a></td></tr>\n");
							}

						}

					}
					else
					{
						print("<tr><td align='left'><font face='arial' size='-1' color='black'>$EventHourFrom - $EventHourTo </font><a href='edit_event.php?EventId=$EventId&mode=$mode&UserName=$UserName&UserLevel=$UserLevel&UserId=$UserId'><font face='arial' size='-1' color='blue'><u>$EventName</u></font></a></td></tr>\n");
					}
				}
			}
			else
			{
				if ((!$EventHourFrom) and (!$EventHourTo))
				{
					if ($EventUserId <> $UserId)
					{
						if ($EventUserId == 0)
						{
							print("<tr><td align='left'><a href='edit_event.php?EventId=$EventId&mode=$mode&UserName=$UserName&UserLevel=$UserLevel&UserId=$UserId'><font face='arial' size='-1' color='$EventColor'><u>$EventName</u></font></a></td></tr>\n");
						}
						else
						{
							if ($Shared == "Y")
							{
								$SharedCount = $SharedCount+1;
								print("<tr><td align='left'><a href='edit_event.php?EventId=$EventId&mode=$mode&UserName=$UserName&UserLevel=$UserLevel&UserId=$UserId'><font face='arial' size='-1' color='$EventColor'>$EventName</font></a></td></tr>\n");
							}

						}
					}
					else
					{
						print("<tr><td align='left'><a href='edit_event.php?EventId=$EventId&mode=$mode&UserName=$UserName&UserLevel=$UserLevel&UserId=$UserId'><font face='arial' size='-1' color='$EventColor'><u>$EventName</u></font></a></td></tr>\n");
					}
				}
				else
				{
					if ($EventUserId <> $UserId)
					{
						if ($EventUserId == 0)
						{
							print("<tr><td align='left'><font face='arial' size='-1' color='black'>$EventHourFrom - $EventHourTo </font><a href='edit_event.php?EventId=$EventId&mode=$mode&UserName=$UserName&UserLevel=$UserLevel&UserId=$UserId'><font face='arial' size='-1' color='$EventColor'><u>$EventName</u></font></a></td></tr>\n");
						}
						else
						{
							if ($Shared == "Y")
							{
								$SharedCount = $SharedCount+1;
								print("<tr><td align='left'><font face='arial' size='-1' color='black'>$EventHourFrom - $EventHourTo </font><a href='edit_event.php?EventId=$EventId&mode=$mode&UserName=$UserName&UserLevel=$UserLevel&UserId=$UserId'><font face='arial' size='-1' color='$EventColor'>$EventName</font></a></td></tr>\n");
							}

						}

					}
					else
					{
						print("<tr><td align='left'><font face='arial' size='-1' color='black'>$EventHourFrom - $EventHourTo </font><a href='edit_event.php?EventId=$EventId&mode=$mode&UserName=$UserName&UserLevel=$UserLevel&UserId=$UserId'><font face='arial' size='-1' color='$EventColor'><u>$EventName</u></font></a></td></tr>\n");
					}
				}
			}

		}
		if (($SharedCount == 0) and ($mode =="group"))
		{
		//	print("<tr><td align='center'><b><font face='arial' size='-1' color='black'>Others in your group have only private events for today.</font></b></td></tr>\n");
		}

		print("</table>\n");


	}

	print("<center><br><br>\n");
//	print("<a href='get_events.php?date=$Today&mode=individual&UserName=$UserName&UserLevel=$UserLevel'><font face='arial' size='-1'>Your Events</font></a>&nbsp;&nbsp;\n");
//	print("<a href='get_events.php?date=$Today&mode=group&UserName=$UserName&UserLevel=$UserLevel'><font face='arial' size='-1'>Group Events</font></a>&nbsp;&nbsp;\n");
	print("<a href='add_event.php?date=$Today&UserName=$UserName&UserLevel=$UserLevel'><font face='arial' size='-1'>Add Event</font></a>&nbsp;&nbsp;\n");
	print("<a href=\"javascript:close()\"><font face='arial' size='-1'>Return to Calendar</font></a></center>\n");


?>
</body>
</html>