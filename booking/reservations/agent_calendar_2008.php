<?php
error_reporting(0);
extract($_GET);
include ("../sqlfuncs.php");

$l = connect_to_db();
$l = select_db($l);

if ($city=="") { $city="SF"; }
$myCities["SB"]="Santa Barbara";
$myCities["SF"]="San Francisco";
$myCities["SD"]="San Diego";
$myCities["LA"]="Los Angeles";

$sql = 'SELECT `TOUR_TYPE` AS tt, `TOUR_NAME` AS tn
FROM `TOUR` 
ORDER BY tt;';
$r = mysql_query($sql);
$nr = mysql_num_rows($r);
for ($i=0;$i<$nr;$i++) {
 $line=mysql_fetch_array($r);
 $myTours[$line['tt']]=ucwords(strtolower($line['tn']));
// echo $line['tt']."=".$line['tn']."<br />\n";
}
?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2 Final//EN">
<HTML>
<HEAD>
<TITLE>Tours from <?php echo $myCities[$city]; ?></TITLE>
<?php if($languageUsed == "JP") { ?>
<meta http-equiv=\"Content-Type\" content="text/html; charset=x-sjis">
<?php
} else {
?>
<meta http-equiv=\"Content-Type\" content="text/html; charset=UTF-8">
<?php
}
?>
<style type="text/css">
BODY {
	font: 300 10px Verdana, Arial, sans-serif;
}
A SPAN { display: none; }
A:HOVER SPAN { display: block; }
</style>

<?php echo "<title>California Tours - Tours From $city</title>\n"; ?>
<script language="JavaScript">
<!--

function MM_swapImgRestore() { //v3.0
 var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_preloadImages() { //v3.0
 var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
   var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
   if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_findObj(n, d) { //v4.0
 var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
   d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
 if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
 for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
 if(!x && document.getElementById) x=document.getElementById(n); return x;
}

function MM_swapImage() { //v3.0
 var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
  if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}
//-->
</script>
</head>
<?php

function getFirstWeekDay($CurrentYear,$CurrentMonth,$CurrentDay) {
 /*
$day=mktime(0, 0, 0, $CurrentMonth,$CurrentDay,$CurrentYear);
 $day=date("w",$day);
 return ($day+1);
*/
$day = date("w",strtotime("$CurrentMonth/1/$CurrentYear"));
 return ($day);
}

function getMonthName($iMonth) {
 if ($iMonth == 1) { return("January"); }
 if ($iMonth == 2) { return("February"); }
 if ($iMonth == 3) { return("March"); }
 if ($iMonth == 4) { return("April"); }
 if ($iMonth == 5) { return("May"); }
 if ($iMonth == 6) { return("June"); }
 if ($iMonth == 7) { return("July"); }
 if ($iMonth == 8) { return("August"); }
 if ($iMonth == 9) { return("September"); }
 if ($iMonth == 10) { return("October"); }
 if ($iMonth == 11) { return("November"); }
 if ($iMonth == 12) { return("December"); }
 return("INVALID MONTH");
}

function getDaysInMonth($CurrentYear,$CurrentMonth) {
 $iDaysInMonth = 31;
 if (getMonthName($CurrentMonth) == "January") { return(31); }
 if (getMonthName($CurrentMonth) == "March") { return(31); }
 if (getMonthName($CurrentMonth) == "April") { return(30); }
 if (getMonthName($CurrentMonth) == "May") { return(31); }
 if (getMonthName($CurrentMonth) == "June") { return(30); }
 if (getMonthName($CurrentMonth) == "July") { return(31); }
 if (getMonthName($CurrentMonth) == "August") { return(31); }
 if (getMonthName($CurrentMonth) == "September") { return(30); }
 if (getMonthName($CurrentMonth) == "October") { return(31); }
 if (getMonthName($CurrentMonth) == "November") { return(30); }
 if (getMonthName($CurrentMonth) == "December") { return(31); }
 if (getMonthName($CurrentMonth) == "February") {
  $strCurrentYear = sprintf("%s",$CurrentYear);
  if (($CurrentYear %4) == 0) {
   return(29);
  } else {
   return(28);
  }
 return ($iDaysInMonth);
 }
}

$day="";
$dtToday = getdate(time());
$year = 2008;
if ($year=="") {
 $year=2005;
 $month=1;
 if($year==2004) {
  $year=2005;
  $month=1;
  $day="";
 }
}
$dtCurrentDate = sprintf("%s/%s/%s",$month,$day,$year);
$CurrentYear = $year;
$CurrentMonth = $month;
$CurrentDay = $day;

echo "<table ALIGN='CENTER' BORDER='0' bgcolor= '#000000' CELLSPACING='0' WIDTH='80%'>";
 $iFirstWeekDay = getFirstWeekDay($year,$month,$day);
 $iDaysInMonth = getDaysInMonth($year,$month);
 $i=$iDaysInMonth+$iFirstWeekDay;
 for ($iLoop = $iFirstWeekDay; $iLoop < $i; $iLoop++)  {
  $aCalendarDays[$iLoop] = $iLoop+1-$iFirstWeekDay;
 }

?>
<STYLE TYPE="text/css">
BODY, TD {
	font: 300 12px Verdana, Arial, sans-serif;
	color: black;
}
.blueType {
	color: #33F;
}
A:link {COLOR: #3366FF; TEXT-DECORATION: none}
A:visited {COLOR: #3366FF; TEXT-DECORATION: none}
A:hover {COLOR: #0000CC; TEXT-DECORATION: none}
.style1 {color: #3300FF}
.style2 {color: #0033FF}
</STYLE>
</head>
<body>
<table width="742" border="0" cellspacing="0" cellpadding="0">
<tr><td width="106">&nbsp;</td>
<td width="261"><img src="/booking/images/california_tours_logo/home153_64.gif" width="153" height="64" alt="California Tours"></td>
<td width="315" align="center"><h3>2008 tours from <?php echo $myCities[$city]; ?></h3></td>
<td width="60">&nbsp;</td>
</tr>
</table>
<br>
<table width="768" border="0" cellspacing="0" cellpadding="0">
<tr align="center" valign="middle" bgcolor="#ffba3d">
<td width="31" height="13" bgcolor="#FFCC66"></td>
<td width="293" height="13" bgcolor="#FFCC66" align="left"></td>
<td width="351" height="13" bgcolor="#FFCC66" valign="middle" align="right"><a href="mailto:tours@california-tour.com"><img src="/booking/images/links_position/contact.gif" width="310" height="17" border="0" alt="California Tour"></a></td>
<td width="63" height="13" bgcolor="#FFCC66"></td>
</tr>
<tr align="center" valign="top">
<td colspan="5">
<table width="720" border="0" cellspacing="0" cellpadding="0">
<tr>
<td width="130" align="left" valign="top">
<table width="124" border="0" cellspacing="0" cellpadding="0">
<tr><td height="12" align="left" valign="top"><img src="/booking/images/spacer/120_10.gif" width="120" height="10"></td>
</tr>
<tr><td height="168" align="left" valign="top">
</td></tr></table></td>
<td width="485" align="left" valign="top">
<table width="477" border="0" cellspacing="0" cellpadding="0">
<tr><td width="8">&nbsp;</td>
<td align="left" valign="top">
<div align="center">
<br>
<font size="2" color="#006699"><strong>
<font face="Verdana, Arial, Helvetica, sans-serif" size="2">
Tours from 
<?php
echo $myCities[$city];
?>
</strong></font>
</font>
<br />
<a href="?city=<?php echo $city; ?>&year=2006&month=1">jan</a>&nbsp;
<a href="?city=<?php echo $city; ?>&year=2006&month=2">feb</a>&nbsp;
<a href="?city=<?php echo $city; ?>&year=2006&month=3">mar</a>&nbsp;
<a href="?city=<?php echo $city; ?>&year=2006&month=4">apr</a>&nbsp;
<a href="?city=<?php echo $city; ?>&year=2006&month=5">may</a>&nbsp;
<a href="?city=<?php echo $city; ?>&year=2006&month=6">june</a>&nbsp;
<a href="?city=<?php echo $city; ?>&year=2006&month=7">july</a>&nbsp;
<a href="?city=<?php echo $city; ?>&year=2006&month=8">aug</a>&nbsp;
<a href="?city=<?php echo $city; ?>&year=2006&month=9">sept</a>&nbsp;
<a href="?city=<?php echo $city; ?>&year=2006&month=10">oct</a>&nbsp;
<a href="?city=<?php echo $city; ?>&year=2006&month=11">nov</a>&nbsp;
<a href="?city=<?php echo $city; ?>&year=2006&month=12">dec</a>
</font>

<br /><br />
<table width="565" cellspacing="0" cellpadding="0" style="border: 0px; padding: 2px;">
<tr bgcolor="#FFFFFF" align="center" valign="top">
<td colspan="7" height="19">
<font face="Verdana, Arial, Helvetica, sans-serif" color="#006699">
<strong><font size="2" color="#FF6600"><?php echo getMonthName($month); ?> <?php echo $year ?></font></strong>
</font>
</td></tr>
<!-- Display Days of the Week -->
<tr bgcolor="#006699" align="center" valign="middle">
<td width="80"><font face="Verdana, Arial, Helvetica, sans-serif" size="2" color="#FFFFFF">Sun</font></td>
<td width="80"><font face="Verdana, Arial, Helvetica, sans-serif" size="2" color="#FFFFFF">Mon</font></td>
<td width="80"><font face="Verdana, Arial, Helvetica, sans-serif" size="2" color="#FFFFFF">Tues</font></td>
<td width="80"><font face="Verdana, Arial, Helvetica, sans-serif" size="2" color="#FFFFFF">Wed</font></td>
<td width="80"><font face="Verdana, Arial, Helvetica, sans-serif" size="2" color="#FFFFFF">Thur</font></td>
<td width="83"><font face="Verdana, Arial, Helvetica, sans-serif" size="2" color="#FFFFFF">Fri</font></td>
<td width="82"><font face="Verdana, Arial, Helvetica, sans-serif" size="2" color="#FFFFFF">Sat</font></td></tr>
<!-- Display Calendar -->
<?php  for ($iDayBanner = 1; $iDayBanner <= $iColumns; $iDayBanner++) ?>
<tr align="center" valign="top">
<?php
$ImgIndex = 0;
$kk=0;
$dm=substr("000".$month,-2);
for ($i = 0; $i < 42; $i++) {
 $dd = $aCalendarDays[$i]+0;
 if ($dd == "") {
  echo"<TD BGCOLOR='WHITE' VALIGN= 'TOP' ALIGN='Left' style='width: 120px; height: 80px;'>&nbsp;</td>\n";
 } else {
//  $dd=($i-$iFirstWeekDay+1);
  $dd=substr("000".$dd,-2);
  echo"<TD BGCOLOR='WHITE' VALIGN= 'TOP' ALIGN='Left' id='tagada$dd' style='width: 120px; height: 80px;'>
<font face='arial'>".($dd+0)."<br />";
  $MonthName = getMonthName($month);
  $ShortYear = substr($year,2,2);
  $query = "SELECT SCHEDULEDTOUR_CODE, TOUR_ID
FROM SCHEDULEDTOUR
WHERE SCHEDULEDTOUR_CODE
LIKE  '%$dm$dd$ShortYear'
AND SCHEDULEDTOUR_CODE like '$city%'";
//  echo "<span style='font-size: 70%;'>$query</span><br />\n";
  $mysql_result = mysql_query($query);
  $totalnumrows = mysql_num_rows($mysql_result);
  if ($totalnumrows>0) {
   while ($row = mysql_fetch_array($mysql_result)) {
    $ScheduledTourCode = strtoupper($row[0]);
    $TourId = strtoupper($row[1]);
    $querytour = "SELECT * FROM TOUR WHERE TOUR_ID = '$TourId'";
    $mysql_result_tour = mysql_query($querytour);
    $rowtour = mysql_fetch_array($mysql_result_tour);
    $TourName = $rowtour[2];
    $ImgList = strtolower($rowtour[24]);
    if (strlen($ImgList) > 0) {
     $ImgListArray = explode(",",$ImgList);
     if ($ImgIndex >= count($ImgListArray)) {
      $ImgIndex = 0;
      $ThmImg = $ImgListArray[$ImgIndex];
     } else {
      $ThmImg = $ImgListArray[$ImgIndex];
      $ImgIndex = $ImgIndex+1;
     }
    }
    $SchedCodeLength = strlen($ScheduledTourCode);
    $ScheduledTourShortCode = substr($ScheduledTourCode,0,$SchedCodeLength - 2);
    $ScheduledTourShortCode = $ScheduledTourCode;
    $myURL="agenttoursignup.php?LocalScheduledTourCode=$ScheduledTourShortCode&languageUsed=$languageUsed&city=$city&year=$year&month=$month&day=$day&SchoolCode=$SchoolCode&UserName=$UserName";
    $ucTourName=$myTours[substr($ScheduledTourShortCode,0,4)];
    echo "\n<a href=\"$myURL\"><font face='arial' size='-1'>$ucTourName</font></a><br />\n";
   }
  }
  echo "</td>\n";
 }
 $kk+=1;
 if ($kk == 7) {
  $kk=0;
  echo "</tr>\n";
 }
}
?>

</table></table></table></table>
<center><FONT SIZE="1" FACE="Verdana, Arial, Helvetica, sans-serif" COLOR="#333333">Schedule subject to change.</FONT>
<br>
<br>
<hr width="99%">
</center>
</body></html>


