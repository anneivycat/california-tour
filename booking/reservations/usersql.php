

<?


/* SQL specific functions call for users.php */

// open a connection to mysql and select the "deltours" database

	// connect to server

	include ("newclasses.php");

	include ("../sqlfuncs.php");

	// open a connection to mysql

	$mysql_link = connect_to_db();
	$mysql_result = select_db($mysql_link);

	// get current database size

	$query = "SELECT * FROM USER ORDER BY USER_ID ASC";

	$mysql_result = mysql_query($query, $mysql_link);

	$totalnumrows = mysql_num_rows($mysql_result);

	if ($totalnumrows > 0)
	{
		if ($totalnumrows == 1)
		{
			$ResultString = "<b><font color='blue'>Database has </font><font color='red'> $totalnumrows </font><font color='blue'> record</font></b>";
		}
		else
		{
			$ResultString = "<b><font color='blue'>Database has </font><font color='red'> $totalnumrows </font><font color='blue'> records</font></b>";
		}
	}
	else
	{
		$ResultString = "<b><font color='red'>Database has no records</font></b>";
	}


//print("User ID=$UserId<br>");
//print("User Name=$UserName<br>");
//print("Access Level=$Access<br>");
//print("PassWord=$PassWord<br>");
//print("Full Name=$FullName<br>");

// check which key was pressed
if ($DelUser)
{

	$CurrentSearch = new SearchClass;
	$CurrentSearch->init("USER_ID",
						"USER",
						"USER_ID",
						"USER_NAME",
						"USER_ACCESS",
						"USER_PASSWORD",
						"USER_FULLNAME",
						"",
						"USER_NAME",
						"USER_ID",
						"USER_NAME",
						"USER_ACCESS",
						"USER_PASSWORD",
						"USER_FULLNAME",
						"USER_TIMESTAMP",
						"AGENT_ID",
						"",
						"",
						"",
						"",
						"",
						"",
						"",
						"",
						"",
						"",
						"",
						"",
						"",
						"",
						"",
						"",
						"",
						"",
						$mysql_link);


//	print("Key Pressed=$DelUser");

// delete current record from db

	$query = "DELETE FROM USER WHERE USER_ID = '$UserId'";

	// print($query);
	$mysql_result = mysql_query($query, $mysql_link);

	$ResultString2 =  $CurrentSearch->print_tb_size($table_name, $mysql_link);
	$ResultString = "<b><font color='red'>Record Deleted</font></font></b><br>";
	$ResultString3 = "";
	$UserId = "";
	$UserName = "";
	$PassWord = "";
	$Access = "";
	$FullName = "";
	$TimeStamp = "";
	$QueryString = "";

}

if ($AddUser)
{


	$LocalUserId 		= $UserId;
	$LocalUserName		= $UserName;
	$LocalAccess		= $Access;
	$LocalPassWord		= $PassWord;
	$LocalFullName		= $FullName;

/*
	function init($tableindex,
					$tblname,
					$searchfield1,
					$searchfield2,
					$searchfield3,
					$searchfield4,
					$searchfield5,
					$searchfield6,
					$sortfield,
					$fieldname1,
					$fieldname2,
					$fieldname3,
					$fieldname4,
					$fieldname5,
					$fieldname6,
					$fieldname7,
					$fieldname8,
					$fieldname9,
					$fieldname10,
					$fieldname11,
					$fieldname12,
					$fieldname13,
					$fieldname14,
					$fieldname15,
					$fieldname16,
					$fieldname17,
					$fieldname18,
					$fieldname19,
					$fieldname20,
					$fieldname21,
					$fieldname22,
					$fieldname23,
					$fieldname24,
					$fieldname25,
					$mysqllink)

$query = "CREATE TABLE USER (
			USER_ID mediumint(2) NOT NULL default '0',
			USER_NAME char(40) NOT NULL default '',
			USER_ACCESS tinyint(1) NOT NULL default '0',
			USER_PASSWORD char(10) NOT NULL default '',
			USER_FULLNAME char(40) NOT NULL default '',
			USER_TIMESTAMP datetime NOT NULL default '0000-00-00 00:00:00',


*/

	$CurrentSearch = new SearchClass;
	$CurrentSearch->init("USER_ID",
						"USER",
						"USER_ID",
						"USER_NAME",
						"USER_ACCESS",
						"USER_PASSWORD",
						"USER_FULLNAME",
						"",
						"USER_NAME",
						"USER_ID",
						"USER_NAME",
						"USER_ACCESS",
						"USER_PASSWORD",
						"USER_FULLNAME",
						"USER_TIMESTAMP",
						"AGENT_ID",
						"",
						"",
						"",

						"",
						"",
						"",
						"",
						"",
						"",
						"",
						"",
						"",
						"",
						"",
						"",
						"",
						"",
						"",
						$mysql_link);

	$row = $CurrentSearch->get_record(			$LocalUserId,
												$LocalUserName,
												$LocalAccess,
												$LocalPassWord,
												$LocalFullName,
												"",
												$mysql_link);
	if ($row[0] != 0)
	{

		$query = $CurrentSearch->get_query(		$LocalUserId,
												$LocalUserName,
												$LocalAccess,
												$LocalPassWord,
												$LocalFullName,
												"",
												$mysql_link);
		$QueryString = $query;


		$mysql_result = mysql_query($query, $mysql_link);
		$numrows = mysql_num_rows($mysql_result);
		if ($numrows == 1)
		{
			$ResultString2 = "<br><b><font color='blue'>Search returned </font><font color='red'>$numrows</font><font color='blue'> record</font></b>";
		}
		else
		{
			$ResultString2 = "<br><b><font color='blue'>Search returned </font><font color='red'>$numrows</font><font color='blue'> records</font></b>";
		}
		if ($numrows > 0)
		{
			//print("Row 0 = $row[0]");
			$row = mysql_fetch_array($mysql_result);
			// get row result
			$UserId 	= $row[0];
			$UserName 	= strtoupper($row[1]);
			$Access 	= $row[2];
			$PassWord 	= strtoupper($row[3]);
			$FullName 	= strtoupper($row[4]);
			$TimeStamp = $row[5];
			$AgentId = $row[6];

		}
	}
	else
	{

		$UserId = $CurrentSearch->get_new_id();

		$ResultString = "<b><font color='red'>New Record</font></font></b><br>";
		$ResultString2 =  $CurrentSearch->print_tb_size();

	}

//	print("Key Pressed=$AddUser");
}

if ($PrevUser)
{
	$CurrentSearch = new SearchClass;
	$CurrentSearch->init("USER_ID",
						"USER",
						"USER_ID",
						"USER_NAME",
						"USER_ACCESS",
						"USER_PASSWORD",
						"USER_FULLNAME",
						"",
						"USER_NAME",
						"USER_ID",
						"USER_NAME",
						"USER_ACCESS",
						"USER_PASSWORD",
						"USER_FULLNAME",
						"USER_TIMESTAMP",
						"AGENT_ID",
						"",
						"",
						"",
						"",
						"",
						"",
						"",
						"",
						"",
						"",
						"",
						"",
						"",
						"",
						"",
						"",
						"",
						"",
						$mysql_link);
	$row = $CurrentSearch->prev_record($QueryString);
	if ($row)
	{
		$QueryString = $CurrentSearch->getquerystring();
		$ResultString = $CurrentSearch->getresult1();
		$ResultString2 = $CurrentSearch->getresult2();
		$ResultString3 = $CurrentSearch->getresult3();
		// get row result
		$UserId 	= $row[0];
		$UserName 	= strtoupper($row[1]);


		$Access 	= $row[2];
		$PassWord 	= strtoupper($row[3]);
		$FullName 	= strtoupper($row[4]);
		$TimeStamp = $row[5];
		$AgentId = $row[6];

	}
	else
	{
		$ResultString =  $CurrentSearch->print_tb_size($table_name, $mysql_link);
		$ResultString2 = "";
		$ResultString3 = "";

	}

//	print("Key Pressed=$PrevUser");
}
if ($LastUser)
{
	$CurrentSearch = new SearchClass;
	$CurrentSearch->init("USER_ID",
						"USER",
						"USER_ID",
						"USER_NAME",
						"USER_ACCESS",
						"USER_PASSWORD",
						"USER_FULLNAME",
						"",
						"USER_NAME",
						"USER_ID",
						"USER_NAME",
						"USER_ACCESS",
						"USER_PASSWORD",
						"USER_FULLNAME",
						"USER_TIMESTAMP",
						"AGENT_ID",
						"",
						"",
						"",
						"",
						"",
						"",
						"",
						"",
						"",
						"",
						"",
						"",
						"",
						"",
						"",
						"",
						"",
						"",
						$mysql_link);
	$row = $CurrentSearch->last_record($QueryString);
	if ($row)
	{
		$QueryString = $CurrentSearch->getquerystring();
		$ResultString = $CurrentSearch->getresult1();
		$ResultString2 = $CurrentSearch->getresult2();
		$ResultString3 = $CurrentSearch->getresult3();
		// get row result
		$UserId 	= $row[0];
		$UserName 	= strtoupper($row[1]);
		$Access 	= $row[2];
		$PassWord 	= strtoupper($row[3]);

		$FullName 	= strtoupper($row[4]);
		$TimeStamp = $row[5];
		$AgentId = $row[6];

	}
	else
	{
		$ResultString =  $CurrentSearch->print_tb_size($table_name, $mysql_link);
		$ResultString2 = "";
		$ResultString3 = "";

	}


//	print("Key Pressed=$LastUser");
}


if ($ChgUser)
{
	$CurrentSearch = new SearchClass;
	$CurrentSearch->init("USER_ID",
						"USER",
						"USER_ID",
						"USER_NAME",
						"USER_ACCESS",
						"USER_PASSWORD",
						"USER_FULLNAME",
						"",
						"USER_NAME",
						"USER_ID",
						"USER_NAME",
						"USER_ACCESS",
						"USER_PASSWORD",
						"USER_FULLNAME",
						"USER_TIMESTAMP",
						"AGENT_ID",
						"",
						"",
						"",
						"",
						"",
						"",
						"",
						"",
						"",
						"",
						"",
						"",
						"",
						"",
						"",
						"",
						"",
						"",
						$mysql_link);



	if (($UserName) AND ($PassWord))
	{

	$query = "SELECT * FROM USER WHERE USER_NAME = '$UserName'";
	$mysql_result = mysql_query($query, $mysql_link);

	$numrows = mysql_num_rows($mysql_result);

	if ($numrows > 0)
	{
			//print("Changing record..\n");

			$row = mysql_fetch_array($mysql_result);
			// get row result
			$UserId = $row[0];

			$query = "DELETE FROM USER WHERE USER_ID = '$UserId'";
			$mysql_result = mysql_query($query, $mysql_link);

			$current_time = getdate(time());
			$current_hours = $current_time["hours"];
			$current_mins = $current_time["minutes"];
			$current_secs = $current_time["seconds"];
			$current_date = date("Y-m-d");

			$TimeStamp = sprintf("%s %d:%d:%d",$current_date,$current_hours,$current_mins,$current_secs);

			$UserName 		= strtoupper($UserName);
			$PassWord 		= strtoupper($PassWord);
			$FullName 		= strtoupper($FullName);

			$query = "INSERT INTO USER (USER_ID,USER_NAME,USER_PASSWORD,USER_ACCESS,USER_FULLNAME,USER_TIMESTAMP,AGENT_ID) VALUES ('$UserId', '$UserName', '$PassWord','$Access','$FullName','$TimeStamp','$AgentId')";
			// print($query);

			$mysql_result = mysql_query($query, $mysql_link);

			$ResultString2 =  $CurrentSearch->print_tb_size($table_name, $mysql_link);
			$ResultString = "<b><font color='red'>Record Changed</font></font></b><br>";


	}

	}
	else
	{
			$ResultString = "<b><font color='red'>Could not change record - USER NAME AND PASSWORD MUST BE NON-BLANK</font></font></b><br>";
			$ResultString2 =  $CurrentSearch->print_tb_size();

			$row = $CurrentSearch->get_record( $UserId,
												"",
												"",
												"",
												"",
												"",
												$mysql_link);

			$UserId 	= $row[0];
			$UserName 	= strtoupper($row[1]);
			$Access 	= $row[2];
			$PassWord 	= strtoupper($row[3]);
			$FullName 	= strtoupper($row[4]);
			$TimeStamp = $row[5];
			$AgentId = $row[6];

	}
//	print("Key Pressed=$ChgUser");
}

if ($NextUser)
{
	$CurrentSearch = new SearchClass;
	$CurrentSearch->init("USER_ID",
						"USER",
						"USER_ID",
						"USER_NAME",
						"USER_ACCESS",
						"USER_PASSWORD",
						"USER_FULLNAME",
						"",
						"USER_NAME",
						"USER_ID",
						"USER_NAME",
						"USER_ACCESS",
						"USER_PASSWORD",
						"USER_FULLNAME",
						"USER_TIMESTAMP",
						"AGENT_ID",
						"",
						"",
						"",
						"",
						"",
						"",
						"",
						"",
						"",
						"",
						"",
						"",
						"",
						"",
						"",
						"",
						"",
						"",
						$mysql_link);
	$row = $CurrentSearch->next_record($QueryString);
	if ($row)
	{
		$QueryString = $CurrentSearch->getquerystring();
		$ResultString = $CurrentSearch->getresult1();
		$ResultString2 = $CurrentSearch->getresult2();
		$ResultString3 = $CurrentSearch->getresult3();
		// get row result
		$UserId 	= $row[0];
		$UserName 	= strtoupper($row[1]);
		$Access 	= $row[2];
		$PassWord 	= strtoupper($row[3]);
		$FullName 	= strtoupper($row[4]);
		$TimeStamp = $row[5];
		$AgentId = $row[6];

	}
	else
	{
		$ResultString =  $CurrentSearch->print_tb_size($table_name, $mysql_link);
		$ResultString2 = "";
		$ResultString3 = "";

	}

//	print("Key Pressed=$NextUser");
}

if ($FirstUser)
{
	$CurrentSearch = new SearchClass;
	$CurrentSearch->init("USER_ID",
						"USER",
						"USER_ID",
						"USER_NAME",
						"USER_ACCESS",
						"USER_PASSWORD",
						"USER_FULLNAME",
						"",
						"USER_NAME",
						"USER_ID",
						"USER_NAME",
						"USER_ACCESS",
						"USER_PASSWORD",
						"USER_FULLNAME",
						"USER_TIMESTAMP",
						"AGENT_ID",
						"",
						"",
						"",
						"",
						"",
						"",
						"",
						"",
						"",
						"",
						"",
						"",
						"",
						"",
						"",
						"",
						"",
						"",
						$mysql_link);
	$row = $CurrentSearch->first_record($QueryString);
	if ($row)
	{
		$QueryString = $CurrentSearch->getquerystring();
		$ResultString = $CurrentSearch->getresult1();
		$ResultString2 = $CurrentSearch->getresult2();
		$ResultString3 = $CurrentSearch->getresult3();
		// get row result
		$UserId 	= $row[0];
		$UserName 	= strtoupper($row[1]);
		$Access 	= $row[2];
		$PassWord 	= strtoupper($row[3]);
		$FullName 	= strtoupper($row[4]);
		$TimeStamp = $row[5];
		$AgentId = $row[6];

	}

	else
	{
		$ResultString =  $CurrentSearch->print_tb_size($table_name, $mysql_link);
		$ResultString2 = "";
		$ResultString3 = "";

	}

}

if ($CancelUser)
{
			$UserId = "";
			$UserName = "";
			$Access = "";
			$PassWord = "";
			$FullName = "";
			$TimeStamp = "";
			$QueryString = "";
			$AgentId = "";

}

if ($SaveUser)
{
	$CurrentSearch = new SearchClass;
	$CurrentSearch->init("USER_ID",
						"USER",
						"USER_ID",
						"USER_NAME",
						"USER_ACCESS",
						"USER_PASSWORD",
						"USER_FULLNAME",
						"",
						"USER_NAME",
						"USER_ID",
						"USER_NAME",
						"USER_ACCESS",
						"USER_PASSWORD",
						"USER_FULLNAME",
						"USER_TIMESTAMP",
						"AGENT_ID",
						"",
						"",
						"",
						"",
						"",
						"",
						"",
						"",
						"",
						"",
						"",
						"",
						"",
						"",
						"",
						"",
						"",
						"",
						$mysql_link);


	$current_time = getdate(time());
	$current_hours = $current_time["hours"];
	$current_mins = $current_time["minutes"];
	$current_secs = $current_time["seconds"];
	$current_date = date("Y-m-d");

	$TimeStamp = sprintf("%s %d:%d:%d",$current_date,$current_hours,$current_mins,$current_secs);


	if ($UserId != '')
	{
		if (($UserName) AND ($PassWord))
		{
			if ($CurrentSearch->insert_new_record(	$UserId,
											strtoupper($UserName),
											$Access,
											strtoupper($PassWord),
											strtoupper($FullName),
											"",
											$UserId,
											strtoupper($UserName),
											$Access,
											strtoupper($PassWord),
											strtoupper($FullName),
											$AgentId,
											"",
											"",
											"",
											"",
											"",
											"",
											"",
											"",
											"",
											"",
											"",
											"",
											"",
											"",
											"",
											"",
											"",
											"",
											"",
											$mysql_link) == 1)
			{
				$ResultString = "<b><font color='red'>Record Saved</font></font></b><br>";
				$ResultString2 =  $CurrentSearch->print_tb_size();
			}
			else
			{
				$ResultString = "<b><font color='red'>Could not save record - record already exists (use Chg to change an existing record)</font></font></b><br>";
				$ResultString2 =  $CurrentSearch->print_tb_size();
				$UserId = "";
				$UserName = "";
				$Access = "";
				$PassWord = "";
				$FullName = "";
				$TimeStamp = "";
				$QueryString = "";
				$AgentId = "";

			}
		}
		else
		{
			$ResultString = "<b><font color='red'>Could not save record - USER NAME AND PASSWORD MUST BE NON-BLANK</font></font></b><br>";
			$ResultString2 =  $CurrentSearch->print_tb_size();
			$UserId = "";
			$UserName = "";
			$Access = "";
			$PassWord = "";
			$FullName = "";
			$TimeStamp = "";
			$QueryString = "";
			$AgentId = "";

		}
	}
	else
	{
		$ResultString = "<b><font color='red'>Could not save record - Invalid User Id</font></font></b><br>";
		$ResultString2 =  $CurrentSearch->print_tb_size();
						$UserId = "";
						$UserName = "";
						$Access = "";
						$PassWord = "";
						$FullName = "";
						$TimeStamp = "";
						$QueryString = "";
			            $AgentId = "";

	}


}

if ($ListUser)
{


	//print("hello");

	$mysql_result = 0;
	$mysql_result1 = 0;
	$QueryString = stripslashes($QueryString);

	//print($QueryString);

	$CurrentSearch = new SearchClass;
	$CurrentSearch->init("USER_ID",
						"USER",
						"USER_ID",
						"USER_NAME",
						"USER_ACCESS",
						"USER_PASSWORD",
						"USER_FULLNAME",
						"",
						"USER_NAME",
						"USER_ID",
						"USER_NAME",
						"USER_ACCESS",
						"USER_PASSWORD",
						"USER_FULLNAME",
						"USER_TIMESTAMP",
						"AGENT_ID",
						"",
						"",
						"",
						"",
						"",
						"",
						"",
						"",
						"",
						"",


						"",
						"",
						"",
						"",
						"",
						"",
						"",
						"",
						$mysql_link);

	$mysql_result = $CurrentSearch->list_records($QueryString);
	//print("result = $mysql_result");
	if ($mysql_result >= 1)
	{
		$QueryString = $CurrentSearch->getquerystring();
		$ResultString = $CurrentSearch->getresult1();
		$ResultString2 = $CurrentSearch->getresult2();
		$ResultString3 = $CurrentSearch->getresult3();

		print("<table align='center' size='100%'>");
		print("<tr>");
		print("<TD ALIGN='LEFT' valign='top'><b><u><Font color='black'>User ID</font></u></b></TD>");
		print("\n");
		print("<TD ALIGN='LEFT' valign='top'></TD>");
		print("\n");
		print("<TD ALIGN='LEFT' valign='top'><b><u><Font color='black'>User Name</font></u></b></TD>");
		print("\n");
		print("<TD ALIGN='LEFT' valign='top'></TD>");
		print("\n");
		print("<TD ALIGN='LEFT' valign='top'><b><u><Font color='black'>User Password</font></u></b></TD>");
		print("\n");
		print("<TD ALIGN='LEFT' valign='top'></TD>");
		print("\n");
		print("<TD ALIGN='LEFT' valign='top'><b><u><Font color='black'>Access Level</font></u></b></TD>");
		print("\n");
		print("<TD ALIGN='LEFT' valign='top'></TD>");
		print("\n");
		print("<TD ALIGN='LEFT' valign='top'><b><u><Font color='black'>Full Name</font></u></b></TD>");
		print("\n");


		print("<TD ALIGN='LEFT' valign='top'></TD>");
		print("\n");
		print("<TD ALIGN='LEFT' valign='top'><b><u><Font color='black'>Agent Code</font></u></b></TD>");
		print("\n");
		print("<TD ALIGN='LEFT' valign='top'></TD>");
		print("\n");

		print("<TD ALIGN='LEFT' valign='top'><b><u><Font color='black'>Last Updated</font></u></b></TD>");
		print("\n");
		print("<TD ALIGN='LEFT' valign='top'></TD>");
		print("\n");
		print("</TR>");
		print("<tr>");
		print("</tr>");

		while ($row = mysql_fetch_array($mysql_result))
		{
				$UserId 	= $row[0];
				$UserName 	= strtoupper($row[1]);
				$Access 	= $row[2];
				$PassWord 	= strtoupper($row[3]);
				$FullName 	= strtoupper($row[4]);
				$TimeStamp = $row[5];
				$AgentId = $row[6];

				$query = "SELECT AGENT_CODE FROM AGENT WHERE AGENT_ID= '$AgentId'";
				$mysql_result = mysql_query($query, $mysql_link);

				$numrows = mysql_num_rows($mysql_result);

				$AgentCode = "NONE";
				if ($numrows > 0)
				{
					$row = mysql_fetch_array($mysql_result);
					$AgentCode = strtoupper($row[0]);
				}

				print("<tr>");
				print("<td>$UserId</td>");
				print("<td></td>");
				print("<td>$UserName</td>");
				print("<td></td>");
				print("<td>$PassWord</td>");
				print("<td></td>");
				print("<td>$Access</td>");
				print("<td></td>");
				print("<td>$FullName</td>");
				print("<td></td>");
				print("<td>$AgentCode</td>");
				print("<td></td>");
				print("<td>$TimeStamp</td>");
				print("<td></td>");
				print("</tr>");
		}
		print("</Table>");
	}

	// assume listing of entire table
	else
	{
		//print("blah blah");
		$mysql_result1 = 0;
		$QueryString = "SELECT * FROM USER WHERE USER_ID >= '1' ORDER BY USER_ID ASC";
		$mysql_result1 = $CurrentSearch->list_records($QueryString);
		$ResultString = $CurrentSearch->getresult1();
		$ResultString2 = $CurrentSearch->getresult2();
		$ResultString3 = $CurrentSearch->getresult3();

		if ($mysql_result1 >= 1)
		{
			print("<table align='center' size='100%'>");
			print("<tr>");
			print("<TD ALIGN='LEFT' valign='top'><b><u><Font color='black'>User ID</font></u></b></TD>");
			print("\n");
			print("<TD ALIGN='LEFT' valign='top'></TD>");
			print("\n");
			print("<TD ALIGN='LEFT' valign='top'><b><u><Font color='black'>User Name</font></u></b></TD>");
			print("\n");
			print("<TD ALIGN='LEFT' valign='top'></TD>");
			print("\n");
			print("<TD ALIGN='LEFT' valign='top'><b><u><Font color='black'>User Password</font></u></b></TD>");
			print("\n");
			print("<TD ALIGN='LEFT' valign='top'></TD>");
			print("\n");
			print("<TD ALIGN='LEFT' valign='top'><b><u><Font color='black'>Access Level</font></u></b></TD>");
			print("\n");
			print("<TD ALIGN='LEFT' valign='top'></TD>");
			print("\n");
			print("<TD ALIGN='LEFT' valign='top'><b><u><Font color='black'>Full Name</font></u></b></TD>");
			print("\n");
			print("<TD ALIGN='LEFT' valign='top'></TD>");
			print("\n");
			print("<TD ALIGN='LEFT' valign='top'><b><u><Font color='black'>Agent Code</font></u></b></TD>");
			print("\n");
			print("<TD ALIGN='LEFT' valign='top'></TD>");
			print("\n");
			print("<TD ALIGN='LEFT' valign='top'><b><u><Font color='black'>Last Updated</font></u></b></TD>");
			print("\n");
			print("<TD ALIGN='LEFT' valign='top'></TD>");
			print("\n");
			print("</TR>");
			print("<tr>");
			print("</tr>");

			while ($row = mysql_fetch_array($mysql_result1))
			{
				$UserId 	= $row[0];
				$UserName 	= strtoupper($row[1]);
				$Access 	= $row[2];
				$PassWord 	= strtoupper($row[3]);
				$FullName 	= strtoupper($row[4]);
				$TimeStamp = $row[5];
				$AgentId = $row[6];

				$query = "SELECT AGENT_CODE FROM AGENT WHERE AGENT_ID= '$AgentId'";
				$mysql_result = mysql_query($query, $mysql_link);

				$numrows = mysql_num_rows($mysql_result);

				$AgentCode = "NONE";
				if ($numrows > 0)
				{
					$row = mysql_fetch_array($mysql_result);
					$AgentCode = strtoupper($row[0]);
				}

				print("<tr>");
				print("<td>$UserId</td>");
				print("<td></td>");
				print("<td>$UserName</td>");
				print("<td></td>");
				print("<td>$PassWord</td>");
				print("<td></td>");
				print("<td>$Access</td>");
				print("<td></td>");
				print("<td>$FullName</td>");
				print("<td></td>");
				print("<td>$AgentCode</td>");
				print("<td></td>");
				print("<td>$TimeStamp</td>");
				print("<td></td>");
				print("</tr>");
			}
			print("</Table>");

		}
		else
		{

			$QueryString = "";
			$ListUser = $CancelUser;
		}
	}


}


?>