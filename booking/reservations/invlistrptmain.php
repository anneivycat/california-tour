<HTML>

<HEAD>
<TITLE>Invoice List Report</TITLE>
<BODY bgcolor="#fffff0">

<SCRIPT LANGUAGE = "JavaScript">

	var strArgString = document.location.search;

		var strArgs = strArgString.substr(6);
		var strArray = strArgs.split(",");

		var strLevel = strArray[0];
		var strUserName = strArray[1];

</SCRIPT>

<?

include ("classes.php");
// connect to server

// open a connection to mysql

include ("../sqlfuncs.php");

$mysql_link = connect_to_db();
$mysql_result = select_db($mysql_link);



$SearchItem1 = new ReportSearchItem;
$SearchItem1->init(	"CITY",
					"CITY_ID",
					"CITY_DESC",
					"City:",
					"CityId",$mysql_link);

$SearchItem2 = new ReportSearchItem;
$SearchItem2->init(	"AGENT",
					"AGENT_ID",
					"AGENT_CODE",
					"Agent Code:",
					"AgentId",$mysql_link);

$SearchItem3 = new ReportSearchItem;
$SearchItem3->init(	"INVOICETYPE",
					"INVOICE_TYPE_ID",
					"INVOICE_TYPE",
					"Invoice Type:",
					"InvoiceTypeId",$mysql_link);
/*

$SearchItem3 = new ReportSearchItem;
$SearchItem3->init(	"INVOICE",
					"INVOICE_DATE",
					"INVOICE_DATE",
					"From Date:",
					"FromDate",$mysql_link);


$SearchItem4 = new ReportSearchItem;
$SearchItem4->init(	"INVOICE",
					"INVOICE_DATE",
					"INVOICE_DATE",
					"To Date:",
					"ToDate",$mysql_link);

*/
// date range as text entry

$SearchItem4 = new ReportTextBoxItem;
$SearchItem4->init("From :","FromDate");

$SearchItem5 = new ReportTextBoxItem;
$SearchItem5->init("To :","ToDate");


$SearchForm = new ReportSearchForm;
$SearchForm->init(	"invlistrptmain.php",
					"invlistrpt.php",
					"invlistrpttotext.php",
					"-- Invoice List Search --",
					$SearchItem1,$SearchItem2,$SearchItem3,$SearchItem4,$SearchItem5);

if ($ExitBtn)
{
	$SearchForm->mainexitcmd();
}

if ($PrintScreenBtn)
{
	if ((!$FromDate) AND (!$ToDate))
	{
		$SearchForm->showmain();
		$message = "<center><font face='arial' color='red'>Please choose dates in the form MM/DD/YY</font></center>";
	}
	else
	{

/* added 12/15/2003 */

		$FromDateLen = strlen($FromDate);
		$FromYear = substr($FromDate,$FromDateLen - 2,2);

		$ToDateLen = strlen($ToDate);
		$ToYear = substr($ToDate,$ToDateLen - 2,2);

		if ($FromYear != $ToYear)
		{
			$SearchForm->showmain();
			$message = "<center><font face='arial' color='red'>Please choose a range within the same year.</font></center>";
		}
		else
		{
			include ($SearchForm->printtoscreen);
		}
	}
}
else
{

	if ($PrintFileBtn)
	{
		include ($SearchForm->printtofile);
	}

	else
	{

		$SearchForm->showmain();
	}
}

	print("<br>");
	print($message);

?>