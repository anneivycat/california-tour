<? session_start(); ?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
   "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>California Tour: Book a Tour</title>
<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
<script type="text/javascript" src="/scripts/emails.js"></script>
<?php
include "../header.php";

//get the optional tour checkbox(s) info - either from previous session (because just got bounced back here b/c or error on the toursignuptotal) or from post
if ( $_POST['OPTIONALTOUR_ID_1'] != ''   ){
	$OPTIONALTOUR_ID_1 = $_POST['OPTIONALTOUR_ID_1'];
	$_SESSION['OPTIONALTOUR_ID_1'] = $OPTIONALTOUR_ID_1;
}
else {
	$OPTIONALTOUR_ID_1 = $_SESSION['OPTIONALTOUR_ID_1'];
}

//get the optional tour info:
//include ("../sqlfuncs.php");
$mysql_link = connect_to_db();
$mysql_result = select_db($mysql_link);
$query2 = "SELECT OPTIONALTOUR_NAME, OPTIONALTOUR_ID, OPTIONALTOUR_PRICE FROM  OPTIONALTOUR WHERE OPTIONALTOUR_ID = '$OPTIONALTOUR_ID_1'";
$mysql_result2 = mysql_query($query2,$mysql_link);
$row2 = mysql_fetch_array($mysql_result2);
$OPTIONALTOUR_ID = $row2['OPTIONALTOUR_ID'];
$OPTIONALTOUR_NAME = $row2['OPTIONALTOUR_NAME'];
$OPTIONALTOUR_PRICE = $row2['OPTIONALTOUR_PRICE'];
?>
		<h2>Book a Tour</h2>
		<h4><span style="color:#999;">Personal Information</span> &gt; Agree to Terms &gt; <span style="color:#999;">Payment Information</span> &gt; <span style="color:#999;">Tour Confirmation</span></h4>
<div id="sidebar" style="width:430px;">
<div class="sidebox">
<h4 style="text-align:center;">** Tour Reservation Summary **</h4>
<table width="100%"cellpadding="2" cellspacing="0" border="0">
<tr><td valign="top">Tour Name: </td><td colspan="3"><?php echo $TourName; ?></td></tr>
<?
if ( $OPTIONALTOUR_ID_1 != '' ) {
	echo "<tr><td valign='top'>Optional Tour: </td><td colspan='3'>" . $OPTIONALTOUR_NAME . "</td></tr>
	";
}
?>
<tr><td>Leaving From: </td><td colspan="3"><?php echo $City; ?> </td></tr>
<tr><td valign="top">Pick-up Location: </td><td colspan="3"><?php echo $PULocationLocation; ?></td></tr>
<tr><td>Departure Date: </td><td width="120"><?php echo date("F j, Y" , strtotime($DepartDate)); ?></td><td>Return Date: </td><td><?php echo date("F j, Y" , strtotime($ReturnDate)); ?></td></tr>
<?php
if($DepartDate != $ReturnDate) {
    $roommate_array  = array('Y' => 'Yes', 'N' => 'No');
    $occupancy_array = array('1' => 'Single', '2' => 'Double', '3' => 'Triple', '4' => 'Quad');
?>
<tr><td>Roommate: </td><td><?php if(@$_POST['Occupancy'] != 1) { echo 'Yes'; } else { echo 'No'; } ?></td><td>Occupancy: </td><td><?php echo @$occupancy_array[@$_POST['Occupancy']]; ?></td></tr> 
<?php }
if ($SchoolCode) {
?>
<tr><td>School Code: </td><td colspan="3"><?php echo $SchoolCode; ?></td></tr>
<?php 
} elseif ($AgentCode && ($AgentCode != "LAWEB") && ($AgentCode != "SFWEB") && ($AgentCode != "SDWEB") && ($AgentCode != "SBWEB")) {
?>
<tr><td>Agent Code:</td><td colspan="3"><?php echo $AgentCode; ?></td></tr>
<?php } ?>
<tr><td colspan="4" style="border-bottom:1px solid #999;">&nbsp;</td></tr>
<tr><td>Basic Tour Charge: </td><td align="right"><?php $TourPrice = sprintf("%5.2f", $TourPrice); echo "$" . $TourPrice . " USD"; ?></td><td colspan="2">&nbsp;</td></tr>
<?
if ( $OPTIONALTOUR_ID_1 != '0' ) {
	echo "<tr><td valign='top'>Optional Tour Charge:<br />($OPTIONALTOUR_NAME) </td><td align='right'>$$OPTIONALTOUR_PRICE USD</td></tr>";
	// friggin $TotalCharge is from where? god PHP's ancient global variables suck. Here I add to it:
	$TotalCharge += $OPTIONALTOUR_PRICE;
}
?>
<tr><td>Room Charge: </td><td align="right"><?php $RoomCharge = sprintf("%5.2f", $RoomCharge); echo "$" . $RoomCharge . " USD"; ?></td><td colspan="2">&nbsp;</td></tr>
<?php if (@$ManSuppCharge) { ?>
<tr><td>Manual Supp Charge: </td><td align="right"><?php $ManSuppCharge = sprintf("%5.2f", $ManSuppCharge); echo "$" . $$ManSuppCharge . " USD"; ?></td><td colspan="2">&nbsp;</td></tr>
<?php }
// check if valid school code (this is the same as the agent code)
$query2         = "SELECT AGENT_ID FROM AGENT WHERE AGENT_CODE = '$SchoolCode'";
$mysql_result2  = mysql_query($query2,$mysql_link);
$row2           = mysql_fetch_array($mysql_result2);

$SchoolSignupID = $row2[0];



//echo '$TourPrice'.$TourPrice.'<br />';
//echo '$RoomCharge'.$RoomCharge.'<br />';

$TotalCharge = $TourPrice + $RoomCharge;
//echo '$TotalCharge'.$TotalCharge.'<br />';


if ($SchoolSignupID) {
	if (!@$AgentProc) {
		$StudentDiscount = sprintf("%5.2f", $TourDiscount);
		$ThisTotalCharge = $TotalCharge - $StudentDiscount - $TourWebDiscount;
		echo "<tr><td>Student Discount:</td><td align='right'>";
		echo "-$" . $StudentDiscount . " USD";
		echo "</td><td colspan='2'>&nbsp;</td></tr>";
	} else {
		$ThisTotalCharge = $TotalCharge;
	}
} else {
	if (((!$AgentCode) || ($AgentCode == "LAWEB") || ($AgentCode == "SFWEB") || ($AgentCode == "SDWEB")  || ($AgentCode == "SBWEB")) && (!$SchoolCode)) {
	// -tt-
		$ThisTotalCharge = $TotalCharge - $TourWebDiscount;
   	} else {
?>
<tr><td>Agent Code:</td><td colspan="3"><?php echo $AgentCode; ?></td></tr>
<?php
   	}
}
if ((!$AgentCode) && ($SchoolCode)) {
   $AgentCode = $SchoolCode;
}

?>
<tr><td>Web Discount: </td><td align="right"><?php ; echo "-$" .sprintf("%5.2f", $TourWebDiscount)." USD"; ?></td><td colspan="2">&nbsp;</td></tr>

<tr><td>Price per person: </td><td align="right"><?php $PricePerPerson = sprintf("%5.2f", $ThisTotalCharge/$total_passenger); echo "$" . $PricePerPerson . " USD"; ?></td><td colspan="2">&nbsp;</td></tr>

<tr><td><b>Total Charge:</b> </td><td align="right">
<?php if ($ThisTotalCharge > 0) {
   $ThisTotalCharge = sprintf("%5.2f", $ThisTotalCharge);
} else {
   $ThisTotalCharge = sprintf("%5.2f", $TotalCharge);
}
echo "<b>$" . $ThisTotalCharge . " USD</b>";
?>
</td><td colspan="2">&nbsp;</td></tr>
</table>
</div>
</div>

		<form method='POST' action='toursignuptotal.php'>
		<div id="content">
			<h4>TERMS and CONDITIONS</h4>
			<p>No refund for cancellation within 7 days for coach tours. No refund on air tour package once it is confirmed. It is the passenger's responsibility to be at the departure point at the appointed time. No refunds will be given for a missed departure. Tours must be booked and paid in advance. Tours and prices are subject to availability at time of booking. All prices include taxes. Meals are not included, unless otherwise noted. Tour prices do not include tips for drivers and/or guides.</p>
			<p>AMERICAN EXPRESS, VISA, JCB and MASTERCARD are accepted. No surcharge for Credit Card Payment. California Tours reserves the right to alter itineraries and accommodations as necessary. Alterations are usually due to adverse weather conditions, government regulation or police action.</p>
			<p>California Seller or Travel Registration number for California Tours, LLC: 2065568-40 www.california-tour.com (877) 338-3883 tour@california-tour.com</p>
			<div align='center'><input type='checkbox' name='agree' value='yes'>I Agree<br /><br /><input type='submit' name='submit' value='submit' class="button"></div>
			<?php
			   foreach($_POST AS $key => $value) {
			?>
			<input type='hidden' name='<?php echo $key; ?>' value='<?php echo $value; ?>'>
			<?php
			   }
			?>
		</div><!--end content -->
		</form>
<br clear="right" /><br />
<?php
include "../footer.php";
?>
