<?php
 /**
  * validate the first name
  * @param string -- $first_name
  * @return bool
  */
 function checkFirstname($first_name) {
    if(strlen($first_name) == 0)
      return false;
    else
      return true;
    //return !preg_match ("/[^A-z]/", $first_name);
 } 
   		 
 /**
  * validate the last name
  * @param string -- $last_name
  * @return bool
  */
 function checkLastname($last_name) {
    if(strlen($last_name) == 0)
      return false;
    else
      return true;
    //return !preg_match ("/[^A-z]/", $last_name);
 } 

 /**
  * validate the email address
  * @param string -- $email
  * @return bool
  */
 function checkEmail($email) {
   $pattern = "/^[A-z0-9\._-]+" 
		    . "@"
		    . "[A-z0-9][A-z0-9-]*"
		    . "(\.[A-z0-9_-]+)*"
		    . "\.([A-z]{2,8})$/";
   return preg_match ($pattern, $email);
 } 

?>