<?php

   include_once '../sqlfuncs.php';
  
  /* $query = "select 
             S.SCHEDULEDTOUR_CODE, S.SCHEDULEDTOUR_PRICE,
	     
	     SB.SCHEDULEDTOURBOOK_ID, SB.FIRST_NAME, SB.LAST_NAME, SB.GENDER, SB.TOTALPAYTODLT, 
	     SB.TOTALPAYTOAGENT, SB.TOTALPAYCC, SB.OCCUPANCY, SB.SCHEDULEDTOURBOOK_ID,
	     
	     T.TOUR_NAME, T.TOUR_SUPP_SNG, T.TOUR_SUPP_DBL, T.TOUR_SUPP_TRP, 
	     T.TOUR_DISC1, T.TOUR_DISC2, T.TOUR_DISC3, T.TOUR_DISC4,  
	     T.TOUR_DISC5, T.TOUR_DISC6, T.TOUR_DISC7, T.TOUR_DISC8, 
	     
	     A.AGENT_CODE, A.AGENT_COMMISIONTYPE, 
	     
	     P.PULOCATION_LOCATION, P.PULOCATION_TIME, P.PULOCATION_CODE,
	     
	     OT.OPTIONALTOUR_PRICE 
	     
	     FROM SCHEDULEDTOUR AS S, SCHEDULEDTOURBOOK AS SB, TOUR AS T,
	     AGENT AS A, PULOCATION AS P, OPTIONALTOUR AS OT WHERE 
	     
	     S.TOUR_ID = T.TOUR_ID AND S.SCHEDULEDTOUR_ID = SB.SCHEDULEDTOUR_ID AND
	     SB.PULOCATION_ID = P.PULOCATION_ID AND SB.AGENT_ID = A.AGENT_ID ";*/
		 
	// ===========	 
	// 10/12/2008 - query update 
	//	above query limits data when SB.PULOCATION_ID contains 0 using left join to get full data list
	
	$query = "select S.SCHEDULEDTOUR_CODE, S.SCHEDULEDTOUR_PRICE, 
				SB.SCHEDULEDTOURBOOK_ID, SB.FIRST_NAME, SB.LAST_NAME, SB.GENDER, SB.TOTALPAYTODLT, 
				SB.TOTALPAYTOAGENT, SB.TOTALPAYCC, SB.OCCUPANCY, SB.SCHEDULEDTOURBOOK_ID,
				T.TOUR_NAME, T.TOUR_SUPP_SNG, T.TOUR_SUPP_DBL, T.TOUR_SUPP_TRP, 
				T.TOUR_DISC1, T.TOUR_DISC2, T.TOUR_DISC3, T.TOUR_DISC4,  
				T.TOUR_DISC5, T.TOUR_DISC6, T.TOUR_DISC7, T.TOUR_DISC8, 
				A.AGENT_CODE, A.AGENT_COMMISIONTYPE, 
				P.PULOCATION_LOCATION, P.PULOCATION_TIME, P.PULOCATION_CODE,
				OT.OPTIONALTOUR_PRICE 
				FROM SCHEDULEDTOUR AS S inner join SCHEDULEDTOURBOOK AS SB on S.SCHEDULEDTOUR_ID = SB.SCHEDULEDTOUR_ID 
				inner join TOUR AS T on S.TOUR_ID = T.TOUR_ID 
				inner join AGENT AS A on SB.AGENT_ID = A.AGENT_ID
				left join OPTIONALTOUR AS OT on SB.OPTIONALTOUR_ID = OT.OPTIONALTOUR_ID
				left join PULOCATION AS P on SB.PULOCATION_ID = P.PULOCATION_ID WHERE 1=1 ";
	     
   if(isset($_POST['ScheduledTourId']) &&  $_POST['ScheduledTourId'] != '') {
     $query .= " AND S.SCHEDULEDTOUR_ID = '" . $_POST['ScheduledTourId'] . "' ";
   }
   
   if(isset($_POST['AgentId']) &&  $_POST['AgentId'] != '') {
     $query .= " AND A.AGENT_ID = '" . $_POST['AgentId'] . "' ";
   }
   
   if(isset($_POST['PULocationId']) &&  $_POST['PULocationId'] != '') {
     $query .= " AND P.PULOCATION_ID = '" . $_POST['PULocationId'] . "' ";
   }
   
   $query .= " AND SB.SCHEDULEDTOURBOOK_TIMESTAMP > '2007-01-01 01:00:00' ";
   
   $query .= " ORDER BY SCHEDULEDTOUR_CODE, PULOCATION_CODE, AGENT_CODE, LAST_NAME, FIRST_NAME ";
   
 //  echo "<p>$query</p>";
   $mysql_link   = connect_to_db();
   $mysql_result = select_db($mysql_link);
   $result       = mysql_query($query) or die("error occures: " . mysql_error());
   
   $data_array = array();
   while($row = mysql_fetch_array($result)) {

     $data_array[$row['SCHEDULEDTOUR_CODE']]['TOUR_NAME'] = $row['TOUR_NAME'];
     
     $data_array[$row['SCHEDULEDTOUR_CODE']]['PULOCATION'][$row['PULOCATION_CODE']]['PULOCATION_LOCATION'] = $row['PULOCATION_LOCATION'];
     $data_array[$row['SCHEDULEDTOUR_CODE']]['PULOCATION'][$row['PULOCATION_CODE']]['PULOCATION_TIME'] = $row['PULOCATION_TIME'];
     
     $data_array[$row['SCHEDULEDTOUR_CODE']]['PULOCATION'][$row['PULOCATION_CODE']]['AGENT'][$row['AGENT_CODE']][$row['SCHEDULEDTOURBOOK_ID']]['AGENT_CODE'] = $row['AGENT_CODE'];
     $data_array[$row['SCHEDULEDTOUR_CODE']]['PULOCATION'][$row['PULOCATION_CODE']]['AGENT'][$row['AGENT_CODE']][$row['SCHEDULEDTOURBOOK_ID']]['LAST_NAME']  = $row['LAST_NAME'];
     $data_array[$row['SCHEDULEDTOUR_CODE']]['PULOCATION'][$row['PULOCATION_CODE']]['AGENT'][$row['AGENT_CODE']][$row['SCHEDULEDTOURBOOK_ID']]['FIRST_NAME'] = $row['FIRST_NAME'];
     $data_array[$row['SCHEDULEDTOUR_CODE']]['PULOCATION'][$row['PULOCATION_CODE']]['AGENT'][$row['AGENT_CODE']][$row['SCHEDULEDTOURBOOK_ID']]['GENDER']     = $row['GENDER'];
     
     $data_array[$row['SCHEDULEDTOUR_CODE']]['PULOCATION'][$row['PULOCATION_CODE']]['AGENT'][$row['AGENT_CODE']][$row['SCHEDULEDTOURBOOK_ID']]['TOTALPAYTODLT']   = $row['TOTALPAYTODLT'];
     $data_array[$row['SCHEDULEDTOUR_CODE']]['PULOCATION'][$row['PULOCATION_CODE']]['AGENT'][$row['AGENT_CODE']][$row['SCHEDULEDTOURBOOK_ID']]['TOTALPAYTOAGENT'] = $row['TOTALPAYTOAGENT'];
     $data_array[$row['SCHEDULEDTOUR_CODE']]['PULOCATION'][$row['PULOCATION_CODE']]['AGENT'][$row['AGENT_CODE']][$row['SCHEDULEDTOURBOOK_ID']]['TOTALPAYCC']      = $row['TOTALPAYCC'];
     
     $TotalCharge = $row['SCHEDULEDTOUR_PRICE'];
     
     if($row['OCCUPANCY'] == 1) 
       $TotalCharge +=  $row['TOUR_SUPP_SNG'];
     if($row['OCCUPANCY'] == 2) 
       $TotalCharge +=  $row['TOUR_SUPP_DBL'];
     if($row['OCCUPANCY'] == 3) 
       $TotalCharge +=  $row['TOUR_SUPP_TRP'];
       
     switch($row['AGENT_COMMISIONTYPE']) {
         case 1:
             $TotalCharge -= $row['TOUR_DISC1'];
         break;
	 
         case 2:
             $TotalCharge -= $row['TOUR_DISC2'];
         break;
       
	 case 3:
	     $TotalCharge -= $row['TOUR_DISC3'];
	 break;
	 
	 case 4:
	     $TotalCharge -= $row['TOUR_DISC4'];
	 break;
	 
	 case 5:
	     $TotalCharge -= $row['TOUR_DISC5'];
	 break;
	 
	 case 6:
	     $TotalCharge -= $row['TOUR_DISC6'];
	 break;
	 
	 case 7:
	     $TotalCharge -= $row['TOUR_DISC7'];
	 break;
	 
	 case 8:
	     $TotalCharge -= $row['TOUR_DISC8'];
	 break;
	 
	 default:
	 
	 break;
     }
        
     $WebDiscount = $TotalCharge - ($row['TOTALPAYTODLT'] + $row['TOTALPAYTOAGENT'] + $row['TOTALPAYCC']);
     $data_array[$row['SCHEDULEDTOUR_CODE']]['PULOCATION'][$row['PULOCATION_CODE']]['AGENT'][$row['AGENT_CODE']][$row['SCHEDULEDTOURBOOK_ID']]['TOTALCHARGE']  = $TotalCharge;
     $data_array[$row['SCHEDULEDTOUR_CODE']]['PULOCATION'][$row['PULOCATION_CODE']]['AGENT'][$row['AGENT_CODE']][$row['SCHEDULEDTOURBOOK_ID']]['WEBDISCOUNT']  = $WebDiscount;
     $data_array[$row['SCHEDULEDTOUR_CODE']]['PULOCATION'][$row['PULOCATION_CODE']]['AGENT'][$row['AGENT_CODE']][$row['SCHEDULEDTOURBOOK_ID']]['TOTALBALANCE'] = 0;

   }

   $sum_people_count = 0;
   $sum_total_charge = 0;
   $sum_web_discount = 0;
   $sum_pay_to_dlt   = 0;
   $sum_pay_to_agent = 0;
   $sum_pay_to_cc    = 0;
   $sum_balance      = 0;
   $no_record = true;
   foreach($data_array AS $s_key => $s_value) {
      $no_record = false;
      echo "<table width='900' style='font-size: 11px;font-family: arial, sans-serif;'>
              <tr><td colspan='9'><hr></td></tr>
              <tr><td colspan='9'><b>" . $s_key . " " .$s_value['TOUR_NAME'] . "</b></td></tr>
              <tr><td width='60'>Agents</td><td width='250'>Name(Last First)</td><td width='50'>Gender</td><td width='90'>Total Chg</td><td width='90'>Web Disc</td><td width='90'>Pd/DLT</td><td width='90'>Pd/AG</td><td width='90'>Pd/CC</td><td width='90'>Bal.</td></tr>
	      <tr><td colspan='9'><hr></td></tr>";

      $tour_people_count = 0;
      $tour_total_charge = 0;
      $tour_web_discount = 0;
      $tour_pay_to_dlt   = 0;
      $tour_pay_to_agent = 0;
      $tour_pay_to_cc    = 0;
      $tour_balance      = 0;
      foreach($s_value['PULOCATION'] AS $p_key => $p_value) {
	 $pu_people_count = 0;
         $pu_total_charge = 0;
         $pu_web_discount = 0;
         $pu_pay_to_dlt   = 0;
         $pu_pay_to_agent = 0;
         $pu_pay_to_cc    = 0;
         $pu_balance      = 0;     
	           
	 $count = 0;
	 echo "<tr><td colspan='9'><b>" . $p_key . " " .  $p_value['PULOCATION_LOCATION'] . "(" . $p_value['PULOCATION_TIME'] . ")" . "</b></td></tr>";
	 
	 foreach($p_value['AGENT'] AS $a_key => $a_value) {
	     foreach($a_value AS $sb_key => $sb_value) {
		 if(++$count <= 5) {
	           echo "<tr><td>" . $a_key . "</td><td>" . $sb_value['LAST_NAME'] . " " . $sb_value['FIRST_NAME'] . "</td><td>" . $sb_value['GENDER'] . "</td><td>" . sprintf("%5.2f",$sb_value['TOTALCHARGE']) . "</td><td>" . sprintf("%5.2f",$sb_value['WEBDISCOUNT']) . "</td><td>" . sprintf("%5.2f",$sb_value['TOTALPAYTODLT']) . "</td><td>" . sprintf("%5.2f",$sb_value['TOTALPAYTOAGENT']) . "</td><td>" . sprintf("%5.2f",$sb_value['TOTALPAYCC']) . "</td><td>" . sprintf("%5.2f",$sb_value['TOTALBALANCE']) . "</td></tr>\n";
		 }
		 else {
		   echo "<tr><td colspan='9'>&nbsp;</td></tr>\n";
		   echo "<tr><td>" . $a_key . "</td><td>" . $sb_value['LAST_NAME'] . " " . $sb_value['FIRST_NAME'] . "</td><td>" . $sb_value['GENDER'] . "</td><td>" . sprintf("%5.2f",$sb_value['TOTALCHARGE']) . "</td><td>" . sprintf("%5.2f",$sb_value['WEBDISCOUNT']) . "</td><td>" . sprintf("%5.2f",$sb_value['TOTALPAYTODLT']) . "</td><td>" . sprintf("%5.2f",$sb_value['TOTALPAYTOAGENT']) . "</td><td>" . sprintf("%5.2f",$sb_value['TOTALPAYCC']) . "</td><td>" . sprintf("%5.2f",$sb_value['TOTALBALANCE']) . "</td></tr>\n";
		   $count = 1;
		 }
		 $tour_people_count++;
	         $tour_total_charge += $sb_value['TOTALCHARGE'];
	         $tour_web_discount += $sb_value['WEBDISCOUNT'];
	         $tour_pay_to_dlt   += $sb_value['TOTALPAYTODLT'];
	         $tour_pay_to_agent += $sb_value['TOTALPAYTOAGENT'];
	         $tour_pay_to_cc    += $sb_value['TOTALPAYCC'];
	         $tour_balance      += $sb_value['TOTALBALANCE'];
		 
		 $pu_people_count++;
	         $pu_total_charge   += $sb_value['TOTALCHARGE'];
	         $pu_web_discount   += $sb_value['WEBDISCOUNT'];
	         $pu_pay_to_dlt     += $sb_value['TOTALPAYTODLT'];
	         $pu_pay_to_agent   += $sb_value['TOTALPAYTOAGENT'];
	         $pu_pay_to_cc      += $sb_value['TOTALPAYCC'];
	         $pu_balance        += $sb_value['TOTALBALANCE'];
	     }
	 }
	 echo "<tr><td>Total:</td><td></td><td>" . $pu_people_count . "</td><td>" . sprintf("%5.2f",$pu_total_charge) . "</td><td>" . sprintf("%5.2f",$pu_web_discount) . "</td><td>" . sprintf("%5.2f",$pu_pay_to_dlt) . "</td><td>" . sprintf("%5.2f",$pu_pay_to_agent) . "</td><td>" . sprintf("%5.2f",$pu_pay_to_cc) . "</td><td>" . sprintf("%5.2f",$pu_balance) . "</td></tr>";
      }
      echo "<tr><td><b>Total:</b></td><td></td><td><b>" . $tour_people_count . "</b></td><td><b>" . sprintf("%5.2f",$tour_total_charge) . "</b></td><td><b>" . sprintf("%5.2f",$tour_web_discount) . "</b></td><td><b>" . sprintf("%5.2f",$tour_pay_to_dlt) . "</b></td><td><b>" . sprintf("%5.2f",$tour_pay_to_agent) . "</b></td><td><b>" . sprintf("%5.2f",$tour_pay_to_cc) . "</b></td><td><b>" . sprintf("%5.2f",$tour_balance) . "</b></td></tr>";
           
      echo  "</table><br><br>";
      
      $sum_people_count += $tour_people_count;
      $sum_total_charge += $tour_total_charge;
      $sum_web_discount += $tour_web_discount;
      $sum_pay_to_dlt   += $tour_pay_to_dlt;
      $sum_pay_to_agent += $tour_pay_to_agent;
      $sum_pay_to_cc    += $tour_pay_to_cc;
      $sum_balance      += $tour_balance;
   
   }
   
  if($no_record) {
     echo "<div align='center'>No Match is found</div>";
  }
  else {
     echo "<table width='900' style='font-size: 11px;font-family: arial, sans-serif;'>";
     echo "<tr><td colspan='2' width='310'><b>GRAND
           TOTAL:</b></td><td width='50'><b>$sum_people_count</b></td><td width='90'><b>" . sprintf("%5.2f", (int)$sum_total_charge) . "</b></td><td width='90'><b>" . sprintf("%5.2f",(int)$sum_web_discount) . "</b></td><td width='90'><b>" . sprintf("%5.2f",(int)$sum_pay_to_dlt) . "</b></td><td width='90'><b>" . sprintf("%5.2f",(int)$sum_pay_to_agent) . "</b></td><td width='90'><b>" . sprintf("%5.2f",(int)$sum_pay_to_cc) . "</b></td><td width='90'><b>" . sprintf("%5.2f",(int)$sum_balance) . "</b></td></tr>"; 
     echo "</table>";
  }

?>