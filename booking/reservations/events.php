<html>

<head>
<title>Today's Events</title>
</head>

<body bgcolor="white" text="black">


<SCRIPT LANGUAGE="JavaScript1.2">

/*
	Copyright 2002
	Andrew George Stanton
	Stanton Web Applications
	649 Jones Street, #407
	San Francisco, CA 94102
	tel: (415)-929-8033, ext. 407
	fax: (415)-441-8033
	email: andy@stantonweb.com
	web: http://www.stantonweb.com

	Permission is given to use this JavaScript code
	for any purpose with the sole proviso that
	the address and copyright notice indicated above
	be retained in the source code
*/

	var dtToday = new Date();
	document.write("<center><b><font size=+1>Today is " + dtToday);
	document.write("</font></b></center>");

	document.write("<table cols=2>");
	document.write("<tr><td align=center width=50%>");

	document.write("<table ALIGN='CENTER' BORDER='1' CELLSPACING='1' WIDTH='50%' HEIGHT='50%'>");
	document.write("<th COLSPAN='7'><font SIZE='+2'>");

//	**************************************************
//	 Display a calendar based on the current date
//	 or date inserted as a variable
//	 *************************************************

//	 first, we we have to get create a function to display the
//	  the name of the current month

//	 We will want to be able to determine the month name
//	 for a given month.  So, let's create a function that
//	 takes the numeric value of a month and displays the name of
//	 the month (i.e, GetMonthName(8) would output August)

	function getPrevMonth(dtCurrentDate)
	{
		var dtPrevMonth;
		var iCurrentYear = dtCurrentDate.getYear();
		var iCurrentMonth = dtCurrentDate.getMonth();
		var iCurrentDate = dtCurrentDate.getDate();
		if (iCurrentMonth == 0)
		{
			dtPrevMonth = new Date(iCurrentYear-1,11,iCurrentDate);
		}
		else
		{
			dtPrevMonth = new Date(iCurrentYear,iCurrentMonth - 1,iCurrentDate);
		}
		return (dtPrevMonth);
	}

	function getNextMonth(dtCurrentDate)
	{
		var dtNextMonth;
		var iCurrentYear = dtCurrentDate.getYear();
		var iCurrentMonth = dtCurrentDate.getMonth();
		var iCurrentDate = dtCurrentDate.getDate();
		if (iCurrentMonth == 11)
		{
			dtNextMonth = new Date(iCurrentYear+1,0,iCurrentDate);
		}
		else
		{
			dtNextMonth = new Date(iCurrentYear,iCurrentMonth + 1,iCurrentDate);
		}
		return (dtNextMonth);
	}

	function getPrevYear(dtCurrentDate)
	{
		var dtPrevYear;
		var iCurrentYear = dtCurrentDate.getYear();
		var iCurrentMonth = dtCurrentDate.getMonth();
		var iCurrentDate = dtCurrentDate.getDate();
		dtPrevYear = new Date(iCurrentYear-1,iCurrentMonth,iCurrentDate);
		return (dtPrevYear);
	}

	function getNextYear(dtCurrentDate)
	{
		var dtNextYear;
		var iCurrentYear = dtCurrentDate.getYear();
		var iCurrentMonth = dtCurrentDate.getMonth();
		var iCurrentDate = dtCurrentDate.getDate();
		dtNextYear = new Date(iCurrentYear+1,iCurrentMonth,iCurrentDate);
		return (dtNextYear);
	}


	function getMonthName(iMonth)
	{
		var strMonthName = "INVALID MONTH";
		if (iMonth == 1) strMonthName = "January";
		if (iMonth == 2) strMonthName = "February";
		if (iMonth == 3) strMonthName = "March";
		if (iMonth == 4) strMonthName = "April";
		if (iMonth == 5) strMonthName = "May";
		if (iMonth == 6) strMonthName = "June";
		if (iMonth == 7) strMonthName = "July";
		if (iMonth == 8) strMonthName = "August";
		if (iMonth == 9) strMonthName = "September";
		if (iMonth == 10) strMonthName = "October";
		if (iMonth == 11) strMonthName = "November";
		if (iMonth == 12) strMonthName = "December";

		return (strMonthName);
	}

	function getDaysInMonth(dtcurrentdate)
	{
		var iDaysInMonth = 31;
		if (getMonthName(dtcurrentdate.getMonth()+1) == "January") iDaysInMonth = 31;
		if (getMonthName(dtcurrentdate.getMonth()+1) == "March") iDaysInMonth = 31;
		if (getMonthName(dtcurrentdate.getMonth()+1) == "April") iDaysInMonth = 30;
		if (getMonthName(dtcurrentdate.getMonth()+1) == "May") iDaysInMonth = 31;
		if (getMonthName(dtcurrentdate.getMonth()+1) == "June") iDaysInMonth = 30;
		if (getMonthName(dtcurrentdate.getMonth()+1) == "July") iDaysInMonth = 31;
		if (getMonthName(dtcurrentdate.getMonth()+1) == "August") iDaysInMonth = 31;
		if (getMonthName(dtcurrentdate.getMonth()+1) == "September") iDaysInMonth = 30;
		if (getMonthName(dtcurrentdate.getMonth()+1) == "October") iDaysInMonth = 31;
		if (getMonthName(dtcurrentdate.getMonth()+1) == "November") iDaysInMonth = 30;
		if (getMonthName(dtcurrentdate.getMonth()+1) == "December") iDaysInMonth = 31;

		if (getMonthName(dtcurrentdate.getMonth()+1) == "February")
		{
			if (dtcurrentdate.getYear()%4 == 0)
			{
				iDaysInMonth = 29;
			}
			else
			{
				iDaysInMonth = 28;
			}
		}
		return (iDaysInMonth);
	}

	function getFirstWeekDay(dtCurrentDate)
	{
		var dtLocalCurrentDate;
		var iFirstWeekDay;
		var iCurrentYear = dtCurrentDate.getYear();
		var iCurrentMonth = dtCurrentDate.getMonth();
		var iCurrentDate = dtCurrentDate.getDate();

		dtLocalCurrentDate = new Date(iCurrentYear,iCurrentMonth,1);
		iFirstWeekDay = dtLocalCurrentDate.getDay()+1;
		return (iFirstWeekDay);
	}

// check date argument - if not zero, use as the current date

	var strSearch = document.location.search;

	// ?date= .. start from position 6..

	var strDate = strSearch.substr(6);

// DEBUG	document.write(strDate.length);

	var dtCurrentDate;

	if (strDate.length > 0)
	{
		var strMonth = "";
		var strDay = "";
		var strYear = "";
		var strRest = "";
		var i=0;

		var strYear = strDate.substr(0,4);

		var strTemp = strYear;
		var blSplit = "FALSE";
		while (i<strTemp.length)
		{
			if (strTemp.substr(i,1) == ",")
			{
				strYear = strDate.substr(0,2);
				strRest = strDate.substr(3);
				blSplit = "TRUE";
			}
			i++;
		}
		if (blSplit == "FALSE")
		{
			strRest = strDate.substr(5);
		}

// DEBUG		document.write(strRest);
// DEBUG		document.write(" ");
// DEBUG		document.write(strRest.length);


		var i=0;
		var iLength = strRest.length;

		while (i < iLength)
		{
			if (strRest.substr(i,1) == ",")
			{
				strMonth = strRest.substr(0,i);
				strDay = strRest.substr(i+1);
			}
			i++;
		}
// DEBUG		document.write(strYear);
// DEBUG		document.write(" ");
// DEBUG		document.write(strMonth);
// DEBUG		document.write(" ");
// DEBUG		document.write(strDay);
		var iYear = parseInt(strYear);
		var iMonth = parseInt(strMonth);
		var iDay = parseInt(strDay);
// DEBUG		document.write(iYear);
		dtCurrentDate = new Date(iYear,iMonth,iDay);
	}
	else
	{
		dtCurrentDate 	= new Date();
// DEBUG		document.write(dtCurrentDate);
	}


	var iMonth 		= dtCurrentDate.getMonth()+1;
	var strMonth 		= getMonthName(iMonth);
	var iYear 		= dtCurrentDate.getYear();


// now, write the date as a header to the table for the current date

	document.write(strMonth);
	if (iYear <= 99)
	{
		document.write(", " + (iYear+1900));
	}
	else
	{
		document.write(", " + iYear);
	}
	document.write("</font></th>");

//	 Now, we need to display a calender.  At most, a calendar can
//	 have 6 weeks.  Since a week is 7 days,  we will show, at most
//	 42 calendar cells.  We will use a TABLE to generate this calendar

//	 we will create an array that will store the 42 possible days of the
//	 month.

	var aCalendarDays = new Array(42);

//	into aCalendarDays will will place the days of the current month.
//	we will use the DatePart function to determine
//	when the first day of the month is.

	var iFirstWeekDay = getFirstWeekDay(dtCurrentDate);

// DEBUG	document.write(iFirstWeekDay);

//	Now, we want to loop from 1 to the number of days in the
//	 current month, populating the array aCalendarDays

	var iDaysInMonth = getDaysInMonth(dtCurrentDate);

	for (var iLoop = 1; iLoop <= iDaysInMonth; iLoop++)
	{
		aCalendarDays[iLoop + iFirstWeekDay -1] = iLoop;
// DEBUG		document.write(aCalendarDays[iLoop]);
	}

//	 now that we have our populated array, we need to display
//	 the array in calendar form.  We will create a table of 7 columns,
//	 one for each day of the week.  The number of rows we will use
//	 will be 6, the total number of possible rows, minus 42 (the upper
//	 bound of the aCalendarDays array) minus the last array position
//	 used (iFirstWeekDay + iDaysInMonth) divided by seven, the number
//	 of days in the week! simple eh!!

	var iColumns;
	var iRows;

	iColumns = 7;
	iRows = 6 - parseInt((42 - (iFirstWeekDay + iDaysInMonth)) / 7);

// DEBUG	document.write(iColumns);
// DEBUG	document.write(iRows);


//	Now, loop through 1 through 7, for the days of the week
//	as a header for each column

	document.write("<TR>");
	for (var iDayBanner = 1; iDayBanner <= iColumns; iDayBanner++)
	{
		document.write("<TD VALIGN='TOP' ALIGN='CENTER' WIDTH='7%'>");
		if (iDayBanner == 1) document.write("<b>Sun</b>");
		if (iDayBanner == 2) document.write("<b>Mon</b>");
		if (iDayBanner == 3) document.write("<b>Tue</b>");
		if (iDayBanner == 4) document.write("<b>Wed</b>");
		if (iDayBanner == 5) document.write("<b>Thu</b>");
		if (iDayBanner == 6) document.write("<b>Fri</b>");
		if (iDayBanner == 7) document.write("<b>Sat</b>");
		document.write("</TD>");
	}

	document.write("</TR>");

//	Now, loop through 1 through iRows, then 1 through iColumns

	var strCalendarDate;
	var iCalendarDay;
	var dtCalendarDate;

	for (var iRowsLoop = 1; iRowsLoop <= iRows; iRowsLoop++)
	{
//		 create a new row
		document.write("<TR>");
		for (var iColumnsLoop = 1; iColumnsLoop <= iColumns; iColumnsLoop++)
		{
//			create a new column
//			if there is a day there, display it, else black out the call
			if (aCalendarDays[(iRowsLoop-1)*iColumns + iColumnsLoop] > 0)
			{
//	display the date
				document.write("<TD VALIGN= 'TOP' ALIGN='RIGHT' WIDTH='7%'>");
				iCalendarDay = aCalendarDays[(iRowsLoop-1)*iColumns + iColumnsLoop];
//				strCalendarDate = dbCurrentDate.(Month(dbCurrentDate)) & "/" & CStr(iCalendarDay) & "/" & _
//						Cstr(Year(dbCurrentDate));
//				dtCalendarDate = CDate(strCalendarDate);
				document.write("<a href='events.php?date=");
				document.write(dtCurrentDate.getYear());
				document.write(",");
				document.write(dtCurrentDate.getMonth());
				document.write(",");
				document.write(iCalendarDay);
				document.write("'>");
//				Response.Write dtCalendarDate & "'"
//				Response.Write ">" & iCalendarDay & "</a>"
				document.write(iCalendarDay);
				document.write("</TD>");
			}
			else
			{
//				black out the cell
				document.write("<TD BGCOLOR='BLACK'></TD>");
			}
		}
//		close the row
		document.write("</TR>");
	}

	document.write("</table><center>");
	document.write("<br>");
//	document.write(dtCurrentDate);
//	document.write("<br>");

//	var dtPrevMonth;
//	var dtPrevPrevMonth;
//	var dtPrevMonth = getPrevMonth(dtCurrentDate);
//	var dtPrevPrevMonth = getPrevMonth(dtPrevMonth);

//	document.write(dtPrevMonth);
//	document.write("<br>");
//	document.write(dtPrevPrevMonth);

	document.write("<table><tr><td>");

	document.write("<a href='events.php?date=");

	var dtLastYear = getPrevYear(dtCurrentDate);

	document.write(dtLastYear.getYear());
	document.write(",");
	document.write(dtLastYear.getMonth());
	document.write(",");
	document.write(dtLastYear.getDate());
	document.write("'>Prev Yr</a>|");
//  	document.write("<input TYPE='SUBMIT' VALUE='Prev Yr'></form></td><td>");

// DEBUG	document.write(dtPrevMonth);

//	document.write("<form METHOD='POST' ACTION='events.php?date=");
	document.write("<a href='events.php?date=");

	var dtPrevMonth = getPrevMonth(dtCurrentDate);

	document.write(dtPrevMonth.getYear());
	document.write(",");
	document.write(dtPrevMonth.getMonth());
	document.write(",");
	document.write(dtPrevMonth.getDate());
	document.write("'>Prev Mo</a>|");
//  	document.write("<input TYPE='SUBMIT' VALUE='Prev Mo'></form></td><td>");
//
//	document.write("<form METHOD='POST' ACTION='events.php'>");
	document.write("<a href='events.php'>Today</a>|");

//  	document.write("<input TYPE='SUBMIT' VALUE='Today'></form></td><td>");

//

//	document.write("<form METHOD='POST' ACTION='events.php?date=");
	document.write("<a href='events.php?date=");

	var dtNextMonth = getNextMonth(dtCurrentDate);

	document.write(dtNextMonth.getYear());
	document.write(",");
	document.write(dtNextMonth.getMonth());
	document.write(",");
	document.write(dtNextMonth.getDate());
	document.write("'>Next Mo</a>|");
//  	document.write("<input TYPE='SUBMIT' VALUE='Next Mo'></form></td></td><td>");

//	document.write("<form METHOD='POST' ACTION='events.php?date=");
	document.write("<a href='events.php?date=");

	var dtNextYear = getNextYear(dtCurrentDate);

	document.write(dtNextYear.getYear());
	document.write(",");
	document.write(dtNextYear.getMonth());
	document.write(",");
	document.write(dtNextYear.getDate());
	document.write("'>Next Yr</a>");

</SCRIPT>

<?
	print("<br><br><center><a href=\"editevent.php?date=$date\"><font color=\"black\">Create Entry</font></a></center>\n");
?>


	</td></tr></table></center>


	</td>



<?
// this lists the activities or events for the currently selected date

	print("<td align=\"left\" width=\"50%\" valign=\"top\">");


	if ($date)
	{

		$dateArray = explode(",",$date);
		$year = $dateArray[0];
		$month = $dateArray[1];
		$day = $dateArray[2];

		if ($day < "10")
		{
			$day = sprintf("0%s",$day);
		}
		if ($month =="12")
		{
			$month = "1";
		}
		else
		{
			$month = $month + 1;
		}

		if ($month < "10")
		{
			$month = sprintf("0%s",$month);
		}

	}
	else
	{
		$day = date("d");
		$month= date("m");
		$year = date("Y");


	}
		$searchDate = sprintf("%s/%s/%s",$month,$day,$year);

		print("<b>");
		print($searchDate);
		print("</b>\n");
		print("<br><br>\n");

		include ("../sqlfuncs.php");

		$mysql_link = connect_to_db();
		$mysql_result = select_db($mysql_link);

// scheduled tours

	$searchDatetemp = ereg_replace("2003","-3",$searchDate);

	$searchDate1 = ereg_replace("0","",$searchDatetemp);
	$searchDate1 = ereg_replace("-3","03",$searchDate1);

	$searchDate2 = ereg_replace("/","-",$searchDate1);
//	print($searchDate1);
//	print($searchDate2);



	$query = "SELECT A.SCHEDULEDTOUR_ID, A.SCHEDULEDTOUR_CODE, A.SCHEDULEDTOUR_DEPART_DATE FROM SCHEDULEDTOUR A WHERE A.SCHEDULEDTOUR_DEPART_DATE  = '$searchDate' or A.SCHEDULEDTOUR_DEPART_DATE  = '$searchDate1' or A.SCHEDULEDTOUR_DEPART_DATE  = '$searchDate2' order by A.SCHEDULEDTOUR_CODE asc";

	$mysql_result = mysql_query($query, $mysql_link);

	$numrows = mysql_num_rows($mysql_result);



	if ($numrows > 0)
	{
		print("<b>Scheduled Tours:</b><br><table>\n");

	}

	while ($row = mysql_fetch_array($mysql_result))

	{

		$SchedTourId	= $row[0];
		$TourCode 		= $row[1];

		$query_pu = "SELECT A.PULOCATION_ID from SCHEDULEDTOURBOOK A WHERE A.SCHEDULEDTOUR_ID  = '$SchedTourId'";

	//	print($query_pu);
		$mysql_result_pu = mysql_query($query_pu, $mysql_link);

		$numrows_pu = mysql_num_rows($mysql_result_pu);
		if ($numrows_pu > 0)
		{
			$row_pu = mysql_fetch_array($mysql_result_pu);
			$PULocationId = $row_pu[0];
			$query_time = "SELECT A.PULOCATION_TIME from PULOCATION A WHERE A.PULOCATION_ID  = '$PULocationId'";
			$mysql_result_time = mysql_query($query_time, $mysql_link);
			$numrows_time = mysql_num_rows($mysql_result_time);
			if ($numrows_time > 0)
			{
				$row_time = mysql_fetch_array($mysql_result_time);
				$PULocationTime = $row_time[0];
			}
		}
		print("<tr>\n");
		print("<td align=\"left\">\n");
		print("<font color=\"red\">$PULocationTime</font>\n");
		print("</td>\n");
		print("<td align=\"left\">\n");
		print("<font color=\"blue\">$TourCode</font>\n");
		print("</td>\n");
		print("</tr>\n");
	}

	if ($numrows > 0)
	{
		print("</table>\n");
	}



// appointments/events

	$query = "SELECT A.event_id,A.event_name, A.event_hours,A.event_date FROM EVENTS A WHERE A.event_date = '$searchDate' order by A.event_hours asc";

	$mysql_result = mysql_query($query, $mysql_link);

	$numrows = mysql_num_rows($mysql_result);


	if ($numrows > 0)
	{
		print("<b>Appointments:</b><br><table>\n");

	}

	while ($row = mysql_fetch_array($mysql_result))

	{
		$EventId 		= $row[0];
		$EventName 		= $row[1];
		$EventHours 	= $row[2];


		print("<tr>\n");
		print("<td align=\"left\">\n");
		print("<font color=\"red\">$EventHours</font>\n");
		print("</td>\n");
		print("<td align=\"left\">\n");
		print("<font color=\"blue\">$EventName</font>\n");
		print("</td>\n");
		print("<td align=\"left\">\n");
		print("<a href=\"editevent.php?eventid=$EventId,$date\"><font color=\"black\">Edit</font></a>\n");
		print("</td>\n");
		print("<td align=\"left\">\n");
		print("<a href=\"delevent.php?eventid=$EventId,$date\"><font color=\"black\">Delete</font></a>\n");
		print("</td>\n");

		print("</tr>\n");
	}

	if ($numrows > 0)
	{
		print("</table>\n");
	}


	print("</td></tr></table>\n");


?>

<center><a href="createvent.php"></body>
</html>
