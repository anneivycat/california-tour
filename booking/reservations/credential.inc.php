<?php

define('UC1', '34thar [gg8y kqcjr#^ hve84t78;34tvm%#()@*@*[');
define('UC2', 'oioiuLIUY R#(&#$KJN V*&GY R)$(R&*)()*& KJ KJLKJJW*#&kljfoij');

class UserCredential {

    /* The functions in this class create, check, and unpack a user credential.

       The user credential is the urlencoded form of a string like:
	   checksum(|urlEncodedKeyword=urlEncodedValue)+
	Each keyword and value is separately urlencoded; then the entire string is urlencoded.

	The (decoded) keyword must be one of:
	    Access	decoded value is an integer
	    Refresh	decoded value is a string (hash of a time() and a secret)
	    SessionId	decoded value is a string
	    UserName	decoded value is a string

	Note that backslash-escapes such as "\n" within strings will not be interpreted. To 
	set a value including an escape sequence, use something like 
	    "'This string\nis two lines long.\n'"

	If a keyword occurs multiple times, only the last value will be retained.

	USAGE:

	* in login page:

	    After checking supplied password and retrieving user's access rights:

	    // require_once("credential.inc.php");
	    // 
	    // $loginTime = time();
	    // $uc = new UserCredential();
	    // $uc->addItem('UserName',  $UserName);
	    // $uc->addItem('Access',    $Access);
	    // $uc->addItem('SessionId', session_id());
	    // $uc->addItem('Refresh',   $LoginTime);
	    // $uc->regenerate();

	    // setcookie('UserCredential', $uc1->getRawURLEncoded());
	    // $_SESSION['loggedIn'] = $loggedIn = true;
	    // $_SESSION['ucRefreshTime'] = $loginTime;

	* in page requiring authentication:

	    // require_once("credential.inc.php");
	    // 
	    // if(@$_REQUEST['args']) {
	    //   If the page currently gets username and access level via "GET" parameter 
	    //   "?args=[menu,]access,name" (aka document.location.search to JavaScript):
	    //   TO CHECK THAT OLD-STYLE USER & ACCESS MATCH NEW-STYLE, EITHER
	    //     list($requestAccess, $requestUserName) = split(",", $_REQUEST['args']);
	    //   OR
	    //     list($ignoreMenu, $requestAccess, $requestUserName) = split(",", $_REQUEST['args']);
	    //   THEN
	    //     $checkItems = array('UserName' => $requestUserName, 'Access' => $requestAccess);
	    // } else {
	    //     $checkItems = NULL;
	    // }
	    // 
	    // if(($s = checkLogin($wantItems = array('UserName', 'Access'), $checkItems)) === false) {
	    //     // checkLogin() shouldn't return unless successful - it should emit a redirect and die()
	    //     die("checkLogin() failed.");
	    // } else {
	    //     // to make old-style GET "?args=[menu,]accessLevel,userName" OPTIONAL
	    //     $UserName = $s['UserName'];
	    //     $Access   = $s['Access'];
	    // }

	* in logout page

	    // require_once("credential.inc.php");
	    //
	    // if(@$_COOKIE['UserCredential']) {
	    //     $uc = new UserCredential($_COOKIE['UserCredential']);
	    // } else {
	    //     $uc = NULL;
	    // }
	    // clearLogin("Logout succeeded.", "/", 1, $uc);

     */

    var $itemString = NULL;	// keywords and values are urlencoded
    var $itemArray = NULL;	// keywords and values are decoded
    var $sum = NULL;

    var $keywordTypes = array(
	'SessionId'	=> 'string',
	'Refresh'	=> 'string',
	'Access'	=> 'integer',
	'UserName'	=> 'string'
    );


    function UserCredential($raw = NULL) {
	@session_start();

	if(!is_null($raw)) {
	    $explode_decoded = explode("|", rawurldecode($raw), 2);
	    $localSum        = $explode_decoded[0];
	    $localItemString = $explode_decoded[1];

	    foreach(explode("|", $localItemString) as $item) {
		$explode_item = explode("=", $item, 2);
		$encodedKeyword = $explode_item[0];
		$encodedValue   = $explode_item[1];

		$keyword = rawurldecode($encodedKeyword);
		$value   = rawurldecode($encodedValue);
		$this->addItem($keyword, $value);
	    }

	    // addItem() wipes out sum and itemString, so set 
	    // them _after_ the call to addItem().
	    $this->sum        = $localSum;
	    $this->itemString = $localItemString;

	    /* Note: At this point, itemString and itemArray may not
	       correspond, as only specific keywords are placed into
	       itemArray. However, to check validity, the original
	       itemString and sum must be preserved.
	     */

	} else {
	    $this->itemArray = $this->itemString = $this->sum = NULL;
	}
    }

    function valid() {
	$valid = true;
	$this->errorString = "";

	if(is_null($this->sum)) {
	    $this->regenerate();
	} else {
	    @session_start();

	    $want = array_keys($this->keywordTypes);
	    foreach($want as $keyword) {
		if($this->includesAll($keyword)) {
		    $haveKeyword[$keyword] = true;
		} else {
		    $valid = $haveKeyword[$keyword] = false;
		    $this->addError("User credential invalid: missing $keyword.");
		}
	    }

	    $have = $this->itemArray['SessionId'];
	    $want = session_id();
	    if($have && $have !== $want) {
		$valid = false;
		$this->addError("User credential invalid: bad session id ($have) != ($want)");
	    }

	    $have = $this->itemArray['Refresh'];
	    $want = $this->hashTime($_SESSION['ucRefreshTime']);
	    if(@$have && $have !== $want) {
		$valid = false;
		$this->addError("User credential invalid: bad refresh value ($have) != ($want)");
	    }

	    $have = $this->sum;
	    $want = $this->hashItemString($this->getItemString());
	    if($have !== $want) {
		$valid = false;
		$this->addError("User credential invalid: bad checksum ($have) != ($want)");
	    }

	}
	return $valid;
    }

    function regenerate () {
	$this->sum = $this->itemString = NULL;
	$this->getSum();
	$this->getItemString();
    }

    function getRawURLEncoded() {
	$this->regenerate();
	return rawurlencode($this->getSum() . "|" . $this->getItemString());
    }

    function getFullyDecoded() {
	$this->regenerate();
	return rawurldecode($this->getSum() . "|" . $this->getItemString());
    }

    function getSum() {
	if(is_null($this->sum)) {
	    $this->sum = $this->hashItemString($this->getItemString());
	}
	return $this->sum;
    }

    function hashTime($time) {
	return md5($time . UC2);
    }

    function hashItemString($s) {
	return '0x' . sha1($s . session_id() . UC1); 
    }

    function getItemString () {
	if(is_null($this->itemString)) {
	    ksort($this->itemArray);
	    foreach($this->itemArray as $keyword => $value) {
		$this->itemString .=
		    ((@strlen($this->itemString) > 0) ? "|" : "")
		    . rawurlencode($keyword) . "=" . rawurlencode($value);
	    }
	}
	return $this->itemString;
    }

    function addItem($keyword, $value) {
	if($keyword === 'Refresh') {
	    // Refresh is a special case - stored in session
	    $_SESSION['ucRefreshTime'] = (integer) $value;
	    $value = $this->hashTime($_SESSION['ucRefreshTime']);
	}
	switch ($this->keywordTypes[$keyword]) {
	    case 'integer':
		$this->itemArray[$keyword] = (integer) $value;
	    break;
	    case 'string':
		$this->itemArray[$keyword] = (string) $value;
	    break;
	}
	$this->itemString = $this->sum = NULL;
    }

    function getItemArray() {
	if(is_null($this->sum)) {
	    $this->getSum();
	}
	if($this->valid() === false) {
	    return false;
	} else {
	    return $this->itemArray;
	}
    }

    function includesAll($checkList) {
	if(!is_array($checkList)) {
	    $checkList = array($checkList);
	}

	$haveAll = true;
	$haveItems = $this->itemArray;
	foreach($checkList as $checkItem) {
	    if(!array_key_exists($checkItem, $haveItems)) {
		$haveAll = false;
		$this->addError("User credential invalid: missing $checkItem.");

	    }
	    $haveAll = ($haveAll && array_key_exists($checkItem, $haveItems));
	}
	return $haveAll;
    }

    var $errorString;

    function addError($s) {
	$this->errorString .= "$s \n";
    }

    function clear() {
	$this->itemString  = NULL; 
	$this->itemArray   = NULL; 
	$this->sum         = NULL; 
	$this->errorString = NULL;

	unset($_SESSION['ucRefreshTime']);
	unset($_COOKIE['UserCredential']);

	setcookie('UserCredential');
    }
}

function checkLogin($wantItems, $checkItems = NULL, $loginFailurePage = "/reservations") {
/*
    $wantItems:  array of items caller needs from credential
    #            (numeric-keyed array - keys in uc->itemArray are values here
    # REQUIRED - so if UserCredential changes are not backward compatible, 
    #            caller will fail if out of sync

    $checkItems: array of items whose values caller needs matched in credential
    #            (string-keyed array  - keys   in uc->itemArray are keys here
    #                                 - values in uc->itemArray must match values here
    # OPTIONAL

    $loginFailurePage:  if login fails, browser will be redirected to this page; 
    #                   function ** DOES NOT RETURN TO CALLER **
    # OPTIONAL

    # returns & side effects
    #       if programming error or other serious problem
    #           may return false
    #       if login is successful
    #           sets session 'loggedIn' and 'ucRefreshTime'
    #           sends cookie 'UserCredential' to user
    #           returns array of items requested and checked by caller
    #                   (keys from both $wantItems and $checkItems)
    #       if login is unsuccessful
    #           ** DOES NOT RETURN TO CALLER! **
    #           calls uc->clear():
    #               unsets $_SESSION variable 'ucRefreshTime'
    #               unsets $_COOKIE  variable 'UserCredential'
    #               clears member properties
    #           sets $_SESSION variable 'loggedIn' to false
    #           emits HTTP to clear 'UserCredential' cookie
    #           emits HTTP to redirect browser to next page (presumably to try again)
 */

    @session_start();
    $debug = true;
    $debug = false;
    $HTTP_HOST = $_SERVER['HTTP_HOST'];

    if(!is_null($checkItems)) {
	$wantItems = array_merge($wantItems, array_keys($checkItems));
    }

    if(is_null($_SESSION['loggedIn']) || !($loggedIn = $_SESSION['loggedIn'])) {
	// the loggedIn flag in the session is not present or is false
	$loggedIn = false;
	$message = "You are not logged in.";

    } else if(is_null($_COOKIE['UserCredential'])) {
	// the UserCredential cookie is not present
	$loggedIn = false;
	$message  = "You may have logged in, but your browser did not return the cookie containing your credentials.";
	$message .= " You must enable cookies from domain \"$HTTP_HOST\" to use this site.";

    } else {

	$uc = new UserCredential($_COOKIE['UserCredential']);
	$loggedIn = true;	// from this point, mark 'false' if there's a problem

	if(!$uc->valid()) {
	    // the credential is not internally consistent
	    $loggedIn = false;
	    $message = "Login failed. Your credential is invalid. " . $uc->errorString;

	} else if (!$uc->includesAll($wantItems)) {
	    // the credential doesn't include some items needed by the caller
	    $loggedIn = false;
	    $message  = "Login failed.";
	    $message .= " Your credential does not include required item(s). " . $uc->errorString;

	} else if(@$checkItems) {

	    // check if certain of the credential's items match the values expected by the caller
	    $items = $uc->getItemArray();
	    foreach($checkItems as $key => $value) {
		if(@$value && ($value != $items[$key])) {
		    $loggedIn = false;
		    if($debug) {
			$message .= " $key: have($items[$key]), want($value).";
		    }
		}
	    }
	    if(!$loggedIn) {
		$message  = "Login failed."
		    . " Some of the items in your credential have unexpected values."
		    . @$message;
	    }
	}
	if(!$loggedIn && !$debug) $uc->clear();
    }

    $_SESSION['loggedIn'] = $loggedIn;

    if(!$loggedIn) {
	clearLogin($message, $loginFailurePage, 15, $uc);

	$_SESSION['loggedIn'] = false;
	setcookie('UserCredential');
	// login failed, so redirect to another page, presumably to request login info, then die()
	?>
	<HTML>
	<HEAD>
	    <TITLE>California Tours</TITLE>
	    <META http-equiv="REFRESH" content="60;URL=<?php echo $loginFailurePage; ?>">
	</HEAD>
	<BODY>
	    <p><?php echo $message; ?></p>
	    <p>If this page does not refresh automatically, please log in <a href='<?php echo $loginFailurePage?>'><b>HERE</b></a>.</p>
	</BODY>
	</HTML>
	<?php
	die();

    } else {
	// return the requested items ("wanted" and "checked")
	foreach($wantItems as $key) {
	    $items = $uc->getItemArray();
	    $have[$key] = $items[$key];
	}
    }
    return ($loggedIn ? $have : false);
}

function clearLogin($message, $nextPage = "/", $holdTime = 5, $uc = NULL) {
    if(!is_null($uc)) {
	$uc->clear();
	unset($uc);
    }
    unset($_SESSION['loggedIn']);
    unset($_SESSION['ucRefreshTime']);
    setcookie('UserCredential');
    ?>
    <HTML>
    <HEAD>
	<TITLE>California Tours</TITLE>
	<META http-equiv="REFRESH" content="<?php echo $holdTime; ?>;URL=<?php echo $nextPage; ?>">
    </HEAD>
    <BODY>
	<p><?php echo $message; ?></p>
	<p>
	    If this page does not refresh automatically, please click 
	    <a href='<?php echo $nextPage?>'><b>HERE</b></a> to continue.
	</p>
    </BODY>
    </HTML>
    <?php
    die();
}

if(false) {
session_start();
$timeStart = time();

$uc1 = new UserCredential();
$uc1->addItem("UserName", "randy"); $uc1->addItem("Access", 4); $uc1->addItem("SessionId", session_id()); $uc1->addItem("Refresh", time());

setcookie('UserCredential', $uc1->getRawURLEncoded());
$_SESSION['loggedIn'] = true;

echo "<HTML><HEAD></HEAD><BODY><PRE>";

echo "\n===== 1: good\n\n";

$v = $uc1->getRawURLEncoded(); var_dump($v); $v = $uc1->getFullyDecoded(); var_dump($v);
$v = $uc1->valid(); var_dump($v); if(!$v) { echo $uc1->errorString; }
$v = $uc1->getItemArray(); var_dump($v);

echo "\n===== 2: all items missing\n";

$uc1->itemArray = array();
$uc1->regenerate();
$v = $uc1->getRawURLEncoded(); var_dump($v); $v = $uc1->getFullyDecoded(); var_dump($v);
$v = $uc1->valid(); var_dump($v); if(!$v) { echo $uc1->errorString; }


echo "\n===== 3: invalid checksum\n";

$uc1->addItem("UserName", "randy"); $uc1->addItem("Access", 4); $uc1->addItem("SessionId", session_id()); $uc1->addItem("Refresh", time());
$v = $uc1->getRawURLEncoded(); var_dump($v); $v = $uc1->getFullyDecoded(); var_dump($v);
$uc1->sum = "this is not the right string";
$v = $uc1->valid(); var_dump($v); if(!$v) { echo $uc1->errorString; }

echo "\n===== 4: good; forced regeneration of checksum\n";


$v = $uc1->getRawURLEncoded(); var_dump($v); $v = $uc1->getFullyDecoded(); var_dump($v);
$uc1->sum = NULL;
$v = $uc1->valid(); var_dump($v); if(!$v) { echo $uc1->errorString; }
$v = $uc1->getItemArray(); var_dump($v);

echo "\n===== 5: replace Access = 2, attempt to add item with invalid keyword\n";


$uc1->addItem('Access', 2);
$uc1->addItem('Far kle', "no value");
$v = $uc1->getRawURLEncoded(); var_dump($v); $v = $uc1->getFullyDecoded(); var_dump($v);
$v = $uc1->valid(); var_dump($v); if(!$v) { echo $uc1->errorString; }
$v = $uc1->getItemArray(); var_dump($v);

echo "\n===== 6\n";

$session = session_id();
echo "phpsession is $session\n";

$foo = NULL; 
is_null($foo) ? print "foo is NULL\n" : print "foo is NOT null\n";

echo "$uc1->one\n";
echo "$uc1->two\n";
echo "$uc1->six\n";
setcookie('HooHoo', "howdy");

echo "</PRE></BODY></HTML>\n";

}


?>
