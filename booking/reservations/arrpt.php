<HTML>

<HEAD>
<TITLE>A/R Report</TITLE>
<BODY bgcolor="#fffff0">

<SCRIPT LANGUAGE = "JavaScript">

	var strArgString = document.location.search;

	var strLevel = strArgString.substr(6);

</SCRIPT>

<?

// open a connection to mysql and select the "deltours" database


	$mysql_link = connect_to_db();
	$mysql_query = select_db($mysql_link);

// print fetched rows

print("<SCRIPT LANGUAGE = 'JavaScript'>\n");
print("if (strLevel == \"3\")\n");
print("document.write(\"<a href='arrptmain.php?args=3'><b><font size='-1' color='blue'>Back to Customer Ledger Search</font></b></a>\");\n");
print("if (strLevel == \"4\")\n");
print("document.write(\"<a href='arrptmain.php?args=4'><b><font size='-1' color='blue'>Back to Customer Ledger Search</font></b></a>\");\n");
print("</SCRIPT>\n");

include ("catourheader.php");

/*
$query = "CREATE TABLE INVOICE (
			INVOICE_ID mediumint(2) NOT NULL default '0',
			INVOICE_AMOUNT decimal(4,2) NOT NULL default '0.00',
			INVOICE_DATE char (10) NOT NULL default '',
			INVOICE_AGENT_ID mediumint(2) NOT NULL default '0',
			INVOICE_STATUS enum ('VALID','VOID') NOT NULL default 'VALID',
			BOOKTYPE_ID tinyint(1) NOT NULL default '0',
			BOOK_ID mediumint(1) NOT NULL default '0',
			INVOICE_TIMESTAMP datetime NOT NULL default '0000-00-00 00:00:00',
			PRIMARY KEY (INVOICE_ID),

*/

	if ($FromDate)
	{
		if ($ToDate)
		{
			if ($AgentId)
			{
				$query = "SELECT INVOICE_ID,
								INVOICE_DATE,
								INVOICE_AGENT_ID,
								BOOK_ID,
								INVOICE_STATUS,
								INVOICE_AMOUNT
								FROM INVOICE WHERE (INVOICE_DATE >= '$FromDate'
											AND INVOICE_DATE <= '$ToDate'
											AND INVOICE_AGENT_ID = '$AgentId')
								ORDER BY INVOICE_AGENT_ID, INVOICE_DATE ASC";
			}
			else
			{
				$query = "SELECT INVOICE_ID,
								INVOICE_DATE,
								INVOICE_AGENT_ID,
								BOOK_ID,
								INVOICE_STATUS,
								INVOICE_AMOUNT
								FROM INVOICE WHERE (INVOICE_DATE >= '$FromDate'
											AND INVOICE_DATE <= '$ToDate')
								ORDER BY INVOICE_AGENT_ID, INVOICE_DATE ASC";

			}
		}
		else
		{
			if ($AgentId)
			{
				$query = "SELECT INVOICE_ID,
								INVOICE_DATE,
								INVOICE_AGENT_ID,
								BOOK_ID,
								INVOICE_STATUS,
								INVOICE_AMOUNT FROM INVOICE
								WHERE (INVOICE_DATE >= '$FromDate'
										AND INVOICE_AGENT_ID = '$AgentId')
								ORDER BY INVOICE_AGENT_ID, INVOICE_DATE ASC";
			}
			else
			{
				$query = "SELECT INVOICE_ID,
								INVOICE_DATE,
								INVOICE_AGENT_ID,
								BOOK_ID,
								INVOICE_STATUS,
								INVOICE_AMOUNT FROM INVOICE
								WHERE INVOICE_DATE >= '$FromDate'
								ORDER BY INVOICE_AGENT_ID, INVOICE_DATE ASC";
			}
		}
	}
	else
	{
		if ($AgentId)
		{
			$query = "SELECT INVOICE_ID,INVOICE_DATE,INVOICE_AGENT_ID, BOOK_ID,INVOICE_STATUS,INVOICE_AMOUNT FROM INVOICE WHERE INVOICE_AGENT_ID = '$AgentId' ORDER BY INVOICE_AGENT_ID, INVOICE_DATE ASC";
		}
		else
		{
			$query = "SELECT INVOICE_ID,INVOICE_DATE,INVOICE_AGENT_ID, BOOK_ID,INVOICE_STATUS,INVOICE_AMOUNT FROM INVOICE ORDER BY INVOICE_AGENT_ID, INVOICE_DATE ASC";
		}
	}



//print($query);

$mysql_result = mysql_query($query, $mysql_link);

$numrows = mysql_num_rows($mysql_result);

if ($numrows > 0)
{

	// print fetched rows

	$current_time = getdate(time());
	$current_hours = $current_time["hours"];
	$current_mins = $current_time["minutes"];
	$current_secs = $current_time["seconds"];
	$current_date = date("Y-m-d");

	$TimeStamp = sprintf("%s %d:%d:%d",$current_date,$current_hours,$current_mins,$current_secs);

	print("<form name='invoice' method='post' action='invoice.php'><br>\n");
	print("<CENTER><b><font size='+1'>A/R Report</font></b></CENTER>\n");
	print("<center><font size='-1' color='blue'>(-- Created $TimeStamp --)</font></center><br>\n");
	print("<TABLE ALIGN='CENTER' WIDTH='100%'>");
	print("\n");


	print("<TABLE ALIGN='CENTER' WIDTH='100%'>");
	print("\n");


	$TotalCreditAmount = 0;
	$TotalDebitAmount = 0;
	$TotalDue = 0;
	$OldAgentName = "";
	$OldTripCode = "";
	while ($row = mysql_fetch_array($mysql_result))
	{


		$InvoiceId = $row[0];
		$InvoiceDate = $row[1];
		$InvoiceAgentId = $row[2];
		$BookId = $row[3];
		$Status = $row[4];
		$Amount = $row[5];

		if ($Status == "VALID")
		{

			$query1 = "SELECT * FROM AGENT WHERE AGENT_ID = '$InvoiceAgentId'";
			$mysql_result1 = mysql_query($query1, $mysql_link);

			$numrows1 = 0;
			$numrows = mysql_num_rows($mysql_result1);

			if ($numrows > 0)
			{
				$row1 = mysql_fetch_array($mysql_result1);
				$AgentCode = strtoupper($row1[1]);
				$AgentName = strtoupper($row1[3]);
				if ($AgentName <> $OldAgentName)
				{
					if ($OldAgentName <> "")
					{

						$TotalDue = $TotalDebitAmount - $TotalCreditAmount;
						$TotalDue = sprintf("%5.2f",$TotalDue);

						print("<TR><TD colspan='5'><hr></TD></TR>\n");
						print("<TR>\n");

						if ($TotalDue == 0)
						{
							print("<TD ALIGN='LEFT' valign='top' colspan = '3'>* NO OUTSTANDING INVOICES *</TD>");
							print("\n");
							print("<TD ALIGN='LEFT' valign='top' size = '+1'><b>Total Due</b></TD>");
							print("<TD ALIGN='LEFT' valign='top' size = '+1'><b>$TotalDue</b></TD>");
							print("\n");
						}
						else
						{
							print("<TD ALIGN='LEFT' valign='top' colspan = '3'></TD>");
							print("\n");
							print("<TD ALIGN='LEFT' valign='top' size = '+1'><b>Total Due</b></TD>");
							print("<TD ALIGN='LEFT' valign='top' size = '+1'><b>$TotalDue</b></TD>");
							print("\n");

						}
						print("</TR>\n");
						print("<TR><TD colspan='5'></TD></TR>\n");
						print("<TR><TD colspan='5'><br></TD></TR>\n");


						$TotalDue = 0;
						$TotalDebitAmount = 0;
						$TotalCreditAmount = 0;

					}
					print("<TR>\n");
					print("<TD ALIGN='LEFT' valign='top'><b>Agent:</b></TD>");
					print("<TD ALIGN='LEFT' valign='top' colspan='5'><b>$AgentCode $AgentName</b></TD>");
					print("</TR>\n");
					print("<TR>\n");

					if ((!$FromDate) AND (!$ToDate))
					{
						print("<TD ALIGN='LEFT' valign='top'><b>Date Range:</b></TD>");
						print("<TD ALIGN='LEFT' valign='top' colspan='5'><b>ALL</b></TD>");

					}
					else
					{
						if (!$FromDate)
						{
							$FromDate = "ALL";
						}
						if (!$ToDate)
						{
							$ToDate = sprintf("%s", date("m/d/Y"));
						}
						print("<TD ALIGN='LEFT' valign='top'><b>Date Range:</b></TD>");
						print("<TD ALIGN='LEFT' valign='top' colspan='5'><b>$FromDate - $ToDate</b></TD>");
					}
					print("</TR>\n");

					print("<TR></TR>\n");
					print("<TR></TR>\n");
					print("<TR>");
					print("\n");
					print("<TD ALIGN='LEFT' valign='top'><b><u><Font color='black'>Date</font></u></b></TD>");
					print("\n");
					print("<TD ALIGN='LEFT' valign='top'><b><u><Font color='black'>Inv.#</font></u></b></TD>");
					print("\n");
					print("<TD ALIGN='LEFT' valign='top'><b><u><Font color='black'>REF</font></u></b></TD>");
					print("\n");
					print("<TD ALIGN='LEFT' valign='top'><b><u><Font color='black'>Trip Code</font></u></b></TD>");
					print("\n");
/*
					print("<TD ALIGN='LEFT' valign='top'><b><u><Font color='black'>Debit</font></u></b></TD>");
					print("\n");
					print("<TD ALIGN='LEFT' valign='top'><b><u><Font color='black'>Credit</font></u></b></TD>");
					print("\n");
*/

					print("<TD ALIGN='LEFT' valign='top'><b><u><Font color='black'>Bal. Due</font></u></b></TD>");
					print("\n");

					print("</TR>");
					print("\n");
				}
				$OldAgentName = $AgentName;

			}


			$query2 = "SELECT SCHEDULEDTOUR_ID, SCHEDULEDTOUR_CODE,TOUR_ID FROM SCHEDULEDTOUR WHERE SCHEDULEDTOUR_ID = '$BookId'";
			$mysql_result2 = mysql_query($query2, $mysql_link);

			$numrows2 = 0;
			$numrows2 = mysql_num_rows($mysql_result2);

			if ($numrows2 > 0)
			{
				$row2 = mysql_fetch_array($mysql_result2);
				$TempBookId = $row2[0];
				$ReferenceName = $row2[1];
				$TempTourId	= $row2[2];
				if ($ReferenceName == "")
				{
					$ReferenceName = "NONE";
				}
			}
			else
			{
				$ReferenceName = "NONE";
			}

			$querydebit = "SELECT DEPOSIT_ID FROM CASHRECEIPT WHERE INVOICE_ID = '$InvoiceId'";
			$mysql_result_query = mysql_query($querydebit, $mysql_link);

			$numrows_query = 0;
			$numrows_query = mysql_num_rows($mysql_result_query);

		//	print($querydebit);
		//	print("<br>\n");
		//	print("Numrows = $numrows_query\n");

			if ($numrows_query == 0)

/*
			{
				$rows_query = mysql_fetch_array($mysql_result_query);
				$DepositId = $rows_query[0];
				$querydeposit = "SELECT DEPOSIT_DATE FROM DEPOSIT WHERE DEPOSIT_ID = '$DepositId'";
				$mysql_result_deposit = mysql_query($querydeposit, $mysql_link);
				$rows_deposit = mysql_fetch_array($mysql_result_deposit);
				$DepositDate = $rows_deposit[0];

				// print row result
				print("<TR>");
				print("\n");
				print("<TD ALIGN='LEFT' valign='top'>$InvoiceDate</TD>");
				print("\n");
				print("<TD ALIGN='LEFT' valign='top'>$InvoiceId</TD>");
				print("\n");
				print("<TD ALIGN='LEFT' valign='top'>INV</TD>");
				print("\n");
				print("<TD ALIGN='LEFT' valign='top'>$ReferenceName</TD>");
				print("\n");
				print("<TD ALIGN='LEFT' valign='top'>$Amount</TD>");
				print("\n");
				print("<TD ALIGN='LEFT' valign='top'></TD>");
				print("\n");
				print("<TD ALIGN='LEFT' valign='top'></TD>");
				print("\n");

				print("</TR>");

				print("<TR>");
				print("\n");
				print("<TD ALIGN='LEFT' valign='top'>$DepositDate</TD>");
				print("\n");
				print("<TD ALIGN='LEFT' valign='top'>$InvoiceId</TD>");
				print("\n");

				print("<TD ALIGN='LEFT' valign='top'>DEP</TD>");

				print("\n");
				print("<TD ALIGN='LEFT' valign='top'>$ReferenceName</TD>");
				print("\n");
				print("<TD ALIGN='LEFT' valign='top'></TD>");
				print("\n");
				print("<TD ALIGN='LEFT' valign='top'>$Amount</TD>");
				print("\n");
				print("<TD ALIGN='LEFT' valign='top'></TD>");
				print("\n");
				print("</TR>");


				print("<TR>");
				print("<TD ALIGN='LEFT' valign='top' colspan = '6'></TD>");
				print("\n");
				print("<TD ALIGN='LEFT' valign='top'>0.00</TD>");
				print("\n");

				print("</TR>");


				$TotalCreditAmount = $TotalCreditAmount + $Amount;
				$TotalDebitAmount = $TotalDebitAmount + $Amount;

			}

			else
*/

			{
				if ($Amount != 0)
				{
					print("<TR>");
					print("\n");
					print("<TD ALIGN='LEFT' valign='top'>$InvoiceDate</TD>");
					print("\n");
					print("<TD ALIGN='LEFT' valign='top'>$InvoiceId</TD>");
					print("\n");

					print("<TD ALIGN='LEFT' valign='top'>INV</TD>");
					print("\n");
					print("<TD ALIGN='LEFT' valign='top'>$ReferenceName</TD>");
					print("\n");
					print("<TD ALIGN='LEFT' valign='top'>$Amount</TD>");
					print("\n");
	/*
				print("<TD ALIGN='LEFT' valign='top'></TD>");
				print("\n");
				print("<TD ALIGN='LEFT' valign='top'></TD>");
				print("\n");
	*/
					print("</TR>");

/*
				print("<TR>");
				print("<TD ALIGN='LEFT' valign='top' colspan = '4'></TD>");
				print("\n");
				print("<TD ALIGN='LEFT' valign='top'>$Amount</TD>");
				print("\n");

				print("</TR>");
*/

				}
				$TotalDebitAmount = $TotalDebitAmount + $Amount;
			}


		}
	}

						$TotalDue = $TotalDebitAmount - $TotalCreditAmount;
						$TotalDue = sprintf("%5.2f",$TotalDue);
						print("<TR><TD colspan='5'><hr></TD></TR>\n");
						print("<TR>\n");

						if ($TotalDue == 0)
						{
							print("<TD ALIGN='LEFT' valign='top' colspan = '3'>* NO OUTSTANDING INVOICES *</TD>");
							print("\n");
							print("<TD ALIGN='LEFT' valign='top' size = '+1'><b>Total Due</b></TD>");
							print("<TD ALIGN='LEFT' valign='top' size = '+1'><b>$TotalDue</b></TD>");
							print("\n");
						}
						else
						{
							print("<TD ALIGN='LEFT' valign='top' colspan = '3'></TD>");
							print("\n");
							print("<TD ALIGN='LEFT' valign='top' size = '+1'><b>Total Due</b></TD>");
							print("<TD ALIGN='LEFT' valign='top' size = '+1'><b>$TotalDue</b></TD>");
							print("\n");

						}
						print("</TR>\n");
						print("<TR><TD colspan='5'></TD></TR>\n");
						print("<TR><TD colspan='5'></TD></TR>\n");

						$TotalDue = 0;
						$TotalDebitAmount = 0;
						$TotalCreditAmount = 0;




	print("</TABLE>");

	print("</form>\n");

}
else
{
print("<br>");
print("*** NO MATCHES ***");
}

?>

</BODY>
</HTML>