<?php
   //begin add
   
date_default_timezone_set('America/Los_Angeles');

   session_start();
   # clear the optional tour session variable
   $_SESSION['OPTIONALTOUR_ID_1'] = '';

   unset($_SESSION['ReservationId']);
   unset($_SESSION['ReservationId_2']);
   unset($_SESSION['ReservationId_3']);
   unset($_SESSION['ReservationId_4']);
   
   setcookie('ReservationId','',time()-3600); 
   setcookie('ReservationId_2','',time()-3600); 
   setcookie('ReservationId_3','',time()-3600); 
   setcookie('ReservationId_4','',time()-3600); 
   
   setcookie('ReservationId','',time()-3600,'/','.california-tour.com'); 
   setcookie('ReservationId_2','',time()-3600,'/','.california-tour.com'); 
   setcookie('ReservationId_3','',time()-3600,'/','.california-tour.com'); 
   setcookie('ReservationId_4','',time()-3600,'/','.california-tour.com'); 
   
   /*
   setcookie('ReservationId'); 
   setcookie('ReservationId_2');
   setcookie('ReservationId_3');
   setcookie('ReservationId_4');
   */
   
   header("Cache-control: private");
   //end add
   setcookie('ReservationId','',time()-3600);
   setcookie('AgentReservationId','',time()-3600);
   setcookie('SavedGender','',time()-3600);
   setcookie('SavedOccupancy','',time()-3600);
   setcookie('SavedPULocationId','',time()-3600);
   setcookie('SavedLastName','',time()-3600);
   setcookie('SavedFirstName','',time()-3600);
   setcookie('SavedTourName','',time()-3600);
   setcookie('SavedAgentCode','',time()-3600);
   setcookie('SavedSchoolCode','',time()-3600);
   setcookie('SavedPULocationLocation','',time()-3600);
   setcookie('SavedPULocationTime','',time()-3600);
   setcookie('SavedDepartDate','',time()-3600);
   setcookie('SavedCityCode','',time()-3600);
   setcookie('SavedRoomCharge','',time()-3600);
   setcookie('SavedEmail','',time()-3600);
   setcookie('SavedTourType','',time()-3600);
   setcookie('SavedManSuppCharge','',time()-3600);
   setcookie('SavedTourDiscount','',time()-3600);
   setcookie('SavedTourWebDiscount','',time()-3600);
   setcookie('SavedTourPrice','',time()-3600);
   setcookie('SavedTotalCharge','',time()-3600);
   setcookie('SavedScheduledTourCode','',time()-3600);
   
   setcookie('ReservationId','',time()-3600,'/','.california-tour.com');
   setcookie('AgentReservationId','',time()-3600,'/','.california-tour.com');
   setcookie('SavedGender','',time()-3600,'/','.california-tour.com');
   setcookie('SavedOccupancy','',time()-3600,'/','.california-tour.com');
   setcookie('SavedPULocationId','',time()-3600,'/','.california-tour.com');
   setcookie('SavedLastName','',time()-3600,'/','.california-tour.com');
   setcookie('SavedFirstName','',time()-3600,'/','.california-tour.com');
   setcookie('SavedTourName','',time()-3600,'/','.california-tour.com');
   setcookie('SavedAgentCode','',time()-3600,'/','.california-tour.com');
   setcookie('SavedSchoolCode','',time()-3600,'/','.california-tour.com');
   setcookie('SavedPULocationLocation','',time()-3600,'/','.california-tour.com');
   setcookie('SavedPULocationTime','',time()-3600,'/','.california-tour.com');
   setcookie('SavedDepartDate','',time()-3600,'/','.california-tour.com');
   setcookie('SavedCityCode','',time()-3600,'/','.california-tour.com');
   setcookie('SavedRoomCharge','',time()-3600,'/','.california-tour.com');
   setcookie('SavedEmail','',time()-3600,'/','.california-tour.com');
   setcookie('SavedTourType','',time()-3600,'/','.california-tour.com');
   setcookie('SavedManSuppCharge','',time()-3600,'/','.california-tour.com');
   setcookie('SavedTourDiscount','',time()-3600,'/','.california-tour.com');
   setcookie('SavedTourWebDiscount','',time()-3600,'/','.california-tour.com');
   setcookie('SavedTourPrice','',time()-3600,'/','.california-tour.com');
   setcookie('SavedTotalCharge','',time()-3600,'/','.california-tour.com');
   setcookie('SavedScheduledTourCode','',time()-3600,'/','.california-tour.com');
   /*
   setcookie('ReservationId');
   setcookie('AgentReservationId');
   setcookie('SavedGender');
   setcookie('SavedOccupancy');
   setcookie('SavedPULocationId');   
   setcookie('SavedLastName');
   setcookie('SavedFirstName');  
   setcookie('SavedTourName');
   setcookie('SavedAgentCode');
   setcookie('SavedSchoolCode');
   setcookie('SavedPULocationLocation');
   setcookie('SavedPULocationTime');
   setcookie('SavedDepartDate');
   setcookie('SavedCityCode');
   setcookie('SavedRoomCharge');
   setcookie('SavedEmail');
   setcookie('SavedTourType');
   setcookie('SavedManSuppCharge');
   setcookie('SavedTourDiscount'); 
   setcookie('SavedTourWebDiscount');
   setcookie('SavedTourPrice');
   setcookie('SavedTotalCharge');  
   setcookie('SavedScheduledTourCode');
   */
if(isset($_REQUEST["LocalScheduledTourCode"]))
	$LocalScheduledTourCode = $_REQUEST["LocalScheduledTourCode"];
if(isset($_REQUEST["languageUsed"]))
	$languageUsed = $_REQUEST["languageUsed"];
	
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>California Tour: Book Your Tour</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<?php
include "../header.php";
include_once '../sqlfuncs.php';
$mysql_link = connect_to_db();
$mysql_result = select_db($mysql_link);

   $one_day_tour = false;
   $TourCodeLen = strlen($LocalScheduledTourCode);
   if (($TourCodeLen > 0) and (strcmp($LocalScheduledTourCode, "BLANK") != 0))
   { 	   
	$query = "SELECT S.TOUR_ID, T.TOUR_NAME, T.TOUR_NAME_JP, S.SCHEDULEDTOUR_DEPART_DATE, S.SCHEDULEDTOUR_RETURN_DATE, T.TOUR_TYPE, T.CITY_ID, C.CITY_CODE, C.CITY_DESC FROM SCHEDULEDTOUR AS S, TOUR AS T, CITY AS C WHERE S.SCHEDULEDTOUR_CODE like '$LocalScheduledTourCode%' AND S.TOUR_ID = T.TOUR_ID AND T.CITY_ID = C.CITY_ID";   
	$result = mysql_query($query, $mysql_link);
	$row	= mysql_fetch_array($result);  
	$TourId   = $row['TOUR_ID'];
	$TourName = $row['TOUR_NAME'];
	$TourNameJP = $row['TOUR_NAME_JP'];
	$TourType = $row['TOUR_TYPE'];
	$CityId   = $row['CITY_ID'];
	$City     = $row['CITY_DESC'];
        $DepartDate = $row['SCHEDULEDTOUR_DEPART_DATE'];
	$ReturnDate = $row['SCHEDULEDTOUR_RETURN_DATE'];
	if($DepartDate == $ReturnDate) {
	   $one_day_tour = true;	
	}
	
    }
    $TourType = strtoupper($TourType);

    $query1 = "SELECT PULOCATION_ID, PULOCATION_CODE,PULOCATION_LOCATION FROM PULOCATION WHERE PULOCATION_CODE LIKE '$TourType%'";

    $mysql_result = mysql_query($query1, $mysql_link);
    $numrows1 = mysql_num_rows($mysql_result);
    if ($numrows1 > 0) {
	$TourOK = "True";
    }
    else {
	$TourOK = "False";
    }

    if ($TourOK == "False") {
	// redirect to message saying no tours are available for this type
?>
<META HTTP-EQUIV="Refresh" CONTENT="0; URL=http://www.california-tour.com/booking/reservations/notours.php">
<?php

    }
?>

<h2>Book a Tour</h2>
    <form name="signup" method="POST" action="toursignup2.php">
	        <input type='hidden' name='languageUsed' value='<?php echo $languageUsed; ?>'>
                <input type='hidden' name='LocalScheduledTourCode' value='<?php echo $LocalScheduledTourCode; ?>'>
<h3>Review Your Tour Selection</h3>

<?php
// if called from reservation program directly (instead of through signup on tour section
if ($LocalScheduledTourCode != "BLANK")
{
        echo "<p>Tour Name: " . $TourName . "<br />"; 
}
else
{
?>
<input type='text' size='25' NAME='TourName' value='<?php echo $TourName; ?>'>
<input type='text' size='15' name='TourType' value='<?php echo $TourType; ?>'>
<?php
}
echo "Leaving From: " . $City . "<br />";
echo "Departure Date: " . date("F j, Y" , strtotime($DepartDate)) . "<br />";
echo "Return Date: " . date("F j, Y" , strtotime($ReturnDate)) . "<br />";
// check to see if there are optional tours, if so provide a radio button for each
$query2 = "SELECT OPTIONALTOUR_ID FROM  OPTIONALTOUR WHERE TOUR_ID = '$TourId'";
$mysql_result2 = mysql_query($query2,$mysql_link);
if ( mysql_num_rows($mysql_result2 )  > 0 ) {
	echo "<p>Would you like to add an Optional Tour?</p>";
	$query2 = "SELECT OPTIONALTOUR_NAME, OPTIONALTOUR_ID, OPTIONALTOUR_PRICE FROM  OPTIONALTOUR WHERE TOUR_ID = '$TourId'";
	$mysql_result2 = mysql_query($query2,$mysql_link);
	$count = 0;
	echo "<input name='OPTIONALTOUR_ID_1' type='radio' value='0' CHECKED>No Optional Tour<br> ";
	while ( $row2 = mysql_fetch_array($mysql_result2) )  {
				$count += 1;
				$OPTIONALTOUR_ID = $row2['OPTIONALTOUR_ID'];
				$OPTIONALTOUR_NAME = $row2['OPTIONALTOUR_NAME'];
				$OPTIONALTOUR_PRICE = $row2['OPTIONALTOUR_PRICE'];
	  		  echo "<input name='OPTIONALTOUR_ID_1' type='radio' value='$OPTIONALTOUR_ID'>$OPTIONALTOUR_NAME $$OPTIONALTOUR_PRICE<br> ";
	}
}
?>
		
<p><span style="color:#FF0000;">Do you have a school code or other discount code?  <br />
If so, please enter it here: <input type='text' name='SchoolCode' class="text" onblur="pickuplocation(this.value, '<?php echo $TourType; ?>')"></span></p>
<p>If you do not have a discount code, please click Continue.</p>
<INPUT TYPE="BUTTON" name="Cancel" value="Cancel" class="button" onclick=CancelReservation();>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<INPUT TYPE="SUBMIT" name="WebSignUp" value="Continue" class="button">
<br /><br />
</form>
<?php
include "../footer.php";
?>
