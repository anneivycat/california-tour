<HTML><HEAD>
<TITLE>List Invoices</TITLE>
<BODY bgcolor="#fffff0">
<SCRIPT LANGUAGE = "JavaScript">
  var strArgString = document.location.search;
  var strLevel = strArgString.substr(6);
</SCRIPT>
<?php
// open a connection to mysql and select the "deltours" database
  $mysql_link = connect_to_db();
  $mysql_query = select_db($mysql_link);
?>
<SCRIPT LANGUAGE = 'JavaScript'>
if (strLevel == "1") {
  document.write("<a href='invlistrptmain.php?args=1'><b><font size='-1' color='blue'>Back to Invoice List Search<"+"/font><"+"/b><"+"/a>");
} else if (strLevel == "2") {
  document.write("<a href='invlistrptmain.php?args=2'><b><font size='-1' color='blue'>Back to Invoice List Search<"+"/font><"+"/b><"+"/a>");
} else if (strLevel == "4") {
  document.write("<a href='invlistrptmain.php?args=4'><b><font size='-1' color='blue'>Back to Invoice List Search<"+"/font><"+"/b><"+"/a>");
}
</SCRIPT>
<?php
/* get the year from the date */
$FromDateLen = strlen($FromDate);
$FromYear = substr($FromDate,$FromDateLen - 2,2);
$ToDateLen = strlen($ToDate);
$ToYear = substr($ToDate,$ToDateLen - 2,2);
/* added 12/15/2003 */
//echo "$FromDate,$ToDate\n";
include ("catourheader.php");
/* added 12/26/2003 */
//echo "Invoice Type = $InvoiceTypeId<br>\n";
// Invoice Type Id 1 is Private Services,
// Invoice Type Id 2 is Sched Tour
if (($InvoiceTypeId == '1') or (!$InvoiceTypeId)) {
if ($CityId) {
  if ($FromDate) {
    if ($ToDate) {
      if ($AgentId) {
        $query = "SELECT INVOICE_ID,
            INVOICE_DATE,
            INVOICE_AGENT_ID,
            BOOK_ID,
            INVOICE_STATUS,
            INVOICE_AMOUNT
            FROM INVOICE,SCHEDULEDTOUR,TOUR
             WHERE (INVOICE.BOOK_ID = SCHEDULEDTOUR.SCHEDULEDTOUR_ID
               AND SCHEDULEDTOUR.TOUR_ID = TOUR.TOUR_ID
              AND INVOICE_DATE >= '$FromDate'
              AND INVOICE_DATE <= '$ToDate'
              AND (INVOICE_DATE like '%$FromYear' OR INVOICE_DATE like '%ToYear')
              AND INVOICE_AGENT_ID = '$AgentId'
               AND TOUR.CITY_ID = '$CityId') ORDER BY INVOICE_DATE ASC";
      } else {
        $query = "SELECT INVOICE_ID,
            INVOICE_DATE,
            INVOICE_AGENT_ID,
            BOOK_ID,
            INVOICE_STATUS,
            INVOICE_AMOUNT
            FROM INVOICE,SCHEDULEDTOUR,TOUR
             WHERE (INVOICE.BOOK_ID = SCHEDULEDTOUR.SCHEDULEDTOUR_ID
               AND SCHEDULEDTOUR.TOUR_ID = TOUR.TOUR_ID
              AND INVOICE_DATE >= '$FromDate'
              AND INVOICE_DATE <= '$ToDate'
              AND (INVOICE_DATE like '%$FromYear' OR INVOICE_DATE like '%ToYear')
               AND TOUR.CITY_ID = '$CityId') ORDER BY INVOICE_DATE ASC";
      }
    } else {
      if ($AgentId) {
        $query = "SELECT INVOICE_ID,
              INVOICE_DATE,
              INVOICE_AGENT_ID,
              BOOK_ID,
              INVOICE_STATUS,
              INVOICE_AMOUNT
              FROM INVOICE,SCHEDULEDTOUR,TOUR
               WHERE (INVOICE.BOOK_ID = SCHEDULEDTOUR.SCHEDULEDTOUR_ID
                 AND SCHEDULEDTOUR.TOUR_ID = TOUR.TOUR_ID
                AND INVOICE_DATE >= '$FromDate'
                AND INVOICE_AGENT_ID = '$AgentId'
                 AND TOUR.CITY_ID = '$CityId') ORDER BY INVOICE_DATE ASC";
      } else {
          $query = "SELECT INVOICE_ID,
                INVOICE_DATE,
                INVOICE_AGENT_ID,
                BOOK_ID,
                INVOICE_STATUS,
                INVOICE_AMOUNT
                FROM INVOICE,SCHEDULEDTOUR,TOUR
                 WHERE (INVOICE.BOOK_ID = SCHEDULEDTOUR.SCHEDULEDTOUR_ID
                   AND SCHEDULEDTOUR.TOUR_ID = TOUR.TOUR_ID
                  AND INVOICE_DATE >= '$FromDate'
                   AND TOUR.CITY_ID = '$CityId') ORDER BY INVOICE_DATE ASC";
      }
    }
  } else {
    if ($AgentId) {
      $query = "SELECT INVOICE_ID,
            INVOICE_DATE,
            INVOICE_AGENT_ID,
            BOOK_ID,
            INVOICE_STATUS,
            INVOICE_AMOUNT
            FROM INVOICE,SCHEDULEDTOUR,TOUR
             WHERE (INVOICE.BOOK_ID = SCHEDULEDTOUR.SCHEDULEDTOUR_ID
               AND SCHEDULEDTOUR.TOUR_ID = TOUR.TOUR_ID
               AND INVOICE_AGENT_ID = '$AgentId'
               AND TOUR.CITY_ID = '$CityId') ORDER BY INVOICE_DATE ASC";
    } else {
      $query = "SELECT INVOICE_ID,
            INVOICE_DATE,
            INVOICE_AGENT_ID,
            BOOK_ID,
            INVOICE_STATUS,
            INVOICE_AMOUNT
            FROM INVOICE,SCHEDULEDTOUR,TOUR
             WHERE (INVOICE.BOOK_ID = SCHEDULEDTOUR.SCHEDULEDTOUR_ID
               AND SCHEDULEDTOUR.TOUR_ID = TOUR.TOUR_ID
               AND TOUR.CITY_ID = '$CityId') ORDER BY INVOICE_DATE ASC";
    }
  }
} else {
  if ($FromDate) {
    if ($ToDate) {
      if ($AgentId) {
        $query = "SELECT INVOICE_ID,
                INVOICE_DATE,
                INVOICE_AGENT_ID,
                BOOK_ID,
                INVOICE_STATUS,
                INVOICE_AMOUNT
                FROM INVOICE WHERE (INVOICE_DATE >= '$FromDate'
                      AND INVOICE_DATE <= '$ToDate'
                      AND (INVOICE_DATE like '%$FromYear' OR INVOICE_DATE like '%ToYear')
                      AND INVOICE_AGENT_ID = '$AgentId')
                ORDER BY INVOICE_DATE ASC";
      } else {
        $query = "SELECT INVOICE_ID,
                INVOICE_DATE,
                INVOICE_AGENT_ID,
                BOOK_ID,
                INVOICE_STATUS,
                INVOICE_AMOUNT
                FROM INVOICE WHERE (INVOICE_DATE >= '$FromDate'
                      AND INVOICE_DATE <= '$ToDate'
                      AND (INVOICE_DATE like '%$FromYear' OR INVOICE_DATE like '%ToYear'))
                ORDER BY INVOICE_DATE ASC";
      }
    } else {
      if ($AgentId) {
        $query = "SELECT INVOICE_ID,
                INVOICE_DATE,
                INVOICE_AGENT_ID,
                BOOK_ID,
                INVOICE_STATUS,
                INVOICE_AMOUNT FROM INVOICE
                WHERE (INVOICE_DATE >= '$FromDate'
                    AND INVOICE_AGENT_ID = '$AgentId')
                ORDER BY INVOICE_DATE ASC";
      } else {
        $query = "SELECT INVOICE_ID,
                INVOICE_DATE,
                INVOICE_AGENT_ID,
                BOOK_ID,
                INVOICE_STATUS,
                INVOICE_AMOUNT FROM INVOICE
                WHERE INVOICE_DATE >= '$FromDate'
                ORDER BY INVOICE_DATE ASC";
      }
    }
  } else {
    if ($AgentId) {
      $query = "SELECT INVOICE_ID,INVOICE_DATE,INVOICE_AGENT_ID, BOOK_ID,INVOICE_STATUS,INVOICE_AMOUNT FROM INVOICE WHERE INVOICE_AGENT_ID = '$AgentId' ORDER BY INVOICE_DATE ASC";
    } else {
      $query = "SELECT INVOICE_ID,INVOICE_DATE,INVOICE_AGENT_ID, BOOK_ID,INVOICE_STATUS,INVOICE_AMOUNT FROM INVOICE ORDER BY INVOICE_DATE ASC";
    }
  }
}
$mysql_result = mysql_query($query, $mysql_link);
$numrows = mysql_num_rows($mysql_result);
if ($numrows > 0) {
  // print fetched rows
  $current_time = getdate(time());
  $current_hours = $current_time["hours"];
  $current_mins = $current_time["minutes"];
  $current_secs = $current_time["seconds"];
  $current_date = date("Y-m-d");
  $TimeStamp = sprintf("%s %d:%d:%d",$current_date,$current_hours,$current_mins,$current_secs);
  echo "<form name='invoice' method='post' action='invoice.php'><br>\n";
  echo "<CENTER><b><font size='+1'>Sched Tour Invoice List</font></b></CENTER>\n";
  echo "<center><font size='-1' color='blue'>(-- Created $TimeStamp --)</font></center>\n";
  echo "<TABLE ALIGN='CENTER' WIDTH='100%'>";
  echo "<TR>\n";
  if ((!$FromDate) AND (!$ToDate)) {
    echo "<TD ALIGN='center' valign='top' colspan='2'><b>Date Range:</b></TD>";
    echo "<TD ALIGN='center' valign='top' colspan='5'><b>ALL</b></TD>";
  } else {
    if (!$FromDate) {
      $FromDate = "ALL";
    }
    if (!$ToDate) {
      $ToDate = sprintf("%s", date("m/d/Y"));
    }
    echo "<TD ALIGN='center' valign='top' colspan='2'><b>Date Range:</b></TD>";
    echo "<TD ALIGN='center' valign='top' colspan='5'><b>$FromDate - $ToDate</b></TD>";
  }
  echo "</TR>\n<TR><TD ALIGN='center' valign='top'><b><u>Inv. ID</u></b></TD>";
  echo "<TD ALIGN='center' valign='top'><b><u>Date</u></b></TD>";
  echo "<TD ALIGN='center' valign='top'><b><u>Agent</u></b></TD>";
  echo "<TD ALIGN='center' valign='top'><b><u>City</u></b></TD>";
  echo "<TD ALIGN='center' valign='top'><b><u>Trip</u></b></TD>";
  echo "<TD ALIGN='center' valign='top'><b><u>Status</u></b></TD>";
/* added 12/15/2003 */
  echo "<TD ALIGN='center' valign='top'><b><u>Total Chg</u></b></TD>";
  echo "<TD ALIGN='center' valign='top'><b><u>Web Disc</u></b></TD>";
  echo "<TD ALIGN='center' valign='top'><b><u>Pd CLT</u></b></TD>";
  echo "<TD ALIGN='center' valign='top'><b><u>CC</u></b></TD>";
  echo "<TD ALIGN='center' valign='top'><b><u>Pd Agent</u></b></TD>";
  echo "<TD ALIGN='center' valign='top'><b><u>Comm.</u></b></TD>";
  echo "<TD ALIGN='center' valign='top'><b><u>Bal.Due</u></b></TD>";
  echo "</TR>";
  $TotalAmount = 0;
  $SubTotalAmount = 0;
  $SubTotalWebDisc = 0;
  $SubTotalCharge = 0;
  $SubTotalDLT   = 0;
  $SubTotalAgent  = 0;
  $SubTotalCC    = 0;
  $SubTotalComm   = 0;
  $PrevBookId = "";
  $rowcount = 0;
  while ($row = mysql_fetch_array($mysql_result)) {
    $InvoiceId = $row[0];
    $InvoiceDate = $row[1];
    $InvoiceAgentId = $row[2];
    $BookId = $row[3];
    $Status = $row[4];
    $Amount = $row[5];
    $TotalAmount = $TotalAmount + $Amount;
/* added 12/15/2003 for calculating total charges, paid to clt, etc...*/
    if ($PrevBookId != $BookId) {
      if ($rowcount > 0) {
        $FinalTotalWebDisc   = $FinalTotalWebDisc + $SubTotalWebDisc;
        $FinalTotalCharge   = $FinalTotalCharge + $SubTotalCharge;
        $FinalTotalDLT     = $FinalTotalDLT + $SubTotalDLT;
        $FinalTotalCC     = $FinalTotalCC + $SubTotalCC;
        $FinalTotalAgent   = $FinalTotalAgent + $SubTotalAgent;
        $FinalTotalComm   = $FinalTotalComm + $SubTotalComm;
        $SubTotalCharge = 0;
        $SubTotalWebDisc = 0;
        $SubTotalDLT = 0;
        $SubTotalCC = 0;
        $SubTotalAgent = 0;
        $SubTotalComm = 0;
        $SubTotalAmount = 0;
      }
    }
/* added 12/15/2003 for calculating total charges, paid to clt, etc...*/
    $querydetails = "SELECT    TOTALPAYTODLT,
                  TOTALPAYTOAGENT,
                  TOTALPAYCC,
                  OCCUPANCY,
                  OPTIONALTOUR_ID,
                  MANSUPPCHARGE
                  FROM SCHEDULEDTOURBOOK WHERE SCHEDULEDTOUR_ID = '$BookId' AND AGENT_ID = '$InvoiceAgentId'";
    $mysql_result_details = mysql_query($querydetails, $mysql_link);
    $numrowdetails = mysql_num_rows($mysql_result_details);
    if ($numrowdetails > 0) {
      $TotalTotalWebDisc = 0;
      $TotalTotalCharge = 0;
      $TotalTotalPayToDLT = 0;
      $TotalTotalPayToAgent = 0;
      $TotalTotalPayCC = 0;
      $TotalTotalComm = 0;
      while ($rowdetails = mysql_fetch_array($mysql_result_details)) {
        $TotalPayToDLT     = $rowdetails[0];
        $TotalPayToAgent   = $rowdetails[1];
        $TotalPayCC      = $rowdetails[2];
        $OccupancyType    = $rowdetails[3];
        $OptionalTourId    = $rowdetails[4];
        $ManSuppCharge    = $rowdetails[5];
        // get the scheduled tour for calculating total cost
        // and the commission
        $query1 = "SELECT TOUR_ID,SCHEDULEDTOUR_PRICE FROM SCHEDULEDTOUR WHERE SCHEDULEDTOUR_ID = '$BookId'";
        $mysql_result1     = mysql_query($query1, $mysql_link);
        $row1         = mysql_fetch_array($mysql_result1);
        $TourId         = $row1[0];
        $ScheduledTourPrice = $row1[1];
        $query1 = "SELECT TOUR_SUPP_SNG,TOUR_SUPP_DBL,TOUR_SUPP_TRP FROM TOUR WHERE TOUR_ID = '$TourId'";
        $mysql_result1     = mysql_query($query1, $mysql_link);
        $row1         = mysql_fetch_array($mysql_result1);
        $TourSngSupp    = $row1[0];
        $TourDblSupp    = $row1[1];
        $TourTrpSupp    = $row1[2];
        // get the commission from the Agent table
        $query1       = "SELECT AGENT_COMMISIONTYPE FROM AGENT WHERE AGENT_ID = '$InvoiceAgentId'";
        $mysql_result1     = mysql_query($query1, $mysql_link);
        $row1         = mysql_fetch_array($mysql_result1);
        $CommissionType   = $row1[0];
        if ($CommissionType == 1)
          $query1  = "SELECT TOUR_COMM1 FROM TOUR WHERE TOUR_ID = '$TourId'";
        if ($CommissionType == 2)
          $query1  = "SELECT TOUR_COMM2 FROM TOUR WHERE TOUR_ID = '$TourId'";
        if ($CommissionType == 3)
          $query1  = "SELECT TOUR_COMM3 FROM TOUR WHERE TOUR_ID = '$TourId'";
        if ($CommissionType == 4)
          $query1  = "SELECT TOUR_COMM4 FROM TOUR WHERE TOUR_ID = '$TourId'";
        if ($CommissionType == 5)
          $query1  = "SELECT TOUR_COMM5 FROM TOUR WHERE TOUR_ID = '$TourId'";
        if ($CommissionType == 6)
          $query1  = "SELECT TOUR_COMM6 FROM TOUR WHERE TOUR_ID = '$TourId'";
        if ($CommissionType == 7)
          $query1  = "SELECT TOUR_COMM7 FROM TOUR WHERE TOUR_ID = '$TourId'";
        if ($CommissionType == 8)
          $query1  = "SELECT TOUR_COMM8 FROM TOUR WHERE TOUR_ID = '$TourId'";
        $mysql_result1     = mysql_query($query1, $mysql_link);
        $row1         = mysql_fetch_array($mysql_result1);
        $CommChg     = $row1[0];
        // get the discount charge
        if ($CommissionType == 1)
          $query1  = "SELECT TOUR_DISC1 FROM TOUR WHERE TOUR_ID = '$TourId'";
        if ($CommissionType == 2)
          $query1  = "SELECT TOUR_DISC2 FROM TOUR WHERE TOUR_ID = '$TourId'";
        if ($CommissionType == 3)
          $query1  = "SELECT TOUR_DISC3 FROM TOUR WHERE TOUR_ID = '$TourId'";
        if ($CommissionType == 4)
          $query1  = "SELECT TOUR_DISC4 FROM TOUR WHERE TOUR_ID = '$TourId'";
        if ($CommissionType == 5)
          $query1  = "SELECT TOUR_DISC5 FROM TOUR WHERE TOUR_ID = '$TourId'";
        if ($CommissionType == 6)
          $query1  = "SELECT TOUR_DISC6 FROM TOUR WHERE TOUR_ID = '$TourId'";
        if ($CommissionType == 7)
          $query1  = "SELECT TOUR_DISC7 FROM TOUR WHERE TOUR_ID = '$TourId'";
        if ($CommissionType == 8)
          $query1  = "SELECT TOUR_DISC8 FROM TOUR WHERE TOUR_ID = '$TourId'";
        $mysql_result1     = mysql_query($query1, $mysql_link);
        $row1         = mysql_fetch_array($mysql_result1);
        $DiscChg     = $row1[0];
        // get the commission from the Tour table
        // get the total charge from the scheduledtour and
        // the occupancy
        // single
        if ($OccupancyType == "1")
          $TotalCharge = $ScheduledTourPrice + $TourSngSupp;
        // double
        if ($OccupancyType == "2")
          $TotalCharge = $ScheduledTourPrice + $TourDblSupp;
        // triple
        if ($OccupancyType == "3")
          $TotalCharge = $ScheduledTourPrice + $TourTrpSupp;
        // triple
        if ($OccupancyType == "4")
          $TotalCharge = $ScheduledTourPrice;
        // calculate the optional tour charge, if any
        $query1 = "SELECT OPTIONALTOUR_PRICE FROM OPTIONALTOUR WHERE OPTIONALTOUR_ID = '$OptionalTourId'";
        $mysql_result1 = mysql_query($query1, $mysql_link);
        $numrows1 = mysql_num_rows($mysql_result1);
        if ($numrows1 > 0) {
          $row1       =   mysql_fetch_array($mysql_result1);
          $OptTourChg   =  $row1[0];
        }
        $TotalCharge = $TotalCharge + $OptTourChg - $DiscChg + $ManSuppCharge;
        $AddTotalCharge = $TotalPayCC + $TotalPayToDLT + $TotalPayToAgent;
        if ($TotalCharge != $AddTotalCharge) {
          $WebDiscount = $TotalCharge - $AddTotalCharge;
        } else {
          $WebDiscount = 0;
        }
      //  print($TotalCharge);
        $TotalTotalWebDisc = $TotalTotalWebDisc + $WebDiscount;
        $TotalTotalCharge = $TotalTotalCharge + $TotalCharge;
        $TotalTotalPayToDLT = $TotalTotalPayToDLT + $TotalPayToDLT;
        $TotalTotalPayToAgent = $TotalTotalPayToAgent + $TotalPayToAgent;
        $TotalTotalPayCC    = $TotalTotalPayCC + $TotalPayCC;
        $TotalTotalComm      = $TotalTotalComm + $CommChg;
        $TotalTotalWebDisc = sprintf("%5.2f",$TotalTotalWebDisc);
        $TotalTotalCharge = sprintf("%5.2f",$TotalTotalCharge);
        $TotalTotalPayToDLT = sprintf("%5.2f",$TotalTotalPayToDLT);
        $TotalTotalPayToAgent = sprintf("%5.2f",$TotalTotalPayToAgent);
        $TotalTotalPayCC    = sprintf("%5.2f",$TotalTotalPayCC);
        $TotalTotalComm      = sprintf("%5.2f",$TotalTotalComm);
      }
    }
/* end of addition 12/15/2003 */
    echo "<TR>";
    echo "<TD ALIGN='right' valign='top'><font face='arial' size='-1'>$InvoiceId</font></TD>";
    echo "<TD ALIGN='right' valign='top'><font face='arial' size='-1'>$InvoiceDate</font></TD>";
    $query1 = "SELECT * FROM AGENT WHERE AGENT_ID = '$InvoiceAgentId'";
    $mysql_result1 = mysql_query($query1, $mysql_link);
    $numrows1 = 0;
    $numrows = mysql_num_rows($mysql_result1);
    if ($numrows > 0) {
      $row1 = mysql_fetch_array($mysql_result1);
      $AgentCode = strtoupper($row1[1]);
      echo "<TD ALIGN='right' valign='top'><font face='arial' size='-1'>$AgentCode</font></TD>";
    } else {
      echo "<TD ALIGN='right' valign='top'><font face='arial' size='-1'>NONE</font></TD>";
    }

        $query2 = "SELECT SCHEDULEDTOUR_ID, SCHEDULEDTOUR_CODE,TOUR_ID FROM SCHEDULEDTOUR WHERE SCHEDULEDTOUR_ID = '$BookId'";
        $mysql_result2 = mysql_query($query2, $mysql_link);
        $numrows2 = 0;
        $numrows2 = mysql_num_rows($mysql_result2);
        if ($numrows2 > 0) {
          $row2 = mysql_fetch_array($mysql_result2);
          $TempBookId = $row2[0];
          $ReferenceName = $row2[1];
          $TempTourId  = $row2[2];
          $query3 = "SELECT CITY_ID FROM TOUR WHERE TOUR_ID = '$TempTourId'";
          $mysql_result3 = mysql_query($query3, $mysql_link);
          $numrows3 = 0;
          $numrows3 = mysql_num_rows($mysql_result3);
          //print($query3);
          //print($numrows3);
          if ($numrows3 > 0) {
            $row3 = mysql_fetch_array($mysql_result3);
            $LocalCityId = $row3[0];
            //print($CityId);
            $query4 = "SELECT CITY_CODE FROM CITY WHERE CITY_ID = '$LocalCityId'";
            $mysql_result4 = mysql_query($query4, $mysql_link);
            $numrows4 = 0;
            $numrows4 = mysql_num_rows($mysql_result4);
            if ($numrows4 > 0) {
              $row4 = mysql_fetch_array($mysql_result4);
              $LocalCityCode = $row4[0];
              echo "<TD ALIGN='right' valign='top'><font face='arial' size='-1'>$LocalCityCode</font></TD>";
            } else {
              echo "<TD ALIGN='right' valign='top'><font face='arial' size='-1'>NONE</font></TD>";
            }
          } else {
            echo "<TD ALIGN='right' valign='top'><font face='arial' size='-1'>NONE</font></TD>";
          }
          if ($ReferenceName == "") {
            echo "<TD ALIGN='right' valign='top'><font face='arial' size='-1'>NONE</font></TD>\n";
          } else {
            echo "<TD ALIGN='right' valign='top'><font face='arial' size='-1'>$ReferenceName</font></TD>\n";
          }
        } else {
          echo "<TD ALIGN='right' valign='top'><font face='arial' size='-1'>NONE</font></TD>\n";
        }
        // other types go here when they are ready
//      }
//    }
    echo "<TD ALIGN='right' valign='top'><font face='arial' size='-1'>$Status</font></TD>";
    if ($Status == "VOID") {
      $TotalTotalWebDisc = 0;
      $TotalTotalCharge = 0;
      $TotalTotalPayToDLT = 0;
      $TotalTotalPayCC = 0;
      $TotalTotalPayToAgent = 0;
      $TotalTotalComm = 0;
      $Amount = 0;
    }
    $TotalTotalWebDisc = sprintf("%5.2f", $TotalTotalWebDisc);
    $TotalTotalCharge = sprintf("%5.2f", $TotalTotalCharge);
    $TotalTotalPayToDLT = sprintf("%5.2f", $TotalTotalPayToDLT);
    $TotalTotalPayCC = sprintf("%5.2f", $TotalTotalPayCC);
    $TotalTotalPayToAgent = sprintf("%5.2f", $TotalTotalPayToAgent);
    $TotalTotalComm = sprintf("%5.2f", $TotalTotalComm);
    $Amount = sprintf("%5.2f", $Amount);
    echo "<TD ALIGN='right' valign='top'><font face='arial' size='-1'>$TotalTotalCharge</font></TD>";
    echo "<TD ALIGN='right' valign='top'><font face='arial' size='-1'>$TotalTotalWebDisc</font></TD>";
    echo "<TD ALIGN='right' valign='top'><font face='arial' size='-1'>$TotalTotalPayToDLT</font></TD>";
    echo "<TD ALIGN='right' valign='top'><font face='arial' size='-1'>$TotalTotalPayCC</font></TD>";
    echo "<TD ALIGN='right' valign='top'><font face='arial' size='-1'>$TotalTotalPayToAgent</font></TD>";
    echo "<TD ALIGN='right' valign='top'><font face='arial' size='-1'>$TotalTotalComm</font></TD>";
    echo "<TD ALIGN='right' valign='top'><font face='arial' size='-1'>$Amount</font></TD>";
    echo "</TR>";
    $PrevBookId = sprintf("%s",$BookId);
    $SubTotalAmount = $SubTotalAmount + $Amount;
    $SubTotalWebDisc = $SubTotalWebDisc + $TotalTotalWebDisc;
    $SubTotalCharge = $SubTotalCharge + $TotalTotalCharge;
    $SubTotalDLT   = $SubTotalDLT + $TotalTotalPayToDLT;
    $SubTotalAgent  = $SubTotalAgent + $TotalTotalPayToAgent;
    $SubTotalCC    = $SubTotalCC + $TotalTotalPayCC;
    $SubTotalComm  = $SubTotalComm + $TotalTotalComm;
    $rowcount = $rowcount+1;
  }
/* added 12/15/2003 */
  $FinalTotalWebDisc   = $FinalTotalWebDisc + $SubTotalWebDisc;
  $FinalTotalCharge   = $FinalTotalCharge + $SubTotalCharge;
  $FinalTotalDLT     = $FinalTotalDLT + $SubTotalDLT;
  $FinalTotalCC     = $FinalTotalCC + $SubTotalCC;
  $FinalTotalAgent   = $FinalTotalAgent + $SubTotalAgent;
  $FinalTotalComm   = $FinalTotalComm + $SubTotalComm;
  $FinalTotalWebDisc   = sprintf("%5.2f",$FinalTotalWebDisc);
  $FinalTotalCharge   = sprintf("%5.2f",$FinalTotalCharge);
  $FinalTotalDLT     = sprintf("%5.2f",$FinalTotalDLT);
  $FinalTotalCC     = sprintf("%5.2f",$FinalTotalCC);
  $FinalTotalAgent   = sprintf("%5.2f",$FinalTotalAgent);
  $FinalTotalComm   = sprintf("%5.2f",$FinalTotalComm);
  $TotalAmount = sprintf("%5.2f",$TotalAmount);
  echo "<tr>";
  echo "<TD ALIGN='right' valign='top' colspan='13'><hr></TD>";
  echo "</TR>";
  echo "<TR>";
  echo "<TD ALIGN='right' valign='top' colspan='5'></font></TD>";
  echo "<TD ALIGN='right' valign='top'><font face='arial' size='-1'><b>Total:</b></font></TD>";
  echo "<TD ALIGN='right' valign='top'><font face='arial' size='-1'><b>$FinalTotalCharge</b></font></TD>";
  echo "<TD ALIGN='right' valign='top'><font face='arial' size='-1'><b>$FinalTotalWebDisc</b></font></TD>";
  echo "<TD ALIGN='right' valign='top'><font face='arial' size='-1'><b>$FinalTotalDLT</b></font></TD>";
  echo "<TD ALIGN='right' valign='top'><font face='arial' size='-1'><b>$FinalTotalCC</b></font></TD>";
  echo "<TD ALIGN='right' valign='top'><font face='arial' size='-1'><b>$FinalTotalAgent</b></font></TD>";
  echo "<TD ALIGN='right' valign='top'><font face='arial' size='-1'><b>$FinalTotalComm</b></font></TD>";
  echo "<TD ALIGN='right' valign='top'><font face='arial' size='-1'><b>$TotalAmount</b></font></TD>";
  echo "</TR>";
/* end of additions 12/14/2003 */
  echo "</TABLE><br>";
} else {
  echo "<br>";
  echo "*** SCHED TOUR INVOICES -- NO MATCHES ***";
}
  if (!$InvoiceTypeId) {
/* added 1/6/04 */
    if ($CityId) {
      $query_city_code = "SELECT CITY_CODE FROM CITY WHERE CITY_ID = '$CityId'";
      $query_city_result = mysql_query($query_city_code,$mysql_link);
      $row_city = mysql_fetch_array($query_city_result);
      $SearchCityCode = $row_city[0];
      if ($FromDate) {
        if ($ToDate) {
/* added 1/9/04 */
          if ($AgentId) {
            $query = "SELECT   A.INVOICE_ID,
                      A.INVOICE_DATE,
                      C.AGENT_CODE,
                      B.FITBOOK_ID,
                      B.FITBOOK_CODE,
                      A.INVOICE_STATUS,
                      B.SERVICE_CHARGE,
                      B.COMM_CHARGE,
                      B.PAY_AGT,
                      B.PAY_CC,
                      B.PAY_CLT,
                      B.CITY_CODE
                    FROM PRIVATEINVOICE A, FITBOOK B, AGENT C
                    WHERE B.FITBOOK_ID = A.FITBOOK_ID
                    AND B.AGENT_ID = C.AGENT_ID
                    AND C.AGENT_ID = '$AgentId' AND
                    B.CITY_CODE = '$SearchCityCode' AND
                    INVOICE_DATE >= '$FromDate'
                    AND INVOICE_DATE <= '$ToDate'
                    AND (INVOICE_DATE like '%$FromYear' OR INVOICE_DATE like '%$ToYear')
                    ORDER BY INVOICE_DATE ASC";
          } else {
            $query = "SELECT   A.INVOICE_ID,
                      A.INVOICE_DATE,
                      C.AGENT_CODE,
                      B.FITBOOK_ID,
                      B.FITBOOK_CODE,
                      A.INVOICE_STATUS,
                      B.SERVICE_CHARGE,
                      B.COMM_CHARGE,
                      B.PAY_AGT,
                      B.PAY_CC,
                      B.PAY_CLT,
                      B.CITY_CODE
                    FROM PRIVATEINVOICE A, FITBOOK B, AGENT C
                    WHERE B.FITBOOK_ID = A.FITBOOK_ID
                    AND B.AGENT_ID = C.AGENT_ID AND
                    B.CITY_CODE = '$SearchCityCode' AND
                    INVOICE_DATE >= '$FromDate'
                    AND INVOICE_DATE <= '$ToDate'
                    AND (INVOICE_DATE like '%$FromYear' OR INVOICE_DATE like '%$ToYear')
                    ORDER BY INVOICE_DATE ASC";
          }
        } else {
          if ($AgentId) {
            $query = "SELECT   A.INVOICE_ID,
                      A.INVOICE_DATE,
                    C.AGENT_CODE,
                    B.FITBOOK_ID,
                    B.FITBOOK_CODE,
                    A.INVOICE_STATUS,
                    B.SERVICE_CHARGE,
                    B.COMM_CHARGE,
                    B.PAY_AGT,
                    B.PAY_CC,
                    B.PAY_CLT,
                    B.CITY_CODE
                  FROM PRIVATEINVOICE A, FITBOOK B, AGENT C
                  WHERE B.FITBOOK_ID = A.FITBOOK_ID
                  AND B.AGENT_ID = C.AGENT_ID
                  AND B.AGENT_ID = '$AgentId' AND
                  B.CITY_CODE = '$SearchCityCode' AND
                  INVOICE_DATE >= '$FromDate'
                  ORDER BY INVOICE_DATE ASC";
          } else {
            $query = "SELECT   A.INVOICE_ID,
                      A.INVOICE_DATE,
                    C.AGENT_CODE,
                    B.FITBOOK_ID,
                    B.FITBOOK_CODE,
                    A.INVOICE_STATUS,
                    B.SERVICE_CHARGE,
                    B.COMM_CHARGE,
                    B.PAY_AGT,
                    B.PAY_CC,
                    B.PAY_CLT,
                    B.CITY_CODE
                  FROM PRIVATEINVOICE A, FITBOOK B, AGENT C
                  WHERE B.FITBOOK_ID = A.FITBOOK_ID
                  AND B.AGENT_ID = C.AGENT_ID AND
                  B.CITY_CODE = '$SearchCityCode' AND
                  INVOICE_DATE >= '$FromDate'
                  ORDER BY INVOICE_DATE ASC";
          }
        }
      } else {
        if ($AgentId) {
          $query = "SELECT   A.INVOICE_ID,
                    A.INVOICE_DATE,
                    C.AGENT_CODE,
                    B.FITBOOK_ID,
                    B.FITBOOK_CODE,
                    A.INVOICE_STATUS,
                    B.SERVICE_CHARGE,
                    B.COMM_CHARGE,
                    B.PAY_AGT,
                    B.PAY_CC,
                    B.PAY_CLT,
                    B.CITY_CODE
                  FROM PRIVATEINVOICE A, FITBOOK B, AGENT C
                  WHERE B.FITBOOK_ID = A.FITBOOK_ID
                  AND B.AGENT_ID = C.AGENT_ID
                  AND B.AGENT_ID = '$AgentId' AND
                  B.CITY_CODE = '$SearchCityCode'
                  ORDER BY INVOICE_DATE ASC";
        } else {
          $query = "SELECT   A.INVOICE_ID,
                    A.INVOICE_DATE,
                    C.AGENT_CODE,
                    B.FITBOOK_ID,
                    B.FITBOOK_CODE,
                    A.INVOICE_STATUS,
                    B.SERVICE_CHARGE,
                    B.COMM_CHARGE,
                    B.PAY_AGT,
                    B.PAY_CC,
                    B.PAY_CLT,
                    B.CITY_CODE
                  FROM PRIVATEINVOICE A, FITBOOK B, AGENT C
                  WHERE B.FITBOOK_ID = A.FITBOOK_ID
                  AND B.AGENT_ID = C.AGENT_ID AND
                  B.CITY_CODE = '$SearchCityCode'
                  ORDER BY INVOICE_DATE ASC";
        }
      }
    } else {
      if ($FromDate) {
        if ($ToDate) {
          if ($AgentId) {
            $query = "SELECT   A.INVOICE_ID,
                    A.INVOICE_DATE,
                    C.AGENT_CODE,
                    B.FITBOOK_ID,
                    B.FITBOOK_CODE,
                    A.INVOICE_STATUS,
                    B.SERVICE_CHARGE,
                    B.COMM_CHARGE,
                    B.PAY_AGT,
                    B.PAY_CC,
                    B.PAY_CLT,
                    B.CITY_CODE
                  FROM PRIVATEINVOICE A, FITBOOK B, AGENT C
                  WHERE B.FITBOOK_ID = A.FITBOOK_ID
                  AND B.AGENT_ID = C.AGENT_ID
                  AND B.AGENT_ID = '$AgentId' AND
                  INVOICE_DATE >= '$FromDate'
                  AND INVOICE_DATE <= '$ToDate'
                  AND (INVOICE_DATE like '%$FromYear' OR INVOICE_DATE like '%$ToYear')
                  ORDER BY INVOICE_DATE ASC";
          } else {
            $query = "SELECT   A.INVOICE_ID,
                    A.INVOICE_DATE,
                    C.AGENT_CODE,
                    B.FITBOOK_ID,
                    B.FITBOOK_CODE,
                    A.INVOICE_STATUS,
                    B.SERVICE_CHARGE,
                    B.COMM_CHARGE,
                    B.PAY_AGT,
                    B.PAY_CC,
                    B.PAY_CLT,
                    B.CITY_CODE
                  FROM PRIVATEINVOICE A, FITBOOK B, AGENT C
                  WHERE B.FITBOOK_ID = A.FITBOOK_ID
                  AND B.AGENT_ID = C.AGENT_ID AND
                  INVOICE_DATE >= '$FromDate'
                  AND INVOICE_DATE <= '$ToDate'
                  AND (INVOICE_DATE like '%$FromYear' OR INVOICE_DATE like '%$ToYear')
                  ORDER BY INVOICE_DATE ASC";
          }
        } else {
          if ($AgentId) {
            $query = "SELECT   A.INVOICE_ID,
                    A.INVOICE_DATE,
                    C.AGENT_CODE,
                    B.FITBOOK_ID,
                    B.FITBOOK_CODE,
                    A.INVOICE_STATUS,
                    B.SERVICE_CHARGE,
                    B.COMM_CHARGE,
                    B.PAY_AGT,
                    B.PAY_CC,
                    B.PAY_CLT,
                    B.CITY_CODE
                  FROM PRIVATEINVOICE A, FITBOOK B, AGENT C
                  WHERE B.FITBOOK_ID = A.FITBOOK_ID
                  AND B.AGENT_ID = C.AGENT_ID
                  AND B.AGENT_ID = '$AgentId' AND
                  INVOICE_DATE >= '$FromDate'
                  ORDER BY INVOICE_DATE ASC";
          } else {
            $query = "SELECT   A.INVOICE_ID,
                    A.INVOICE_DATE,
                    C.AGENT_CODE,
                    B.FITBOOK_ID,
                    B.FITBOOK_CODE,
                    A.INVOICE_STATUS,
                    B.SERVICE_CHARGE,
                    B.COMM_CHARGE,
                    B.PAY_AGT,
                    B.PAY_CC,
                    B.PAY_CLT,
                    B.CITY_CODE
                  FROM PRIVATEINVOICE A, FITBOOK B, AGENT C
                  WHERE B.FITBOOK_ID = A.FITBOOK_ID
                  AND B.AGENT_ID = C.AGENT_ID AND
                  INVOICE_DATE >= '$FromDate'
                  ORDER BY INVOICE_DATE ASC";
          }
        }
      } else {
        if ($AgentId) {
          $query = "SELECT   A.INVOICE_ID,
                    A.INVOICE_DATE,
                    C.AGENT_CODE,
                    B.FITBOOK_ID,
                    B.FITBOOK_CODE,
                    A.INVOICE_STATUS,
                    B.SERVICE_CHARGE,
                    B.COMM_CHARGE,
                    B.PAY_AGT,
                    B.PAY_CC,
                    B.PAY_CLT,
                    B.CITY_CODE
                  FROM PRIVATEINVOICE A, FITBOOK B, AGENT C
                  WHERE B.FITBOOK_ID = A.FITBOOK_ID
                  AND B.AGENT_ID = C.AGENT_ID
                  AND B.AGENT_ID = '$AgentId'
                  ORDER BY INVOICE_DATE ASC";
        } else {
          $query = "SELECT   A.INVOICE_ID,
                    A.INVOICE_DATE,
                    C.AGENT_CODE,
                    B.FITBOOK_ID,
                    B.FITBOOK_CODE,
                    A.INVOICE_STATUS,
                    B.SERVICE_CHARGE,
                    B.COMM_CHARGE,
                    B.PAY_AGT,
                    B.PAY_CC,
                    B.PAY_CLT,
                    B.CITY_CODE
                  FROM PRIVATEINVOICE A, FITBOOK B, AGENT C
                  WHERE B.FITBOOK_ID = A.FITBOOK_ID
                  AND B.AGENT_ID = C.AGENT_ID
                  ORDER BY INVOICE_DATE ASC";
        }
      }
    }
//    print($query);
      $mysql_result = mysql_query($query, $mysql_link);
      $numrows = mysql_num_rows($mysql_result);
      if ($numrows > 0) {
        echo "<CENTER><b><font size='+1'>Private Services Invoice List</font></b></CENTER>\n";
        echo "<TABLE ALIGN='CENTER' WIDTH='100%'>";
        echo "<TR>\n";
        if ((!$FromDate) AND (!$ToDate)) {
          echo "<TD ALIGN='center' valign='top' colspan='2'><b>Date Range:</b></TD>";
          echo "<TD ALIGN='center' valign='top' colspan='11'><b>$FromDate - $ToDate</b></TD>";
        } else {
          if (!$FromDate) {
            $FromDate = "ALL";
          }
          if (!$ToDate) {
            $ToDate = sprintf("%s", date("m/d/Y"));
          }
          echo "<TD ALIGN='center' valign='top' colspan='2'><b>Date Range:</b></TD>";
          echo "<TD ALIGN='center' valign='top' colspan='11'><b>$FromDate - $ToDate</b></TD>";
        }
        echo "</TR>\n";
      echo "<TR>";
      echo "<TD ALIGN='center' valign='top'><b><u>Inv.#</u></b></TD>";
      echo "<TD ALIGN='center' valign='top'><b><u>Date</u></b></TD>";
      echo "<TD ALIGN='center' valign='top'><b><u>Agent</u></b></TD>";
      echo "<TD ALIGN='center' valign='top'><b><u>City</u></b></TD>";
      echo "<TD ALIGN='center' valign='top'><b><u>Trip</u></b></TD>";
      echo "<TD ALIGN='center' valign='top'><b><u>FIT #</u></b></TD>";
      echo "<TD ALIGN='center' valign='top'><b><u>Status</u></b></TD>";
      echo "<TD ALIGN='center' valign='top'><b><u>Total Chg</u></b></TD>";
      echo "<TD ALIGN='center' valign='top'><b><u>Pd CLT</u></b></TD>";
      echo "<TD ALIGN='center' valign='top'><b><u>CC</u></b></TD>";
      echo "<TD ALIGN='center' valign='top'><b><u>Pd Agent</u></b></TD>";
      echo "<TD ALIGN='center' valign='top'><b><u>Comm.</u></b></TD>";
      echo "<TD ALIGN='center' valign='top'><b><u>Bal.Due</u></b></TD>";
      echo "</TR>";
        $TotalInvoiceAmount = 0;
        $TotalCCAmount = 0;
        $TotalCLTAmount = 0;
        $TotalServiceAmount = 0;
        $TotalCommAmount = 0;
        $TotalAGTAmount = 0;
        while ($row = mysql_fetch_array($mysql_result)) {
          $InvoiceId       = $row[0];
          $InvoiceDate     = $row[1];
          $InvoiceReference   = $row[2];
          $FITBookId       = $row[3];
          $InvoiceService   = $row[4];
          $InvoiceStatus     = $row[5];
          $ServiceAmount     = $row[6];
          $CommAmount     = $row[7];
          $PayAGTAmount     = $row[8];
          $PayCCAmount     = $row[9];
          $PayCLTAmount     = $row[10];
          $CityCode       = $row[11];
          $InvoiceAmount = $ServiceAmount - $CommAmount - $PayCCAmount - $PayCLTAmount;
        if ($InvoiceStatus == "VOID") {
          $InvoiceAmount = 0;
          $ServiceAmount = 0;
          $PayCCAmount = 0;
          $PayCLTAmount = 0;
          $CommAmount = 0;
          $PayAGTAmount = 0;
        }
          $TotalInvoiceAmount = $TotalInvoiceAmount + $InvoiceAmount;
          $TotalCommAmount     = $TotalCommAmount + $CommAmount;
          $TotalAGTAmount     = $TotalAGTAmount + $PayAGTAmount;
          $TotalCCAmount     = $TotalCCAmount + $PayCCAmount;
          $TotalCLTAmount   = $TotalCLTAmount + $PayCLTAmount;
          $TotalServiceAmount = $TotalServiceAmount + $ServiceAmount;
          $InvoiceAmount = sprintf("%5.2f", $InvoiceAmount);
          $ServiceAmount = sprintf("%5.2f", $ServiceAmount);
          $PayCCAmount = sprintf("%5.2f", $PayCCAmount);
          $PayCLTAmount = sprintf("%5.2f", $PayCLTAmount);
          $CommAmount = sprintf("%5.2f", $CommAmount);
          $PayAGTAmount = sprintf("%5.2f", $PayAGTAmount);
          echo "<TR>";
          echo "<TD ALIGN='right' valign='top'><Font color='black' size='-1' face='arial'>$InvoiceId</font></TD>";
          echo "<TD ALIGN='right' valign='top'><Font color='black' size='-1' face='arial'>$InvoiceDate</font></TD>";
          echo "<TD ALIGN='right' valign='top'><Font color='black' size='-1' face='arial'>$InvoiceReference</font></TD>";
          echo "<TD ALIGN='right' valign='top'><Font color='black' size='-1' face='arial'>$CityCode</font></TD>";
          echo "<TD ALIGN='right' valign='top'><Font color='black' size='-2' face='arial'>$InvoiceService</font></TD>";
          echo "<TD ALIGN='right' valign='top'><Font color='black' size='-1' face='arial'>$FITBookId</font></TD>";
          echo "<TD ALIGN='right' valign='top'><Font color='black' size='-1' face='arial'>$InvoiceStatus</font></TD>";
          echo "<TD ALIGN='right' valign='top'><Font color='black' size='-1' face='arial'>$ServiceAmount</font></TD>";
          echo "<TD ALIGN='right' valign='top'><Font color='black' size='-1' face='arial'>$PayCLTAmount</font></TD>";
          echo "<TD ALIGN='right' valign='top'><Font color='black' size='-1' face='arial'>$PayCCAmount</font></TD>";
          echo "<TD ALIGN='right' valign='top'><Font color='black' size='-1' face='arial'>$PayAGTAmount</font></TD>";
          echo "<TD ALIGN='right' valign='top'><Font color='black' size='-1' face='arial'>$CommAmount</font></TD>";
          echo "<TD ALIGN='right' valign='top'><Font color='black' size='-1' face='arial'>$InvoiceAmount</font></TD>";
          echo "</TR>";
        }
          $TotalInvoiceAmount = sprintf("%5.2f", $TotalInvoiceAmount);
          $TotalCommAmount   = sprintf("%5.2f", $TotalCommAmount);
          $TotalAGTAmount   = sprintf("%5.2f", $TotalAGTAmount);
          $TotalCCAmount     = sprintf("%5.2f", $TotalCCAmount);
          $TotalCLTAmount   = sprintf("%5.2f", $TotalCLTAmount);
          $TotalServiceAmount = sprintf("%5.2f", $TotalServiceAmount);
          echo "<TR><td colspan='13'><hr></td></TR><TR>\n";
          echo "<TD ALIGN='right' valign='top'><Font color='black' size='-1'></font></TD>";
          echo "<TD ALIGN='right' valign='top' colspan='2'><Font color='black' size='-1' face='arial'></font></TD>";
          echo "<TD ALIGN='right' valign='top' colspan='3'><Font color='black' size='-1' face='arial'></font></TD>";
          echo "<TD ALIGN='right' valign='top'><Font color='black' face='arial' size='-1'><b>Total:</b></font></TD>";
          echo "<TD ALIGN='right' valign='top'><Font color='black' face='arial' size='-1'><b>$TotalServiceAmount</b></font></TD>";
          echo "<TD ALIGN='right' valign='top'><Font color='black' face='arial' size='-1'><b>$TotalCLTAmount</b></font></TD>";
          echo "<TD ALIGN='right' valign='top'><Font color='black' face='arial' size='-1'><b>$TotalCCAmount</b></font></TD>";
          echo "<TD ALIGN='right' valign='top'><Font color='black' face='arial' size='-1'><b>$TotalAGTAmount</b></font></TD>";
          echo "<TD ALIGN='right' valign='top'><Font color='black' face='arial' size='-1'><b>$TotalCommAmount</b></font></TD>";
          echo "<TD ALIGN='right' valign='top'><Font color='black' face='arial' size='-1'><b>$TotalInvoiceAmount</b></font></TD>";
          echo "</TR>";
        echo "</table>\n";
      } else {
        echo "<br>";
        echo "*** PRIVATE SERVICE INVOICES -- NO MATCHES ***";
      }
  }
  echo "</form>\n";
} else {
  if ($InvoiceTypeId == '2') {
    if ($CityId) {
      $query_city_code = "SELECT CITY_CODE FROM CITY WHERE CITY_ID = '$CityId'";
      $query_city_result = mysql_query($query_city_code,$mysql_link);
      $row_city = mysql_fetch_array($query_city_result);
      $SearchCityCode = $row_city[0];
      if ($FromDate) {
        if ($ToDate) {
          if ($AgentId) {
            $query = "SELECT   A.INVOICE_ID,
                    A.INVOICE_DATE,
                    C.AGENT_CODE,
                    B.FITBOOK_ID,
                    B.FITBOOK_CODE,
                    A.INVOICE_STATUS,
                    B.SERVICE_CHARGE,
                    B.COMM_CHARGE,
                    B.PAY_AGT,
                    B.PAY_CC,
                    B.PAY_CLT,
                    B.CITY_CODE
                  FROM PRIVATEINVOICE A, FITBOOK B, AGENT C
                  WHERE B.FITBOOK_ID = A.FITBOOK_ID
                  AND B.AGENT_ID = C.AGENT_ID
                  AND B.AGENT_ID = '$AgentId' AND
                  B.CITY_CODE = '$SearchCityCode' AND
                  INVOICE_DATE >= '$FromDate'
                  AND INVOICE_DATE <= '$ToDate'
                  AND (INVOICE_DATE like '%$FromYear' OR INVOICE_DATE like '%$ToYear')
                  ORDER BY INVOICE_DATE ASC";
          } else {
            $query = "SELECT   A.INVOICE_ID,
                    A.INVOICE_DATE,
                    C.AGENT_CODE,
                    B.FITBOOK_ID,
                    B.FITBOOK_CODE,
                    A.INVOICE_STATUS,
                    B.SERVICE_CHARGE,
                    B.COMM_CHARGE,
                    B.PAY_AGT,
                    B.PAY_CC,
                    B.PAY_CLT,
                    B.CITY_CODE
                  FROM PRIVATEINVOICE A, FITBOOK B, AGENT C
                  WHERE B.FITBOOK_ID = A.FITBOOK_ID
                  AND B.AGENT_ID = C.AGENT_ID AND
                  B.CITY_CODE = '$SearchCityCode' AND
                  INVOICE_DATE >= '$FromDate'
                  AND INVOICE_DATE <= '$ToDate'
                  AND (INVOICE_DATE like '%$FromYear' OR INVOICE_DATE like '%$ToYear')
                  ORDER BY INVOICE_DATE ASC";
          }
        } else {
          if ($AgentId) {
            $query = "SELECT   A.INVOICE_ID,
                    A.INVOICE_DATE,
                    C.AGENT_CODE,
                    B.FITBOOK_ID,
                    B.FITBOOK_CODE,
                    A.INVOICE_STATUS,
                    B.SERVICE_CHARGE,
                    B.COMM_CHARGE,
                    B.PAY_AGT,
                    B.PAY_CC,
                    B.PAY_CLT,
                    B.CITY_CODE
                  FROM PRIVATEINVOICE A, FITBOOK B, AGENT C
                  WHERE B.FITBOOK_ID = A.FITBOOK_ID
                  AND B.AGENT_ID = C.AGENT_ID
                  AND B.AGENT_ID = '$AgentId' AND
                  B.CITY_CODE = '$SearchCityCode' AND
                  INVOICE_DATE >= '$FromDate'
                  ORDER BY INVOICE_DATE ASC";
          } else {
            $query = "SELECT   A.INVOICE_ID,
                    A.INVOICE_DATE,
                    C.AGENT_CODE,
                    B.FITBOOK_ID,
                    B.FITBOOK_CODE,
                    A.INVOICE_STATUS,
                    B.SERVICE_CHARGE,
                    B.COMM_CHARGE,
                    B.PAY_AGT,
                    B.PAY_CC,
                    B.PAY_CLT,
                    B.CITY_CODE
                  FROM PRIVATEINVOICE A, FITBOOK B, AGENT C
                  WHERE B.FITBOOK_ID = A.FITBOOK_ID
                  AND B.AGENT_ID = C.AGENT_ID AND
                  B.CITY_CODE = '$SearchCityCode' AND
                  INVOICE_DATE >= '$FromDate'
                  ORDER BY INVOICE_DATE ASC";
          }
        }
      } else {
        if ($AgentId) {
          $query = "SELECT   A.INVOICE_ID,
                    A.INVOICE_DATE,
                    C.AGENT_CODE,
                    B.FITBOOK_ID,
                    B.FITBOOK_CODE,
                    A.INVOICE_STATUS,
                    B.SERVICE_CHARGE,
                    B.COMM_CHARGE,
                    B.PAY_AGT,
                    B.PAY_CC,
                    B.PAY_CLT,
                    B.CITY_CODE
                  FROM PRIVATEINVOICE A, FITBOOK B, AGENT C
                  WHERE B.FITBOOK_ID = A.FITBOOK_ID
                  AND B.AGENT_ID = C.AGENT_ID
                  AND B.AGENT_ID = '$AgentId' AND
                  B.CITY_CODE = '$SearchCityCode'
                  ORDER BY INVOICE_DATE ASC";
        } else {
          $query = "SELECT   A.INVOICE_ID,
                    A.INVOICE_DATE,
                    C.AGENT_CODE,
                    B.FITBOOK_ID,
                    B.FITBOOK_CODE,
                    A.INVOICE_STATUS,
                    B.SERVICE_CHARGE,
                    B.COMM_CHARGE,
                    B.PAY_AGT,
                    B.PAY_CC,
                    B.PAY_CLT,
                    B.CITY_CODE
                  FROM PRIVATEINVOICE A, FITBOOK B, AGENT C
                  WHERE B.FITBOOK_ID = A.FITBOOK_ID
                  AND B.AGENT_ID = C.AGENT_ID AND
                  B.CITY_CODE = '$SearchCityCode'
                  ORDER BY INVOICE_DATE ASC";
        }
      }
    } else {
      if ($FromDate) {
        if ($ToDate) {
          if ($AgentId) {
            $query = "SELECT   A.INVOICE_ID,
                    A.INVOICE_DATE,
                    C.AGENT_CODE,
                    B.FITBOOK_ID,
                    B.FITBOOK_CODE,
                    A.INVOICE_STATUS,
                    B.SERVICE_CHARGE,
                    B.COMM_CHARGE,
                    B.PAY_AGT,
                    B.PAY_CC,
                    B.PAY_CLT,
                    B.CITY_CODE
                  FROM PRIVATEINVOICE A, FITBOOK B, AGENT C
                  WHERE B.FITBOOK_ID = A.FITBOOK_ID
                  AND B.AGENT_ID = C.AGENT_ID
                  AND B.AGENT_ID = '$AgentId' AND
                  INVOICE_DATE >= '$FromDate'
                  AND INVOICE_DATE <= '$ToDate'
                  AND (INVOICE_DATE like '%$FromYear' OR INVOICE_DATE like '%$ToYear')
                  ORDER BY INVOICE_DATE ASC";
          } else {
            $query = "SELECT   A.INVOICE_ID,
                    A.INVOICE_DATE,
                    C.AGENT_CODE,
                    B.FITBOOK_ID,
                    B.FITBOOK_CODE,
                    A.INVOICE_STATUS,
                    B.SERVICE_CHARGE,
                    B.COMM_CHARGE,
                    B.PAY_AGT,
                    B.PAY_CC,
                    B.PAY_CLT,
                    B.CITY_CODE
                  FROM PRIVATEINVOICE A, FITBOOK B, AGENT C
                  WHERE B.FITBOOK_ID = A.FITBOOK_ID
                  AND B.AGENT_ID = C.AGENT_ID AND
                  INVOICE_DATE >= '$FromDate'
                  AND INVOICE_DATE <= '$ToDate'
                  AND (INVOICE_DATE like '%$FromYear' OR INVOICE_DATE like '%$ToYear')
                  ORDER BY INVOICE_DATE ASC";
          }
        } else {
          if ($AgentId) {
            $query = "SELECT   A.INVOICE_ID,
                    A.INVOICE_DATE,
                    C.AGENT_CODE,
                    B.FITBOOK_ID,
                    B.FITBOOK_CODE,
                    A.INVOICE_STATUS,
                    B.SERVICE_CHARGE,
                    B.COMM_CHARGE,
                    B.PAY_AGT,
                    B.PAY_CC,
                    B.PAY_CLT,
                    B.CITY_CODE
                  FROM PRIVATEINVOICE A, FITBOOK B, AGENT C
                  WHERE B.FITBOOK_ID = A.FITBOOK_ID
                  AND B.AGENT_ID = C.AGENT_ID
                  AND B.AGENT_ID = '$AgentId' AND
                  INVOICE_DATE >= '$FromDate'
                  ORDER BY INVOICE_DATE ASC";
          } else {
            $query = "SELECT   A.INVOICE_ID,
                      A.INVOICE_DATE,
                    C.AGENT_CODE,
                    B.FITBOOK_ID,
                    B.FITBOOK_CODE,
                    A.INVOICE_STATUS,
                    B.SERVICE_CHARGE,
                    B.COMM_CHARGE,
                    B.PAY_AGT,
                    B.PAY_CC,
                    B.PAY_CLT,
                    B.CITY_CODE
                  FROM PRIVATEINVOICE A, FITBOOK B, AGENT C
                  WHERE B.FITBOOK_ID = A.FITBOOK_ID
                  AND B.AGENT_ID = C.AGENT_ID AND
                  INVOICE_DATE >= '$FromDate'
                  ORDER BY INVOICE_DATE ASC";
          }
        }
      } else {
        if ($AgentId) {
          $query = "SELECT   A.INVOICE_ID,
                    A.INVOICE_DATE,
                    C.AGENT_CODE,
                    B.FITBOOK_ID,
                    B.FITBOOK_CODE,
                    A.INVOICE_STATUS,
                    B.SERVICE_CHARGE,
                    B.COMM_CHARGE,
                    B.PAY_AGT,
                    B.PAY_CC,
                    B.PAY_CLT,
                    B.CITY_CODE
                  FROM PRIVATEINVOICE A, FITBOOK B, AGENT C
                  WHERE B.FITBOOK_ID = A.FITBOOK_ID
                  AND B.AGENT_ID = C.AGENT_ID
                  AND B.AGENT_ID = '$AgentId'
                  ORDER BY INVOICE_DATE ASC";
        } else {
          $query = "SELECT   A.INVOICE_ID,
                    A.INVOICE_DATE,
                    C.AGENT_CODE,
                    B.FITBOOK_ID,
                    B.FITBOOK_CODE,
                    A.INVOICE_STATUS,
                    B.SERVICE_CHARGE,
                    B.COMM_CHARGE,
                    B.PAY_AGT,
                    B.PAY_CC,
                    B.PAY_CLT,
                    B.CITY_CODE
                  FROM PRIVATEINVOICE A, FITBOOK B, AGENT C
                  WHERE B.FITBOOK_ID = A.FITBOOK_ID
                  AND B.AGENT_ID = C.AGENT_ID
                  ORDER BY INVOICE_DATE ASC";
        }
      }
    }
    //print($query);
    $mysql_result = mysql_query($query, $mysql_link);
    $numrows = mysql_num_rows($mysql_result);
    if ($numrows > 0) {
      // print fetched rows
      $current_time = getdate(time());
      $current_hours = $current_time["hours"];
      $current_mins = $current_time["minutes"];
      $current_secs = $current_time["seconds"];
      $current_date = date("Y-m-d");
      $TimeStamp = sprintf("%s %d:%d:%d",$current_date,$current_hours,$current_mins,$current_secs);
      echo "<CENTER><b><font size='+1'>Private Services Invoice List</font></b></CENTER>\n";
      echo "<center><font size='-1' color='blue'>(-- Created $TimeStamp --)</font></center>\n";
      echo "<TABLE ALIGN='CENTER' WIDTH='100%'>";
      echo "<TR>\n";
        if ((!$FromDate) AND (!$ToDate)) {
          echo "<TD ALIGN='center' valign='top' colspan='2'><b>Date Range:</b></TD>";
          echo "<TD ALIGN='center' valign='top' colspan='11'><b>$FromDate - $ToDate</b></TD>";
        } else {
          if (!$FromDate) {
            $FromDate = "ALL";
          }
          if (!$ToDate) {
            $ToDate = sprintf("%s", date("m/d/Y"));
          }
          echo "<TD ALIGN='center' valign='top' colspan='2'><b>Date Range:</b></TD>";
          echo "<TD ALIGN='center' valign='top' colspan='11'><b>$FromDate - $ToDate</b></TD>";
        }
      echo "</TR>\n";
      echo "<TR>";
      echo "<TD ALIGN='center' valign='top'><b><u>Inv.#</u></b></TD>";
      echo "<TD ALIGN='center' valign='top'><b><u>Date</u></b></TD>";
      echo "<TD ALIGN='center' valign='top'><b><u>Agent</u></b></TD>";
      echo "<TD ALIGN='center' valign='top'><b><u>City</u></b></TD>";
      echo "<TD ALIGN='center' valign='top'><b><u>Trip</u></b></TD>";
      echo "<TD ALIGN='center' valign='top'><b><u>FIT #</u></b></TD>";
      echo "<TD ALIGN='center' valign='top'><b><u>Status</u></b></TD>";
      echo "<TD ALIGN='center' valign='top'><b><u>Total Chg</u></b></TD>";
      echo "<TD ALIGN='center' valign='top'><b><u>Pd CLT</u></b></TD>";
      echo "<TD ALIGN='center' valign='top'><b><u>CC</u></b></TD>";
      echo "<TD ALIGN='center' valign='top'><b><u>Pd Agent</u></b></TD>";
      echo "<TD ALIGN='center' valign='top'><b><u>Comm.</u></b></TD>";
      echo "<TD ALIGN='center' valign='top'><b><u>Bal.Due</u></b></TD>";
      echo "</TR>";
        $TotalInvoiceAmount = 0;
        $TotalCCAmount = 0;
        $TotalCLTAmount = 0;
        $TotalServiceAmount = 0;
        $TotalCommAmount = 0;
        $TotalAGTAmount = 0;
        while ($row = mysql_fetch_array($mysql_result)) {
          $InvoiceId       = $row[0];
          $InvoiceDate     = $row[1];
          $InvoiceReference   = $row[2];
          $FITBookId       = $row[3];
          $InvoiceService   = $row[4];
          $InvoiceStatus     = $row[5];
          $ServiceAmount     = $row[6];
          $CommAmount     = $row[7];
          $PayAGTAmount     = $row[8];
          $PayCCAmount     = $row[9];
          $PayCLTAmount     = $row[10];
          $CityCode       = $row[11];
          $InvoiceAmount = $ServiceAmount - $CommAmount - $PayCCAmount - $PayCLTAmount;
        if ($InvoiceStatus == "VOID") {
          $InvoiceAmount = 0;
          $ServiceAmount = 0;
          $PayCCAmount = 0;
          $PayCLTAmount = 0;
          $CommAmount = 0;
          $PayAGTAmount = 0;
        }
          $TotalInvoiceAmount = $TotalInvoiceAmount + $InvoiceAmount;
          $TotalCommAmount     = $TotalCommAmount + $CommAmount;
          $TotalAGTAmount     = $TotalAGTAmount + $PayAGTAmount;
          $TotalCCAmount     = $TotalCCAmount + $PayCCAmount;
          $TotalCLTAmount   = $TotalCLTAmount + $PayCLTAmount;
          $TotalServiceAmount = $TotalServiceAmount + $ServiceAmount;
          $InvoiceAmount = sprintf("%5.2f", $InvoiceAmount);
          $ServiceAmount = sprintf("%5.2f", $ServiceAmount);
          $PayCCAmount = sprintf("%5.2f", $PayCCAmount);
          $PayCLTAmount = sprintf("%5.2f", $PayCLTAmount);
          $CommAmount = sprintf("%5.2f", $CommAmount);
          $PayAGTAmount = sprintf("%5.2f", $PayAGTAmount);
          echo "<TR>";
          echo "<TD ALIGN='right' valign='top'><Font color='black' size='-1' face='arial'>$InvoiceId</font></TD>";
          echo "<TD ALIGN='right' valign='top'><Font color='black' size='-1' face='arial'>$InvoiceDate</font></TD>";
          echo "<TD ALIGN='right' valign='top'><Font color='black' size='-1' face='arial'>$InvoiceReference</font></TD>";
          echo "<TD ALIGN='right' valign='top'><Font color='black' size='-1' face='arial'>$CityCode</font></TD>";
          echo "<TD ALIGN='right' valign='top'><Font color='black' size='-2' face='arial'>$InvoiceService</font></TD>";
          echo "<TD ALIGN='right' valign='top'><Font color='black' size='-1' face='arial'>$FITBookId</font></TD>";
          echo "<TD ALIGN='right' valign='top'><Font color='black' size='-1' face='arial'>$InvoiceStatus</font></TD>";
          echo "<TD ALIGN='right' valign='top'><Font color='black' size='-1' face='arial'>$ServiceAmount</font></TD>";
          echo "<TD ALIGN='right' valign='top'><Font color='black' size='-1' face='arial'>$PayCLTAmount</font></TD>";
          echo "<TD ALIGN='right' valign='top'><Font color='black' size='-1' face='arial'>$PayCCAmount</font></TD>";
          echo "<TD ALIGN='right' valign='top'><Font color='black' size='-1' face='arial'>$PayAGTAmount</font></TD>";
          echo "<TD ALIGN='right' valign='top'><Font color='black' size='-1' face='arial'>$CommAmount</font></TD>";
          echo "<TD ALIGN='right' valign='top'><Font color='black' size='-1' face='arial'>$InvoiceAmount</font></TD>";
          echo "</TR>";
        }
          $TotalInvoiceAmount = sprintf("%5.2f", $TotalInvoiceAmount);
          $TotalCommAmount   = sprintf("%5.2f", $TotalCommAmount);
          $TotalAGTAmount   = sprintf("%5.2f", $TotalAGTAmount);
          $TotalCCAmount     = sprintf("%5.2f", $TotalCCAmount);
          $TotalCLTAmount   = sprintf("%5.2f", $TotalCLTAmount);
          $TotalServiceAmount = sprintf("%5.2f", $TotalServiceAmount);
          echo "<TR><td colspan='13'><hr></td></TR><TR>\n";
          echo "<TD ALIGN='right' valign='top'><Font color='black' size='-1'></font></TD>\n";
          echo "<TD ALIGN='right' valign='top' colspan='2'><Font color='black' size='-1' face='arial'></font></TD>\n";
          echo "<TD ALIGN='right' valign='top' colspan='3'><Font color='black' size='-1' face='arial'></font></TD>\n";
          echo "<TD ALIGN='right' valign='top'><Font color='black' face='arial' size='-1'><b>Total:</b></font></TD>\n";
          echo "<TD ALIGN='right' valign='top'><Font color='black' face='arial' size='-1'><b>$TotalServiceAmount</b></font></TD>\n";
          echo "<TD ALIGN='right' valign='top'><Font color='black' face='arial' size='-1'><b>$TotalCLTAmount</b></font></TD>\n";
          echo "<TD ALIGN='right' valign='top'><Font color='black' face='arial' size='-1'><b>$TotalCCAmount</b></font></TD>\n";
          echo "<TD ALIGN='right' valign='top'><Font color='black' face='arial' size='-1'><b>$TotalAGTAmount</b></font></TD>\n";
          echo "<TD ALIGN='right' valign='top'><Font color='black' face='arial' size='-1'><b>$TotalCommAmount</b></font></TD>\n";
          echo "<TD ALIGN='right' valign='top'><Font color='black' face='arial' size='-1'><b>$TotalInvoiceAmount</b></font></TD></TR>\n";
          echo "</table>\n";
    }
  } else {
    echo "<br>*** PRIVATE SERVICE INVOICES -- NO MATCHES ***";
  }
}
?>
</BODY>
</HTML>