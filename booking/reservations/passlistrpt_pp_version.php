<HTML>
<HEAD>
<TITLE>Passenger List</TITLE>
<BODY bgcolor="#fffff0">
<SCRIPT LANGUAGE = "JavaScript">
   var strArgString = document.location.search;
   var strLevel = strArgString.substr(6);
</SCRIPT>
<?php

include ("parsefuncs.php");


$mysql_link = connect_to_db();
$mysql_result = select_db($mysql_link);

// print fetched rows

print("<SCRIPT LANGUAGE = 'JavaScript'>\n");
print("if (strLevel == \"1\")\n");
print("document.write(\"<a href='passlistrptmain.php?args=1'><b><font size='-1' color='blue'>Back to Passenger List Search</font></b></a>\");\n");
print("if (strLevel == \"2\")\n");
print("document.write(\"<a href='passlistrptmain.php?args=2'><b><font size='-1' color='blue'>Back to Passenger List Search</font></b></a>\");\n");
print("if (strLevel == \"4\")\n");
print("document.write(\"<a href='passlistrptmain.php?args=4'><b><font size='-1' color='blue'>Back to Passenger List Search</font></b></a>\");\n");
print("</SCRIPT>\n");


include ("catourheader.php");

$scheduledtour_id = @$_REQUEST['ScheduledTourId'];
$pulocation_id    = @$_REQUEST['PULocationId'];
$agent_id         = @$_REQUEST['AgentId'];


$start_time = getmicrotime();

$query = "select SB.FIRST_NAME, SB.LAST_NAME, SB.GENDER, SB.OCCUPANCY, T.TOUR_NAME,
          P.PULOCATION_LOCATION, P.PULOCATION_TIMESTAMP, A.AGENT_NAME, S.SCHEDULEDTOUR_CODE,
	  S.SCHEDULEDTOUR_DEPART_DATE FROM 
	  SCHEDULEDTOURBOOK AS SB, TOUR AS T, PULOCATION AS P, AGENT AS A, 
	  SCHEDULEDTOUR AS S where SB.SCHEDULEDTOUR_ID = S.SCHEDULEDTOUR_ID 
	  AND S.TOUR_ID = T.TOUR_ID AND SB.PULOCATION_ID = P.PULOCATION_ID 
	  AND SB.AGENT_ID = A.AGENT_ID ";

if(array_key_exists('passengerlist', $_GET) && $_GET['passengerlist'] == 'upcoming') {
	$query .= "AND S.SCHEDULEDTOUR_DEPART_DATE >= '" . date("Y-m-d") . "' ";		  
}

if($scheduledtour_id) {
  $query .= "AND S.SCHEDULEDTOUR_ID = $scheduledtour_id ";
}

if($pulocation_id) {
  $query .= "AND P.PULOCATION_ID = $pulocation_id ";  
}

if($agent_id) {
  $query .= "AND A.AGENT_ID = $agent_id";
}

$query .= " order by TOUR_NAME, SCHEDULEDTOUR_CODE, PULOCATION_LOCATION, AGENT_NAME, LAST_NAME, FIRST_NAME";

$result = mysql_query($query);

$occupancy = array('4' => 'Quad', '3' => 'Triple', '2' => 'Double', '1' => 'Single', '0' => '');

$count   = 0;
$st_code = '';
ob_start();
echo '<table border=0>';
while($info = mysql_fetch_array($result)) {
     $count++;

  if($st_code != $info['SCHEDULEDTOUR_CODE']) {
     echo '<tr><td colspan=6></td></tr>';
     echo '<tr><td valign=top>Tour Name:<br><font color=\'blue\' size=\'-1\'>' . $info['TOUR_NAME']. '<br>' . $info['SCHEDULEDTOUR_CODE'] . '&nbsp;</font></td>' .
              '<td colspan=5 valign=top>Depart Date:<br><font color=\'blue\' size=\'-1\'>' . $info['SCHEDULEDTOUR_DEPART_DATE']. '&nbsp;</font></td>' . 
          '</tr>' .
	  '<tr><td>&nbsp;</td><td>Pick Up Location</td><td>Name:</td><td>Gender:</td><td>Agent Name:</td><td>Occupancy:</td></tr>';
     $st_code = $info['SCHEDULEDTOUR_CODE'];
  }
     echo '<tr><td>&nbsp;</td>' .
              '<td><font color=\'blue\' size=\'-1\'>' . $info['PULOCATION_LOCATION'] . '&nbsp;</font></td>' . 
              '<td><font color=\'blue\' size=\'-1\'>' . $info['LAST_NAME']. ', ' . $info['FIRST_NAME'] . '&nbsp;</font></td>' .
	      '<td><font color=\'blue\' size=\'-1\'>' . $info['GENDER']. '&nbsp;</font></td>' .
	      '<td><font color=\'blue\' size=\'-1\'>' . $info['AGENT_NAME'] . '&nbsp;</font></td>' .
	      '<td><font color=\'blue\' size=\'-1\'>' . $occupancy[$info['OCCUPANCY']]. '&nbsp;</font></td>' .
          '</tr>';
}
echo '</table>';
//$content = ob_get_clean();

//echo $content;
echo "<div align='center'> There are total " . $count . " record(s)</div>";

$end_time = getmicrotime();
echo "<div align='center' ><font color='blue'>Page executed in ".(substr($end_time-$start_time,0,5))." seconds.</font></div>";
function getmicrotime()
{
  list($usec, $sec) = explode(" ",microtime());
  return ((float)$usec + (float)$sec);
} 
?>

</BODY>
</HTML>