


<?

// open a connection to mysql and select the "deltours" database


	include ("parsefuncs.php");

	$mysql_link = connect_to_db();
	$mysql_query = select_db($mysql_link);

// print fetched rows



	/*
	** open file for writing
	*/


	$current_time = getdate(time());
	$current_hours = $current_time["hours"];
	$current_mins = $current_time["minutes"];
	$current_secs = $current_time["seconds"];
	$current_date = date("Y-m-d");

	$TimeStamp = sprintf("%s %d:%d:%d",$current_date,$current_hours,$current_mins,$current_secs);

	$NewFileName = sprintf("cashrecrpt-%s.htm",$current_date);
	$NewFileNamePath = sprintf("/home/califor/public_html/printouts/%s",$NewFileName);


	$SavedFile = fopen($NewFileNamePath,"w");

	/*
	** make sure the open was successful
	*/
	if(!($SavedFile))
	{
		fputs($SavedFile,"Error: ");
		fputs($SavedFile,"'printout.txt' could not be created\n");
		exit;
	}

include ("catourheadertofile.php");


		// construct SQL clause from Search query

		if (($AgentId) OR ($ScheduledTourId))
		{
			if (($FromDepositDate) OR ($ToDepositDate))
			{
				$queryinfix = "FROM CASHRECEIPT, DEPOSIT, INVOICE WHERE ";
			}
			else
			{
				$queryinfix = "FROM CASHRECEIPT, INVOICE WHERE ";
			}
		}
		else
		{
			if (($FromDepositDate) OR ($ToDepositDate))
			{
				$queryinfix = "FROM CASHRECEIPT, DEPOSIT WHERE ";
			}
			else
			{
				$queryinfix = "FROM CASHRECEIPT ";
			}

		}

		if ($AgentId)
		{
			$query1 = "INVOICE.INVOICE_AGENT_ID = '$AgentId' AND INVOICE.INVOICE_ID = CASHRECEIPT.INVOICE_ID";
		}
		if (($FromDepositDate) AND ($ToDepositDate))
		{
 			$query2 = "CASHRECEIPT.DEPOSIT_ID = DEPOSIT.DEPOSIT_ID AND DEPOSIT.DEPOSIT_DATE >= '$FromDepositDate'";
			$query3 = "CASHRECEIPT.DEPOSIT_ID = DEPOSIT.DEPOSIT_ID AND DEPOSIT.DEPOSIT_DATE <= '$ToDepositDate'";
		}
		else
		{

			if ($FromDepositDate)
			{
	 			$query2 = "CASHRECEIPT.DEPOSIT_ID = DEPOSIT.DEPOSIT_ID AND DEPOSIT.DEPOSIT_DATE >= '$FromDepositDate'";
			}
			if ($ToDepositDate)
			{
				$query3 = "CASHRECEIPT.DEPOSIT_ID = DEPOSIT.DEPOSIT_ID AND DEPOSIT.DEPOSIT_DATE <= '$ToDepositDate'";
			}
		}
		if ($ScheduledTourId)
		{
			$query4 = "INVOICE.BOOK_ID = '$ScheduledTourId' AND INVOICE.INVOICE_ID = CASHRECEIPT.INVOICE_ID";
		}

		$wherequery = "";
		if ($query1)
		{
			$wherequery = $query1;
		}
		if ($query2)
		{
			if ($wherequery)
				$wherequery = sprintf("%s AND %s",$wherequery,$query2);
			else
				$wherequery = $query2;
		}
		if ($query3)
		{
			if ($wherequery)
				$wherequery = sprintf("%s AND %s",$wherequery,$query3);
			else
				$wherequery = $query3;
		}
		if ($query4)
		{
			if ($wherequery)
				$wherequery = sprintf("%s AND %s", $wherequery,$query4);
			else
				$wherequery = $query4;
		}

		$query = sprintf("SELECT CASHRECEIPT.CASHRECEIPT_ID, CASHRECEIPT.BATCH_ID, CASHRECEIPT.DEPOSIT_ID, CASHRECEIPT.INVOICE_ID, CASHRECEIPT.CASHRECEIPT_TIMESTAMP %s %s ORDER BY CASHRECEIPT.CASHRECEIPT_ID ASC", $queryinfix,$wherequery);



//fputs($SavedFile,$query);

$mysql_result = mysql_query($query, $mysql_link);

$numrows = mysql_num_rows($mysql_result);
if ($numrows > 0)
{

	// print fetched rows

	$current_time = getdate(time());
	$current_hours = $current_time["hours"];
	$current_mins = $current_time["minutes"];
	$current_secs = $current_time["seconds"];
	$current_date = date("Y-m-d");

	$TimeStamp = sprintf("%s %d:%d:%d",$current_date,$current_hours,$current_mins,$current_secs);


	fputs($SavedFile,"<br>\n");
	fputs($SavedFile,"<CENTER><b><font size='+1'>Cash Receipt Report</font></b></CENTER>\n");
	fputs($SavedFile,"<center><font size='-1' color='blue'>(-- Created $TimeStamp --)</font></center>\n");
	fputs($SavedFile,"<br>\n");

		fputs($SavedFile,"<table align='center' size='100%'>");
		fputs($SavedFile,"<tr>");

							fputs($SavedFile,"<TR>\n");

							if ((!$FromDate) AND (!$ToDate))
							{
								fputs($SavedFile,"<TD ALIGN='LEFT' valign='top' colspan='2'><b>Date Range:</b></TD>");
								fputs($SavedFile,"<TD ALIGN='LEFT' valign='top' colspan='10'><b>ALL</b></TD>");

							}
							else
							{
								if (!$FromDate)
								{
									$FromDate = "ALL";
								}
								if (!$ToDate)
								{
									$ToDate = sprintf("%s", date("m/d/Y"));
								}
								fputs($SavedFile,"<TD ALIGN='LEFT' valign='top'colspan='2'><b>Date Range:</b></TD>");
								fputs($SavedFile,"<TD ALIGN='LEFT' valign='top' colspan='10'><b>$FromDate - $ToDate</b></TD>");
							}
							fputs($SavedFile,"</TR>\n");

	//	fputs($SavedFile,"<TD ALIGN='LEFT' valign='top'><b><u><Font color='black'>Batch #</font></u></b></TD>");
	//	fputs($SavedFile,"\n");
	//	fputs($SavedFile,"<TD ALIGN='LEFT' valign='top'></TD>");
	//	fputs($SavedFile,"\n");
	//	fputs($SavedFile,"<TD ALIGN='LEFT' valign='top'><b><u><Font color='black'>Dep #</font></u></b></TD>");
	//	fputs($SavedFile,"\n");
	//	fputs($SavedFile,"<TD ALIGN='LEFT' valign='top'></TD>");
	//	fputs($SavedFile,"\n");
		fputs($SavedFile,"<TD ALIGN='LEFT' valign='top'><b><u><Font color='black'>Dep Date</font></u></b></TD>");
		fputs($SavedFile,"\n");
		fputs($SavedFile,"<TD ALIGN='LEFT' valign='top'></TD>");
		fputs($SavedFile,"\n");
		fputs($SavedFile,"<TD ALIGN='LEFT' valign='top'><b><u><Font color='black'>Inv #</font></u></b></TD>");
		fputs($SavedFile,"\n");
		fputs($SavedFile,"<TD ALIGN='LEFT' valign='top'></TD>");
		fputs($SavedFile,"\n");
		fputs($SavedFile,"<TD ALIGN='LEFT' valign='top'><b><u><Font color='black'>Trip Code</font></u></b></TD>");
		fputs($SavedFile,"\n");
		fputs($SavedFile,"<TD ALIGN='LEFT' valign='top'></TD>");
		fputs($SavedFile,"\n");
		fputs($SavedFile,"<TD ALIGN='LEFT' valign='top'><b><u><Font color='black'>Agent Code</font></u></b></TD>");
		fputs($SavedFile,"\n");
		fputs($SavedFile,"<TD ALIGN='LEFT' valign='top'></TD>");
		fputs($SavedFile,"\n");
		fputs($SavedFile,"<TD ALIGN='LEFT' valign='top'><b><u><Font color='black'>Amount</font></u></b></TD>");
		fputs($SavedFile,"\n");
		fputs($SavedFile,"<TD ALIGN='LEFT' valign='top'></TD>");
		fputs($SavedFile,"\n");
		fputs($SavedFile,"<TD ALIGN='LEFT' valign='top'><b><u><Font color='black'>Batch Total</font></u></b></TD>");
		fputs($SavedFile,"\n");
		fputs($SavedFile,"<TD ALIGN='LEFT' valign='top'></TD>");
		fputs($SavedFile,"\n");

		fputs($SavedFile,"</TR>");
		fputs($SavedFile,"<tr>");
		fputs($SavedFile,"</tr>");

		$OldBatchId = 0;
		$OldDepositId = 0;
		$DepositTotal = 0;
		while ($row = mysql_fetch_array($mysql_result))
		{


			$BatchId = $row[1];
				$query1 = "SELECT BATCH_TOTAL FROM BATCH WHERE BATCH_ID = '$BatchId'";
				$mysql_result1 = mysql_query($query1, $mysql_link);
				$row1 = mysql_fetch_array($mysql_result1);
				$BatchTotal = $row1[0];

			$DepositId = $row[2];
				$query2 = "SELECT DEPOSIT_DATE FROM DEPOSIT WHERE DEPOSIT_ID = '$DepositId'";
				$mysql_result2 = mysql_query($query2, $mysql_link);
				$row2 = mysql_fetch_array($mysql_result2);
				$DepositDate = $row2[0];

			$InvoiceId = $row[3];
				$query3 = "SELECT INVOICE_AMOUNT, INVOICE_AGENT_ID,BOOK_ID FROM INVOICE WHERE INVOICE_ID = '$InvoiceId'";
				$mysql_result3 = mysql_query($query3, $mysql_link);
				$row3 = mysql_fetch_array($mysql_result3);
				$InvoiceAmount = $row3[0];

			$AgentId = $row3[1];
				$query4 = "SELECT AGENT_CODE FROM AGENT WHERE AGENT_ID = '$AgentId'";
				$mysql_result4 = mysql_query($query4, $mysql_link);
				$row4 = mysql_fetch_array($mysql_result4);
				$AgentCode = $row4[0];

			$BookId = $row3[2];
				$query5 = "SELECT SCHEDULEDTOUR_CODE FROM SCHEDULEDTOUR WHERE SCHEDULEDTOUR_ID = '$BookId'";
				$mysql_result5 = mysql_query($query5, $mysql_link);
				$row5 = mysql_fetch_array($mysql_result5);
				$TripCode = $row5[0];

				if (($BatchId > $OldBatchId) AND ($OldBatchId > 0))
				{
					fputs($SavedFile,"<tr></tr>\n");
					fputs($SavedFile,"<tr>");
	//				fputs($SavedFile,"<td></td>");
	//				fputs($SavedFile,"<td></td>");

	//				fputs($SavedFile,"<td></td>");
	//				fputs($SavedFile,"<td></td>");

					fputs($SavedFile,"<td></td>");
					fputs($SavedFile,"<td></td>");
					fputs($SavedFile,"<td></td>");
					fputs($SavedFile,"<td></td>");
					fputs($SavedFile,"<td></td>");
					fputs($SavedFile,"<td></td>");
					fputs($SavedFile,"<td colspan='3'><b>Batch# $OldBatchId </b></td>");
					fputs($SavedFile,"<td></td>");
					$OldBatchTotal = sprintf("%5.2f", $OldBatchTotal);
					fputs($SavedFile,"<td>$OldBatchTotal</td>");
					fputs($SavedFile,"<td></td>");
					fputs($SavedFile,"</tr>");

					$DepositTotal = $DepositTotal + $OldBatchTotal;

				}

				if (($DepositId > $OldDepositId) AND ($OldDepositId > 0))

				{
					fputs($SavedFile,"<tr></tr>\n");
					fputs($SavedFile,"<tr>");
	//				fputs($SavedFile,"<td></td>");
	//				fputs($SavedFile,"<td></td>");
	//				fputs($SavedFile,"<td></td>");
	//				fputs($SavedFile,"<td></td>");

					fputs($SavedFile,"<td></td>");
					fputs($SavedFile,"<td></td>");
					fputs($SavedFile,"<td></td>");
					fputs($SavedFile,"<td></td>");
					fputs($SavedFile,"<td></td>");
					fputs($SavedFile,"<td></td>");
					fputs($SavedFile,"<td colspan='3'><b>Dep# $OldDepositId Total</b></td>");
					fputs($SavedFile,"<td></td>");
					$DepositTotal = sprintf("%5.2f", $DepositTotal);

					fputs($SavedFile,"<td>$DepositTotal</td>");
					fputs($SavedFile,"<td></td>");
					fputs($SavedFile,"</tr>");
					$DepositTotal = 0;
					fputs($SavedFile,"<tr><td colspan='12'><hr></td></tr>\n");

				}




				fputs($SavedFile,"<tr>");
	//			fputs($SavedFile,"<td>$BatchId</td>");
	//			fputs($SavedFile,"<td></td>");
	//			fputs($SavedFile,"<td>$DepositId</td>");
	//			fputs($SavedFile,"<td></td>");

				fputs($SavedFile,"<td>$DepositDate</td>");
				fputs($SavedFile,"<td></td>");
				fputs($SavedFile,"<td>$InvoiceId</td>");
				fputs($SavedFile,"<td></td>");
				fputs($SavedFile,"<td>$TripCode</td>");
				fputs($SavedFile,"<td></td>");
				fputs($SavedFile,"<td>$AgentCode</td>");
				fputs($SavedFile,"<td></td>");
				fputs($SavedFile,"<td>$InvoiceAmount</td>");
				fputs($SavedFile,"<td></td>");
				fputs($SavedFile,"<td></td>");
				fputs($SavedFile,"<td></td>");
				fputs($SavedFile,"</tr>");


				$OldBatchId = $BatchId;
				$OldBatchTotal = $BatchTotal;
				$OldDepositId = $DepositId;

			}

					fputs($SavedFile,"<tr></tr>\n");
					fputs($SavedFile,"<tr>");
	//				fputs($SavedFile,"<td></td>");
	//				fputs($SavedFile,"<td></td>");
	//				fputs($SavedFile,"<td></td>");
	//				fputs($SavedFile,"<td></td>");

					fputs($SavedFile,"<td></td>");
					fputs($SavedFile,"<td></td>");
					fputs($SavedFile,"<td></td>");
					fputs($SavedFile,"<td></td>");
					fputs($SavedFile,"<td></td>");
					fputs($SavedFile,"<td></td>");
					fputs($SavedFile,"<td colspan='3'><b>Batch# $BatchId </b></td>");
					fputs($SavedFile,"<td></td>");
					$BatchTotal = sprintf("%5.2f", $BatchTotal);

					fputs($SavedFile,"<td>$BatchTotal</td>");
					fputs($SavedFile,"<td></td>");
					fputs($SavedFile,"</tr>");

			$DepositTotal = $DepositTotal + $BatchTotal;

					fputs($SavedFile,"<tr></tr>\n");
					fputs($SavedFile,"<tr>");
	//				fputs($SavedFile,"<td></td>");
	//				fputs($SavedFile,"<td></td>");
	//				fputs($SavedFile,"<td></td>");
	//				fputs($SavedFile,"<td></td>");

					fputs($SavedFile,"<td></td>");
					fputs($SavedFile,"<td></td>");
					fputs($SavedFile,"<td></td>");
					fputs($SavedFile,"<td></td>");
					fputs($SavedFile,"<td></td>");
					fputs($SavedFile,"<td></td>");
					fputs($SavedFile,"<td colspan='3'><b>Dep# $DepositId Total</b></td>");
					fputs($SavedFile,"<td></td>");
					$DepositTotal = sprintf("%5.2f", $DepositTotal);

					fputs($SavedFile,"<td>$DepositTotal</td>");
					fputs($SavedFile,"<td></td>");
					fputs($SavedFile,"</tr>");
					fputs($SavedFile,"<tr><td colspan='12'><hr></td></tr>\n");



		fputs($SavedFile,"</Table>");

				fclose($SavedFile); // close the file

				$FullPath = sprintf("http://www.california-tour.com/printouts/%s",$NewFileName);

				print("Click <a href='$FullPath'><b><font color='blue'>here</font></b></a> to view the latest Cash Receipt Report printout ($NewFileName)<br>\n");
				print("Click <a href='http://www.california-tour.com/printouts/'><b><font color='blue'>here</font></b></a> to view a complete list of printouts in this directory\n");

}
else
{
	print("<br>");
	print("*** NO MATCHES ***");
}

?>
