


<?

// open a connection to mysql and select the "deltours" database


	include ("parsefuncs.php");

	$mysql_link = connect_to_db();
	$mysql_query = select_db($mysql_link);

// print fetched rows



	/*
	** open file for writing
	*/


	$current_time = getdate(time());
	$current_hours = $current_time["hours"];
	$current_mins = $current_time["minutes"];
	$current_secs = $current_time["seconds"];
	$current_date = date("Y-m-d");

	$TimeStamp = sprintf("%s %d:%d:%d",$current_date,$current_hours,$current_mins,$current_secs);

	$NewFileName = sprintf("ccrpt-%s.htm",$current_date);
	$NewFileNamePath = sprintf("/home/califor/public_html/printouts/%s",$NewFileName);


	$SavedFile = fopen($NewFileNamePath,"w");

	/*
	** make sure the open was successful
	*/
	if(!($SavedFile))
	{
		fputs($SavedFile,"Error: ");
		fputs($SavedFile,"'printout.txt' could not be created\n");
		exit;
	}


/*



$query = "CREATE TABLE SCHEDULEDTOURBOOK (
			SCHEDULEDTOURBOOK_ID mediumint(2) NOT NULL default '0',
			SCHEDULEDTOUR_ID mediumint(2) NOT NULL default '0',
			LAST_NAME char(40) NOT NULL default '',
			FIRST_NAME char(40) NOT NULL default '',
			GENDER enum('M','F') NOT NULL default 'M',
			OCCUPANCY tinyint(1) NOT NULL default '0',
			ROOMMATE enum('Y','N') NOT NULL default 'Y',
			OPTIONALTOUR_ID mediumint(2) NOT NULL default '0',
			TOTALCHARGE decimal(4,2) NOT NULL default '0.00',
			AGENT_ID mediumint(2) NOT NULL default '0',
			SCHEDULEDTOURBOOK_TIMESTAMP datetime NOT NULL default '0000-00-00 00:00:00',
			PULOCATION_ID tinyint(1) NOT NULL default '0',
			TOTALPAYTODLT decimal(4,2) NOT NULL default '0',
			PAYTODLT_ID tinyint(1) NOT NULL default '0',
			PAYTODLTDATE char(10) NOT NULL default '',
			TOTALPAYTOAGENT decimal(4,2) NOT NULL default '0',
			PAYTOAGENT_ID tinyint(1) NOT NULL default '0',
			PAYTOAGENTDATE char(10) NOT NULL default '',
			TOTALPAYCC decimal(4,2) NOT NULL default '0',
			PAYBYCC_ID tinyint(1) NOT NULL default '0',
			PAYCCDATE char (10) NOT NULL default '',
			EMAIL char (20) NOT NULL default '',
			PRIMARY KEY (SCHEDULEDTOURBOOK_ID),
			UNIQUE KEY SCHEDULEDTOURBOOK_ID (SCHEDULEDTOURBOOK_ID),
			KEY SCHEDULEDTOURBOOK_ID_2 (SCHEDULEDTOURBOOK_ID)
		) TYPE=MyISAM";



*/

	if ($FromDate)
	{
		if ($ToDate)
		{
			if ($AgentId)
			{
				if ($TourId)
				{
					$query = "SELECT SCHEDULEDTOURBOOK.PAYCCDATE,
							SCHEDULEDTOURBOOK.AGENT_ID,
							SCHEDULEDTOURBOOK.LAST_NAME,
							SCHEDULEDTOURBOOK.FIRST_NAME,
							SCHEDULEDTOURBOOK.TOTALPAYCC,
							SCHEDULEDTOURBOOK.SCHEDULEDTOUR_ID
							FROM SCHEDULEDTOURBOOK,SCHEDULEDTOUR
						 	WHERE (SCHEDULEDTOURBOOK.PAYCCDATE >= '$FromDate'
								AND SCHEDULEDTOURBOOK.PAYCCDATE <= '$ToDate'
								AND SCHEDULEDTOURBOOK.AGENT_ID = '$AgentId'
								AND SCHEDULEDTOURBOOK.SCHEDULEDTOUR_ID = SCHEDULEDTOUR.SCHEDULEDTOUR_ID
								AND SCHEDULEDTOUR.TOUR_ID = '$TourId')
						 		ORDER BY PAYCCDATE ASC";
				}
				else
				{
					$query = "SELECT PAYCCDATE,
							AGENT_ID,
							LAST_NAME,
							FIRST_NAME,
							TOTALPAYCC,
							SCHEDULEDTOUR_ID
							FROM SCHEDULEDTOURBOOK
						 	WHERE (PAYCCDATE >= '$FromDate'
								AND PAYCCDATE <= '$ToDate'
								AND AGENT_ID = '$AgentId')
						 		ORDER BY PAYCCDATE ASC";

				}
			}
			else
			{
				if ($TourId)
				{
					$query = "SELECT SCHEDULEDTOURBOOK.PAYCCDATE,
							SCHEDULEDTOURBOOK.AGENT_ID,
							SCHEDULEDTOURBOOK.LAST_NAME,
							SCHEDULEDTOURBOOK.FIRST_NAME,
							SCHEDULEDTOURBOOK.TOTALPAYCC,
							SCHEDULEDTOURBOOK.SCHEDULEDTOUR_ID
							FROM SCHEDULEDTOURBOOK,SCHEDULEDTOUR,TOUR
						 	WHERE (SCHEDULEDTOURBOOK.PAYCCDATE >= '$FromDate'
								AND SCHEDULEDTOURBOOK.PAYCCDATE <= '$ToDate'
								AND SCHEDULEDTOURBOOK.SCHEDULEDTOUR_ID = SCHEDULEDTOUR.SCHEDULEDTOUR_ID
								AND SCHEDULEDTOUR.TOUR_ID = TOUR.TOUR_ID
								AND TOUR.TOUR_ID = '$TourId'
						 		ORDER BY PAYCCDATE ASC";
				}
				else

				{
					$query = "SELECT PAYCCDATE,
							AGENT_ID,
							LAST_NAME,
							FIRST_NAME,
							TOTALPAYCC,
							SCHEDULEDTOUR_ID
							FROM SCHEDULEDTOURBOOK
						 	WHERE (PAYCCDATE >= '$FromDate'
								AND PAYCCDATE <= '$ToDate')
						 		ORDER BY PAYCCDATE ASC";

				}
			}
		}
		else
		{
			if ($AgentId)
			{
				if ($TourId)
				{
				$query = "SELECT SCHEDULEDTOURBOOK.PAYCCDATE,
						SCHEDULEDTOURBOOK.AGENT_ID,
						SCHEDULEDTOURBOOK.LAST_NAME,
						SCHEDULEDTOURBOOK.FIRST_NAME,
						SCHEDULEDTOURBOOK.TOTALPAYCC,
						SCHEDULEDTOURBOOK.SCHEDULEDTOUR_ID
						FROM SCHEDULEDTOURBOOK,SCHEDULEDTOUR,TOUR
					 	WHERE (SCHEDULEDTOURBOOK.PAYCCDATE >= '$FromDate'
								AND AGENT_ID = '$AgentId'
								AND SCHEDULEDTOURBOOK.SCHEDULEDTOUR_ID = SCHEDULEDTOUR.SCHEDULEDTOUR_ID
								AND SCHEDULEDTOUR.TOUR_ID = TOUR.TOUR_ID
								AND TOUR.TOUR_ID = '$TourId')
						 		ORDER BY PAYCCDATE ASC";

					}
				else
				{
				$query = "SELECT PAYCCDATE,
						AGENT_ID,
						LAST_NAME,
						FIRST_NAME,
						TOTALPAYCC,
						SCHEDULEDTOUR_ID
						FROM SCHEDULEDTOURBOOK
					 	WHERE (PAYCCDATE >= '$FromDate'
							AND AGENT_ID = '$AgentId')
					 		ORDER BY PAYCCDATE ASC";

				}
			}
			else
			{
				if ($TourId)
				{
				$query = "SELECT SCHEDULEDTOURBOOK.PAYCCDATE,
						SCHEDULEDTOURBOOK.AGENT_ID,
						SCHEDULEDTOURBOOK.LAST_NAME,
						SCHEDULEDTOURBOOK.FIRST_NAME,
						SCHEDULEDTOURBOOK.TOTALPAYCC,
						SCHEDULEDTOURBOOK.SCHEDULEDTOUR_ID
						FROM SCHEDULEDTOURBOOK,SCHEDULEDTOUR,TOUR
					 	WHERE (SCHEDULEDTOURBOOK.PAYCCDATE >= '$FromDate'
								AND SCHEDULEDTOURBOOK.SCHEDULEDTOUR_ID = SCHEDULEDTOUR.SCHEDULEDTOUR_ID
								AND SCHEDULEDTOUR.TOUR_ID = TOUR.TOUR_ID
								AND TOUR.TOUR_ID = '$TourId')
						 		ORDER BY PAYCCDATE ASC";

				}
				else
				{
				$query = "SELECT PAYCCDATE,
						AGENT_ID,
						LAST_NAME,
						FIRST_NAME,
						TOTALPAYCC,
						SCHEDULEDTOUR_ID
						FROM SCHEDULEDTOURBOOK
					 	WHERE PAYCCDATE >= '$FromDate'
					 		ORDER BY PAYCCDATE ASC";

				}
			}
		}
	}
	else
	{
		if ($AgentId)
		{
			if ($TourId)
			{
				$query = "SELECT SCHEDULEDTOURBOOK.PAYCCDATE,
						SCHEDULEDTOURBOOK.AGENT_ID,
						SCHEDULEDTOURBOOK.LAST_NAME,
						SCHEDULEDTOURBOOK.FIRST_NAME,
						SCHEDULEDTOURBOOK.TOTALPAYCC,
						SCHEDULEDTOURBOOK.SCHEDULEDTOUR_ID
						FROM SCHEDULEDTOURBOOK,SCHEDULEDTOUR,TOUR
					 	WHERE (AGENT_ID = '$AgentId'
								AND SCHEDULEDTOURBOOK.SCHEDULEDTOUR_ID = SCHEDULEDTOUR.SCHEDULEDTOUR_ID
								AND SCHEDULEDTOUR.TOUR_ID = TOUR.TOUR_ID
								AND TOUR.TOUR_ID = '$TourId')
						 		ORDER BY PAYCCDATE ASC";

			}
			else
			{
				$query = "SELECT PAYCCDATE,
						AGENT_ID,
						LAST_NAME,
						FIRST_NAME,
						TOTALPAYCC,
						SCHEDULEDTOUR_ID
						FROM SCHEDULEDTOURBOOK
					 	WHERE AGENT_ID >= '$AgentId'
					 		ORDER BY PAYCCDATE ASC";

			}
		}
		else
		{
			if ($TourId)
			{
				$query = "SELECT SCHEDULEDTOURBOOK.PAYCCDATE,
						SCHEDULEDTOURBOOK.AGENT_ID,
						SCHEDULEDTOURBOOK.LAST_NAME,
						SCHEDULEDTOURBOOK.FIRST_NAME,
						SCHEDULEDTOURBOOK.TOTALPAYCC,
						SCHEDULEDTOURBOOK.SCHEDULEDTOUR_ID
						FROM SCHEDULEDTOURBOOK,SCHEDULEDTOUR,TOUR
					 	WHERE (SCHEDULEDTOURBOOK.SCHEDULEDTOUR_ID = SCHEDULEDTOUR.SCHEDULEDTOUR_ID
								AND SCHEDULEDTOUR.TOUR_ID = TOUR.TOUR_ID
								AND TOUR.TOUR_ID = '$TourId')
						 		ORDER BY PAYCCDATE ASC";
			}
			else
			{
				$query = "SELECT PAYCCDATE,
						AGENT_ID,
						LAST_NAME,
						FIRST_NAME,
						TOTALPAYCC,
						SCHEDULEDTOUR_ID
						FROM SCHEDULEDTOURBOOK
					 	ORDER BY PAYCCDATE ASC";

			}
		}
	}


//print($query);

$mysql_result = mysql_query($query, $mysql_link);

$numrows = mysql_num_rows($mysql_result);

if ($numrows > 0)
{

	// print fetched rows

	$current_time = getdate(time());
	$current_hours = $current_time["hours"];

	$current_mins = $current_time["minutes"];
	$current_secs = $current_time["seconds"];
	$current_date = date("Y-m-d");

	$TimeStamp = sprintf("%s %d:%d:%d",$current_date,$current_hours,$current_mins,$current_secs);

	fputs($SavedFile,"<form name='invoice' method='post' action='invoice.php'><br>\n");
	fputs($SavedFile,"<CENTER><b><font size='+1'>Credit Card Report</font></b></CENTER>\n");
	fputs($SavedFile,"<center><font size='-1' color='blue'>(-- Created $TimeStamp --)</font></center>\n");
	fputs($SavedFile,"<TABLE ALIGN='CENTER' WIDTH='100%'>");
	fputs($SavedFile,"\n");


	fputs($SavedFile,"<TABLE ALIGN='CENTER' WIDTH='100%'>");
	fputs($SavedFile,"\n");
	fputs($SavedFile,"<TR>");
	fputs($SavedFile,"\n");
	fputs($SavedFile,"<TD ALIGN='LEFT' valign='top'><b><u><Font color='black'>Proc. Date</font></u></b></TD>");
	fputs($SavedFile,"\n");
	fputs($SavedFile,"<TD ALIGN='LEFT' valign='top'><b><u><Font color='black'>Agent</font></u></b></TD>");
	fputs($SavedFile,"\n");
	fputs($SavedFile,"<TD ALIGN='LEFT' valign='top'><b><u><Font color='black'>Pass. Name</font></u></b></TD>");
	fputs($SavedFile,"\n");
	fputs($SavedFile,"<TD ALIGN='LEFT' valign='top'><b><u><Font color='black'>Trip Code</font></u></b></TD>");
	fputs($SavedFile,"\n");
	fputs($SavedFile,"<TD ALIGN='LEFT' valign='top'><b><u><Font color='black'>CC #</font></u></b></TD>");
	fputs($SavedFile,"\n");
	fputs($SavedFile,"<TD ALIGN='LEFT' valign='top'><b><u><Font color='black'>Auth #</font></u></b></TD>");
	fputs($SavedFile,"\n");

	fputs($SavedFile,"<TD ALIGN='LEFT' valign='top'><b><u><Font color='black'>Amt</font></u></b></TD>");
	fputs($SavedFile,"\n");
	fputs($SavedFile,"</TR>");
	fputs($SavedFile,"\n");


	$TotalAmount = 0;

	while ($row = mysql_fetch_array($mysql_result))
	{


		$ProcessDate = $row[0];
		$TempAgentId = $row[1];
		$LastName = strtoupper($row[2]);
		$FirstName = first_upper($row[3]);
		$Amount = $row[4];
		$ScheduledTourId = $row[5];

		$query1 = "SELECT SCHEDULEDTOUR_CODE FROM SCHEDULEDTOUR WHERE SCHEDULEDTOUR_ID = '$ScheduledTourId'";
		$mysql_result1 = mysql_query($query1, $mysql_link);

		$numrows1 = 0;
		$numrows = mysql_num_rows($mysql_result1);

		if ($numrows > 0)
		{
			$row1 = mysql_fetch_array($mysql_result1);
			$ScheduledTourCode = $row1[0];
		}
		else
		{
			$ScheduledTourCode = "NONE";
		}


		$TotalAmount = $TotalAmount + $Amount;

		// print row result

		fputs($SavedFile,"<TR>");
		fputs($SavedFile,"\n");
		fputs($SavedFile,"<TD ALIGN='LEFT' valign='top'>$ProcessDate</TD>");
		fputs($SavedFile,"\n");

		$query1 = "SELECT * FROM AGENT WHERE AGENT_ID = '$TempAgentId'";
		$mysql_result1 = mysql_query($query1, $mysql_link);

		$numrows1 = 0;
		$numrows = mysql_num_rows($mysql_result1);

		if ($numrows > 0)
		{
			$row1 = mysql_fetch_array($mysql_result1);
			$AgentName = strtoupper($row1[3]);
			fputs($SavedFile,"<TD ALIGN='LEFT' valign='top'>$AgentName</TD>");
			fputs($SavedFile,"\n");
		}
		else
		{
			fputs($SavedFile,"<TD ALIGN='LEFT' valign='top'>NONE</TD>");
			fputs($SavedFile,"\n");
		}


		fputs($SavedFile,"<TD ALIGN='LEFT' valign='top'>$LastName, $FirstName</TD>");
		fputs($SavedFile,"\n");

		fputs($SavedFile,"<TD ALIGN='LEFT' valign='top'>$ScheduledTourCode</TD>");
		fputs($SavedFile,"\n");
		fputs($SavedFile,"<TD ALIGN='LEFT' valign='top'>XXXXXX</TD>");
		fputs($SavedFile,"\n");
		fputs($SavedFile,"<TD ALIGN='LEFT' valign='top'>NNNNNN</TD>");
		fputs($SavedFile,"\n");

		fputs($SavedFile,"<TD ALIGN='LEFT' valign='top'>$Amount</TD>");
		fputs($SavedFile,"\n");
		fputs($SavedFile,"</TR>");

	}

	fputs($SavedFile,"<tr>");
	fputs($SavedFile,"<TD ALIGN='LEFT' valign='top' colspan='7'><hr></TD>");
	fputs($SavedFile,"\n");
	fputs($SavedFile,"</TR>");

	fputs($SavedFile,"<tr>");
	fputs($SavedFile,"<TD ALIGN='LEFT' valign='top' colspan='5'></TD>");
	fputs($SavedFile,"<TD ALIGN='LEFT' valign='top' colspan='1'><b>TOTAL:</b></TD>");
	fputs($SavedFile,"<TD ALIGN='LEFT' valign='top' colspan='1'>$TotalAmount</TD>");
	fputs($SavedFile,"\n");
	fputs($SavedFile,"</TR>");

	fputs($SavedFile,"</TABLE>");

	fputs($SavedFile,"</form>\n");

		fclose($SavedFile); // close the file

		$FullPath = sprintf("http://www.california-tour.com/printouts/%s",$NewFileName);

		print("Click <a href='$FullPath'><b><font color='blue'>here</font></b></a> to view the latest Credit Report Report printout ($NewFileName)<br>\n");
		print("Click <a href='http://www.california-tour.com/printouts/'><b><font color='blue'>here</font></b></a> to view a complete list of printouts in this directory\n");


}
else
{
	print("<br>");
	print("*** NO MATCHES ***");
}

?>
