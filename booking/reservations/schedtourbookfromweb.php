<?


include ("schedtourbookfuncs.php");

include ("../sqlfuncs.php");
include ("parsefuncs.php");


// connect to database

$mysql_link = connect_to_db();

// specify table name

$table_name = "SCHEDULEDTOURBOOK";

$FullCode = "FALSE";

// print db size
$ResultString = print_tb_size($table_name, $mysql_link);
//print("Before adding record...\n");
//print($ResultString);

// SAVE INFO FROM WEB SIGN UP FORM ...

// check if user name exists, if not create record
// if it exists, populate the fields with the current values
// for the record

// check to make sure we can have space available for the tour


// get new Tour Book ID

	$ScheduledTourBookId = get_new_id($mysql_link);

// get Last and first name from full name

	$FullName = strtoupper($FullName);

	$NameArray = split(" ",$FullName,3);

	$FirstName = $NameArray[0];
	$maxcount = count($NameArray);
	if ($maxcount == 2)
	{
		$LastName = $NameArray[1];
	}
	else
	{
		$LastName = $NameArray[2];
	}

// print($ScheduledTourId);

// get City Code

	$query2 = "SELECT SCHEDULEDTOUR_CODE,TOUR_ID,SCHEDULEDTOUR_DEPART_DATE FROM SCHEDULEDTOUR WHERE SCHEDULEDTOUR_ID = '$ScheduledTourId'";
	$mysql_result2 = mysql_query($query2,$mysql_link);
	$row2 = mysql_fetch_array($mysql_result2);
	$ScheduledTourCode = $row2[0];
	$TourId = $row2[1];
	$DepartDate = $row2[2];

	$query2 = "SELECT TOUR_NAME,CITY_ID FROM TOUR WHERE TOUR_ID = '$TourId'";
	$mysql_result2 = mysql_query($query2,$mysql_link);
	$row2 = mysql_fetch_array($mysql_result2);
	$TourName = $row2[0];
	$CityId = $row2[1];

	$query2 = "SELECT CITY_CODE FROM CITY WHERE CITY_ID = '$CityId'";
	$mysql_result2 = mysql_query($query2,$mysql_link);
	$row2 = mysql_fetch_array($mysql_result2);
	$CityCode = $row2[0];

// get Agent ID from City (SFWEB, LAWEB, SDWEB)

// these are agents used to indicate sign on from
// the web


	if ($CityCode == "LA")
	{
		$query2 = "SELECT AGENT_ID FROM AGENT WHERE AGENT_CODE = 'LAWEB'";
		$mysql_result2 = mysql_query($query2,$mysql_link);
		$row2 = mysql_fetch_array($mysql_result2);
		$AgentId = $row2[0];
	}
	if ($CityCode == "SD")
	{
		$query2 = "SELECT AGENT_ID FROM AGENT WHERE AGENT_CODE = 'SDWEB'";
		$mysql_result2 = mysql_query($query2,$mysql_link);
		$row2 = mysql_fetch_array($mysql_result2);
		$AgentId = $row2[0];
	}
	if ($CityCode == "SF")
	{
		$query2 = "SELECT AGENT_ID FROM AGENT WHERE AGENT_CODE = 'SFWEB'";
		$mysql_result2 = mysql_query($query2,$mysql_link);
		$row2 = mysql_fetch_array($mysql_result2);
		$AgentId = $row2[0];
	}

// get total charge for this tour
// this is going to be paid w/ credit card
// web-based agents have a discount schedule of 1

	$query2 = "SELECT SCHEDULEDTOUR_PRICE FROM SCHEDULEDTOUR WHERE TOUR_ID = '$TourId'";
	$mysql_result2 = mysql_query($query2,$mysql_link);
	$row2 = mysql_fetch_array($mysql_result2);
	$TourPrice = $row2[0];

	$query1 			= "SELECT AGENT_COMMISIONTYPE FROM AGENT WHERE AGENT_ID = '$AgentId'";
	$mysql_result1 		= mysql_query($query1, $mysql_link);
	$row1 				= mysql_fetch_array($mysql_result1);

	$CommissionType 	= $row1[0];
	if ($CommissionType == 1)
		$query1  = "SELECT TOUR_SUPP_SNG, TOUR_SUPP_DBL, TOUR_DISC1 FROM TOUR WHERE TOUR_ID = '$TourId'";
	if ($CommissionType == 2)
		$query1  = "SELECT TOUR_SUPP_SNG, TOUR_SUPP_DBL, TOUR_DISC2 FROM TOUR WHERE TOUR_ID = '$TourId'";
	if ($CommissionType == 3)
		$query1  = "SELECT TOUR_SUPP_SNG, TOUR_SUPP_DBL, TOUR_DISC3 FROM TOUR WHERE TOUR_ID = '$TourId'";
	if ($CommissionType == 4)
		$query1  = "SELECT TOUR_SUPP_SNG, TOUR_SUPP_DBL, TOUR_DISC4 FROM TOUR WHERE TOUR_ID = '$TourId'";
	if ($CommissionType == 5)
		$query1  = "SELECT TOUR_SUPP_SNG, TOUR_SUPP_DBL, TOUR_DISC5 FROM TOUR WHERE TOUR_ID = '$TourId'";
	if ($CommissionType == 6)
		$query1  = "SELECT TOUR_SUPP_SNG, TOUR_SUPP_DBL, TOUR_DISC6 FROM TOUR WHERE TOUR_ID = '$TourId'";
	if ($CommissionType == 7)
		$query1  = "SELECT TOUR_SUPP_SNG, TOUR_SUPP_DBL, TOUR_DISC7 FROM TOUR WHERE TOUR_ID = '$TourId'";
	if ($CommissionType == 8)
		$query1  = "SELECT TOUR_SUPP_SNG, TOUR_SUPP_DBL, TOUR_DISC8 FROM TOUR WHERE TOUR_ID = '$TourId'";
	$mysql_result1 		= mysql_query($query1, $mysql_link);
	$row1 				= mysql_fetch_array($mysql_result1);

	$TourSuppSng = $row1[0];
	$TourSuppDbl = $row1[1];
	$TourDiscount = $row1[2];

//	print($TourPrice);
//	print("<br>");
//	print($TourSuppSng);
//	print("<br>");
//	print($TourSuppDbl);
//	print("<br>");
//	print($TourDiscount);

	if ($Occupancy == "1")
	{
		$TourPrice = $TourPrice + $TourSuppSng;
	}
	if ($Occupancy == "2")
	{
		$TourPrice = $TourPrice + $TourSuppDbl;
	}

	$TotalTourPrice = $TourPrice - $TourDiscount;


	$TotalPayToDLT = "0.00";
	$PayToDLTId = "";
	$PayToDLTDate = "";
	$TotalPayToAgent = "0.00";
	$PayToAgentId = "";
	$PayToAgentDate = "";

	$TotalPayCC = $TotalTourPrice;
	$PayByCCId = "";
	$PayCCDate = "";
//	$Email = "";

		$current_time = getdate(time());
		$current_hours = $current_time["hours"];
		$current_mins = $current_time["minutes"];
		$current_secs = $current_time["seconds"];
		$current_date = date("Y-m-d");

		$TimeStamp = sprintf("%s %d:%d:%d",$current_date,$current_hours,$current_mins,$current_secs);


	if ($ScheduledTourId <> "")
	{
		if  (check_if_space_available(1, $ScheduledTourId, $mysql_link) == "TRUE")
		{
			if ($ScheduledTourBookId <> "")
			{
			 insert_new_record(	$ScheduledTourBookId,
							  	$ScheduledTourId,
							   	$DepartCity,
							   	$DepartDate,
							   	$LastName,
							   	$FirstName,
							   	$Gender,
							   	$Occupancy,
							   	$AgentId,
							   	$TimeStamp,
							   	$PULocationId,
							   	$TotalPayToDLT,
							   	$PayToDLTId,
							   	$PayToDLTDate,
								$TotalPayToAgent,
								$PayToAgentId,
								$PayToAgentDate,
								$TotalPayCC,
								$PayByCCId,
								$PayCCDate,
								$Email,
							   	$mysql_link);
				adjust_space_available($ScheduledTourId,$mysql_link);

				$TotalCharge = get_total_charge($ScheduledTourBookId, $mysql_link);
				$RoomCharge = get_room_charge($ScheduledTourBookId, $mysql_link);
				$TourCharge = $TotalCharge - $RoomCharge;

				$DepartCity = get_city($ScheduledTourBookId, $mysql_link);
				$DepartDate = get_date($ScheduledTourBookId, $mysql_link);

				$ResultString = "<b><font color='red'>Record Saved</font></font></b><br>";
				$ResultString2 =  print_tb_size($table_name, $mysql_link);
			}
		}
		else
		{
			$FullCode = "TRUE";

			$ScheduledTourBookId = "";
			$ScheduledTourId = "";
			$LastName = "";
			$FirstName = "";
			$AgentId = "";
			$TotalCharge = "0.00";
			$Gender = "";
			$Occupancy = "";
			$RoomMate = "";
			$TimeStamp = "";
			$PULocationId = "";

			//$ResultString = "<b><font color='red'>Can't insert because space not available!</font></font></b><br>";
			//$ResultString2 =  print_tb_size($table_name, $mysql_link);
		}

	}
	else
	{
			if ($ScheduledTourBookId <> "")
			{

			insert_new_record(	$ScheduledTourBookId,
						  	$ScheduledTourId,
						   	$DepartCity,
						   	$DepartDate,
						   	$LastName,
						   	$FirstName,
						   	$Gender,
						   	$Occupancy,
						   	$AgentId,
						   	$TimeStamp,
						   	$PULocationId,
						   	$TotalPayToDLT,
						   	$PayToDLTId,
						   	$PayToDLTDate,
							$TotalPayToAgent,
							$PayToAgentId,
							$PayToAgentDate,
							$TotalPayCC,
							$PayByCCId,
							$PayCCDate,
							$Email,
						   	$mysql_link);


				$TotalCharge = get_total_charge($ScheduledTourBookId, $mysql_link);
				$RoomCharge = get_room_charge($ScheduledTourBookId, $mysql_link);
				$TourCharge = $TotalCharge - $RoomCharge;

				$DepartCity = get_city($ScheduledTourBookId, $mysql_link);
				$DepartDate = get_date($ScheduledTourBookId, $mysql_link);

				$ResultString = "<b><font color='red'>Record Saved</font></font></b><br>";
				$ResultString2 =  print_tb_size($table_name, $mysql_link);


			}
	}


	//print($ScheduledTourId);

	// print db size
	$ResultString = print_tb_size($table_name, $mysql_link);
	//print("<br>After adding record...");
	//print($ResultString);
//	print("Key Pressed=$AddUser");

/*


	include ("newclasses.php");

		$CurrentSearch = new SearchClass;
		$CurrentSearch->init("CONFIRMATIONLETTER_ID",
							"CONFIRMATIONLETTER",
							"CONFIRMATIONLETTER_ID",
							"BOOKTYPE_ID",
							"BOOK_ID",
							"",
							"",
							"",
							"CONFIRMATIONLETTER_ID",
							"CONFIRMATIONLETTER_ID",
							"BOOKTYPE_ID",
							"BOOK_ID",
							"CONFIRMATIONLETTER_TIMESTAMP",
							"",
							"",
							"",
							"",
							"",
							"",
							"",
							"",
							"",
							"",
							"",
							"",
							"",
							"",
							"",
							"",
							"",
							"",
							"",
							"",
							"",
							$mysql_link);


	$ConfirmationLetterId = $CurrentSearch->get_new_id();

*/

	$query2 = "SELECT BOOKTYPE_ID FROM BOOKTYPE WHERE BOOKTYPE_DESC = 'SCHEDULED TOUR'";
	$mysql_result2 = mysql_query($query2,$mysql_link);
	$row2 = mysql_fetch_array($mysql_result2);
	$BookTypeId = $row2[0];

	$query2 = "SELECT SCHEDULEDTOUR_CODE,TOUR_ID,SCHEDULEDTOUR_DEPART_DATE FROM SCHEDULEDTOUR WHERE SCHEDULEDTOUR_ID = '$ScheduledTourId'";
	$mysql_result2 = mysql_query($query2,$mysql_link);
	$row2 = mysql_fetch_array($mysql_result2);
	$ScheduledTourCode = $row2[0];
	$TourId = $row2[1];
	$DepartDate = $row2[2];

	$query2 = "SELECT TOUR_NAME,CITY_ID FROM TOUR WHERE TOUR_ID = '$TourId'";
	$mysql_result2 = mysql_query($query2,$mysql_link);
	$row2 = mysql_fetch_array($mysql_result2);
	$TourName = $row2[0];
	$CityId = $row2[1];

	$query2 = "SELECT CITY_CODE FROM CITY WHERE CITY_ID = '$CityId'";
	$mysql_result2 = mysql_query($query2,$mysql_link);
	$row2 = mysql_fetch_array($mysql_result2);
	$CityCode = $row2[0];

/*
	$CurrentSearch->insert_new_record(	$ConfirmationLetterId,
										$BookTypeId,
										$ScheduledTourBookId,
										"",
										"",
										"",
										$ConfirmationLetterId,
										$BookTypeId,
										$ScheduledTourBookId,
										$TimeStamp,
										"",
										"",
										"",
										"",
										"",
										"",
										"",
										"",
										"",
										"",
										"",
										"",
										"",
										"",
										"",
										"",
										"",
										"",
										"",
										"",
										"",
										$mysql_link);


*/

	// system returned space available

	if ($FullCode == "FALSE")
	{

		$ScheduledTourBookId = 10000 + $ScheduledTourBookId;


		print("<center>\n");
		print("<br><b>Thank You $FullName</b>\n");
		print("<br><b><font color='black'>** RESERVATION SUMMARY **</font></b>\n");
		print("</center>\n");

		print("<table align='center' width='80%'>");

		print("<TR>\n");
		print("<TD ALIGN='left'>\n");
		print("<b><font color='blue'>Your Reservation Number is: </font></b>");
		print("</TD>\n");
		print("<TD ALIGN='left'>\n");
		print("<b><font color='red'>$ScheduledTourBookId</font></b>\n");
		print("</TD>\n");
		print("</TR>\n");

		print("<TR>\n");
		print("<TD ALIGN='left'>\n");
		print("<b><font color='blue'>Scheduled Tour Code: </font></b>\n");
		print("</TD>\n");
		print("<TD ALIGN='left'>\n");
		print("<b><font color='red'>$ScheduledTourCode</font></b>\n");
		print("</TD>\n");
		print("</TR>\n");

		print("<TR>\n");
		print("<TD ALIGN='left'>\n");
		print("<b><font color='blue'>Tour Name: </font></b>\n");
		print("</TD>\n");
		print("<TD ALIGN='left'>\n");
		print("<b><font color='red'>$TourName</font></b>\n");
		print("</TD>\n");
		print("</TR>\n");


		print("<TR>\n");
		print("<TD ALIGN='left'>\n");
		print("<b><font color='blue'>Departing: </font></b>\n");
		print("</TD>\n");
		print("<TD ALIGN='left'>\n");
		print("<b><font color='red'>$DepartDate</font></b>\n");
		print("</TD>\n");
		print("</TR>\n");

		print("<TR>\n");
		print("<TD ALIGN='left'>\n");
		print("<b><font color='blue'>From: </font></b>\n");
		print("</TD>\n");
		print("<TD ALIGN='left'>\n");
		print("<b><font color='red'>$CityCode</font></b>\n");
		print("</TD>\n");
		print("</TR>\n");

		print("<TR>\n");
		print("<TD ALIGN='left'>\n");
		print("<b><font color='blue'>Your Reservation was made on: </font><b>\n");
		print("</TD>\n");
		print("<TD ALIGN='left'>\n");
		print("<b><font color='red'>$TimeStamp</font></b>\n");
		print("</TD>\n");
		print("</TR>\n");

		print("<TR>\n");
		print("<TD ALIGN='left'>\n");
		print("<b><font color='blue'>Name on Reservation: </font></b>\n");
		print("</TD>\n");
		print("<TD ALIGN='left'>\n");
		$ReferenceName = sprintf("%s, %s",$LastName, first_upper($FirstName));

		print("<b><font color='red'>$ReferenceName</font></b>\n");
		print("</TD>\n");
		print("</TR>\n");


		print("<TR>\n");
		print("<TD ALIGN='left'>\n");
		print("<b><font color='blue'>Basic Tour Charge: </font></b>\n");
		print("</TD>\n");
		$TourCharge = sprintf("%5.2f", $TourCharge);

		print("<TD ALIGN='left'><b><font color='red'>$TourCharge</font></b></TD>\n");
		print("</TR>\n");


		print("<TR>\n");
		print("<TD ALIGN='left'>\n");
		print("<b><font color='blue'>Supp Room Charges: </font></b>\n");

		print("</TD>\n");

		$RoomCharge = sprintf("%5.2f", $RoomCharge);

		print("<TD ALIGN='left'><b><font color='red'>$RoomCharge</font></b></TD>\n");
		print("</TR>\n");

		print("<TR>\n");
		print("<TD ALIGN='left'>\n");
		print("<b><font color='blue'>Total Charges: </font></b>\n");
		print("</TD>\n");

		$TotalCharge = sprintf("%5.2f", $TotalCharge);

		print("<TD ALIGN='left'><b><font color='red'>$TotalCharge</font></b></TD>\n");
		print("</TR>\n");
		print("</TABLE>\n");
	}
	else
	{
		print("<center>\n");
		print("<br><b>Thank You $FullName</b>\n");
		print("<br><b><font color='red'>UNFORTUNATELY - THIS TOUR IS FULL</font></b>\n");
		print("</center>\n");

	}


?>

