<HTML>

<HEAD>
<TITLE>Customer Ledger Report</TITLE>
<BODY bgcolor="#fffff0">

<SCRIPT LANGUAGE = "JavaScript">

	var strArgString = document.location.search;

		var strArgs = strArgString.substr(6);
		var strArray = strArgs.split(",");

		var strLevel = strArray[0];
		var strUserName = strArray[1];

</SCRIPT>

<?

include ("classes.php");
// connect to server

// open a connection to mysql

include ("../sqlfuncs.php");

$mysql_link = connect_to_db();
$mysql_result = select_db($mysql_link);




$SearchItem1 = new ReportSearchItem;
$SearchItem1->init(	"AGENT",
					"AGENT_ID",
					"AGENT_CODE",
					"Agent Code:",
					"AgentId",$mysql_link);



/*
$SearchItem2 = new ReportSearchItem;
$SearchItem2->init(	"INVOICE",
					"INVOICE_DATE",
					"INVOICE_DATE",
					"Valid Dates:",
					"Dates",$mysql_link);


$SearchItem3 = new ReportSearchItem;
$SearchItem3->init(	"INVOICE",
					"INVOICE_DATE",
					"INVOICE_DATE",
					"To Date:",
					"ToDate",$mysql_link);
*/


// date range as text entry

$SearchItem2 = new ReportTextBoxItem;
$SearchItem2->init("From :","FromDate");

$SearchItem3 = new ReportTextBoxItem;
$SearchItem3->init("To :","ToDate");


$SearchForm = new ReportSearchForm;
$SearchForm->init(	"custledgrptmain.php",
					"custledgrpt.php",
					"custledgrpttotext.php",
					"-- Customer Ledger Search --",
					$SearchItem1,$SearchItem2,$SearchItem3,"","");

if ($ExitBtn)
{
	$SearchForm->acctexitcmd();
}

if ($PrintScreenBtn)
{

/* added 1/2/2004 */

	if ((!$FromDate) AND (!$ToDate))
	{
		$SearchForm->showmain();
		$message = "<center><font face='arial' color='red'>Please choose dates in the form MM/DD/YY</font></center>";
	}
	else
	{


		$FromDateLen = strlen($FromDate);
		$FromYear = substr($FromDate,$FromDateLen - 2,2);

		$ToDateLen = strlen($ToDate);
		$ToYear = substr($ToDate,$ToDateLen - 2,2);

		if ($FromYear != $ToYear)
		{
			$SearchForm->showmain();
			$message = "<center><font face='arial' color='red'>Please choose a range within the same year.</font></center>";
		}
		else
		{
			include ($SearchForm->printtoscreen);
		}
	}

}
else
{

	if ($PrintFileBtn)
	{
		include ($SearchForm->printtofile);
	}

	else
	{
		$SearchForm->showmain();
	}
}

	print("<br>");
	print($message);

?>