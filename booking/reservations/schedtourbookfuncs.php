<?


function get_new_id($mysql_link)
{
	$sql = "LOCK TABLES TOURBOOK_SEQ WRITE";
	mysql_query($sql, $mysql_link);
	$sql = "SELECT TOURBOOK_ID FROM TOURBOOK_SEQ LIMIT 1";
	$mysql_result = mysql_query($sql, $mysql_link);
	$row = mysql_fetch_array($mysql_result);
	$nextId = intval($row[0]) + 1;
	$sql = "UPDATE TOURBOOK_SEQ SET TOURBOOK_ID = " . $nextId;
	mysql_query($sql, $mysql_link);
	
	$sql = "UNLOCK TABLES";
	mysql_query($sql, $mysql_link);
	
	//$_SESSION['ReservationId'] = $nextId + 10000;
	//print("<p>nextId : " . $_SESSION['ReservationId']);
	//exit();
	return $nextId;
	
	
	// $newrowid = 1;
	// $query = "SELECT * FROM SCHEDULEDTOURBOOK ORDER BY SCHEDULEDTOURBOOK_ID ASC";
	// $mysql_result = mysql_query($query, $mysql_link);
	// $numrows = mysql_num_rows($mysql_result);
	// if ($numrows > 0)
	// {
		// while ($row = mysql_fetch_array($mysql_result))
		// {
			// $rowid = intval($row[0]);
			// if ($rowid == $newrowid)
			// {
				// $newrowid++;
			// }
		// }
	// }
	// return ($newrowid);

}

function get_room_charge($LocalScheduledTourBookId, $Localmysql_link)
{
	$query = "SELECT * FROM SCHEDULEDTOURBOOK WHERE SCHEDULEDTOURBOOK_ID = '$LocalScheduledTourBookId' ORDER BY SCHEDULEDTOURBOOK_ID ASC";
	$mysql_result = mysql_query($query, $Localmysql_link);
	$numrows = mysql_num_rows($mysql_result);
	if ($numrows > 0)
	{
		$row = mysql_fetch_array($mysql_result);
		// get row result
		$ScheduledTourBookId = $row[0];
		$ScheduledTourId = $row[1];
		$Occupancy = $row[5];
		$query2 = "SELECT * FROM SCHEDULEDTOUR WHERE SCHEDULEDTOUR_ID = '$ScheduledTourId'";
		//print($query2);
		$mysql_result2 = mysql_query($query2, $Localmysql_link);
		$numrows2 = mysql_num_rows($mysql_result2);
		if ($numrows2 > 0)
		{
			$row2 		= 	mysql_fetch_array($mysql_result2);
			$TourId 	=	$row2[2];
			$ScheduledTourPrice 	=	$row2[4];
			$query3 = "SELECT * FROM TOUR WHERE TOUR_ID = '$TourId'";

			$mysql_result3 = mysql_query($query3, $Localmysql_link);
			$numrows3 = mysql_num_rows($mysql_result3);
			if ($numrows3 > 0)
			{
				$row3 = mysql_fetch_array($mysql_result3);
				$TourSngSupp = $row3[4];
				$TourDblSupp = $row3[5];
			}
		}
	}
	//print("Occupancy = ");

	//print($Occupancy);
	//print("\n");
	//print("Tour Price = ");

	//print($ScheduledTourPrice);
	//print("\n");
	// single
	if (intval($Occupancy) == 1)
		$RoomCharge = $TourSngSupp;
	// double
	if (intval($Occupancy) == 2)
		$RoomCharge = $TourDblSupp;
	if (intval($Occupancy) == 3)
		$RoomCharge = 0;

	return ($RoomCharge);

}

function get_total_charge($LocalScheduledTourBookId, $Localmysql_link)
{
	$query = "SELECT * FROM SCHEDULEDTOURBOOK WHERE SCHEDULEDTOURBOOK_ID = '$LocalScheduledTourBookId' ORDER BY SCHEDULEDTOURBOOK_ID ASC";
	$mysql_result = mysql_query($query, $Localmysql_link);
	$numrows = mysql_num_rows($mysql_result);
	if ($numrows > 0)
	{
		$row = mysql_fetch_array($mysql_result);
		// get row result
		$ScheduledTourBookId = $row[0];
		$ScheduledTourId = $row[1];
		$Occupancy = $row[5];
		$query2 = "SELECT * FROM SCHEDULEDTOUR WHERE SCHEDULEDTOUR_ID = '$ScheduledTourId'";
		//print($query2);
		$mysql_result2 = mysql_query($query2, $Localmysql_link);
		$numrows2 = mysql_num_rows($mysql_result2);
		if ($numrows2 > 0)
		{
			$row2 		= 	mysql_fetch_array($mysql_result2);
			$TourId 	=	$row2[2];
			$ScheduledTourPrice 	=	$row2[4];
			$query3 = "SELECT * FROM TOUR WHERE TOUR_ID = '$TourId'";

			$mysql_result3 = mysql_query($query3, $Localmysql_link);
			$numrows3 = mysql_num_rows($mysql_result3);
			if ($numrows3 > 0)
			{
				$row3 = mysql_fetch_array($mysql_result3);
				$TourSngSupp = $row3[4];
				$TourDblSupp = $row3[5];
			}
		}
	}
	$TotalCharge = 0;
	//print("Occupancy = ");

	//print($Occupancy);
	//print("\n");
	//print("Tour Price = ");

	//print($ScheduledTourPrice);
	//print("\n");
	// single
	if (intval($Occupancy) == 1)
		$TotalCharge = $ScheduledTourPrice + $TourSngSupp;
	// double
	if (intval($Occupancy) == 2)
		$TotalCharge = $ScheduledTourPrice + $TourDblSupp;
	if (intval($Occupancy) == 3)
		$TotalCharge = $ScheduledTourPrice;
	if (intval($Occupancy) == 4)
		$TotalCharge = $ScheduledTourPrice;


	return ($TotalCharge);
}

function get_city($LocalScheduledTourBookId, $Localmysql_link)
{
	$query = "SELECT * FROM SCHEDULEDTOURBOOK WHERE SCHEDULEDTOURBOOK_ID = '$LocalScheduledTourBookId' ORDER BY SCHEDULEDTOURBOOK_ID ASC";
	$mysql_result = mysql_query($query, $Localmysql_link);
	$numrows = mysql_num_rows($mysql_result);
	if ($numrows > 0)
	{
		$row = mysql_fetch_array($mysql_result);
		// get row result
		$ScheduledTourBookId = $row[0];
		$ScheduledTourId = $row[1];
		$query2 = "SELECT * FROM SCHEDULEDTOUR WHERE SCHEDULEDTOUR_ID = '$ScheduledTourId'";
		//print($query2);
		$mysql_result2 = mysql_query($query2, $Localmysql_link);
		$numrows2 = mysql_num_rows($mysql_result2);
		if ($numrows2 > 0)
		{
			$row2 		= 	mysql_fetch_array($mysql_result2);
			$TourId 	=	$row2[2];
			$query3 = "SELECT * FROM TOUR WHERE TOUR_ID = '$TourId'";
			$mysql_result3 = mysql_query($query3, $Localmysql_link);
			$numrows3 = mysql_num_rows($mysql_result3);
			if ($numrows3 > 0)
			{
				$row3 = mysql_fetch_array($mysql_result3);
				$CityId = $row3[3];
				$query4 = "SELECT * FROM CITY WHERE CITY_ID = '$CityId'";
				$mysql_result4 = mysql_query($query4, $Localmysql_link);
				$numrows4 = mysql_num_rows($mysql_result4);
				if ($numrows4 > 0)
				{
					$row4 = mysql_fetch_array($mysql_result4);
					$LocalDepartCity = $row4[1];
				}
			}
			$LocalDepartDate = 	$row2[3];
		}
	}
	return ($LocalDepartCity);

}

function get_date($LocalScheduledTourBookId, $Localmysql_link)
{
	$query = "SELECT * FROM SCHEDULEDTOURBOOK WHERE SCHEDULEDTOURBOOK_ID = '$LocalScheduledTourBookId' ORDER BY SCHEDULEDTOURBOOK_ID ASC";
	$mysql_result = mysql_query($query, $Localmysql_link);
	$numrows = mysql_num_rows($mysql_result);
	if ($numrows > 0)
	{
		$row = mysql_fetch_array($mysql_result);
		// get row result
		$ScheduledTourBookId = $row[0];
		$ScheduledTourId = $row[1];
		$query2 = "SELECT * FROM SCHEDULEDTOUR WHERE SCHEDULEDTOUR_ID = '$ScheduledTourId'";
		//print($query2);
		$mysql_result2 = mysql_query($query2, $Localmysql_link);
		$numrows2 = mysql_num_rows($mysql_result2);
		if ($numrows2 > 0)
		{
			$row2 		= 	mysql_fetch_array($mysql_result2);
			$TourId 	=	$row2[2];
			$query3 = "SELECT * FROM TOUR WHERE TOUR_ID = '$TourId'";
			$mysql_result3 = mysql_query($query3, $Localmysql_link);
			$numrows3 = mysql_num_rows($mysql_result3);
			if ($numrows3 > 0)
			{
				$row3 = mysql_fetch_array($mysql_result3);
				$CityId = $row3[3];
				$query4 = "SELECT * FROM CITY WHERE CITY_ID = '$CityId'";
				$mysql_result4 = mysql_query($query4, $Localmysql_link);
				$numrows4 = mysql_num_rows($mysql_result4);
				if ($numrows4 > 0)
				{
					$row4 = mysql_fetch_array($mysql_result4);
					$LocalDepartCity = $row4[1];
				}
			}
			$LocalDepartDate = 	$row2[3];
		}
	}
	return ($LocalDepartDate);
}



function get_record(	$ScheduledTourBookId,
						$ScheduledTourId,
						$LastName,
						$FirstName,
						$AgentId,
						$PULocationId,
						$mysql_link)
{

	$query = get_query(	$ScheduledTourBookId,
						$ScheduledTourId,
						$LastName,
						$FirstName,
						$AgentId,
						$PULocationId,
						$mysql_link);

	//print($query);

	if ($mysql_link != 0)
	{
		$mysql_result = mysql_query($query, $mysql_link);
		if ($mysql_result)
		{
			$numrows = mysql_num_rows($mysql_result);
			if ($numrows > 0)
			{
				$row = mysql_fetch_array($mysql_result);
			}
			else
			{
				$row[0] = 0;
			}
		}
		else
		{
			$row[0] = 0;
		}
	}
	else
	{
		$row[0] = 0;
	}
	return ($row);
}

function get_query(		$ScheduledTourBookId,
						$ScheduledTourId,
						$LastName,
						$FirstName,
						$AgentId,
						$PULocationId,
						$mysql_link)
{
	$index = 0;


	if ($AgentId)
	{
		$where1 = "AGENT_ID = '$AgentId'";
		$WhereArray[$index] = $where1;
		$index = $index + 1;
	}
	if ($ScheduledTourBookId)
	{
		$where2 = "SCHEDULEDTOURBOOK_ID = '$ScheduledTourBookId'";
		$WhereArray[$index] = $where2;
		$index = $index + 1;
	}
	if ($ScheduledTourId)
	{
		$where3 = "SCHEDULEDTOUR_ID	= '$ScheduledTourId'";
		$WhereArray[$index] = $where3;
		$index = $index + 1;
	}
	if ($LastName)
	{
		$where4 = "LAST_NAME = '$LastName'";
		$WhereArray[$index] = $where4;
		$index = $index + 1;
	}
	if ($FirstName)
	{
		$where5 = "FIRST_NAME = '$FirstName'";
		$WhereArray[$index] = $where5;

		$index = $index + 1;
	}
	if ($PULocationId)
	{
		$where6 = "PULOCATION_ID = '$PULocationId'";
		$WhereArray[$index] = $where6;
		$index = $index + 1;
	}

	$indexlimit = $index;

	if ($indexlimit == 1)
	{
		$query = sprintf("SELECT * FROM SCHEDULEDTOURBOOK WHERE %s ORDER BY SCHEDULEDTOURBOOK_ID ASC",$WhereArray[0]);
	}
	if ($indexlimit == 2)
	{
		$query = sprintf("SELECT * FROM SCHEDULEDTOURBOOK WHERE %s AND %s ORDER BY SCHEDULEDTOURBOOK_ID ASC",$WhereArray[0],$WhereArray[1]);
	}
	if ($indexlimit == 3)
	{
		$query = sprintf("SELECT * FROM SCHEDULEDTOURBOOK WHERE %s AND %s AND %s ORDER BY SCHEDULEDTOURBOOK_ID ASC",$WhereArray[0],$WhereArray[1],$WhereArray[2]);
	}
	if ($indexlimit == 4)
	{
		$query = sprintf("SELECT * FROM SCHEDULEDTOURBOOK WHERE %s AND %s AND %s AND %s ORDER BY SCHEDULEDTOURBOOK_ID ASC",$WhereArray[0],$WhereArray[1],$WhereArray[2],$WhereArray[3]);
	}
	if ($indexlimit == 5)
	{
		$query = sprintf("SELECT * FROM SCHEDULEDTOURBOOK WHERE %s AND %s AND %s AND %s AND %s ORDER BY SCHEDULEDTOURBOOK_ID ASC",$WhereArray[0],$WhereArray[1],$WhereArray[2],$WhereArray[3],$WhereArray[4]);
	}
	if ($indexlimit == 6)
	{
		$query = sprintf("SELECT * FROM SCHEDULEDTOURBOOK WHERE %s AND %s AND %s AND %s AND %s AND %s ORDER BY SCHEDULEDTOURBOOK_ID ASC",$WhereArray[0],$WhereArray[1],$WhereArray[2],$WhereArray[3],$WhereArray[4],$WhereArray[5]);
	}

	//print($query);

	return ($query);
}

function insert_new_record(	$ScheduledTourBookId,
						  	$ScheduledTourId,
						   	$DepartCity,
						   	$DepartDate,
						   	$LastName,
						   	$FirstName,
						   	$Gender,
						   	$Occupancy,
						   	$AgentId,
						   	$TimeStamp,
						   	$LocationId,
						   	$TotalPayToDLT,
						   	$PayToDLTId,
						   	$PayToDLTDate,
							$TotalPayToAgent,
							$PayToAgentId,
							$PayToAgentDate,
							$TotalPayCC,
							$PayByCCId,
							$PayCCDate,
							$Email,
							$ManSuppCharge,
						   	$mysql_link)
{
	$row = get_record(	$ScheduledTourBookId,
						  	$ScheduledTourId,
						   	$LastName,
						   	$FirstName,
						   	$AgentId,
						   	$PULocationId,
						   	$mysql_link);

	if ($row[0] == 0)
	{
		$current_time = getdate(time());
		$current_hours = $current_time["hours"];
		$current_mins = $current_time["minutes"];
		$current_secs = $current_time["seconds"];
		$current_date = date("Y-m-d");
		$Gender = strtoupper($Gender);
		if ($Gender == "") $Gender = "M";
		$LastName = strtoupper($LastName);
		$FirstName = strtoupper($FirstName);
		$TimeStamp = sprintf("%s %d:%d:%d",$current_date,$current_hours,$current_mins,$current_secs);

		$query = "INSERT INTO SCHEDULEDTOURBOOK
					(SCHEDULEDTOURBOOK_ID,
						SCHEDULEDTOUR_ID,
						LAST_NAME,
						FIRST_NAME,
						AGENT_ID,
						GENDER,
						OCCUPANCY,
						SCHEDULEDTOURBOOK_TIMESTAMP,
						PULOCATION_ID,
						TOTALPAYTODLT,
						PAYTODLT_ID,
						PAYTODLTDATE,
						TOTALPAYTOAGENT,
						PAYTOAGENT_ID,
						PAYTOAGENTDATE,
						TOTALPAYCC,
						PAYBYCC_ID,
						PAYCCDATE,
						EMAIL,
						MANSUPPCHARGE)
						VALUES
					('$ScheduledTourBookId',
					'$ScheduledTourId',
					'$LastName',
					'$FirstName',
					'$AgentId',
					'$Gender',
					'$Occupancy',
					'$TimeStamp',
					'$LocationId',
				   	'$TotalPayToDLT',
				   	'$PayToDLTId',
				   	'$PayToDLTDate',
					'$TotalPayToAgent',
					'$PayToAgentId',
					'$PayToAgentDate',
					'$TotalPayCC',
					'$PayByCCId',
					'$PayCCDate',
					'$Email',
					'$ManSuppCharge')";
		//print($query);

		$mysql_result = mysql_query($query, $mysql_link);
	}

}


function get_total_occupancy($ScheduledTourId, $mysql_link)
{
	$query = "SELECT SCHEDULEDTOUR_OCCUPANCY FROM SCHEDULEDTOUR WHERE SCHEDULEDTOUR_ID = '$ScheduledTourId'";
	$mysql_result = mysql_query($query, $mysql_link);
	$numrows = mysql_num_rows($mysql_result);
	if ($numrows > 0)
	{
		$row 		= 	mysql_fetch_array($mysql_result);
		$ScheduledTourOccupancy 	=	$row["SCHEDULEDTOUR_OCCUPANCY"];
	}
	return ($ScheduledTourOccupancy);
}

function check_if_space_available($ThisTourOccupancy, $ScheduledTourId, $mysql_link)
{

	$query = "SELECT * FROM SCHEDULEDTOUR WHERE SCHEDULEDTOUR_ID = '$ScheduledTourId'";
	$mysql_result = mysql_query($query, $mysql_link);
	$numrows = mysql_num_rows($mysql_result);
	if ($numrows > 0)
	{
		$row 		= 	mysql_fetch_array($mysql_result);
		$ScheduledTourOccupancy 	=	$row[6];
		$ScheduledTourAvailable 	=	$row[17];
	}

	//print("Requested Occupancy = ");
	//print($ThisTourOccupancy);
	//print("<br>\n");
	//print("Current Available Occupancy = ");
	//print($ScheduledTourAvailable);
	//print("<br>\n");
	//print("Current Total Occupancy = ");
	//print($ScheduledTourOccupancy);
	//print("<br>\n");


	if ($ThisTourOccupancy == "")
		return ("TRUE");
	else
	{
		if (intval($ThisTourOccupancy) > intval($ScheduledTourAvailable))
		{
			return("FALSE");
		}
		else
		{
			return ("TRUE");
		}
	}
}

function get_tour_space_used($ScheduledTourId, $mysql_link)
{
		// calculate new space available
		$TotalTourOccupancy = 0;


		$query = "SELECT sum(OCCUPANCY) FROM SCHEDULEDTOURBOOK WHERE SCHEDULEDTOUR_ID = '$ScheduledTourId' and SCHEDULEDTOURBOOK_TIMESTAMP > '2011-01-01 00:00:00'";
		$mysql_result = mysql_query($query, $mysql_link);
		$numrows = mysql_num_rows($mysql_result);
		if ($numrows > 0)

		{
			while ($row = mysql_fetch_array($mysql_result))
			{
				//$Occupancy 	=	$row[0];
				$TotalTourOccupancy = $row["OCCUPANCY"];
//				$Occupancy = 1;
//				$TotalTourOccupancy = intval($Occupancy) + intval($TotalTourOccupancy);
			}
		}

		//print("Total Occupancy (from booking table) for this tour = ");
		//print($TotalTourOccupancy);
		//print("\n");

		return ($TotalTourOccupancy);

}

function adjust_space_available($ScheduledTourId,$mysql_link)
{

		// get total occupancy used on this tour (from all customers, including current one)

		$CurrentTourOccupancyUsed = get_tour_space_used($ScheduledTourId, $mysql_link);

		$TotalOccupancy = get_total_occupancy($ScheduledTourId, $mysql_link);

		$TourAvailable = intval($TotalOccupancy) - intval($CurrentTourOccupancyUsed);
		
		if($TourAvailable < 0)
			$TourAvailable = 0;

		//print("After adjustment...<br>");
		//print("Current Total Tour Occupacy Requested...<br>");
		//print($CurrentTourOccupancyUsed);
		//print("<br>\n");
		//print("Total Tour Occupacy...<br>");
		//print($TotalOccupancy);
		//print("<br>\n");
		//print("Available Tour Occupacy...<br>");
		//print($TourAvailable);
		//print("<br>\n");

		$current_time = getdate(time());
		$current_hours = $current_time["hours"];
		$current_mins = $current_time["minutes"];
		$current_secs = $current_time["seconds"];
		$current_date = date("Y-m-d");

		$query = "UPDATE SCHEDULEDTOUR SET
					SCHEDULEDTOUR_AVAILABLE = '$TourAvailable'
					WHERE SCHEDULEDTOUR_ID = '$ScheduledTourId'";
					
		// no need to update scheduled tour timestamp with every reservation

		
/*		$TimeStamp = sprintf("%s %d:%d:%d",$current_date,$current_hours,$current_mins,$current_secs);

		$query = "UPDATE SCHEDULEDTOUR SET
					SCHEDULEDTOUR_AVAILABLE = '$TourAvailable',
					SCHEDULEDTOUR_TIMESTAMP = '$TimeStamp'
					WHERE SCHEDULEDTOUR_ID = '$ScheduledTourId'";
*/
		//print($query);
		$mysql_result = mysql_query($query, $mysql_link);
}

?>