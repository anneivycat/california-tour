


<?
// table formatting specific code for the Invoice table entry screen

print("<TABLE ALIGN='CENTER' WIDTH='100%'>\n");

if ((!$View) AND (!$PrintToFile))
{
	print("<TR>\n");
	print("<TD>\n");
	print("$ResultString\n");
	print("$ResultString2\n");
	print("$ResultString3\n");
	print("</TD>\n");
	print("</TR>\n");
}

if ((!$List) AND (!$View) AND (!$PrintToFile))
{ // if no action, display search form & add button

print("<TR>\n");
print("<TD valign='top'>\n");

print("<TABLE ALIGN='CENTER' WIDTH='100%'>\n");

print("<TR>");print("<TR>");
print("<TD ALIGN='LEFT' valign='top'><Font color='blue'> * Invoice ID:</TD>");
print("<TD ALIGN='LEFT' valign='top'><input type='text' size='4' name='InvoiceId' value='$InvoiceId'></TD>\n");
print("</TR>");

print("<TR>");
print("<TD ALIGN='LEFT' valign='top'><Font color='blue'>Created:</TD>");
print("<TD ALIGN='LEFT' valign='top'>$TimeStamp</TD>");
print("</TR>");

print("<TR>");
print("<TD ALIGN='LEFT' valign='top'><Font color='blue'>Status:</TD>");
print("<TD ALIGN='LEFT' valign='top'>$InvoiceStatus</TD>");
print("</TR>");

print("<TR>");
print("<TD ALIGN='LEFT' valign='top'><Font color='blue'>* Invoice Date:</TD>");
print("<TD ALIGN='LEFT' valign='top'><input type='text' size='12' name='InvoiceDate' value='$InvoiceDate'></TD>");
print("</TR>");

print("<TR>");
print("<TD ALIGN='LEFT' valign='top'><Font color='blue'>Amount Due:</TD>");
$InvoiceAmount = sprintf("%5.2f", $InvoiceAmount);

print("<TD ALIGN='LEFT' valign='top'>$InvoiceAmount</TD>");
print("</TR>");

print("<TR>");
print("<TD ALIGN='LEFT' valign='top'><Font color='blue'>* Agent Code:</TD>");
print("<TD ALIGN='LEFT' valign='top'><SELECT name = 'AgentId' size='1'>\n");

// connect to book type table to get list of book types

	$query = "SELECT * FROM AGENT WHERE AGENT_ID = '$AgentId'";
	$mysql_result = mysql_query($query, $mysql_link);

	$numrows = 0;
	$BookTypeDesc = "";
	$TempBookTypeDesc = "";
	$numrows = mysql_num_rows($mysql_result);

	if ($numrows > 0)
	{
		$row = mysql_fetch_array($mysql_result);
		$TempAgentId = $row[0];
		$AgentCode = strtoupper($row[1]);
		print("<OPTION value='$TempAgentId'>$AgentCode</OPTION>\n");
	}

	print("<OPTION value=''>NONE</OPTION>\n");


	$query = "SELECT * FROM AGENT ORDER BY AGENT_CODE ASC";
	$mysql_result = mysql_query($query, $mysql_link);
	$numrows = mysql_num_rows($mysql_result);
	if ($numrows > 0)
	{
		while ($row = mysql_fetch_array($mysql_result))
		{
			// get row result
			$TempAgentId = $row[0];
			$TempAgentCode = strtoupper($row[1]);
			if ($TempAgentCode <> $AgentCode)
			{
				print("<OPTION value='$TempAgentId'>$TempAgentCode</OPTION>\n");
			}
		}
	}

print("</SELECT>\n");
print("</TD>\n");
print("</TR>");


print("<TR>");
print("<TD ALIGN='LEFT' valign='top'><Font color='blue'>* Trip Name:</TD>");

print("<TD ALIGN='LEFT' valign='top'><SELECT name = 'BookId' size='1'>\n");


	if ($BookId)
	{
		$query2 = "SELECT * FROM SCHEDULEDTOUR WHERE SCHEDULEDTOUR_ID = '$BookId'";
			$mysql_result2 = mysql_query($query2, $mysql_link);

			$numrows2 = 0;
			$numrows2 = mysql_num_rows($mysql_result2);

		//	print("Num rows = $numrows\n");

			if ($numrows2 > 0)
			{
				while ($row2 = mysql_fetch_array($mysql_result2))
				{
					$TempBookId = $row2[0];
					$ScheduledTourCode = $row2[1];
					print("<OPTION value='$TempBookId'>$ScheduledTourCode</OPTION>\n");
				}
			}

	}

	print("<OPTION value=''>NONE</OPTION>\n");

			// default search is for SCHEDULED TOURS

			$query = "SELECT * FROM SCHEDULEDTOUR ORDER BY SCHEDULEDTOUR_CODE ASC";
			$mysql_result = mysql_query($query, $mysql_link);

			$numrows = 0;
			$numrows = mysql_num_rows($mysql_result);

//			print("Num rows = $numrows\n");

			if ($numrows > 0)
			{
				while ($row = mysql_fetch_array($mysql_result))
				{
					$TempBookId = $row[0];
					$TempScheduledTourCode = $row[1];
					if ($TempScheduledTourCode <> $ScheduledTourCode)
					{
						print("<OPTION value='$TempBookId'>$TempScheduledTourCode</OPTION>\n");
					}
				}
			}



print("</SELECT>\n");
print("</TD>\n");
print("</TR>");

}

else
{ // if action (list, view, print to file

	if ($List)
	{
		print("<TR>");
		print("<TD ALIGN='LEFT' valign='top' colspan='2'>");
		print("<input type='submit' name='First' value='Back'>\n");
		print("</TD>");
		print("</TR>");
	}
}

if ((!$View) AND (!$PrintToFile))
{
	// end of normal operations
		print("<TR>");
		print("<TD ALIGN='LEFT' valign='top' colspan='4'><hr>");
		print("</TD>");
		print("</TR>");

/*
		print("<TR><TD>");
		print("<br>");
		print("<br>");
		print("<br>");
		print("<br>");
		print("<br>");
		print("<br>");
		print("<br>");
		print("<br>");
		print("</TD></TR>");
*/
/*
		print("<TR>");
		print("<TD ALIGN='LEFT' valign='top' colspan='4'><Font color='blue'>Search String:</font><br>\n");
		print("<TEXTAREA name='QueryString' rows='10' cols='50' readonly>");
		print("$QueryString");
		print("</TEXTAREA></TR>\n");
*/

}

print("<input type=\"hidden\" name=\"QueryString\" value=\"$QueryString\">\n");


		print("</TABLE>\n");
		print("</TD>\n");


if ((!$List) AND (!$View) AND (!$PrintToFile))
{
	include ("invmenutbl.php"); // displays control buttons on side of form
}

?>