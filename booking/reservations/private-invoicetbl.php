


<?
// table formatting specific code for the Invoice table entry screen

print("<TABLE ALIGN='CENTER' WIDTH='100%'>\n");

if ((!$View) AND (!$PrintToFile))
{
	print("<TR>\n");
	print("<TD>\n");
	print("$ResultString\n");
	print("$ResultString2\n");
	print("$ResultString3\n");
	print("</TD>\n");
	print("</TR>\n");
}

if ((!$List) AND (!$View) AND (!$PrintToFile))
{

print("<TR>\n");
print("<TD valign='top'>\n");

print("<TABLE ALIGN='CENTER' WIDTH='100%'>\n");

print("<TR>\n");
print("<TD ALIGN='LEFT' valign='top'><Font color='blue'> * FIT Invoice #:</TD>");
print("<TD ALIGN='LEFT' valign='top'><input type='text' size='4' name='InvoiceId' value='$InvoiceId'></TD>\n");
print("</TR>\n");

print("<TR>\n");
print("<TD ALIGN='LEFT' valign='top'><Font color='blue'>Status:</TD>");
print("<TD ALIGN='LEFT' valign='top'>$InvoiceStatus</TD>");
print("</TR>\n");

print("<TR>\n");
print("<TD ALIGN='LEFT' valign='top'><Font color='blue'>* Invoice Date:</TD>");
print("<TD ALIGN='LEFT' valign='top'><input type='text' size='12' name='InvoiceDate' value='$InvoiceDate'></TD>");
print("</TR>\n");

//print($FITBookId);

print("<TR>");
print("<TD ALIGN='LEFT' valign='top'><Font color='blue'>* FIT Book #:</TD>");

print("<TD ALIGN='LEFT' valign='top'><input type='text' name='FITBookId' value='$FITBookId' size='5'></TD>\n");
print("</TR>");



	if ($FITBookId)
	{
		$query2 = "SELECT * FROM FITBOOK WHERE FITBOOK_ID = '$FITBookId'";
			$mysql_result2 = mysql_query($query2, $mysql_link);

			$numrows2 = 0;
			$numrows2 = mysql_num_rows($mysql_result2);

		//	print("Num rows = $numrows\n");

			if ($numrows2 > 0)
			{

				print("<TR>");
				print("<TD ALIGN='LEFT' valign='top'><Font color='blue'>* FIT Code:</TD>");

				print("<TD ALIGN='LEFT' valign='top'>\n");

				$row2 = mysql_fetch_array($mysql_result2);
				$TempBookId = $row2[0];
				$FITBookCode = $row2[1];
				print($FITBookCode);

				print("</TD>\n");
				print("</TR>");

			}

	}




/*

print("<TR>");
print("<TD ALIGN='LEFT' valign='top'><Font color='blue'>* FIT Name:</TD>");

print("<TD ALIGN='LEFT' valign='top'><SELECT name = 'FITBookId' size='1'>\n");


	if ($FITBookId)
	{
		$query2 = "SELECT * FROM FITBOOK WHERE FITBOOK_ID = '$FITBookId'";
			$mysql_result2 = mysql_query($query2, $mysql_link);

			$numrows2 = 0;
			$numrows2 = mysql_num_rows($mysql_result2);

		//	print("Num rows = $numrows\n");

			if ($numrows2 > 0)
			{
				while ($row2 = mysql_fetch_array($mysql_result2))
				{
					$TempBookId = $row2[0];
					$FITBookCode = $row2[1];
					print("<OPTION value='$TempBookId'>$FITBookCode</OPTION>\n");
				}
			}

	}

	print("<OPTION value=''>NONE</OPTION>\n");


			$query = "SELECT * FROM FITBOOK ORDER BY FITBOOK_CODE ASC";
			$mysql_result = mysql_query($query, $mysql_link);

			$numrows = 0;
			$numrows = mysql_num_rows($mysql_result);

//			print("Num rows = $numrows\n");

			if ($numrows > 0)
			{
				while ($row = mysql_fetch_array($mysql_result))
				{
					$TempBookId = $row[0];
					$TempFITBookCode = $row[1];
					if ($TempFITBookCode <> $FITBookCode)
					{
						print("<OPTION value='$TempBookId'>$TempFITBookCode</OPTION>\n");
					}
				}
			}



print("</SELECT>\n");
print("</TD>\n");
print("</TR>");

*/

	if ($FITBookId)
	{
		$query2 = "SELECT AGENT_ID,SERVICE_DESC,SERVICE_CHARGE,COMM_CHARGE, FITBOOK_CODE,PAY_AGT,PAY_CC,PAY_CLT FROM FITBOOK WHERE FITBOOK_ID = '$FITBookId'";
			$mysql_result2 = mysql_query($query2, $mysql_link);

			$numrows2 = 0;
			$numrows2 = mysql_num_rows($mysql_result2);

		//	print("Num rows = $numrows\n");

			if ($numrows2 > 0)
			{
				$row2 = mysql_fetch_array($mysql_result2);
				$AgentId = $row2[0];
				$ServiceDesc = $row2[1];
				$ServiceCharge = $row2[2];
				$CommCharge = $row2[3];
				$FITBookCode = $row2[4];
				$PayAGT = $row2[5];
				$PayCC = $row2[6];
				$PayCLT = $row2[7];

				$query3 = "SELECT AGENT_CODE FROM AGENT WHERE AGENT_ID = '$AgentId'";
				$mysql_result3 = mysql_query($query3, $mysql_link);
				$row3 = mysql_fetch_array($mysql_result3);

				$AgentCode = $row3[0];


				$InvoiceAmount = $ServiceCharge  - $CommCharge - $PayCC - $PayCLT;
				$InvoiceAmount = sprintf("%5.2f", $InvoiceAmount);
			}

	}


	if ($InvoiceAmount)
	{
		print("<TR>");
		print("<TD ALIGN='LEFT' valign='top'><Font color='blue'>Invoice Amount:</font></TD>");
		if ($InvoiceStatus == "VOID")
		{
			print("<TD ALIGN='LEFT' valign='top'>0.00</TD>");
		}
		else
		{
			print("<TD ALIGN='LEFT' valign='top'>$InvoiceAmount</TD>");

		}
		print("</TR>\n");
	}

	if ($AgentCode)
	{
		print("<TR>");
		print("<TD ALIGN='LEFT' valign='top'><Font color='blue'>Agent/Cust:</font></TD>");
		print("<TD ALIGN='LEFT' valign='top'>$AgentCode</TD>");
		print("</TR>");
	}

	if ($ServiceDesc)
	{
		print("<TR>");
		print("<TD ALIGN='LEFT' valign='top'><Font color='blue'>Service:</font></TD>");
		print("<TD ALIGN='LEFT' valign='top'>$ServiceDesc</TD>");
		print("</TR>");
	}

}

else
{

	if ($List)
	{
		print("<TR>");
		print("<TD ALIGN='LEFT' valign='top' colspan='2'>");
		print("<input type='submit' name='First' value='Back'>\n");
		print("</TD>");
		print("</TR>");
	}
}


if ((!$View) AND (!$PrintToFile))
{
	// end of normal operations
		print("<TR>");
		print("<TD ALIGN='LEFT' valign='top' colspan='4'><hr>");
		print("</TD>");
		print("</TR>");


}

print("<input type=\"hidden\" name=\"QueryString\" value=\"$QueryString\">\n");

		print("</TABLE>\n");
		print("</TD>\n");


if ((!$List) AND (!$View) AND (!$PrintToFile))
{
	include ("private-invmenutbl.php");
}

?>