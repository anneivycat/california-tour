		    <?php // echo "Hello \"$PHP_SELF\""; ?>
		    <table cellpadding="0" cellspacing="0" border="0" class="blue" style="width:300px;margin:10px 10px 10px 10px">
			<tr>
			    <th colspan="2"><h4 style="border:0;">
			    <?php
				if (@$PayCash > 0) {
				    echo "** Customer Receipt **<br>";
				} else {
				    echo "** Reservation Summary **<br>";
				}
			    ?>
			    </h4>
			    </th>
			</tr>
			    <TD class='blueType' nowrap>
				<?php
				    if (@$PayCash > 0) {
					echo "Reservation #";
				    }
				    // else { echo "Temporary Reservation #"; }
				?>
			    </TD>
			    <td class='subtotal'><?php  //echo $ScheduledTourBookId; ?></td>
			</TR>
			<TR>
			    <td class='blueType'>

			    </TD>
			    <TD class='subtotal'><?php //echo $ScheduledTourCode; ?></TD>
			</TR>
			<TR>
			    <td class='blueType'>
				Tour Name:
			    </TD>
			<TD class='subtotal'><?php echo $TourName; ?></TD>
			</TR>
			<TR>
			    <td class='blueType'>
				Departing:
			    </TD>
			    <TD class='subtotal'><?php echo date("F j, Y" , strtotime($DepartDate)); ?></TD>
			</TR>
			<TR>
			    <td class='blueType'>
				From:
			    </TD>
			    <TD class='subtotal'>
				<?php echo $CityCode; ?>
			    </TD>
			</TR>
			<TR>
			    <td class='blueType'>
				Returning:
			    </TD>
			    <TD class='subtotal'>
				<?php echo date("F j, Y" , strtotime($ReturnDate)); ?>
			    </TD>
			</TR>
			<TR>
			    <td class='blueType'>
				Pick-Up Location:
			    </TD>
			    <TD class='subtotal'>
				 <?php echo $PULocationLocation; ?>
			    </TD>
			</TR>
			<?php
			    if($DepartDate != $ReturnDate) {
				$roommate_array  = array('Y' => 'Yes', 'N' => 'No');
				$occupancy_array = array('1' => 'Single', '2' => 'Double', '3' => 'Triple', '4' => 'Quad');
			?>
				<TR>
				    <td class='blueType'>
					Roommate:
				    </TD>
				    <TD class='subtotal'>
					<?php
					    if(@$_POST['Occupancy'] != 1)
						echo 'Yes';
					    else
						echo 'No';
					?>
				    </TD>
				</TR>
				<TR>
				    <td class='blueType'>
					Occupancy:
				    </TD>
				    <TD class='subtotal'>
					<?php echo @$occupancy_array[@$_POST['Occupancy']]; ?>
				    </TD>
				</TR>
			<?php
			  }
			?>
			<tr>
			    <td colspan='2'>&nbsp;</td>
			</tr>
			<TR>
			    <td class='blueType'>
				Basic Tour Charge:
			    </TD>
			    <td class='subtotal'>
				<?php
				   $TourPrice = sprintf("%5.2f", $TourPrice);
				   echo $TourPrice;
				?>
			    </td>
			</TR>
			<TR>
			    <td class='blueType'>
				Supp Room Charges:
			    </TD>
			    <td class='subtotal'>
				<?php
				    $RoomCharge = sprintf("%5.2f", $RoomCharge);
				    echo $RoomCharge;
				?>
			    </td>
			</TR>
			<?php
			    if ($TourDiscount > 0) {
			?>
				<TR>
				    <td class='blueType'>
					Student Discount:
				    </TD>
				    <td class='subtotal'>
					<?php
					    $ThisTourDiscount = sprintf("-%5.2f", $TourDiscount);
					    echo $ThisTourDiscount;
					?>
				    </td>
				</TR>
			<?php
			    }
			?>
			<?php
			    if (@$ManSuppCharge) {
			?>
				<TR>
				    <td class='blueType'>
					Manual Supp Charge:
				    </TD>
				    <td class='subtotal'>
					<?php
					   $ManSuppCharge = sprintf("%5.2f", $ManSuppCharge);
					   echo $ManSuppCharge;
					?>
				    </td>
				</TR>
			    <?php
			    }
			// to calculate student discount ($5.00)
			// check if valid school code (this is the same as the agent code)
			// direct student sign up is 5.00 discount on the regular price only
			// if an agent signs up a student, agent will use the Reservation system
			// directly.
			$query2         = "SELECT AGENT_ID FROM AGENT WHERE AGENT_CODE = '$SchoolCode'";
			$mysql_result2  = mysql_query($query2,$mysql_link);
			$row2           = mysql_fetch_array($mysql_result2);
			$SchoolSignupID = $row2[0];
			// default student discount (if no school code)
			$StudentDiscount = 10.00;
			$StudentDiscount = $StudentDiscount * $total_passenger;

			if ($SchoolSignupID) {
			?>
			    <tr>
			    <td class='blueType'>School Code:</td>
			    <td class='subtotal'><?php echo $SchoolCode; ?></td>
			    </tr>
			    <?php
			    if (!@$AgentProc) {
			    ?>
				<TR>
				    <td class='blueType'>Web Discount</TD>
				    <?php
					$StudentDiscount = sprintf("%5.2f", $TourWebDiscount);
					$ThisTotalCharge = $TotalCharge - $StudentDiscount;
				    ?>
				    <td class='subtotal'>-<?php echo $StudentDiscount; ?></td>
				</TR>
			    <?php
			    } else {
				$ThisTotalCharge = $TotalCharge;
			    }
			} else {
			    // if someone signup up on web, without an agent, then
			    // default web discount applies
			    // BUT, only if not processing a manual credit card job
			    if (((!$AgentCode) || ($AgentCode == "LAWEB") || ($AgentCode == "SFWEB") || ($AgentCode == "SDWEB")  || ($AgentCode == "SBWEB")) && (!$SchoolCode)) {
				$StudentDiscount = sprintf("%5.2f", $StudentDiscount);
				$ThisTotalCharge = $TotalCharge - $StudentDiscount;
			    ?>
				<TR>
				<td class='blueType'>Web Discount</TD>
				<td class='subtotal'>-<?php echo $StudentDiscount; ?></td>
				</TR>
			    <?php
			    } else {
			    ?>
				<TR>
				<td class='blueType'>Agent Code:</TD>
				<td class='subtotal'><?php echo $AgentCode; ?></td>
				</TR>
			    <?php
			    }
			}
			if ((!$AgentCode) && ($SchoolCode)) {
			    $AgentCode = $SchoolCode;
			}
			?>
			<TR>
			    <td class='blueType'>
				<span style='color: #ff0000'>
				Total Charges:
				</span>
			    </TD>
			    <br />
			    <td class='subtotal'>
				<span style='color: #ff0000'>
				<?php
				    if ($ThisTotalCharge >0) {
					$ThisTotalCharge = sprintf("%5.2f", $ThisTotalCharge);
				    } else {
					$ThisTotalCharge = sprintf("%5.2f", $TotalCharge);
				    }
				    echo $ThisTotalCharge;
				?>
				</span>
			    </td>
			</TR>
			<TR>
			    <td class='blueType'>
				Price per person:
			    </TD>
			    <td class='subtotal'>
				<?php
				    $PricePerPerson = sprintf("%5.2f", $ThisTotalCharge/$total_passenger);
				    echo $PricePerPerson;
				?>
			    </td>
			</TR>
			<?php
			if (@$AgentProc) {
			    /* break out portion to pay in Cash/CC */
			    $PayCash = sprintf("%5.2f", $PayCash);
			    $PayCC   = sprintf("%5.2f", $PayCC);
			    $CalcTotalCharge = $PayCash + $PayCC;
			    $Partial = "False";
			    if (($PayCash > 0) and ($PayCash < $ThisTotalCharge)) {
				$Partial = "True";
			    }
			    if (($PayCC > 0) and ($PayCC < $ThisTotalCharge)) {
				$Partial = "True";
			    }
			    $CalcTotalCharge = sprintf("%5.2f", $CalcTotalCharge);
			    if (($CalcTotalCharge <> $ThisTotalCharge) or ($Partial == "True")) {
				echo "<script language='Javascript'>\n";
				$StrLocation = "document.location='agenttoursignuptotal.php?AgentProcRefresh=True&LocalReservationId=$ScheduledTourBookId";
				$StrLocation = sprintf("%s'",$StrLocation);
				print($StrLocation);
				echo "</script>\n";
			    } else {
				$TotalCharge = $ThisTotalCharge;
				$TourDiscount = 0-$TourDiscount;
			    }
			    if ($PayCash >0) {
			    ?>
				<TR>
				    <td class='blueType'>Paid w/ Cash:</TD>
				    <td class='subtotal'><?php echo $PayCash; ?></td>
				</TR>
			    <?php
			    }
			    if ($PayCC >0) {
			    ?>
				<TR>
				<td class='blueType'>Paid w/ CC:</TD>
				<td class='subtotal'><?php echo $PayCC; ?></td>
				</TR>
			    <?php
			    }
			}

			if (@$PayCash > 0) {
			?>
			    <TR>
				<TD ALIGN='left' colspan='2'>Trip Info Link:</TD>
			    </TR>
			    <TR>
				<TD ALIGN='left' colspan='2'>
				<a href='http://<?php echo $HTTP_HOST; ?>/trip_info/english/<?php echo $TourType; ?>.html' class='Warning'>
					 http://<?php echo $HTTP_HOST; ?>/trip_info/english/<?php echo $TourType; ?>.html</a></TD>
			    </TR>
			    <?php
				$mailBody .= "Trip Info Link: http://" . $HTTP_HOST . "/trip_info/english/$TourType.html\n";
				$query_check        = "SELECT PULOCATION_CODE FROM PULOCATION WHERE PULOCATION_ID = '$PULocationId'";
				$mysql_result_check = mysql_query($query_check, $mysql_link);
				$numrows_check      = mysql_num_rows($mysql_result_check);
				$rowcheck           = mysql_fetch_array($mysql_result_check);
				$PULocationCode = $rowcheck[0];
			    ?>
			    <TR>
				<TD ALIGN='left' colspan='2'>PU Location Map:</TD>
			    </TR>
			    <TR>
				<TD ALIGN='left' colspan='2'>
				<a href='http://<?php echo $HTTP_HOST; ?>/PU_location/english/<?php echo $PULocationCode; ?>.gif' class='Warning'>
					 http://<?php echo $HTTP_HOST; ?>/PU_location/english/<?php echo $PULocationCode; ?>.gif</a></TD>
			    </TR>
			    <?php
			    if ($Email) {
			    ?>
				<TR>
				    <td class='blueType'>E-mail: </TD>
				    <td class='Warning'><br><?php echo $Email; ?></TD>
				</TR>
			    <?php
			    }
			    $mailBody .= "PU Location Map: http://" . $HTTP_HOST . "/PU_location/english/$PULocationCode.gif\n";
			}
			?>
		    </table>
