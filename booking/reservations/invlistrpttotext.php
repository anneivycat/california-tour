<?


// open a connection to mysql and select the "deltours" database


	$mysql_link = connect_to_db();
	$mysql_query = select_db($mysql_link);

		$current_time = getdate(time());
		$current_hours = $current_time["hours"];
		$current_mins = $current_time["minutes"];
		$current_secs = $current_time["seconds"];
		$current_date = date("Y-m-d");

		$TimeStamp = sprintf("%s %d:%d:%d",$current_date,$current_hours,$current_mins,$current_secs);


		$NewFileName = sprintf("invlist-%s.htm",$current_date);
		$NewFileNamePath = sprintf("/public_html/printouts/%s",$NewFileName);


		$SavedFile = fopen($NewFileNamePath,"w");

		/*
		** make sure the open was successful
		*/
		if(!($SavedFile))
		{
			fputs($SavedFile,"Error: ");
			fputs($SavedFile,"'printout.txt' could not be created\n");
			exit;
		}

include ("catourheadertofile.php");

/*
$query = "CREATE TABLE INVOICE (
			INVOICE_ID mediumint(2) NOT NULL default '0',
			INVOICE_AMOUNT decimal(4,2) NOT NULL default '0.00',
			INVOICE_DATE char (10) NOT NULL default '',
			INVOICE_AGENT_ID mediumint(2) NOT NULL default '0',
			INVOICE_STATUS enum ('VALID','VOID') NOT NULL default 'VALID',
			BOOKTYPE_ID tinyint(1) NOT NULL default '0',
			BOOK_ID mediumint(1) NOT NULL default '0',
			INVOICE_TIMESTAMP datetime NOT NULL default '0000-00-00 00:00:00',
			PRIMARY KEY (INVOICE_ID),


$query = "CREATE TABLE TOUR (
			TOUR_ID mediumint(2) NOT NULL default '0',
			TOUR_TYPE char(4) NOT NULL default '',
			TOUR_NAME char(40) NOT NULL default '',
			CITY_ID tinyint(1) NOT NULL default '0',
			TOUR_SUPP_SNG decimal(4,2) NOT NULL default '0.00',
			TOUR_SUPP_DBL decimal(4,2) NOT NULL default '0.00',
			TOUR_COMM1 decimal(4,2) NOT NULL default '0.00',
			TOUR_DISC1 decimal(4,2) NOT NULL default '0.00',
			TOUR_COMM2 decimal(4,2)

$query = "CREATE TABLE SCHEDULEDTOUR (
			SCHEDULEDTOUR_ID mediumint(2) NOT NULL default '0',
			SCHEDULEDTOUR_CODE char(12) NOT NULL default '',
			TOUR_ID mediumint(2) NOT NULL default '0',
			SCHEDULEDTOUR_DEPART_DATE char(10) NOT NULL default '',

$query = "CREATE TABLE CITY (
			CITY_ID tinyint(1) NOT NULL default '0',
			CITY_CODE char(2) NOT NULL default '',


*/

if ($CityId)
{
	if ($FromDate)
	{
		if ($ToDate)
		{
			if ($AgentId)
			{
				$query = "SELECT INVOICE_ID,
						INVOICE_DATE,
						INVOICE_AGENT_ID,
						BOOK_ID,
						INVOICE_STATUS,
						INVOICE_AMOUNT
						FROM INVOICE,SCHEDULEDTOUR,TOUR
					 	WHERE (INVOICE.BOOK_ID = SCHEDULEDTOUR.SCHEDULEDTOUR_ID
					 		AND SCHEDULEDTOUR.TOUR_ID = TOUR.TOUR_ID
							AND INVOICE_DATE >= '$FromDate'
							AND INVOICE_DATE <= '$ToDate'
							AND INVOICE_AGENT_ID = '$AgentId'
					 		AND TOUR.CITY_ID = '$CityId') ORDER BY INVOICE_DATE ASC";
			}
			else
			{
				$query = "SELECT INVOICE_ID,
						INVOICE_DATE,
						INVOICE_AGENT_ID,
						BOOK_ID,
						INVOICE_STATUS,
						INVOICE_AMOUNT
						FROM INVOICE,SCHEDULEDTOUR,TOUR
					 	WHERE (INVOICE.BOOK_ID = SCHEDULEDTOUR.SCHEDULEDTOUR_ID
					 		AND SCHEDULEDTOUR.TOUR_ID = TOUR.TOUR_ID
							AND INVOICE_DATE >= '$FromDate'
							AND INVOICE_DATE <= '$ToDate'
					 		AND TOUR.CITY_ID = '$CityId') ORDER BY INVOICE_DATE ASC";

			}
		}
		else
		{
			if ($AgentId)
			{
				$query = "SELECT INVOICE_ID,
							INVOICE_DATE,
							INVOICE_AGENT_ID,
							BOOK_ID,
							INVOICE_STATUS,
							INVOICE_AMOUNT
							FROM INVOICE,SCHEDULEDTOUR,TOUR
						 	WHERE (INVOICE.BOOK_ID = SCHEDULEDTOUR.SCHEDULEDTOUR_ID
						 		AND SCHEDULEDTOUR.TOUR_ID = TOUR.TOUR_ID
								AND INVOICE_DATE >= '$FromDate'
								AND INVOICE_AGENT_ID = '$AgentId'
						 		AND TOUR.CITY_ID = '$CityId') ORDER BY INVOICE_DATE ASC";
			}
			else
			{
					$query = "SELECT INVOICE_ID,
								INVOICE_DATE,
								INVOICE_AGENT_ID,
								BOOK_ID,
								INVOICE_STATUS,
								INVOICE_AMOUNT
								FROM INVOICE,SCHEDULEDTOUR,TOUR
							 	WHERE (INVOICE.BOOK_ID = SCHEDULEDTOUR.SCHEDULEDTOUR_ID
							 		AND SCHEDULEDTOUR.TOUR_ID = TOUR.TOUR_ID
									AND INVOICE_DATE >= '$FromDate'
							 		AND TOUR.CITY_ID = '$CityId') ORDER BY INVOICE_DATE ASC";

			}
		}
	}
	else
	{
		if ($AgentId)
		{
			$query = "SELECT INVOICE_ID,
						INVOICE_DATE,
						INVOICE_AGENT_ID,
						BOOK_ID,
						INVOICE_STATUS,
						INVOICE_AMOUNT
						FROM INVOICE,SCHEDULEDTOUR,TOUR
					 	WHERE (INVOICE.BOOK_ID = SCHEDULEDTOUR.SCHEDULEDTOUR_ID
					 		AND SCHEDULEDTOUR.TOUR_ID = TOUR.TOUR_ID
					 		AND INVOICE_AGENT_ID = '$AgentId'
					 		AND TOUR.CITY_ID = '$CityId') ORDER BY INVOICE_DATE ASC";
		}
		else
		{
			$query = "SELECT INVOICE_ID,
						INVOICE_DATE,
						INVOICE_AGENT_ID,
						BOOK_ID,
						INVOICE_STATUS,
						INVOICE_AMOUNT
						FROM INVOICE,SCHEDULEDTOUR,TOUR
					 	WHERE (INVOICE.BOOK_ID = SCHEDULEDTOUR.SCHEDULEDTOUR_ID
					 		AND SCHEDULEDTOUR.TOUR_ID = TOUR.TOUR_ID
					 		AND TOUR.CITY_ID = '$CityId') ORDER BY INVOICE_DATE ASC";

		}
	}

}
else
{
	if ($FromDate)
	{
		if ($ToDate)
		{
			if ($AgentId)
			{
				$query = "SELECT INVOICE_ID,
								INVOICE_DATE,
								INVOICE_AGENT_ID,
								BOOK_ID,
								INVOICE_STATUS,
								INVOICE_AMOUNT
								FROM INVOICE WHERE (INVOICE_DATE >= '$FromDate'
											AND INVOICE_DATE <= '$ToDate'
											AND INVOICE_AGENT_ID = '$AgentId')
								ORDER BY INVOICE_DATE ASC";
			}
			else
			{
				$query = "SELECT INVOICE_ID,
								INVOICE_DATE,
								INVOICE_AGENT_ID,
								BOOK_ID,
								INVOICE_STATUS,
								INVOICE_AMOUNT
								FROM INVOICE WHERE (INVOICE_DATE >= '$FromDate'
											AND INVOICE_DATE <= '$ToDate')
								ORDER BY INVOICE_DATE ASC";

			}
		}
		else
		{
			if ($AgentId)
			{
				$query = "SELECT INVOICE_ID,
								INVOICE_DATE,
								INVOICE_AGENT_ID,
								BOOK_ID,
								INVOICE_STATUS,
								INVOICE_AMOUNT FROM INVOICE
								WHERE (INVOICE_DATE >= '$FromDate'
										AND INVOICE_AGENT_ID = '$AgentId')
								ORDER BY INVOICE_DATE ASC";
			}
			else
			{
				$query = "SELECT INVOICE_ID,
								INVOICE_DATE,
								INVOICE_AGENT_ID,
								BOOK_ID,
								INVOICE_STATUS,
								INVOICE_AMOUNT FROM INVOICE
								WHERE INVOICE_DATE >= '$FromDate'
								ORDER BY INVOICE_DATE ASC";
			}
		}
	}
	else
	{
		if ($AgentId)
		{
			$query = "SELECT INVOICE_ID,INVOICE_DATE,INVOICE_AGENT_ID,BOOK_ID,INVOICE_STATUS,INVOICE_AMOUNT FROM INVOICE WHERE INVOICE_AGENT_ID = '$AgentId' ORDER BY INVOICE_DATE ASC";
		}
		else
		{
			$query = "SELECT INVOICE_ID,INVOICE_DATE,INVOICE_AGENT_ID, BOOK_ID,INVOICE_STATUS,INVOICE_AMOUNT FROM INVOICE ORDER BY INVOICE_DATE ASC";
		}
	}
}

//print($query);

$mysql_result = mysql_query($query, $mysql_link);

$numrows = mysql_num_rows($mysql_result);

if ($numrows > 0)
{

	// print fetched rows

	$current_time = getdate(time());
	$current_hours = $current_time["hours"];
	$current_mins = $current_time["minutes"];
	$current_secs = $current_time["seconds"];
	$current_date = date("Y-m-d");

	$TimeStamp = sprintf("%s %d:%d:%d",$current_date,$current_hours,$current_mins,$current_secs);

	fputs($SavedFile,"<form name='invoice' method='post' action='invoice.php'><br>\n");
	fputs($SavedFile,"<CENTER><b><font size='+1'>Invoice List</font></b></CENTER>\n");
	fputs($SavedFile,"<center><font size='-1' color='blue'>(-- Created $TimeStamp --)</font></center>\n");
	fputs($SavedFile,"<TABLE ALIGN='CENTER' WIDTH='100%'>");
	fputs($SavedFile,"\n");


	fputs($SavedFile,"<TABLE ALIGN='CENTER' WIDTH='100%'>");
	fputs($SavedFile,"\n");

	fputs($SavedFile,"<TR>\n");

	if ((!$FromDate) AND (!$ToDate))
	{
		fputs($SavedFile,"<TD ALIGN='LEFT' valign='top' colspan='2'><b>Date Range:</b></TD>");
		fputs($SavedFile,"<TD ALIGN='LEFT' valign='top' colspan='5'><b>ALL</b></TD>");

	}
	else
	{
		if (!$FromDate)
		{
			$FromDate = "ALL";
		}
		if (!$ToDate)
		{

			$ToDate = sprintf("%s", date("m/d/Y"));
		}
		fputs($SavedFile,"<TD ALIGN='LEFT' valign='top' colspan='2'><b>Date Range:</b></TD>");
		fputs($SavedFile,"<TD ALIGN='LEFT' valign='top' colspan='5'><b>$FromDate - $ToDate</b></TD>");
	}
	fputs($SavedFile,"</TR>\n");


	fputs($SavedFile,"<TR>");
	fputs($SavedFile,"\n");
	fputs($SavedFile,"<TD ALIGN='LEFT' valign='top'><b><u><Font color='black'>Inv. ID</font></u></b></TD>");
	fputs($SavedFile,"\n");
	fputs($SavedFile,"<TD ALIGN='LEFT' valign='top'><b><u><Font color='black'>Date</font></u></b></TD>");
	fputs($SavedFile,"\n");
	fputs($SavedFile,"<TD ALIGN='LEFT' valign='top'><b><u><Font color='black'>Agent Code</font></u></b></TD>");
	fputs($SavedFile,"\n");
	fputs($SavedFile,"<TD ALIGN='LEFT' valign='top'><b><u><Font color='black'>City</font></u></b></TD>");
	fputs($SavedFile,"\n");
	fputs($SavedFile,"<TD ALIGN='LEFT' valign='top'><b><u><Font color='black'>Trip Code</font></u></b></TD>");
	fputs($SavedFile,"\n");
	fputs($SavedFile,"<TD ALIGN='LEFT' valign='top'><b><u><Font color='black'>Stat</font></u></b></TD>");
	fputs($SavedFile,"\n");
	fputs($SavedFile,"<TD ALIGN='LEFT' valign='top'><b><u><Font color='black'>Amt</font></u></b></TD>");
	fputs($SavedFile,"\n");
	fputs($SavedFile,"</TR>");
	fputs($SavedFile,"\n");


	$TotalAmount = 0;

	while ($row = mysql_fetch_array($mysql_result))
	{


		$InvoiceId = $row[0];
		$InvoiceDate = $row[1];
		$InvoiceAgentId = $row[2];
		$BookId = $row[3];
		$Status = $row[4];
		$Amount = $row[5];

		$TotalAmount = $TotalAmount + $Amount;

		// print row result

		fputs($SavedFile,"<TR>");
		fputs($SavedFile,"\n");
		fputs($SavedFile,"<TD ALIGN='LEFT' valign='top'>$InvoiceId</TD>");
		fputs($SavedFile,"\n");
		fputs($SavedFile,"<TD ALIGN='LEFT' valign='top'>$InvoiceDate</TD>");
		fputs($SavedFile,"\n");

		$query1 = "SELECT * FROM AGENT WHERE AGENT_ID = '$InvoiceAgentId'";
		$mysql_result1 = mysql_query($query1, $mysql_link);

		$numrows1 = 0;
		$numrows = mysql_num_rows($mysql_result1);

		if ($numrows > 0)
		{
			$row1 = mysql_fetch_array($mysql_result1);
			$AgentCode = strtoupper($row1[1]);
			fputs($SavedFile,"<TD ALIGN='LEFT' valign='top'>$AgentCode</TD>");
			fputs($SavedFile,"\n");
		}
		else
		{
			fputs($SavedFile,"<TD ALIGN='LEFT' valign='top'>NONE</TD>");
			fputs($SavedFile,"\n");
		}


	//	fputs($SavedFile,"<TD ALIGN='LEFT' valign='top'>CITY</TD>");
	//	fputs($SavedFile,"\n");

//		$query1 = "SELECT * FROM BOOKTYPE WHERE BOOKTYPE_ID = '$BookTypeId'";
//		$mysql_result1 = mysql_query($query1, $mysql_link);
//
//		$numrows1 = 0;
//		$BookTypeDesc = "";
//		$TempBookTypeDesc = "";
//		$numrows1 = mysql_num_rows($mysql_result1);
//
//		if ($numrows1 > 0)
//		{
//			$row1 = mysql_fetch_array($mysql_result1);
//			$TempBookTypeId = $row1[0];
//			$BookTypeDesc = strtoupper($row1[1]);
//			fputs($SavedFile,"<TD ALIGN='LEFT' valign='top'>$BookTypeDesc</TD>\n");

//			if ($BookTypeDesc == "SCHEDULED TOUR")
//			{

				$query2 = "SELECT SCHEDULEDTOUR_ID, SCHEDULEDTOUR_CODE,TOUR_ID FROM SCHEDULEDTOUR WHERE SCHEDULEDTOUR_ID = '$BookId'";
				$mysql_result2 = mysql_query($query2, $mysql_link);

				$numrows2 = 0;
				$numrows2 = mysql_num_rows($mysql_result2);

				if ($numrows2 > 0)
				{
					$row2 = mysql_fetch_array($mysql_result2);
					$TempBookId = $row2[0];
					$ReferenceName = $row2[1];
					$TempTourId	= $row2[2];

					$query3 = "SELECT CITY_ID FROM TOUR WHERE TOUR_ID = '$TempTourId'";
					$mysql_result3 = mysql_query($query3, $mysql_link);
					$numrows3 = 0;
					$numrows3 = mysql_num_rows($mysql_result3);

					//print($query3);
					//print($numrows3);

					if ($numrows3 > 0)
					{

						$row3 = mysql_fetch_array($mysql_result3);
						$CityId = $row3[0];
						//print($CityId);
						$query4 = "SELECT CITY_CODE FROM CITY WHERE CITY_ID = '$CityId'";
						$mysql_result4 = mysql_query($query4, $mysql_link);
						$numrows4 = 0;
						$numrows4 = mysql_num_rows($mysql_result4);
						if ($numrows4 > 0)
						{
							$row4 = mysql_fetch_array($mysql_result4);
							$CityCode = $row4[0];
							fputs($SavedFile,"<TD ALIGN='LEFT' valign='top'>$CityCode</TD>");
							fputs($SavedFile,"\n");
						}
						else
						{
							fputs($SavedFile,"<TD ALIGN='LEFT' valign='top'>NONE</TD>");
							fputs($SavedFile,"\n");
						}
					}
					else
					{
						fputs($SavedFile,"<TD ALIGN='LEFT' valign='top'>NONE</TD>");
						fputs($SavedFile,"\n");
					}

					if ($ReferenceName == "")
					{
						fputs($SavedFile,"<TD ALIGN='LEFT' valign='top'>NONE</TD>\n");
					}
					else
					{
						fputs($SavedFile,"<TD ALIGN='LEFT' valign='top'>$ReferenceName</TD>\n");
					}
				}
				else
				{
					fputs($SavedFile,"<TD ALIGN='LEFT' valign='top'>NONE</TD>\n");
				}
				// other types go here when they are ready
//			}
//		}


		fputs($SavedFile,"<TD ALIGN='LEFT' valign='top'>$Status</TD>");
		fputs($SavedFile,"\n");

		fputs($SavedFile,"<TD ALIGN='LEFT' valign='top'>$Amount</TD>");
		fputs($SavedFile,"\n");
		fputs($SavedFile,"</TR>");

	}

	fputs($SavedFile,"<tr>");
	fputs($SavedFile,"<TD ALIGN='LEFT' valign='top' colspan='7'><hr></TD>");
	fputs($SavedFile,"\n");
	fputs($SavedFile,"</TR>");

	fputs($SavedFile,"<tr>");

	fputs($SavedFile,"<TD ALIGN='LEFT' valign='top' colspan='5'></TD>");
	fputs($SavedFile,"<TD ALIGN='LEFT' valign='top' colspan='1'><b>TOTAL:</b></TD>");
	$TotalAmount = sprintf("%5.2f", $TotalAmount);
	fputs($SavedFile,"<TD ALIGN='LEFT' valign='top' colspan='1'>$TotalAmount</TD>");
	fputs($SavedFile,"\n");
	fputs($SavedFile,"</TR>");

	fputs($SavedFile,"</TABLE>");

	fputs($SavedFile,"</form>\n");

	fclose($SavedFile); // close the file

	$FullPath = sprintf("http://www.california-tour.com/printouts/%s",$NewFileName);

	print("Click <a href='$FullPath'><b><font color='blue'>here</font></b></a> to view the latest Invoice List printout ($NewFileName)<br>\n");
	print("Click <a href='http://www.california-tour.com/printouts/'><b><font color='blue'>here</font></b></a> to view a complete list of printouts in this directory\n");



}
else
{

print("<SCRIPT LANGUAGE = 'JavaScript'>\n");
print("if (strLevel == \"1\")\n");
print("document.write(\"<a href='invlistrptmain.php?args=1'><b><font size='-1' color='blue'>Back to Invoice List Search</font></b></a>\");\n");
print("if (strLevel == \"2\")\n");
print("document.write(\"<a href='invlistrptmain.php?args=2'><b><font size='-1' color='blue'>Back to Invoice List Search</font></b></a>\");\n");
print("if (strLevel == \"4\")\n");
print("document.write(\"<a href='invlistrptmain.php?args=4'><b><font size='-1' color='blue'>Back to Invoice List Search</font></b></a>\");\n");
print("</SCRIPT>\n");
print("<br>");
print("*** NO MATCHES ***");
}

?>

