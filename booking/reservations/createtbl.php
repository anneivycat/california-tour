<HTML>

<BODY>

<?

include ("../sqlfuncs.php");

// open a connection to mysql

$mysql_link = connect_to_db();

// create the deltours database, if not already created

$mysql_result = mysql_create_db("califor_reservations", $mysql_link);

print("Create Database DELTOURS, result code = $mysql_result\n");
print("<br>");

$mysql_result = select_db($mysql_link);

print("Selecting Database DELTOURS, result code = $mysql_result\n");
print("<br>");

//$query = "DROP TABLE AGENT";

//$mysql_result = mysql_query($query, $mysql_link);

//print("TABLE AGENT dropped, result code = $mysql_result\n");
//print("<br>");

// create the AGENT table if not already created
$query = "CREATE TABLE AGENT (
			AGENT_ID mediumint(2) NOT NULL default '0',
			AGENT_CODE varchar(4) NOT NULL default '',
			AGENTTYPE_ID tinyint(1) NOT NULL default '0',
			AGENT_NAME varchar(40) NOT NULL default '',
			AGENT_CONTACT varchar(40) NOT NULL default '',
			AGENT_ADDR1 varchar(40) NOT NULL default '',
			AGENT_ADDR2 varchar(40) NOT NULL default '',
			AGENT_COMMISIONTYPE varchar(4) NOT NULL default '',
			PULOCATION_ID1 tinyint(1) NOT NULL default '0',
			PULOCATION_ID2 tinyint(1) NOT NULL default '0',
			PULOCATION_ID3 tinyint(1) NOT NULL default '0',
			AGENT_CITY varchar(20) NOT NULL default '',
			AGENT_STATE varchar(2) NOT NULL default '',
			AGENT_ZIP varchar(10) NOT NULL default '',
			AGENT_WEB varchar(40) NOT NULL default '',
			AGENT_EMAIL varchar(40) NOT NULL default '',
			AGENT_TEL1 varchar(15) NOT NULL default '',
			AGENT_TEL2 varchar(15) NOT NULL default '',
			AGENT_FAX varchar(15) NOT NULL default '',
			AGENT_COMMENTS longtext NOT NULL,
			AGENT_TIMESTAMP datetime NOT NULL default '0000-00-00 00:00:00',
			PRIMARY KEY (AGENT_ID),
			UNIQUE KEY AGENT_ID (AGENT_ID, AGENT_CODE),
			KEY AGENT_ID_2 (AGENT_ID)
		) TYPE=MyISAM";

$mysql_result = mysql_query($query, $mysql_link);

print("Create TABLE AGENT, result code = $mysql_result\n");
print("<br>");

$query = "CREATE TABLE AGENTTYPE (
			AGENTTYPE_ID tinyint(1) NOT NULL default '0',
			AGENTTYPE_DESC char(20) NOT NULL default '',
			AGENTTYPE_TIMESTAMP datetime NOT NULL default '0000-00-00 00:00:00',
			PRIMARY KEY (AGENTTYPE_ID),
			UNIQUE KEY AGENTTYPE_ID (AGENTTYPE_ID, AGENTTYPE_DESC),
			KEY AGENTTYPE_ID_2 (AGENTTYPE_ID)
		) TYPE=MyISAM";

$mysql_result = mysql_query($query, $mysql_link);

print("Create TABLE AGENTTYPE, result code = $mysql_result\n");
print("<br>");

$query = "CREATE TABLE AIRPORT (
			AIRPORT_ID tinyint(1) NOT NULL default '0',
			AIRPORT_CODE char(3) NOT NULL default '',
			AIRPORT_DESC char(20) NOT NULL default '',
			AIRPORT_TIMESTAMP datetime NOT NULL default '0000-00-00 00:00:00',
			PRIMARY KEY (AIRPORT_ID),
			UNIQUE KEY AIRPORT_ID (AIRPORT_ID, AIRPORT_DESC),
			KEY AIRPORT_ID_2 (AIRPORT_ID)
		) TYPE=MyISAM";

$mysql_result = mysql_query($query, $mysql_link);

print("Create TABLE AIRPORT, result code = $mysql_result\n");
print("<br>");

$query = "CREATE TABLE CITY (
			CITY_ID tinyint(1) NOT NULL default '0',
			CITY_CODE char(2) NOT NULL default '',
			CITY_DESC char(20) NOT NULL default '',
			CITY_TIMESTAMP datetime NOT NULL default '0000-00-00 00:00:00',
			PRIMARY KEY (CITY_ID),
			UNIQUE KEY CITY_ID (CITY_ID, CITY_DESC),
			KEY CITY_ID_2 (CITY_ID)
		) TYPE=MyISAM";

$mysql_result = mysql_query($query, $mysql_link);

print("Create TABLE CITY, result code = $mysql_result\n");
print("<br>");

$query = "CREATE TABLE INVOICE (
			INVOICE_ID mediumint(2) NOT NULL default '0',
			INVOICE_AMOUNT decimal(4,2) NOT NULL default '0.00',
			INVOICE_DATE char (10) NOT NULL default '',
			INVOICE_AGENT_ID mediumint(2) NOT NULL default '0',
			INVOICE_STATUS enum ('VALID','VOID') NOT NULL default 'VALID',
			BOOKTYPE_ID tinyint(1) NOT NULL default '0',
			BOOK_ID mediumint(1) NOT NULL default '0',
			INVOICE_TIMESTAMP datetime NOT NULL default '0000-00-00 00:00:00',
			PRIMARY KEY (INVOICE_ID),
			UNIQUE KEY INVOICE_ID (INVOICE_ID),
			KEY INVOICE_ID_2 (INVOICE_ID)
		) TYPE=MyISAM";

$mysql_result = mysql_query($query, $mysql_link);

print("Create TABLE INVOICE, result code = $mysql_result\n");
print("<br>");

$query = "CREATE TABLE DEPOSIT (
			DEPOSIT_ID mediumint(2) NOT NULL default '0',
			DEPOSIT_TOTAL decimal(5,2) NOT NULL default '0.00',
			DEPOSIT_DATE char (10) NOT NULL default '',
			DEPOSIT_TIMESTAMP datetime NOT NULL default '0000-00-00 00:00:00',
			PRIMARY KEY (DEPOSIT_ID),
			UNIQUE KEY DEPOSIT_ID (DEPOSIT_ID),
			KEY DEPOSIT_ID_2 (DEPOSIT_ID)
		) TYPE=MyISAM";

$mysql_result = mysql_query($query, $mysql_link);

print("Create TABLE DEPOSIT, result code = $mysql_result\n");
print("<br>");

$query = "DROP TABLE BATCH";

$mysql_result = mysql_query($query, $mysql_link);

print("TABLE BATCH dropped, result code = $mysql_result\n");
print("<br>");


$query = "CREATE TABLE CASHRECEIPT (
		  CASHRECEIPT_ID mediumint(2) NOT NULL default '0',
		  BATCH_ID mediumint(2) NOT NULL default '0',
		  DEPOSIT_ID mediumint(2) NOT NULL default '0',
		  INVOICE_ID mediumint(2) NOT NULL default '0',
		  CASHRECEIPT_TIMESTAMP datetime NOT NULL default '0000-00-00 00:00:00',
		  PRIMARY KEY  (CASHRECEIPT_ID),
		  UNIQUE KEY CASHRECEIPT_ID (CASHRECEIPT_ID),
		  KEY CASHRECEIPT_ID_2 (CASHRECEIPT_ID)
		) TYPE=MyISAM";

$mysql_result = mysql_query($query, $mysql_link);

print("Create TABLE CASHRECEIPT, result code = $mysql_result\n");
print("<br>");

$query = "DROP TABLE DEPOSITLINEITEM";

$mysql_result = mysql_query($query, $mysql_link);

print("TABLE DEPOSITLINEITEM dropped, result code = $mysql_result\n");
print("<br>");


$query = "CREATE TABLE COUNTRY (
			COUNTRY_ID tinyint(1) NOT NULL default '0',
			COUNTRY_CODE char(2) NOT NULL default '',
			COUNTRY_DESC char(20) NOT NULL default '',
			COUNTRY_TIMESTAMP datetime NOT NULL default '0000-00-00 00:00:00',
			PRIMARY KEY (COUNTRY_ID),
			UNIQUE KEY COUNTRY_ID (COUNTRY_ID, COUNTRY_DESC),
			KEY COUNTRY_ID_2 (COUNTRY_ID)
		) TYPE=MyISAM";

$mysql_result = mysql_query($query, $mysql_link);

print("Create TABLE COUNTRY, result code = $mysql_result\n");
print("<br>");

$query = "CREATE TABLE CONFIRMATIONLETTER (
			CONFIRMATIONLETTER_ID mediumint(1) NOT NULL default '0',
			BOOKTYPE_ID tinyint(1) NOT NULL default '0',
			BOOK_ID mediumint(1) NOT NULL default '0',
			CONFIRMATIONLETTER_TIMESTAMP datetime NOT NULL default '0000-00-00 00:00:00',
			PRIMARY KEY (CONFIRMATIONLETTER_ID),
			UNIQUE KEY CONFIRMATIONLETTER_ID (CONFIRMATIONLETTER_ID),
			KEY CONFIRMATIONLETTER_ID_2 (CONFIRMATIONLETTER_ID)
		) TYPE=MyISAM";

$mysql_result = mysql_query($query, $mysql_link);

print("Create TABLE CONFIRMATIONLETTER, result code = $mysql_result\n");
print("<br>");

$query = "CREATE TABLE BOOKTYPE (
			BOOKTYPE_ID tinyint(1) NOT NULL default '0',
			BOOKTYPE_DESC char(15) NOT NULL default '',
			BOOKTYPE_TIMESTAMP datetime NOT NULL default '0000-00-00 00:00:00',
			PRIMARY KEY (BOOKTYPE_ID),
			UNIQUE KEY BOOKTYPE_ID (BOOKTYPE_ID),
			KEY BOOKTYPE_ID_2 (BOOKTYPE_ID)
		) TYPE=MyISAM";

$mysql_result = mysql_query($query, $mysql_link);

print("Create TABLE BOOKTYPE, result code = $mysql_result\n");
print("<br>");



$query = "CREATE TABLE OPTIONALTOUR (
			OPTIONALTOUR_ID tinyint(1) NOT NULL default '0',
			OPTIONALTOUR_CODE char (4) NOT NULL default '',
			OPTIONALTOUR_NAME char (40) NOT NULL default '',
			OPTIONALTOUR_PRICE decimal (4,2) NOT NULL default '0.00',
			VENDOR_ID mediumint(2) NOT NULL default '0',
			OPTIONALTOUR_TIMESTAMP datetime NOT NULL default '0000-00-00 00:00:00',
			PRIMARY KEY (OPTIONALTOUR_ID),
			UNIQUE KEY OPTIONALTOUR_ID (OPTIONALTOUR_ID, OPTIONALTOUR_CODE),
			KEY OPTIONALTOUR_ID_2 (OPTIONALTOUR_ID)
		) TYPE=MyISAM";

$mysql_result = mysql_query($query, $mysql_link);

print("Create TABLE OPTIONALTOUR, result code = $mysql_result\n");
print("<br>");

$query = "CREATE TABLE PAYMENTTYPE (
			PAYMENTTYPE_ID tinyint(1) NOT NULL default '0',

			PAYMENTTYPE_DESC char(10) NOT NULL default '',
			PAYMENTTYPE_TYPE enum ('CC', 'CASH') NOT NULL default 'CC',
			PAYMENTTYPE_TIMESTAMP datetime NOT NULL default '0000-00-00 00:00:00',
			PRIMARY KEY (PAYMENTTYPE_ID),
			UNIQUE KEY PAYMENTTYPE_ID (PAYMENTTYPE_ID,PAYMENTTYPE_DESC),
			UNIQUE KEY PULOCATION_ID (PAYMENTTYPE_ID),
			KEY PAYMENTTYPE_ID_2 (PAYMENTTYPE_ID)
		) TYPE=MyISAM";

$mysql_result = mysql_query($query, $mysql_link);

print("Create TABLE PAYMENTTYPE, result code = $mysql_result\n");
print("<br>");

$query = "CREATE TABLE PULOCATION (
			PULOCATION_ID mediumint(2) NOT NULL default '0',
			PULOCATION_CODE char(5) NOT NULL default '',
			PULOCATION_LOCATION char(40) NOT NULL default '',
			PULOCATION_TIME char(5) NOT NULL default '',
			PULOCATION_TIMESTAMP datetime NOT NULL default '0000-00-00 00:00:00',
			PRIMARY KEY (PULOCATION_ID),
			UNIQUE KEY PULOCATION_CODE (PULOCATION_CODE),
			UNIQUE KEY PULOCATION_ID (PULOCATION_ID),
			KEY PULOCATION_ID_2 (PULOCATION_ID)
		) TYPE=MyISAM";

$mysql_result = mysql_query($query, $mysql_link);

print("Create TABLE PULOCATION, result code = $mysql_result\n");
print("<br>");

$query = "CREATE TABLE SCHEDULEDTOUR (
			SCHEDULEDTOUR_ID mediumint(2) NOT NULL default '0',
			SCHEDULEDTOUR_CODE char(12) NOT NULL default '',
			TOUR_ID mediumint(2) NOT NULL default '0',
			SCHEDULEDTOUR_DEPART_DATE char(10) NOT NULL default '',
			SCHEDULEDTOUR_PRICE decimal(4,2) NOT NULL default '0.00',
			VENDOR_ID_TOURGUIDE mediumint(2) NOT NULL default '0',
			SCHEDULEDTOUR_OCCUPANCY tinyint(1) NOT NULL default '0',
			VENDOR_ID_HOTEL mediumint(2) NOT NULL default '0',
			SCHEDULEDTOUR_DEPART_FLIGHT char(5) NOT NULL default '',
			SCHEDULEDTOUR_DEPART_DEP_TIME char(5) NOT NULL default '',
			SCHEDULEDTOUR_DEPART_ARR_TIME char(5) NOT NULL default '',
			SCHEDULEDTOUR_DEPART_AIRPORT_ID tinyint(1) NOT NULL default '0',
			SCHEDULEDTOUR_RETURN_DATE char(10) NOT NULL default '',
			SCHEDULEDTOUR_RETURN_FLIGHT char(5) NOT NULL default '',
			SCHEDULEDTOUR_RETURN_DEP_TIME char(5) NOT NULL default '',
			SCHEDULEDTOUR_RETURN_ARR_TIME char(5) NOT NULL default '',
			SCHEDULEDTOUR_RETURN_AIRPORT_ID tinyint(1) NOT NULL default '0',
			SCHEDULEDTOUR_AVAILABLE tinyint(1) NOT NULL default '0',
			SCHEDULEDTOUR_TIMESTAMP datetime NOT NULL default '0000-00-00 00:00:00',
			PRIMARY KEY (SCHEDULEDTOUR_ID),
			UNIQUE KEY SCHEDULEDTOUR_ID (SCHEDULEDTOUR_ID,SCHEDULEDTOUR_CODE),
			KEY SCHEDULEDTOUR_ID_2 (SCHEDULEDTOUR_ID)
		) TYPE=MyISAM";

$mysql_result = mysql_query($query, $mysql_link);

print("Create TABLE SCHEDULEDTOUR, result code = $mysql_result\n");
print("<br>");

$query = "DROP TABLE SCHEDULEDTOURBOOK";

$mysql_result = mysql_query($query, $mysql_link);

print("TABLE SCHEDULEDTOURBOOK dropped, result code = $mysql_result\n");
print("<br>");

$query = "CREATE TABLE SCHEDULEDTOURBOOK (
			SCHEDULEDTOURBOOK_ID mediumint(2) NOT NULL default '0',
			SCHEDULEDTOUR_ID mediumint(2) NOT NULL default '0',
			LAST_NAME char(40) NOT NULL default '',
			FIRST_NAME char(40) NOT NULL default '',
			GENDER enum('M','F') NOT NULL default 'M',
			OCCUPANCY tinyint(1) NOT NULL default '0',
			ROOMMATE enum('Y','N') NOT NULL default 'Y',
			OPTIONALTOUR_ID mediumint(2) NOT NULL default '0',
			TOTALCHARGE decimal(4,2) NOT NULL default '0.00',
			AGENT_ID mediumint(2) NOT NULL default '0',
			SCHEDULEDTOURBOOK_TIMESTAMP datetime NOT NULL default '0000-00-00 00:00:00',
			PULOCATION_ID tinyint(1) NOT NULL default '0',
			TOTALPAYTODLT decimal(4,2) NOT NULL default '0',
			PAYTODLT_ID tinyint(1) NOT NULL default '0',
			PAYTODLTDATE char(10) NOT NULL default '',
			TOTALPAYTOAGENT decimal(4,2) NOT NULL default '0',
			PAYTOAGENT_ID tinyint(1) NOT NULL default '0',
			PAYTOAGENTDATE char(10) NOT NULL default '',
			TOTALPAYCC decimal(4,2) NOT NULL default '0',
			PAYBYCC_ID tinyint(1) NOT NULL default '0',
			PAYCCDATE char (10) NOT NULL default '',
			EMAIL char (20) NOT NULL default '',
			PRIMARY KEY (SCHEDULEDTOURBOOK_ID),
			UNIQUE KEY SCHEDULEDTOURBOOK_ID (SCHEDULEDTOURBOOK_ID),
			KEY SCHEDULEDTOURBOOK_ID_2 (SCHEDULEDTOURBOOK_ID)
		) TYPE=MyISAM";

$mysql_result = mysql_query($query, $mysql_link);

print("Create TABLE SCHEDULEDTOURBOOK, result code = $mysql_result\n");
print("<br>");


$query = "CREATE TABLE TOUR (
			TOUR_ID mediumint(2) NOT NULL default '0',
			TOUR_TYPE char(4) NOT NULL default '',
			TOUR_NAME char(40) NOT NULL default '',
			CITY_ID tinyint(1) NOT NULL default '0',
			TOUR_SUPP_SNG decimal(4,2) NOT NULL default '0.00',
			TOUR_SUPP_DBL decimal(4,2) NOT NULL default '0.00',
			TOUR_COMM1 decimal(4,2) NOT NULL default '0.00',
			TOUR_DISC1 decimal(4,2) NOT NULL default '0.00',
			TOUR_COMM2 decimal(4,2) NOT NULL default '0.00',
			TOUR_DISC2 decimal(4,2) NOT NULL default '0.00',
			TOUR_COMM3 decimal(4,2) NOT NULL default '0.00',
			TOUR_DISC3 decimal(4,2) NOT NULL default '0.00',
			TOUR_COMM4 decimal(4,2) NOT NULL default '0.00',
			TOUR_DISC4 decimal(4,2) NOT NULL default '0.00',
			TOUR_COMM5 decimal(4,2) NOT NULL default '0.00',
			TOUR_DISC5 decimal(4,2) NOT NULL default '0.00',
			TOUR_COMM6 decimal(4,2) NOT NULL default '0.00',
			TOUR_DISC6 decimal(4,2) NOT NULL default '0.00',
			TOUR_COMM7 decimal(4,2) NOT NULL default '0.00',
			TOUR_DISC7 decimal(4,2) NOT NULL default '0.00',

			TOUR_COMM8 decimal(4,2) NOT NULL default '0.00',
			TOUR_DISC8 decimal(4,2) NOT NULL default '0.00',
			TOUR_TIMESTAMP datetime NOT NULL default '0000-00-00 00:00:00',
			PRIMARY KEY (TOUR_ID),
			UNIQUE KEY TOUR_TYPE (TOUR_TYPE),
			UNIQUE KEY TOUR_ID (TOUR_ID),
			KEY TOUR_ID_2 (TOUR_ID)
		) TYPE=MyISAM";

$mysql_result = mysql_query($query, $mysql_link);

print("Create TABLE TOUR, result code = $mysql_result\n");
print("<br>");


// create the USER table, if not already created

$query = "CREATE TABLE USER (
			USER_ID mediumint(2) NOT NULL default '0',
			USER_NAME char(40) NOT NULL default '',
			USER_ACCESS tinyint(1) NOT NULL default '0',
			USER_PASSWORD char(10) NOT NULL default '',
			USER_FULLNAME char(40) NOT NULL default '',
			USER_TIMESTAMP datetime NOT NULL default '0000-00-00 00:00:00',
			PRIMARY KEY (USER_ID),
			UNIQUE KEY USER_NAME (USER_NAME),
			UNIQUE KEY USER_ID (USER_ID),
			KEY USER_ID_2 (USER_ID)
		) TYPE=MyISAM";

$mysql_result = mysql_query($query, $mysql_link);

print("Create TABLE USER, result code = $mysql_result\n");
print("<br>");


// create the VENDOR table if not already created

$query = "CREATE TABLE VENDOR (
			VENDOR_ID mediumint(2) NOT NULL default '0',
			VENDOR_CODE varchar(4) NOT NULL default '',
			VENDORTYPE_ID tinyint(1) NOT NULL default '0',
			VENDOR_NAME varchar(40) NOT NULL default '',
			VENDOR_CONTACT varchar(40) NOT NULL default '',
			VENDOR_ADDR1 varchar(40) NOT NULL default '',
			VENDOR_ADDR2 varchar(40) NOT NULL default '',
			VENDOR_CITY varchar(20) NOT NULL default '',
			VENDOR_STATE varchar(2) NOT NULL default '',
			VENDOR_ZIP varchar(10) NOT NULL default '',
			VENDOR_WEB varchar(40) NOT NULL default '',
			VENDOR_EMAIL varchar(40) NOT NULL default '',
			VENDOR_TEL1 varchar(15) NOT NULL default '',
			VENDOR_TEL2 varchar(15) NOT NULL default '',
			VENDOR_FAX varchar(15) NOT NULL default '',
			VENDOR_COMMENTS longtext NOT NULL,
			VENDOR_TIMESTAMP datetime NOT NULL default '0000-00-00 00:00:00',
			PRIMARY KEY (VENDOR_ID),
			UNIQUE KEY VENDOR_ID (VENDOR_ID, VENDOR_CODE),
			KEY AGENT_ID_2 (VENDOR_ID)
		) TYPE=MyISAM";

$mysql_result = mysql_query($query, $mysql_link);

print("Create TABLE VENDOR, result code = $mysql_result\n");
print("<br>");



$query = "CREATE TABLE VENDORTYPE (
			VENDORTYPE_ID tinyint(1) NOT NULL default '0',
			VENDORTYPE_DESC char(20) NOT NULL default '',
			VENDORTYPE_TIMESTAMP datetime NOT NULL default '0000-00-00 00:00:00',
			PRIMARY KEY (VENDORTYPE_ID),
			UNIQUE KEY VENDORTYPE_ID (VENDORTYPE_ID, VENDORTYPE_DESC),
			KEY VENDORTYPE_ID_2 (VENDORTYPE_ID)
		) TYPE=MyISAM";

$mysql_result = mysql_query($query, $mysql_link);

print("Create TABLE VENDORTYPE, result code = $mysql_result\n");
print("<br>");


$query = "CREATE TABLE VENDORLETTER (
			VENDORLETTER_ID mediumint(1) NOT NULL default '0',
			VENDOR_ID mediumint(1) NOT NULL default '0',
			VENDORLETTER_TIMESTAMP datetime NOT NULL default '0000-00-00 00:00:00',
			PRIMARY KEY (VENDORLETTER_ID),
			UNIQUE KEY VENDORLETTER_ID (VENDORLETTER_ID),
			KEY VENDORLETTER_ID_2 (VENDORLETTER_ID)
		) TYPE=MyISAM";

$mysql_result = mysql_query($query, $mysql_link);

print("Create TABLE VENDORLETTER, result code = $mysql_result\n");
print("<br>");


// INSERTING DEFAULT VALUES BELOW


$current_time = getdate(time());
$current_hours = $current_time["hours"];
$current_mins = $current_time["minutes"];
$current_secs = $current_time["seconds"];
$current_date = date("Y-m-d");

$TimeStamp = sprintf("%s %d:%d:%d",$current_date,$current_hours,$current_mins,$current_secs);

$query = "INSERT INTO
			AGENTTYPE (		AGENTTYPE_ID,
							AGENTTYPE_DESC,
							AGENTTYPE_TIMESTAMP)
			VALUES (	'1',
						'SCHOOL',
						'$TimeStamp')";

$mysql_result = mysql_query($query, $mysql_link);

$query = "INSERT INTO
			AGENTTYPE (		AGENTTYPE_ID,
							AGENTTYPE_DESC,
							AGENTTYPE_TIMESTAMP)
			VALUES (	'2',
						'TRAVEL AGENT',
						'$TimeStamp')";

$mysql_result = mysql_query($query, $mysql_link);
$query = "INSERT INTO
			AGENTTYPE (		AGENTTYPE_ID,
							AGENTTYPE_DESC,
							AGENTTYPE_TIMESTAMP)
			VALUES (	'3',
						'OTHER',
						'$TimeStamp')";

$mysql_result = mysql_query($query, $mysql_link);

print("Inserting default values (TRAVEL AGENT, SCHOOL, OTHER) into TABLE AGENTTYPE, result code = $mysql_result\n");
print("<br>");

$query = "INSERT INTO
			CITY (		CITY_ID,
						CITY_CODE,
						CITY_DESC,
						CITY_TIMESTAMP)
			VALUES (	'1',
						'LA',
						'LOS ANGELES',
						'$TimeStamp')";

$mysql_result = mysql_query($query, $mysql_link);

$query = "INSERT INTO
			CITY (		CITY_ID,
						CITY_CODE,
						CITY_DESC,
						CITY_TIMESTAMP)
			VALUES (	'2',
						'SF',
						'SAN FRANCISCO',
						'$TimeStamp')";

$mysql_result = mysql_query($query, $mysql_link);

$query = "INSERT INTO
			CITY (		CITY_ID,
						CITY_CODE,
						CITY_DESC,
						CITY_TIMESTAMP)
			VALUES (	'3',
						'SD',
						'SAN DIEGO',
						'$TimeStamp')";

$mysql_result = mysql_query($query, $mysql_link);

print("Inserting default values (LA, SD, SF) into TABLE CITY, result code = $mysql_result\n");
print("<br>");

$query = "INSERT INTO
			AIRPORT (		AIRPORT_ID,
						AIRPORT_CODE,
						AIRPORT_DESC,
						AIRPORT_TIMESTAMP)
			VALUES (	'1',
						'OAK',
						'OAKLAND INTER',
						'$TimeStamp')";


$mysql_result = mysql_query($query, $mysql_link);

$query = "INSERT INTO
			AIRPORT (		AIRPORT_ID,
						AIRPORT_CODE,
						AIRPORT_DESC,
						AIRPORT_TIMESTAMP)
			VALUES (	'2',
						'SFO',
						'SAN FRANCISCO',
						'$TimeStamp')";

$mysql_result = mysql_query($query, $mysql_link);

$query = "INSERT INTO
			AIRPORT (		AIRPORT_ID,
						AIRPORT_CODE,
						AIRPORT_DESC,
						AIRPORT_TIMESTAMP)
			VALUES (	'3',
						'LAX',
						'LOS ANGELES',
						'$TimeStamp')";

$mysql_result = mysql_query($query, $mysql_link);

print("Inserting default values (LAX, SFO, OAK) into TABLE AIRPORT, result code = $mysql_result\n");
print("<br>");


$query = "INSERT INTO
			VENDORTYPE (		VENDORTYPE_ID,
								VENDORTYPE_DESC,
								VENDORTYPE_TIMESTAMP)
			VALUES (	'1',
						'HOTEL',
						'$TimeStamp')";

$mysql_result = mysql_query($query, $mysql_link);

$query = "INSERT INTO
			VENDORTYPE (		VENDORTYPE_ID,
							VENDORTYPE_DESC,
							VENDORTYPE_TIMESTAMP)
			VALUES (	'2',
						'BUS',
						'$TimeStamp')";

$mysql_result = mysql_query($query, $mysql_link);

$query = "INSERT INTO
			VENDORTYPE (		VENDORTYPE_ID,
							VENDORTYPE_DESC,

							VENDORTYPE_TIMESTAMP)
			VALUES (	'3',
						'AIRLINE',
						'$TimeStamp')";

$mysql_result = mysql_query($query, $mysql_link);

$query = "INSERT INTO
			VENDORTYPE (		VENDORTYPE_ID,
							VENDORTYPE_DESC,
							VENDORTYPE_TIMESTAMP)
			VALUES (	'4',
						'TOUR GUIDE',
						'$TimeStamp')";

$mysql_result = mysql_query($query, $mysql_link);

$query = "INSERT INTO
			VENDORTYPE (		VENDORTYPE_ID,
							VENDORTYPE_DESC,
							VENDORTYPE_TIMESTAMP)
			VALUES (	'5',
						'OTHER',
						'$TimeStamp')";

$mysql_result = mysql_query($query, $mysql_link);

print("Inserting default values (BUS, HOTEL, AIRLINE, TOUR GUIDE, OTHER) into TABLE VENDORTYPE, result code = $mysql_result\n");
print("<br>");

$query = "INSERT INTO
			USER (		USER_ID,
						USER_NAME,
						USER_ACCESS,
						USER_PASSWORD,
						USER_FULLNAME,
						USER_TIMESTAMP)
			VALUES (	'1',
						'USER1',
						'1',
						'USER1',
						'USER1',
						'$TimeStamp')";

$mysql_result = mysql_query($query, $mysql_link);

$query = "INSERT INTO
			USER (		USER_ID,
						USER_NAME,
						USER_ACCESS,
						USER_PASSWORD,
						USER_FULLNAME,
						USER_TIMESTAMP)
			VALUES (	'2',
						'USER2',
						'2',
						'USER2',
						'USER2',
						'$TimeStamp')";

$mysql_result = mysql_query($query, $mysql_link);

$query = "INSERT INTO
			USER (		USER_ID,
						USER_NAME,
						USER_ACCESS,
						USER_PASSWORD,
						USER_FULLNAME,
						USER_TIMESTAMP)
			VALUES (	'3',
						'USER3',
						'3',
						'USER3',
						'USER3',
						'$TimeStamp')";

$mysql_result = mysql_query($query, $mysql_link);

$query = "INSERT INTO
			USER (		USER_ID,
						USER_NAME,
						USER_ACCESS,
						USER_PASSWORD,
						USER_FULLNAME,
						USER_TIMESTAMP)
			VALUES (	'4',
						'USER4',
						'4',
						'USER4',
						'USER4',
						'$TimeStamp')";

$mysql_result = mysql_query($query, $mysql_link);

print("Inserting default values (USER1, USER2, USER3, USER4) into TABLE USER, result code = $mysql_result\n");
print("<br>");

$query = "INSERT INTO
			PAYMENTTYPE (		PAYMENTTYPE_ID,
							PAYMENTTYPE_DESC,
							PAYMENTTYPE_TYPE,
							PAYMENTTYPE_TIMESTAMP)
			VALUES (	'1',
						'AMEX',
						'CC',
						'$TimeStamp')";

$mysql_result = mysql_query($query, $mysql_link);

$query = "INSERT INTO
			PAYMENTTYPE (		PAYMENTTYPE_ID,
							PAYMENTTYPE_DESC,
							PAYMENTTYPE_TYPE,
							PAYMENTTYPE_TIMESTAMP)
			VALUES (	'2',
						'VISA',
						'CC',
						'$TimeStamp')";

$mysql_result = mysql_query($query, $mysql_link);

$query = "INSERT INTO
			PAYMENTTYPE (		PAYMENTTYPE_ID,
							PAYMENTTYPE_DESC,
							PAYMENTTYPE_TYPE,
							PAYMENTTYPE_TIMESTAMP)
			VALUES (	'3',
						'MC',
						'CC',
						'$TimeStamp')";

$mysql_result = mysql_query($query, $mysql_link);

$query = "INSERT INTO
			PAYMENTTYPE (		PAYMENTTYPE_ID,
							PAYMENTTYPE_DESC,
							PAYMENTTYPE_TYPE,
							PAYMENTTYPE_TIMESTAMP)
			VALUES (	'4',
						'CASH',
						'CASH',
						'$TimeStamp')";

$mysql_result = mysql_query($query, $mysql_link);


$query = "INSERT INTO
			PAYMENTTYPE (		PAYMENTTYPE_ID,
							PAYMENTTYPE_DESC,
							PAYMENTTYPE_TYPE,
							PAYMENTTYPE_TIMESTAMP)
			VALUES (	'5',
						'TC',
						'CASH',
						'$TimeStamp')";

$mysql_result = mysql_query($query, $mysql_link);

$query = "INSERT INTO
			PAYMENTTYPE (		PAYMENTTYPE_ID,
							PAYMENTTYPE_DESC,
							PAYMENTTYPE_TYPE,
							PAYMENTTYPE_TIMESTAMP)
			VALUES (	'6',
						'CHECK',
						'CASH',
						'$TimeStamp')";

$mysql_result = mysql_query($query, $mysql_link);


print("Inserting default values (VISA, AMEX, MC, CASH, TC, CHECK) into TABLE PAYMENTTYPE, result code = $mysql_result\n");
print("<br>");

$query = "INSERT INTO
			BOOKTYPE (		BOOKTYPE_ID,
							BOOKTYPE_DESC,
							BOOKTYPE_TIMESTAMP)
			VALUES (	'1',
						'SCHEDULED TOUR',
						'$TimeStamp')";


$mysql_result = mysql_query($query, $mysql_link);

$query = "INSERT INTO
			BOOKTYPE (		BOOKTYPE_ID,
							BOOKTYPE_DESC,
							BOOKTYPE_TIMESTAMP)
			VALUES (	'2',
						'FIT',
						'$TimeStamp')";

$mysql_result = mysql_query($query, $mysql_link);

$query = "INSERT INTO
			BOOKTYPE (		BOOKTYPE_ID,
							BOOKTYPE_DESC,
							BOOKTYPE_TIMESTAMP)
			VALUES (	'3',
						'ACTIVITY',
						'$TimeStamp')";

$mysql_result = mysql_query($query, $mysql_link);

print("Inserting default values (SCHEDULED TOUR, FIT, ACTIVYT) into TABLE BOOKTYPE, result code = $mysql_result\n");
print("<br>");


//$mysql_result = mysql_drop_db("deltours", $mysql_link);

//print("Dropping Database DELTOURS, result code = $mysql_result\n");
//print("<br>");

mysql_close($mysql_link);

?>

</BODY>
</HTML>
