<?php

include_once 'phpmailer/class.phpmailer.php';
$attachment = str_replace("http://www.california-tour.com","",$itinerary);
$attachment = str_replace("http://california-tour.com","",$attachment);
$attachment = $_SERVER["DOCUMENT_ROOT"] . $attachment;
$mail = new PHPMailer();
//$mail->IsSMTP(); // telling the class to use SMTP
$mail->Host = "localhost"; // SMTP server
$mail->From = "tour@california-tour.com";
$mail->FromName = "California Tour";
$mail->AddAddress($_POST['e_mail']);
$mail->AddAttachment($attachment);
$mail->IsHTML(true); 

$mail->Subject = "Thank you for booking at California-Tour";

// HTML body
$body = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">';
$body .= '<html><head>';
$body .= '<title>California Tours - ' . $title . '</title>';
$body .= '</head>';
$body .= '<body style="font-family: Verdana,helvetica,arial,sans-serif; background-color:#eee; font-size:11px; color:#222; line-height: 15px;padding:15px;">';
$body .= '<table width="600" cellpadding="10" cellspacing="10" align="center" style="background-color:#fff; border:1px solid #d4d4d4;">';
$body .= '<tr><td width="160"><p><a href="http://www.california-tour.com"><img src="http://www.california-tour.com/california/images/california_tours_logo/californiatours140.gif" width="140" height="61" alt="California Tours" border="0" /></a></p></td>';
$body .= '<td width="440" align="right"><p>Address: 2015 Center Street, 1st floor<br />Berkeley, CA 94704<br />Toll Free: (877) 338-3883<br />Phone: (510) 549-4211<br /> Fax: (510) 549-4210</p></td></tr>';
$body .= '<tr><td colspan="2"><h3>Your reservation is confirmed!</h3><p>Please print this page for your records.  Be sure to note your pick up location and time. </p><p>Please print this page for your records.  Be sure to note your pick up location and time. </p>';
$body .= '<p style="font-weight:bold;text-align:center;">Reservation Number: ' . $_POST['reservation_num'] . '<br /> Customer Name: ' . ucfirst(strtolower($_POST['first_name'])) . ' ' .  ucfirst(strtolower($_POST['last_name'])) . '</p>';
if($ScheduledTourBookId_2 != 0 && is_numeric($ScheduledTourBookId_2)) {
   $body .= '<p style="font-weight:bold;text-align:center;">Reservation 2 Number: ' . $ReservationId_2 . '<br /> Customer 2 Name: ' . ucfirst(strtolower($FIRST_NAME_2)) . ' ' .  ucfirst(strtolower($LAST_NAME_2)) . '</p>';
}
if($ScheduledTourBookId_3 != 0 && is_numeric($ScheduledTourBookId_3)) {
   $body .= '<p style="font-weight:bold;text-align:center;">Reservation 3 Number: ' . $ReservationId_3 . '<br /> Customer 3 Name: ' . ucfirst(strtolower($FIRST_NAME_3)) . ' ' .  ucfirst(strtolower($LAST_NAME_3)) . '</p>';
}
if($ScheduledTourBookId_4 != 0 && is_numeric($ScheduledTourBookId_4)) {
   $body .= '<p style="font-weight:bold;text-align:center;">Reservation 4 Number: ' . $ReservationId_4 . '<br /> Customer 4 Name: ' . ucfirst(strtolower($FIRST_NAME_4)) . ' ' .  ucfirst(strtolower($LAST_NAME_4)) . '</p>';
}
$body .= '<p>Please refer to your reservation number if you need to contact California Tours for any reason.</p>';
$body .= '<p>Thank you for choosing California Tours and have a nice trip!</p>';
$body .= '<h3>Your Receipt</h3>';
$body .= '<table cellpadding="0" cellspacing="0" border="0" style="width:500px;" style="padding:15px;margin:15px;">';
$body .= '<tr><th colspan="2">' . $_POST['tour_name'] . '</th></tr>';
$body .= '<tr><td>Total Charge:</td><td>' . $_POST['total_charge'] . '</td></tr>';
$body .= '<tr><td>Price per person:</td><td>' . sprintf("%0.2f",$_POST['total_charge']/$total_passenger_count) . '</td></tr>';
$body .= '<tr><td>Depart date:</td><td>' . date("F j, Y" , strtotime($_POST['depart_date']))  . '</td></tr>';
$body .= '<tr><td>Return date:</td><td>' . date("F j, Y" , strtotime($_POST['return_date'])) . '</td></tr>';
if($_POST['depart_date'] != $_POST['return_date']) {
   $body .='<tr><td>Occupancy:</td><td>' . @$occupancy_array[@$_POST['occupancy']] . '</td></tr>';
   $body .= '<tr><td>Roommate:</td><td>';
   if(@$_POST['occupancy'] > 1) {
      $body .= 'Yes';
   } else {
      $body .= 'No';
   }
   $body .= '</td></tr>';
}
$body .= '<tr><th colspan="2"><hr /></th></tr>';
$body .= '<tr><td>Pick-up time:</td><td>' . $pu_time . '</td></tr>';
$body .= '<tr><td>Pick-up location:</td><td>' . $pu_location . '</td></tr>';
$body .= '<tr><td colspan="2">Your complete itinerary is attached. You may also download your itinerary from:<br />';
$body .= $itinerary;
$body .= '</td></tr>';
$body .= '</table><br />';
if ($pu_imagelink != "") {
   $body .= '<p><img src="' . $pu_imagelink . '" alt="Pick up location map" /></p>';
}
$body .= '</td></tr></table></body></html>';

$text_body .= 'California Tours' . "\n";
$text_body .= 'Address: 2015 Center Street, 1st floor' . "\n";
$text_body .= 'Berkeley, CA 94704' . "\n";
$text_body .= 'Toll Free: (877) 338-3883' . "\n";
$text_body .= 'Phone: (510) 549-4211' . "\n";
$text_body .= 'Fax: (510) 549-4210' . "\n\n";
$text_body .= 'Your reservation is confirmed!' . "\n\n";
$text_body .= 'Please print this page for your records.  Be sure to note your pick up location and time.' . "\n\n";
$text_body .= 'Reservation Number: ' . $_POST['reservation_num'] . "\n";
$text_body .= 'Customer Name: ' . ucfirst(strtolower($_POST['first_name'])) . ' ' .  ucfirst(strtolower($_POST['last_name'])) . "\n\n";
if($ScheduledTourBookId_2 != 0 && is_numeric($ScheduledTourBookId_2)) {
   $text_body .= 'Reservation 2 Number: ' . $ReservationId_2 . "\n";
   $text_body .= 'Customer 2 Name: ' . ucfirst(strtolower($FIRST_NAME_2)) . ' ' .  ucfirst(strtolower($LAST_NAME_2)) . '\n\n';
}
if($ScheduledTourBookId_3 != 0 && is_numeric($ScheduledTourBookId_3)) {
   $text_body .= 'Reservation 3 Number: ' . $ReservationId_3 . "\n";
   $text_body .= 'Customer 3 Name: ' . ucfirst(strtolower($FIRST_NAME_3)) . ' ' .  ucfirst(strtolower($LAST_NAME_3)) . '\n\n';
}
if($ScheduledTourBookId_4 != 0 && is_numeric($ScheduledTourBookId_4)) {
   $text_body .= 'Reservation 4 Number: ' . $ReservationId_4 . "\n";
   $text_body .= 'Customer 4 Name: ' . ucfirst(strtolower($FIRST_NAME_4)) . ' ' .  ucfirst(strtolower($LAST_NAME_4)) . '\n\n';
}
$text_body .= 'Please refer to your reservation number if you need to contact California Tours for any reason.' . "\n\n";
$text_body .= 'Thank you for choosing California Tours and have a nice trip!' . "\n\n";
$text_body .= '------------------------------------------------------------' . "\n\n";
$text_body .= 'Your Receipt' . "\n\n";
$text_body .= 'Tour Name: ' . $_POST['tour_name'] . "\n";
$text_body .= 'Total Charge: ' . $_POST['total_charge'] . "\n";
$text_body .= 'Price per person: ' . sprintf('%0.2f', $_POST['total_charge']/$total_passenger_count) . "\n";
$text_body .= 'Depart date: ' . date("F j, Y" , strtotime($_POST['depart_date'])) . "\n";
$text_body .= 'Return date: ' . date("F j, Y" , strtotime($_POST['return_date'])) . "\n";
if($_POST['depart_date'] != $_POST['return_date']) {
   $text_body .= 'Occupancy: ' . @$occupancy_array[@$_POST['occupancy']] . "\n";
   $text_body .= 'Roommate: ';
   if(@$_POST['occupancy'] > 1) {
      $text_body .= 'Yes';
   } else {
      $text_body .= 'No';
   }
}
$text_body .= "\n";
$text_body .= 'Pick-up time: ' . $pu_time . "\n";
$text_body .= 'Pick-up location: ' . $pu_location . "\n";
$text_body .= 'Download pick-up location map: ' . $pu_imagelink . "\n";
$text_body .= 'Download Itinerary: \n' . $itinerary . "\n";

$mail->Body = $body;
$mail->AltBody = $text_body;

if(!$mail->Send())
{ 
   echo "Message was not sent\n";
   echo "Mailer Error: " . $mail->ErrorInfo;
   echo "Please contact the webmaster: tour@california-tour.com";die();
}

$mail->ClearAddresses();
$mail->ClearAttachments();

?>
