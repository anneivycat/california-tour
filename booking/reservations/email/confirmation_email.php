<?php

 error_reporting(E_ALL);
date_default_timezone_set('America/Los_Angeles');
 
 if(isset($itinerary))
	$attachment = str_replace("http://www.california-tour.com","",$itinerary);
else
	$itinerary = '';
 if(isset($attachment)){
	$attachment = str_replace("http://california-tour.com","",$attachment);
	$attachment = $_SERVER["DOCUMENT_ROOT"] . $attachment;
}else{
	$attachment = '';
}


$attach_size = '';
$file_handle = '';
$file_content = '';
$file_content = '';
$file_name = '';

// process attachment
if(file_exists($attachment)){
	$attach_size = filesize($attachment);
	$file_handle = fopen($attachment,"r");
	$file_content = fread($file_handle,$attach_size);
	fclose($file_handle);
	$file_content = chunk_split(base64_encode($file_content));
	$file_name = basename($attachment);
	$inc_file = true;
	$itin_note = 'Your itinerary is attached. You may also download your itinerary from:';
}else{
	$inc_file = false;
	$itin_note = 'You may download your itinerary from:';
}

$mail_From = "tour@california-tour.com";
$mail_FromName = "California Tour";
$mail_Address = $_REQUEST['e_mail'];

$mail_Subject = "Thank you for booking at California-Tour";

// HTML body
$body = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
';
$body .= '<html>
<head>
';
$body .= '<title>California Tours - Confirmation</title>
';
$body .= '
</head>
';
$body .= '<body style="font-family: Verdana,helvetica,arial,sans-serif; background-color:#eee; font-size:11px; color:#222; line-height: 15px;padding:15px;">
';
$body .= '<table width="600" cellpadding="10" cellspacing="10" align="center" style="background-color:#fff; border:1px solid #d4d4d4;">
';
$body .= '<tr><td width="160"><p><a href="http://www.california-tour.com"><img src="http://www.california-tour.com/california/images/california_tours_logo/californiatours140.gif" width="140" height="61" alt="California Tours" border="0" /></a></p></td>
';
$body .= '<td width="440" align="right"><p>Address: 2015 Center Street, 1st floor<br />Berkeley, CA 94704<br />Toll Free: (877) 338-3883<br />Phone: (510) 549-4211<br /> Fax: (510) 549-4210</p></td></tr>
';
$body .= '<tr><td colspan="2"><h3>Your reservation is confirmed!</h3><p>Please print this page for your records.  Be sure to note your pick up location and time. </p>
';
$body .= '<p style="font-weight:bold;text-align:center;">Reservation Number: ' . $_REQUEST['reservation_num'] . '<br /> Customer Name: ' . ucfirst(strtolower($_REQUEST['first_name'])) . ' ' .  ucfirst(strtolower($_REQUEST['last_name'])) . '</p>';
if(isset($ScheduledTourBookId_2) && $ScheduledTourBookId_2 != 0 && is_numeric($ScheduledTourBookId_2)) {
   $body .= '<p style="font-weight:bold;text-align:center;">Reservation 2 Number: ' . $ReservationId_2 . '<br /> Customer 2 Name: ' . ucfirst(strtolower($FIRST_NAME_2)) . ' ' .  ucfirst(strtolower($LAST_NAME_2)) . '</p>';
}
if(isset($ScheduledTourBookId_3) && $ScheduledTourBookId_3 != 0 && is_numeric($ScheduledTourBookId_3)) {
   $body .= '<p style="font-weight:bold;text-align:center;">Reservation 3 Number: ' . $ReservationId_3 . '<br /> Customer 3 Name: ' . ucfirst(strtolower($FIRST_NAME_3)) . ' ' .  ucfirst(strtolower($LAST_NAME_3)) . '</p>';
}
if(isset($ScheduledTourBookId_4) && $ScheduledTourBookId_4 != 0 && is_numeric($ScheduledTourBookId_4)) {
   $body .= '<p style="font-weight:bold;text-align:center;">Reservation 4 Number: ' . $ReservationId_4 . '<br /> Customer 4 Name: ' . ucfirst(strtolower($FIRST_NAME_4)) . ' ' .  ucfirst(strtolower($LAST_NAME_4)) . '</p>';
}
$body .= '
<p>Please refer to your reservation number if you need to contact California Tours for any reason.</p>
';
$body .= '<p>Thank you for choosing California Tours and have a nice trip!</p>
';
$body .= '<h3>Your Receipt</h3>
';
$body .= '<table cellpadding="0" cellspacing="0" border="0" style="width:500px;padding:15px;margin:15px;">
';
$body .= '<tr><th colspan="2">' . $_REQUEST['tour_name'] . '</th></tr>
';
$body .= '<tr><td>Total Charge:</td><td>' . $_REQUEST['total_charge'] . '</td></tr>
';
$body .= '<tr><td>Price per person:</td><td>' . sprintf("%0.2f",$_REQUEST['total_charge']/$total_passenger_count) . '</td></tr>
';
$body .= '<tr><td>Depart date:</td><td>' . date("F j, Y" , strtotime($_REQUEST['depart_date']))  . '</td></tr>
';
$body .= '<tr><td>Return date:</td><td>' . date("F j, Y" , strtotime($_REQUEST['return_date'])) . '</td></tr>
';
if($_REQUEST['depart_date'] != $_REQUEST['return_date']) {
   $body .='<tr><td>Occupancy:</td><td>' . @$occupancy_array[@$_REQUEST['occupancy']] . '</td></tr>';
   $body .= '<tr><td>Roommate:</td><td>';
   if(@$_REQUEST['occupancy'] > 1) {
      $body .= 'Yes';
   } else {
      $body .= 'No';
   }
   $body .= '</td></tr>
   ';
}
$body .= '<tr><th colspan="2"><hr /></th></tr>
';
$body .= '<tr><td>Pick-up time:</td><td>' . $pu_time . '</td></tr>
';
$body .= '<tr><td>Pick-up location:</td><td>' . $pu_location . '</td></tr>
';	
$body .= '<tr><td colspan="2">'.$itin_note.'<br />';
$body .= $itinerary;
$body .= '</td></tr>';
$body .= '</table><br />
';
if ($pu_imagelink != "") {
   $body .= '<p><img src="' . $pu_imagelink . '" alt="Pick up location map" /></p>';
}
$body .= '</td></tr></table>
</body></html>
';

$text_body .= 'California Tours
Address: 2015 Center Street, 1st floor
Berkeley, CA 94704
Toll Free: (877) 338-3883
Phone: (510) 549-4211
Fax: (510) 549-4210
Your reservation is confirmed!
Please print this page for your records.  Be sure to note your pick up location and time.

Reservation Number: ' . $_REQUEST['reservation_num'] . '
Customer Name: ' . ucfirst(strtolower($_REQUEST['first_name'])) . ' ' .  ucfirst(strtolower($_REQUEST['last_name'])) . "

";
if($ScheduledTourBookId_2 != 0 && is_numeric($ScheduledTourBookId_2)) {
   $text_body .= 'Reservation 2 Number: ' . $ReservationId_2 . "\n";
   $text_body .= 'Customer 2 Name: ' . ucfirst(strtolower($FIRST_NAME_2)) . ' ' .  ucfirst(strtolower($LAST_NAME_2)) . '
   
   ';
}
if($ScheduledTourBookId_3 != 0 && is_numeric($ScheduledTourBookId_3)) {
   $text_body .= 'Reservation 3 Number: ' . $ReservationId_3 . "\n";
   $text_body .= 'Customer 3 Name: ' . ucfirst(strtolower($FIRST_NAME_3)) . ' ' .  ucfirst(strtolower($LAST_NAME_3)) . '
   
   ';
}
if($ScheduledTourBookId_4 != 0 && is_numeric($ScheduledTourBookId_4)) {
   $text_body .= 'Reservation 4 Number: ' . $ReservationId_4 . "\n";
   $text_body .= 'Customer 4 Name: ' . ucfirst(strtolower($FIRST_NAME_4)) . ' ' .  ucfirst(strtolower($LAST_NAME_4)) . '
   
   ';
}
$text_body .= 'Please refer to your reservation number if you need to contact California Tours for any reason.

Thank you for choosing California Tours and have a nice trip!

------------------------------------------------------------

Your Receipt

Tour Name: ' . $_REQUEST['tour_name'] . '
Total Charge: ' . $_REQUEST['total_charge'] . '
Price per person: ' . sprintf('%0.2f', $_REQUEST['total_charge']/$total_passenger_count) . '
Depart date: ' . date("F j, Y" , strtotime($_REQUEST['depart_date'])) . '
Return date: ' . date("F j, Y" , strtotime($_REQUEST['return_date'])) . '
';
if($_REQUEST['depart_date'] != $_REQUEST['return_date']) {
   $text_body .= 'Occupancy: ' . @$occupancy_array[@$_REQUEST['occupancy']] . '
';
   $text_body .= 'Roommate: ';
   if(@$_REQUEST['occupancy'] > 1) {
      $text_body .= 'Yes';
   } else {
      $text_body .= 'No';
   }
}
$text_body .= '
';
if($pu_time != '')
$text_body .= 'Pick-up time: ' . $pu_time . '
';
if($pu_location != '')
$text_body .= 'Pick-up location: ' . $pu_location . '
';
if($pu_imagelink != '')
$text_body .= 'Download pick-up location map: ' . $pu_imagelink . '
';
if($itinerary != '')
$text_body .= 'Download Itinerary: \n' . $itinerary . '
';

$boundary = 'This_is_the_boundary-'.md5(date('r', time()));

$mail_message = "
--$boundary
Content-Type: text/plain; charset=\"utf-8\"\r\nContent-Transfer-Encoding: 7bit

$text_body

--$boundary
Content-Type: text/html; charset=\"utf-8\"\r\nContent-Transfer-Encoding: 7bit

$body

";

if($file_content != '' && $inc_file){

	$mail_message .= "--$boundary
Content-Type: application/octet-stream; name=\"$file_name\"
Content-Transfer-Encoding: base64
Content-Disposition: attachment
filename=\"$file_name\"

$file_content
";
}

$mail_message .= "
--$boundary--";

$cont_type = 'multipart/alternative';
if($file_content != '' && $inc_file)
	$cont_type = 'multipart/mixed';

$mailYes = mail($mail_Address,$mail_Subject, $mail_message,
				"From: $mail_FromName <$mail_From>\r\nMIME-Version: 1.0\r\nContent-type: $cont_type;boundary=$boundary\r\nContent-Transfer-Encoding: 7bit\r\n\r\n");
			
$mailYes3 = mail("tod@almost-everything.com","COPY - ".$_REQUEST['reservation_num'] . " - ".$mail_Subject, $mail_message,
				"From: $mail_FromName <$mail_From>\r\nMIME-Version: 1.0\r\nContent-type: $cont_type;boundary=$boundary\r\nContent-Transfer-Encoding: 7bit\r\n\r\n");

if(!$mailYes)
{ 
   echo "Message was not sent\n";
   echo "Mailer Error: " . $mail->ErrorInfo;
   echo "Please contact the webmaster: tour@california-tour.com";die();
}


?>
