<?php
 if(@$TripInformation) $TripInformation = stripslashes($TripInformation);
  //table formatting specific code for the P/U Location table entry screen
  $mysql_link = connect_to_db();
  $mysql_result = select_db($mysql_link);
?>
<TABLE ALIGN='CENTER' WIDTH='100%'>
  <TR>
    <TD>
    <?php
       echo @$ResultString  . "<br>";
       echo @$ResultString2 . "<br>";
       echo @$ResultString3 . "<br>";
    ?>
    </TD>
  </TR>

<?php
 if (!@$ListUser)
 {
?>
  <TR>
    <TD valign='top'>
      <TABLE ALIGN='CENTER' WIDTH='100%' BORDER='0'>
        <TR>
          <TD ALIGN='LEFT' valign='top'><Font color='blue'>* P/U Location ID:</TD>
          <TD ALIGN='LEFT' valign='top'><input type='text' size='4' name='PULocationId' value='<?php echo @$PULocationId; ?>'></TD>
        </TR>
        <TR>
          <TD ALIGN='LEFT' valign='top'><Font color='blue'>Updated:</TD>
          <TD ALIGN='LEFT' valign='top'><?php echo @$TimeStamp; ?></TD>
        </TR>
        <TR>
          <TD ALIGN='LEFT' valign='top'><Font color='blue'>* P/U Code:</TD>
          <TD ALIGN='LEFT' valign='top'><input type='text' size = '40' name='PULocationCode' value='<?php echo @$PULocationCode; ?>'></TD>
        </TR>
        <TR>
          <TD ALIGN='LEFT' valign='top'><Font color='blue'>* P/U Location (location only, not times):</TD>
          <TD ALIGN='LEFT' valign='top'><input type='text' size = '40' name='PULocationLocation' value='<?php echo @$PULocationLocation; ?>'></TD>
        </TR>
		 <TR>
          <TD ALIGN='LEFT' valign='top'><Font color='blue'>P/U Location Time:</TD>
          <TD ALIGN='LEFT' valign='top'><input type='text' size = '40' name='PULocationTime' value='<?php echo @$PULocationTime; ?>'></TD>
        </TR>
        <TR>
          <TD ALIGN='LEFT' valign='top'><Font color='blue'>P/U Location Image Link:</TD>
          <TD ALIGN='LEFT' valign='top'><input type='text' size='40' name='PULocationImageLink' value='<?php echo @$PULocationImageLink; ?>'></TD>
        </TR>
	<TR>
	  <TD ALIGN='LEFT' valign='top'><Font color='blue'>Google Map Coordinate:</TD>
          <TD ALIGN='LEFT' valign='top'><input type='text' size='40' name='GoogleMap' value='<?php echo @$GoogleMap; ?>'></TD>
	</TR>
<?php
    if (@$PULocationImageLink)
    {
?>
	<TR>
	  <TD ALIGN='LEFT' valign='top' colspan='2'><Font color='blue'><a href='<?php echo $PULocationImageLink; ?>' target='_blank'>View Image</a></TD>
	</TR>
<?php
    }
 }
 else
 {
?>
	 <TR>
	   <TD ALIGN='LEFT' valign='top' colspan='2'>
	     <input type='submit' name='FirstUser' value='Back'>
	   </TD>
	 </TR>
<?php 
 }
 // end of normal operations
?>
        <TR>
          <TD ALIGN='LEFT' valign='top' colspan='4'><hr>
          </TD>
        </TR>
        <TR>
	  <TD>
	    <br><br><br><br><br><br><br><br>
          </TD>
	</TR>
        <TR>
          <TD ALIGN='LEFT' valign='top' colspan='4'><Font color='blue'>Search String:</font><br>
            <TEXTAREA name='QueryString' rows='10' cols='50' readonly><?php echo trim(@$QueryString); ?>
	    </TEXTAREA>
	  </TR>
      </TABLE>
    </TD>

<?php
 if(!@$ListUser)
 {
   include ("menutbl.php");
 }

?>
