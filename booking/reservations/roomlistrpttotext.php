<?


include ("parsefuncs.php");

// open a connection to mysql

	$mysql_link = connect_to_db();
	$mysql_result = select_db($mysql_link);

	/*
	** open file for writing
	*/


	$current_time = getdate(time());
	$current_hours = $current_time["hours"];
	$current_mins = $current_time["minutes"];
	$current_secs = $current_time["seconds"];
	$current_date = date("Y-m-d");

	$TimeStamp = sprintf("%s %d:%d:%d",$current_date,$current_hours,$current_mins,$current_secs);


	$NewFileName = sprintf("roomlist-%s.htm",$current_date);
	$NewFileNamePath = sprintf("/home/califor/public_html/printouts/%s",$NewFileName);


	$SavedFile = fopen($NewFileNamePath,"w");

	/*
	** make sure the open was successful
	*/
	if(!($SavedFile))
	{
		fputs($SavedFile,"Error: ");
		fputs($SavedFile,"'roomlist.txt' could not be created\n");
		exit;
	}




//$mysql_query = mysql_select_db("andy_TEST", $mysql_link);

//fputs($SavedFile,"Return Code = $mysql_query");

include ("catourheadertofile.php");

// print fetched rows


$AgentCode = "";
$AgentId = "";
$PULocationId1 = "";
$PULocationId2 = "";
$PULocationId3 = "";
$PULocationTime = "";
$PULocationLocation = "";
$Occupancy = 0;
$LastName = "";
$FirstName = "";
$Gender = "";
$totalcount = 0;
$roomcount = 0;
$lastcount = 0;
$singlecount = 0;
$doublecount = 0;
$triplecount = 0;

$TourName = "";
$TourId = "";
//$ScheduledTourId = "";
$ScheduledTourCode = "";

//print($ScheduledTourId);

if ($ScheduledTourId <> "")
{
	$query1 = "SELECT DISTINCT * FROM SCHEDULEDTOUR WHERE SCHEDULEDTOUR_ID = '$ScheduledTourId' ORDER BY SCHEDULEDTOUR_CODE ASC";
	$mysql_result1 = mysql_query($query1, $mysql_link);
	$numrows1 = mysql_num_rows($mysql_result1);
}
else
{
	$query1 = "SELECT DISTINCT * FROM SCHEDULEDTOUR ORDER BY SCHEDULEDTOUR_CODE ASC";
	$mysql_result1 = mysql_query($query1, $mysql_link);
	$numrows1 = mysql_num_rows($mysql_result1);
}
if ($numrows1 > 0)
{

	fputs($SavedFile,"<form name='deluserrow' method='post' action='deluser2.php'><br>\n");

	fputs($SavedFile,"<CENTER><b><font size='+1'>Rooming List</font></b></CENTER>\n");
	fputs($SavedFile,"<center><font size='-1' color='blue'>(-- Created $TimeStamp --)</font></center>\n");

	fputs($SavedFile,"<TABLE ALIGN='CENTER' WIDTH='100%'>");
	fputs($SavedFile,"\n");


	while ($row1 = mysql_fetch_array($mysql_result1))
	{

		$totalcount = 0;
		$roomcount = 0;

		$ScheduledTourId   = $row1[0];
		$ScheduledTourCode = $row1[1];
		$TourId = $row1[2];
		$query2 = "SELECT * FROM TOUR WHERE TOUR_ID = '$TourId'";
		$mysql_result2 = mysql_query($query2, $mysql_link);
		$numrows2 = mysql_num_rows($mysql_result2);
		if ($numrows2 > 0)
		{
			while ($row2 = mysql_fetch_array($mysql_result2))
			{
				$TourName = $row2[2];

				$roomcount = 0;
				$singlecount = 0;

				$query3 = "SELECT * FROM SCHEDULEDTOURBOOK WHERE SCHEDULEDTOUR_ID = '$ScheduledTourId' AND OCCUPANCY = '1'";

				$mysql_result3 = mysql_query($query3, $mysql_link);
				$numrows3 = mysql_num_rows($mysql_result3);
				if ($numrows3 > 0)
				{
					while ($row3 = mysql_fetch_array($mysql_result3))
					{
						$LastName = $row3[2];
						$FirstName = first_upper($row3[3]);
						$Gender = $row3[4];
						$Occupancy = $row3[5];
						$roomcount = $roomcount + 1;
						$singlecount = $singlecount + 1;
						$totalcount = $totalcount + 1;

						// print header row
						if ($totalcount == 1)
						{
							fputs($SavedFile,"<TR>\n");
							fputs($SavedFile,"\n");
							fputs($SavedFile,"<TD ALIGN='LEFT' valign='top' colspan='9'><hr>\n<b>$ScheduledTourCode $TourName</b></TD>");
							fputs($SavedFile,"\n");
							fputs($SavedFile,"</TR>");
							fputs($SavedFile,"<TR>\n");
							fputs($SavedFile,"</TR>\n");
							fputs($SavedFile,"<TR>");
							fputs($SavedFile,"\n");
							fputs($SavedFile,"<TD ALIGN='LEFT' valign='top'><b><u><Font color='black'>Room #</font></u></b></TD>");
							fputs($SavedFile,"\n");
							fputs($SavedFile,"<TD ALIGN='LEFT' valign='top'><b><u><Font color='black'>Name(Last First)</font></u></b></TD>");
							fputs($SavedFile,"\n");
							fputs($SavedFile,"<TD ALIGN='LEFT' valign='top'><b><u><Font color='black'>Gender</font></u></b></TD>");
							fputs($SavedFile,"\n");
							fputs($SavedFile,"<TD ALIGN='LEFT' valign='top'><b><u><Font color='black'>Occupancy</font></u></b></TD>");
							fputs($SavedFile,"\n");
							fputs($SavedFile,"</TR>");
							fputs($SavedFile,"\n");
						}

						fputs($SavedFile,"<TR>");
						fputs($SavedFile,"\n");
						fputs($SavedFile,"<TD ALIGN='LEFT' valign='top'><b>$roomcount)</b></TD>");
						fputs($SavedFile,"\n");
						fputs($SavedFile,"<TD ALIGN='LEFT' valign='top'>$LastName");
						fputs($SavedFile," $FirstName</TD>");
						fputs($SavedFile,"\n");
						fputs($SavedFile,"<TD ALIGN='LEFT' valign='top'>$Gender</TD>");
						fputs($SavedFile,"\n");
						fputs($SavedFile,"<TD ALIGN='LEFT' valign='top'>SNG</TD>");
						fputs($SavedFile,"\n");
						fputs($SavedFile,"</TR>");

						// reset variables to default values
						$LastName = "";
						$FirstName = "";
						$Gender = "";
					}
				}

				$lastcount = 1;

				$query3 = "SELECT * FROM SCHEDULEDTOURBOOK WHERE SCHEDULEDTOUR_ID = '$ScheduledTourId' AND OCCUPANCY = '2'";

				$mysql_result3 = mysql_query($query3, $mysql_link);
				$numrows3 = mysql_num_rows($mysql_result3);
				if ($numrows3 > 0)
				{
					while ($row3 = mysql_fetch_array($mysql_result3))
					{
						$LastName = $row3[2];
						$FirstName = first_upper($row3[3]);
						$Gender = $row3[4];
						$Occupancy = $row3[5];
						if ($lastcount == 1)
						{
							$roomcount = $roomcount + 1;
							$doublecount = $doublecount + 1;
							fputs($SavedFile,"<TR><TD><br></TD>\n");
							fputs($SavedFile,"</TR>\n");

						}
						if ($lastcount == 2)
						{
							$lastcount = 0;
						}

						$totalcount = $totalcount + 1;

						// print header row
						if ($totalcount == 1)
						{
							fputs($SavedFile,"<TR>\n");
							fputs($SavedFile,"\n");
							fputs($SavedFile,"<TD ALIGN='LEFT' valign='top' colspan='9'><hr>\n<b>$ScheduledTourCode $TourName</b></TD>");
							fputs($SavedFile,"\n");
							fputs($SavedFile,"</TR>");
							fputs($SavedFile,"<TR>\n");
							fputs($SavedFile,"</TR>\n");
							fputs($SavedFile,"<TR>");
							fputs($SavedFile,"\n");
							fputs($SavedFile,"<TD ALIGN='LEFT' valign='top'><b><u><Font color='black'>Room #</font></u></b></TD>");
							fputs($SavedFile,"\n");
							fputs($SavedFile,"<TD ALIGN='LEFT' valign='top'><b><u><Font color='black'>Name(Last First)</font></u></b></TD>");
							fputs($SavedFile,"\n");
							fputs($SavedFile,"<TD ALIGN='LEFT' valign='top'><b><u><Font color='black'>Gender</font></u></b></TD>");
							fputs($SavedFile,"\n");
							fputs($SavedFile,"<TD ALIGN='LEFT' valign='top'><b><u><Font color='black'>Occupancy</font></u></b></TD>");
							fputs($SavedFile,"\n");
							fputs($SavedFile,"</TR>");
							fputs($SavedFile,"\n");
						}

						fputs($SavedFile,"<TR>");
						fputs($SavedFile,"\n");
						if ($lastcount == 1)
						{
							fputs($SavedFile,"<TD ALIGN='LEFT' valign='top'><b>$roomcount)</b></TD>");
						}
						else
						{
							fputs($SavedFile,"<TD ALIGN='LEFT' valign='top'></TD>");
						}
						fputs($SavedFile,"\n");
						fputs($SavedFile,"<TD ALIGN='LEFT' valign='top'>$LastName");
						fputs($SavedFile," $FirstName</TD>");
						fputs($SavedFile,"\n");
						fputs($SavedFile,"<TD ALIGN='LEFT' valign='top'>$Gender</TD>");
						fputs($SavedFile,"\n");
						fputs($SavedFile,"<TD ALIGN='LEFT' valign='top'>DBL</TD>");
						fputs($SavedFile,"\n");
						fputs($SavedFile,"</TR>");
						// reset variables to default values
						$LastName = "";
						$FirstName = "";
						$Gender = "";
						$lastcount = $lastcount + 1;
					}
				}

				$lastcount = 2;

				$query3 = "SELECT * FROM SCHEDULEDTOURBOOK WHERE SCHEDULEDTOUR_ID = '$ScheduledTourId' AND OCCUPANCY = '3'";


				$mysql_result3 = mysql_query($query3, $mysql_link);
				$numrows3 = mysql_num_rows($mysql_result3);
				if ($numrows3 > 0)
				{
					while ($row3 = mysql_fetch_array($mysql_result3))
					{
						$LastName = $row3[2];
						$FirstName = first_upper($row3[3]);
						$Gender = $row3[4];
						$Occupancy = $row3[5];
						if ($lastcount == 2)
						{
							$roomcount = $roomcount + 1;
							$triplecount = $triplecount + 1;
							fputs($SavedFile,"<TR><TD><br></TD>\n");
							fputs($SavedFile,"</TR>\n");


						}
						if ($lastcount == 3)
						{
							$lastcount = 0;
						}

						$totalcount = $totalcount + 1;

						// print header row
						if ($totalcount == 1)
						{
							fputs($SavedFile,"<TR>\n");
							fputs($SavedFile,"\n");
							fputs($SavedFile,"<TD ALIGN='LEFT' valign='top' colspan='9'><hr>\n<b>$ScheduledTourCode $TourName</b></TD>");
							fputs($SavedFile,"\n");
							fputs($SavedFile,"</TR>");
							fputs($SavedFile,"<TR>\n");
							fputs($SavedFile,"</TR>\n");
							fputs($SavedFile,"<TR>");
							fputs($SavedFile,"\n");
							fputs($SavedFile,"<TD ALIGN='LEFT' valign='top'><b><u><Font color='black'>Room #</font></u></b></TD>");
							fputs($SavedFile,"\n");
							fputs($SavedFile,"<TD ALIGN='LEFT' valign='top'><b><u><Font color='black'>Name(Last First)</font></u></b></TD>");
							fputs($SavedFile,"\n");
							fputs($SavedFile,"<TD ALIGN='LEFT' valign='top'><b><u><Font color='black'>Gender</font></u></b></TD>");
							fputs($SavedFile,"\n");
							fputs($SavedFile,"<TD ALIGN='LEFT' valign='top'><b><u><Font color='black'>Occupancy</font></u></b></TD>");
							fputs($SavedFile,"\n");
							fputs($SavedFile,"</TR>");
							fputs($SavedFile,"\n");
						}

						fputs($SavedFile,"<TR>");
						fputs($SavedFile,"\n");
						if ($lastcount == 2)
						{
							fputs($SavedFile,"<TD ALIGN='LEFT' valign='top'><b>$roomcount)</b></TD>");
						}
						else
						{
							fputs($SavedFile,"<TD ALIGN='LEFT' valign='top'></TD>");
						}
						fputs($SavedFile,"\n");
						fputs($SavedFile,"<TD ALIGN='LEFT' valign='top'>$LastName");
						fputs($SavedFile," $FirstName</TD>");
						fputs($SavedFile,"\n");
						fputs($SavedFile,"<TD ALIGN='LEFT' valign='top'>$Gender</TD>");
						fputs($SavedFile,"\n");
						fputs($SavedFile,"<TD ALIGN='LEFT' valign='top'>TRP</TD>");
						fputs($SavedFile,"\n");
						fputs($SavedFile,"</TR>");
						// reset variables to default values
						$LastName = "";
						$FirstName = "";
						$Gender = "";
						$lastcount = $lastcount + 1;
					}
				}


				$query3 = "SELECT * FROM SCHEDULEDTOUR WHERE SCHEDULEDTOUR_ID = '$ScheduledTourId'";

				$mysql_result3 = mysql_query($query3, $mysql_link);
				$numrows3 = mysql_num_rows($mysql_result3);
				if ($numrows3 > 0)
				{
					$row3 = mysql_fetch_array($mysql_result3);
					$TourGuideId = $row3[5];
					$query4 = "SELECT * FROM VENDOR WHERE VENDOR_ID = '$TourGuideId'";

					$mysql_result4 = mysql_query($query4, $mysql_link);
					$numrows4 = mysql_num_rows($mysql_result4);
					if ($numrows4 > 0)
					{
						if ($totalcount > 0)
						{
							$row4 = mysql_fetch_array($mysql_result4);

							$roomcount = $roomcount + 1;
							$singlecount = $singlecount + 1;
							$TourGuideName = sprintf("%s (%s)",$row4[4], $row4[3]);
							fputs($SavedFile,"<TR><TD><br></TD>\n");
							fputs($SavedFile,"</TR>\n");

							fputs($SavedFile,"<TR>");
							fputs($SavedFile,"\n");
							fputs($SavedFile,"<TD ALIGN='LEFT' valign='top'><b>$roomcount)</b></TD>");
							fputs($SavedFile,"\n");
							fputs($SavedFile,"<TD ALIGN='LEFT' valign='top'>$TourGuideName</TD>");
							fputs($SavedFile,"\n");
							fputs($SavedFile,"<TD ALIGN='LEFT' valign='top'></TD>");
							fputs($SavedFile,"\n");
							fputs($SavedFile,"<TD ALIGN='LEFT' valign='top'></TD>");
							fputs($SavedFile,"\n");
							fputs($SavedFile,"</TR>");
							fputs($SavedFile,"<TR>\n");


							fputs($SavedFile,"</TR>\n");
						}

					}
				}

				if ($totalcount > 0)
				{
					fputs($SavedFile,"<TR>\n");
					fputs($SavedFile,"\n");
					fputs($SavedFile,"<TD ALIGN='LEFT' valign='top' colspan='4'><hr>\n</TD>");
					fputs($SavedFile,"\n");
					fputs($SavedFile,"</TR>");
					fputs($SavedFile,"<TR>\n");
					fputs($SavedFile,"\n");
					fputs($SavedFile,"<TD ALIGN='LEFT' valign='top' colspan='4'><b>Total # of rooms</b></TD>\n");
					fputs($SavedFile,"</TR>\n");
					fputs($SavedFile,"<TR><TD></TD>\n");
					fputs($SavedFile,"<TD align='left' valign='top'># of singles</TD>\n");
					fputs($SavedFile,"<TD align='left' valign='top' colspan='2'>$singlecount</TD>\n");
					fputs($SavedFile,"</TR>\n");
					fputs($SavedFile,"<TR><TD></TD>\n");
					fputs($SavedFile,"<TD align='left' valign='top'># of doubles</TD>\n");
					fputs($SavedFile,"<TD align='left' valign='top' colspan='2'>$doublecount</TD>\n");
					fputs($SavedFile,"</TR>\n");
					fputs($SavedFile,"<TR><TD></TD>\n");
					fputs($SavedFile,"<TD align='left' valign='top'># of triples</TD>\n");
					fputs($SavedFile,"<TD align='left' valign='top' colspan='2'>$triplecount</TD>\n");
					fputs($SavedFile,"</TR>\n");

					fputs($SavedFile,"<TR>");
					fputs($SavedFile,"\n");
					fputs($SavedFile,"<TD ALIGN='LEFT' valign='top' colspan='4'><hr></TD>");

					fputs($SavedFile,"\n");
					fputs($SavedFile,"</TR>");
					fputs($SavedFile,"\n");
					fputs($SavedFile,"<TR>");
					fputs($SavedFile,"\n");
					fputs($SavedFile,"<TD ALIGN='LEFT' valign='top' colspan='2'><b>Total Num of Passengers:</b></TD>");
					fputs($SavedFile,"\n");
					fputs($SavedFile,"<TD ALIGN='LEFT' valign='top' colspan='2'><b><font color='blue'>$totalcount</font></b></TD>");
					fputs($SavedFile,"\n");
					fputs($SavedFile,"</TR>");
				}
				else
				{
					fputs($SavedFile,"<TR>\n");
					fputs($SavedFile,"\n");
					fputs($SavedFile,"<TD ALIGN='LEFT' valign='top' colspan='4'><hr>\n<b>$ScheduledTourCode $TourName</b></TD>");
					fputs($SavedFile,"\n");
					fputs($SavedFile,"</TR>");
					fputs($SavedFile,"<TR>\n");
					fputs($SavedFile,"</TR>\n");
					fputs($SavedFile,"<TR>");
					fputs($SavedFile,"\n");
					fputs($SavedFile,"<TD ALIGN='LEFT' valign='top' colspan='4'><font color='red'><b>NO PASSENGERS ON THIS TOUR</b></font>\n</TD>");
					fputs($SavedFile,"\n");
					fputs($SavedFile,"</TR>");
				}


				$roomcount = 0;
				$totalcount = 0;
				$lastcount = 0;
				$singlecount = 0;
				$doublecount = 0;
				$triplecount = 0;

			}
		}

		$ScheduledTourId   = "";
		$ScheduledTourCode = "";
		$TourId = "";
	}
	fputs($SavedFile,"<TR>\n");
	fputs($SavedFile,"\n");
	fputs($SavedFile,"<TD ALIGN='LEFT' valign='top' colspan='9'><hr>\n</TD>");
	fputs($SavedFile,"\n");
	fputs($SavedFile,"</TR>");

	fputs($SavedFile,"</TABLE>");
	fputs($SavedFile,"</form>\n");

}





	fclose($SavedFile); // close the file

	$FullPath = sprintf("http://www.california-tour.com/printouts/%s",$NewFileName);

	print("Click <a href='$FullPath'><b><font color='blue'>here</font></b></a> to view the latest Rooming List printout ($NewFileName)<br>\n");
	print("Click <a href='http://www.california-tour.com/printouts/'><b><font color='blue'>here</font></b></a> to view a complete list of printouts in this directory\n");


?>

</BODY>
</HTML>