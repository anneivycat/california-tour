<HTML>

<HEAD>
<TITLE>Cash Receipt Report</TITLE>
<BODY bgcolor="#fffff0">

<SCRIPT LANGUAGE = "JavaScript">

	var strArgString = document.location.search;

		var strArgs = strArgString.substr(6);
		var strArray = strArgs.split(",");

		var strLevel = strArray[0];
		var strUserName = strArray[1];

</SCRIPT>

<?

include ("classes.php");
// connect to server

// open a connection to mysql

include ("../sqlfuncs.php");

$mysql_link = connect_to_db();
$mysql_result = select_db($mysql_link);



$SearchItem1 = new ReportSearchItem;
$SearchItem1->init(	"TOUR",
					"TOUR_ID",
					"TOUR_TYPE",
					"Tour:",
					"TourId",$mysql_link);

$SearchItem2 = new ReportSearchItem;
$SearchItem2->init(	"AGENT",
					"AGENT_ID",
					"AGENT_CODE",
					"Agent Code:",
					"AgentId",$mysql_link);

/*
$SearchItem3 = new ReportSearchItem;
$SearchItem3->init(	"DEPOSIT",
					"DEPOSIT_DATE",
					"DEPOSIT_DATE",
					"From Date:",
					"FromDepositDate",$mysql_link);


$SearchItem4 = new ReportSearchItem;
$SearchItem4->init(	"DEPOSIT",
					"DEPOSIT_DATE",
					"DEPOSIT_DATE",
					"To Date:",
					"ToDepositDate",$mysql_link);
*/

// date range as text entry

$SearchItem3 = new ReportTextBoxItem;
$SearchItem3->init("From :","FromDate");

$SearchItem4 = new ReportTextBoxItem;
$SearchItem4->init("To :","ToDate");

$SearchForm = new ReportSearchForm;
$SearchForm->init(	"cashrecrptmain.php",
					"cashrecrpt.php",
					"cashrecrpttotext.php",
					"-- Cash Receipt Report Search --",
					$SearchItem1,$SearchItem2,$SearchItem3,$SearchItem4,"");

if ($ExitBtn)
{
	$SearchForm->mainexitcmd();
}

if ($PrintScreenBtn)
{
	include ($SearchForm->printtoscreen);
}
else
{

	if ($PrintFileBtn)
	{
		include ($SearchForm->printtofile);
	}

	else
	{
		$SearchForm->showmain();
	}
}

?>