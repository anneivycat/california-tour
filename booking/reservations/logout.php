<?php

require_once("credential.inc.php");
if(@$_COOKIE['UserCredential']) {
    $uc = new UserCredential($_COOKIE['UserCredential']);
} else {
    $uc = NULL;
}
clearLogin("Logout succeeded.", "/", 1, $uc);

?>
<HTML>
    <HEAD>
	<TITLE>California Tour</TITLE>
    </HEAD>
    <BODY>
	<?php
	echo "Logout failed. To log out, please quit your browser.";
	if(@$debug) {
	    echo "<pre>\n";
	    var_dump($_SESSION);
	    var_dump($_COOKIE);
	    echo "</pre>\n";
	}
	?>
    </BODY>
</HTML>
