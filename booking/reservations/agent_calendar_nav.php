<html>
<head>
<title>California Tours - Tours From San Francisco 2004</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<script language="JavaScript">
<!--

function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_findObj(n, d) { //v4.0
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && document.getElementById) x=document.getElementById(n); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}
//-->
</script>
</head>

<body bgcolor="#FFFFFF" text="#000000" onLoad="MM_preloadImages('../booking/images/links_main/tours1.gif','../booking/images/links_main/welcome1.gif','../booking/images/links_main/dmc_services1.gif','../booking/images/links_main/tour_schedule1.gif','../booking/images/links_main/education_special_program1.gif','../booking/images/links_main/about_us1.gif','../booking/images/links_main/contact1.gif','../booking/images/links_main/special1.gif','../booking/images/links_main/site_map1.gif')" link="#006699" vlink="#003399" alink="#000099">
<table width="758" border="0" cellspacing="0" cellpadding="0" align="center"> 
<tr> <td align="center" valign="top" height="907"> <br> <table width="742" border="0" cellspacing="0" cellpadding="0"> 
<tr> <td width="106">&nbsp;</td><td width="261"><img src="../booking/images/california_tours_logo/home153_64.gif" width="153" height="64" alt="California Tours"></td><td width="315" align="right"><img src="../booking/images/top_slogan/tour_schdule.gif" width="300" height="64" alt="tour schedule and reservations"></td><td width="60">&nbsp;</td></tr> 
</table><br> <table width="738" border="0" cellspacing="0" cellpadding="0"> <tr align="center" valign="middle" bgcolor="#ffba3d"> 
<td width="31" height="13" bgcolor="#FFCC66"><font color="#FFFFCC"></font></td><td width="293" height="13" bgcolor="#FFCC66" align="left"><font color="#FFFFCC"><img src="../booking/images/links_position/home_tour_schedule.gif" width="240" height="17" usemap="#Map" border="0" alt="home &gt; tour schedule and reservations"></font></td><td width="351" height="13" bgcolor="#FFCC66" valign="middle" align="right"><a href="mailto:tours@california-tour.com"><img src="../booking/images/links_position/contact.gif" width="310" height="17" border="0" alt="California Tour"></a></td><td width="63" height="13" bgcolor="#FFCC66"><font color="#FFFFCC"></font><font color="#FFFFCC"></font></td></tr> 
<tr align="center" valign="top"> <td colspan="5" height="781"> <table width="738" border="0" cellspacing="0" cellpadding="0"> 
<tr> <td width="143" height="770" align="left" valign="top"> <table width="140" border="0" cellspacing="0" cellpadding="0"> 
<tr> <td height="27" align="left" valign="bottom" width="135"><a href="../booking/welcome/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('Image4','','../booking/images/links_main/welcome1.gif',1)"><img name="Image4" border="0" src="../booking/images/links_main/welcome.gif" width="135" height="17" alt="welcome"></a></td><td height="155" align="left" valign="bottom" rowspan="10" background="../booking/images/spacer/vertical_line.gif" width="10">&nbsp;</td></tr> 
<tr> <td align="left" valign="bottom" height="17" width="135"><a href="../booking/tours/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('Image3','','../booking/images/links_main/tours1.gif',1)"><img name="Image3" border="0" src="../booking/images/links_main/tours.gif" width="135" height="17" alt="tours"></a></td></tr> 
<tr> <td align="left" valign="bottom" height="2" width="135"><a href="../booking/services/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('Image5','','../booking/images/links_main/dmc_services1.gif',1)"><img name="Image5" border="0" src="../booking/images/links_main/dmc_services.gif" width="135" height="17" alt="DMC services"></a></td></tr> 
<tr> <td align="left" valign="bottom" height="17" width="135"><a href="../booking/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('Image6','','../booking/images/links_main/tour_schedule1.gif',1)"><img name="Image6" border="0" src="../booking/images/links_main/tour_schedule1.gif" width="135" height="27" alt="tour schedule and reservations"></a></td></tr> 
<tr> <td align="left" valign="bottom" height="17" width="135"><a href="../booking/education/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('Image7','','../booking/images/links_main/education_special_program1.gif',1)"><img name="Image7" border="0" src="../booking/images/links_main/education_special_program.gif" width="135" height="27" alt="education and special program"></a></td></tr> 
<tr> <td align="left" valign="bottom" height="17" width="135"><a href="../booking/about_us/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('Image8','','../booking/images/links_main/about_us1.gif',1)"><img name="Image8" border="0" src="../booking/images/links_main/about_us.gif" width="135" height="17" alt="about us"></a></td></tr> 
<tr> <td align="left" valign="bottom" height="17" width="135"><a href="../booking/contact/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('Image9','','../booking/images/links_main/contact1.gif',1)"><img name="Image9" border="0" src="../booking/images/links_main/contact.gif" width="135" height="17" alt="contact us"></a></td></tr> 
<tr> <td align="left" valign="bottom" height="17" width="135"><a href="../booking/special/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('Image10','','../booking/images/links_main/special1.gif',1)"><img name="Image10" border="0" src="../booking/images/links_main/special.gif" width="135" height="17" alt="special package"></a></td></tr> 
<tr> <td align="left" valign="bottom" height="17" width="135"><a href="../booking/site/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('Image11','','../booking/images/links_main/site_map1.gif',1)"><img name="Image11" border="0" src="../booking/images/links_main/site_map.gif" width="135" height="17" alt="site map"></a></td></tr> 
<tr> <td align="left" valign="top" height="7" width="135"><img src="../booking/images/spacer/ffcc66_top.gif" width="135" height="17"></td></tr> 
</table></td><td width="10" height="770" align="left" valign="top">&nbsp;</td><td width="585" height="770" align="left" valign="top"> 
<?php
require_once("agent_calendarNew.php");
?>
<table width="569" border="0" cellspacing="0" cellpadding="0"> 
<tr align="left" valign="middle"> 
 <td width="29" align="center" height="27">&nbsp;</td>
 <td width="253" align="left" height="27"><img src="../booking/images/buttons/copyright2004.gif" width="270" height="14" alt="california tour - copyright 2004"></td>
 <td width="287" height="27">&nbsp;</td>
</tr> 
</table>

</td></tr> </table></td></tr> </table><div align="center"></div><map name="Map"> 
<area shape="rect" coords="3,1,42,16" href="../index.htm" alt="California Tours Home Page" title="California Tours Home Page"> 
</map> </table>
</body>
</html>
