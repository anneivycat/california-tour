<html><head>
<style type='text/css'>
ul.primary {
  border-collapse: collapse;
  padding: 0em 0em 0em 1em;
  white-space: nowrap;
  list-style: none;
  margin: 5px;
  height: auto;
  line-height: normal;
  border-bottom: 1px solid #bbb;
} 
ul.primary li {
  display: inline;
}
ul.primary li a {
  background-color: #ddd;
  border-color: #bbb;
  border-width: 1px;
  border-style: solid solid none solid;
  height: auto;
  margin-right: 0.5em;
  padding: 0em 1em;
  text-decoration: none;
}
ul.primary li.active a {
  background-color: #fff;
  border: 1px solid #bbb;
  border-bottom: #fff 1px solid;
}
ul.primary li a:hover {
  background-color: #eee;
  border-color: #ccc;
  border-bottom-color: #eee;
}





BODY {
  font-size: 11px;
}

#navBar {
  width: 80%;
  top: 10px;
  left: 10%;
  position: absolute;
}

#resultFrame {
  width: 80%;
  height: 80%;
  top: 28px;
  left: 10%;
  position: absolute;
  border: 1px #ccc solid;
  background: #eee;
}

STRONG {
  font-size: 110%;
}

.myButton {
  background: #ddd;
  border-top: 1px #ccc solid;
  border-left: 1px #ccc solid;
  border-right: 1px #999 solid;
  border-bottom: 1px #999 solid;
  font-size: 100%;
}

.myInputField {
  background: #fff;
  border: 0px;
  border-top: 1px #ccc solid;
  border-left: 1px #ccc solid;
  border-right: 1px #999 solid;
  border-bottom: 1px #999 solid;
  width: 120px;
  position: absolute;
  left: 100px;
  padding: 1px;
  font-size: 100%;
}

.myInputField:focus {
  background: #eee;
}

.myPopupMenu {
  border: 0px;
  border-top: 1px #ccc solid;
  border-left: 1px #ccc solid;
  border-right: 1px #999 solid;
  border-bottom: 1px #999 solid;
  font-size: 10px;
}

</style>

<script type="text/javascript">
function gotoPage(n) {
	alert(n);
	document.getElementById('liTab1').style.class='primary';
	document.getElementById('liTab2').style.class='primary';
	document.getElementById('liTab3').style.class='primary';
	document.getElementById('liTab'+n).style.class='primary active';
	if (n==1) {
		document.getElementById('resultFrame').location="0-PU/newPUlocation.php";
	} else if (n==2) {
		document.getElementById('resultFrame').location="0-PU/newPUlocation.php";
	} else if (n==3) {
		document.getElementById('resultFrame').location="0-PU/reset.php";
	}
}

</script>

</head><body>
<?php
require ("../sqlfuncs.php");
$link=connect_to_db();
$rl=select_db($link);

function buildAgent2pulocation() {
$r=mysql_query("TRUNCATE TABLE `agent2pulocation`");
$r=mysql_query("SELECT * FROM `AGENT`");
@$nr=mysql_numrows($r)+0;

if($nr>0) {
  for($i=0;$i<$nr;$i++) {
    $line=mysql_fetch_array($r);
    extract($line);
    echo "$AGENT_CODE, $AGENT_NAME: ID $AGENT_ID<BR />\n";
    $PULOCATION_ID1+=0;
    $PULOCATION_ID2+=0;
    $PULOCATION_ID3+=0;
    if($PULOCATION_ID1>0) {
      $q="INSERT INTO `agent2pulocation` (`agent`, `pulocation`) VALUES ($AGENT_ID, $PULOCATION_ID1);";
      echo " --> $q<br />\n";
      $r2=mysql_query($q);
    }
    if($PULOCATION_ID2>0) {
      $q="INSERT INTO `agent2pulocation` (`agent`, `pulocation`) VALUES ($AGENT_ID, $PULOCATION_ID2);";
      echo " --> $q<br />\n";
      $r2=mysql_query($q);
    }
    if($PULOCATION_ID3>0) {
      $q="INSERT INTO `agent2pulocation` (`agent`, `pulocation`) VALUES ($AGENT_ID, $PULOCATION_ID3);";
      echo " --> $q<br />\n";
      $r2=mysql_query($q);
    }
    
  }
}
exit;
}

function addAgentPuLocation($agent, $pulocation) {
  $q="INSERT INTO `agent2pulocation` (`agent`, `pulocation`) VALUES ($agent, $pulocation);";
  $r2=mysql_query($q);
  if($r2!='') {
    $r2=mysql_query("SELECT `PULOCATION_LOCATION` FROM `PULOCATION` WHERE `PULOCATION_ID`=$pulocation;");
    $line=mysql_fetch_row($r2);
    $r2=mysql_query("SELECT `AGENT_CODE`, `AGENT_NAME` FROM `AGENT` WHERE `AGENT_ID`=$agent;");
    $line2=mysql_fetch_row($r2);
    echo "Agent ".$line2[0]." now has ".$line[0]." as a pick-up location.<br />";
    $r2=mysql_query("SELECT `pulocation` FROM `agent2pulocation` WHERE `agent`=$agent;");
    echo "Agent ".$line2[0]."'s pick-up locations are:<ul>\n";
    @$nr=mysql_num_rows($r2)+0;
    for($i=0;$i<$nr;$i++) {
      $line=mysql_fetch_row($r2);
      $r3=mysql_query("SELECT `PULOCATION_LOCATION` FROM `PULOCATION` WHERE `PULOCATION_ID`=".$line[0].";");
      $line=mysql_fetch_row($r3);
      echo "<li>".$line[0]."</li>\n";
    }
    echo "</ul>\n";
  } else {
    echo "<strong>There was a problem with this request. Please try again. Or contact Didier!</strong>";
  }
  exit;
}

function createPuLocation($pulCode, $location, $time) {
  $q="INSERT INTO `PULOCATION` (`PULOCATION_CODE`, `PULOCATION_LOCATION`, `PULOCATION_TIME`) VALUES ('$pulCode', '$location', '$time');";
  $r2=mysql_query($q);
  $lid=mysql_insert_id();
  if($r2!='') {
    echo "Location $location added successfully, id# $lid.\n";
  } else {
    echo "<strong>There was a problem with this request. Please try again. Or contact Didier!</strong>";
  }
exit;
}

if ($_POST['action'] == 'rebuild') {
  buildAgent2pulocation();
}

if ($_POST['action'] == 'addAgentPuLocation') {
  addAgentPuLocation($_POST['agent'], $_POST['pulocation']);
}

if ($_POST['action'] == 'createPuLocation') {
  createPuLocation($_POST['PULOCATION_CODE'], $_POST['PULOCATION_LOCATION'], $_POST['PULOCATION_TIME']);
}

?>

<div id='navBar'>
<ul class='primary'>
<li id="liTab1" class='primary active'><a href="0-PU/newPUlocation.php" target="resultFrame">New PUlocation</a></li>
<li id="liTab2" class='primary'><a href="0-PU/bindings.php" target="resultFrame">Bindings</a></li>
<li id="liTab3" class='primary'><a href="0-PU/reset.php" target="resultFrame">Don't touch!</a></li></ul>
</div>

<iframe id='resultFrame' name='resultFrame' src='0-PU/newPUlocation.php'></iframe>
