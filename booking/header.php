<meta name="distribution" content="Global" />
<meta name="Classification" content="California travel" />
<meta name="robots" content="index,follow" />
<meta name="revisit" content="15" />
<meta name="reply-to" content="tour@california-tour.com" />
<meta name="copyright" content="California Tour" />
<meta name="author" content="Copyright 2008 California Tours - ALL RIGHTS RESERVED" />

<script language="JavaScript" type="text/javascript" src="/california/scripts/engine.js"></script>

<link rel="stylesheet" rev="stylesheet" href="/california/styles/main_sub.css" type="text/css" />
<link rel="stylesheet" type="text/css" media="all" href="/wp-content/themes/catours/style.css" />
<script src='/wp-content/themes/catours/ca_tour_scripts.js' type='text/javascript' language='javascript'>
</script>

<link rel="shortcut icon" href="/favicon.ico" />
<link rel="icon" type="image/vnd.microsoft.icon" href="/favicon.ico" />

<style type="text/css">
#mainContent h4 { font-size:14px; }
</style>
<script type="text/javascript" src="/scripts/pickuplocation.js"></script>
<script language="JavaScript">
function CancelReservation() {
    document.location = "clearreservation.php";
}

function checkOccupancy() {
   if(document.signup.Occupancy.value == '1') { 
      document.getElementById('ROOMMATE_1').style.display = 'none';
      document.getElementById('ROOMMATE_2').style.display = 'none';
      document.getElementById('ROOMMATE_3').style.display = 'none';
      document.getElementById('roommate1_check').style.display = '';
   }
   else if(document.signup.Occupancy.value == '2') {
      document.getElementById('ROOMMATE_1').style.display = '';
      document.getElementById('ROOMMATE_2').style.display = 'none';
      document.getElementById('ROOMMATE_3').style.display = 'none';
      document.getElementById('roommate1_check').style.display = '';
   }
   else if(document.signup.Occupancy.value == '3') {
      document.getElementById('ROOMMATE_1').style.display = '';
      document.getElementById('ROOMMATE_2').style.display = '';
      document.getElementById('ROOMMATE_3').style.display = 'none';
      document.getElementById('roommate1_check').style.display = '';
   }
   else { 
      document.getElementById('ROOMMATE_1').style.display = '';
      document.getElementById('ROOMMATE_2').style.display = '';
      document.getElementById('ROOMMATE_3').style.display = '';
      document.getElementById('roommate1_check').style.display = 'none';
   }
}
</script>
</head>
<body class="page page-id-40 page-template page-template-default" onload='checkOccupancy();'>
<div id="wrapper">
	<div id="header">
		<a href='http://california-tour.com'><img src='/wp-content/themes/catours/images/ca-tours_logo.gif' id='logo'></a>
		<div id='head_box'>
			<div id='top_phone'>877 338 3883</div>
			<div id='second_line'><form role="search" method="get" id="searchform" action="/">
				<input type='text' name='s' id='search_field' value='Search' onfocus="if(this.value=='Search'){this.value='';}" onblur="if(this.value==''){this.value='Search';}">
				<input type='image' src='/wp-content/themes/catours/images/search-go.gif' id='searchsubmit'>
			</form>
			| <!--<a href="">LOG IN</a> | --><a href="/?page_id=40">CONTACT US</a></div>
		</div>
	</div><!-- #header -->
		<div id="access" role="navigation">
		  			<div class="skip-link screen-reader-text"><a href="#content" title="Skip to content">Skip to content</a></div>
						<div class="menu-header"><ul id="menu-top-navigation" class="menu"><li id="menu-item-60" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-60"><a href="/?page_id=9">San Francisco Vacations</a>
<ul class="sub-menu">
	<li id="menu-item-193" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-193"><a href="/?page_id=113">San Francisco &#038; Yosemite Vacation</a></li>
	<li id="menu-item-192" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-192"><a href="/?page_id=115">San Francisco &#038; Los Angeles by Coach Vacation</a></li>
	<li id="menu-item-191" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-191"><a href="/?page_id=117">San Francisco &#038; Los Angeles by Air Vacation</a></li>
	<li id="menu-item-190" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-190"><a href="/?page_id=120">San Francisco &#038; Las Vegas Vacation</a></li>
	<li id="menu-item-210" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-210"><a href="/?page_id=125">San Francisco &#038; California Coast Vacation</a></li>
</ul>
</li>
<li id="menu-item-57" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-57"><a href="/?page_id=10">Los Angeles Vacations</a>
<ul class="sub-menu">
	<li id="menu-item-106" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-106"><a href="/?page_id=89">Los Angeles Vacation</a></li>
	<li id="menu-item-105" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-105"><a href="/?page_id=91">Los Angeles &#038; Las Vegas by Coach Vacation</a></li>
	<li id="menu-item-104" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-104"><a href="/?page_id=93">Los Angeles &#038; Las Vegas by Air Vacation</a></li>
	<li id="menu-item-103" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-103"><a href="/?page_id=95">Los Angeles &#038; San Francisco by Coach Vacation</a></li>
	<li id="menu-item-102" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-102"><a href="/?page_id=97">Los Angeles &#038; San Francisco by Air Vacation</a></li>
	<li id="menu-item-101" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-101"><a href="/?page_id=99">Los Angeles, San Francisco &#038; Las Vegas by Air Vacation</a></li>
</ul>
</li>
<li id="menu-item-53" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-53"><a href="/?page_id=12">California Vacations</a>
<ul class="sub-menu">
	<li id="menu-item-217" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-217"><a href="/?page_id=108">California LAX-SFO Vacation</a></li>
	<li id="menu-item-218" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-218"><a href="/?page_id=111">California SFO-LAX Vacation</a></li>
</ul>
</li>
<li id="menu-item-58" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-58"><a href="/?page_id=14">Napa Valley Vacations</a>
<ul class="sub-menu">
	<li id="menu-item-215" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-215"><a href="/?page_id=127">Napa Valley &#038; San Francisco Vacation</a></li>
	<li id="menu-item-214" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-214"><a href="/?page_id=129">Napa Valley &#038; San Francisco Connoisseur Vacation</a></li>
	<li id="menu-item-213" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-213"><a href="/?page_id=133">Napa Valley &#038; San Francisco Calistoga Spa Vacation</a></li>
	<li id="menu-item-212" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-212"><a href="/?page_id=131">Napa Valley &#038; San Francisco Calistoga Resort Vacation</a></li>
</ul>
</li>
<li id="menu-item-59" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-59"><a href="/?page_id=16">New York Vacations</a>
<ul class="sub-menu">
	<li id="menu-item-186" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-186"><a href="/?page_id=151">New York Vacation</a></li>
	<li id="menu-item-185" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-185"><a href="/?page_id=153">New York &#038; Washington D.C. Vacation</a></li>
	<li id="menu-item-184" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-184"><a href="/?page_id=155">New York, Amish Country &#038; Philadelphia Vacation</a></li>
	<li id="menu-item-183" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-183"><a href="/?page_id=157">New York, Boston &#038; Washington D.C. Vacation</a></li>
</ul>
</li>
<li id="menu-item-56" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-56"><a href="/?page_id=20">Las Vegas Vacations</a>
<ul class="sub-menu">
	<li id="menu-item-219" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-219"><a href="/?page_id=135">Las Vegas &#038; Grand Canyon Vacation</a></li>
	<li id="menu-item-223" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-223"><a href="/?page_id=137">Las Vegas, Zion National Park &#038; Grand Canyon Vacation</a></li>
	<li id="menu-item-222" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-222"><a href="/?page_id=139">Las Vegas &#038; Southwest National Park Vacation</a></li>
	<li id="menu-item-221" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-221"><a href="/?page_id=141">Las Vegas &#038; San Francisco Vacation</a></li>
	<li id="menu-item-220" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-220"><a href="/?page_id=143">Las Vegas &#038; Los Angeles Vacation</a></li>
</ul>
</li>
<li id="menu-item-61" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-61"><a href="/?page_id=22">Washington Vacations</a>
<ul class="sub-menu">
	<li id="menu-item-182" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-182"><a href="/?page_id=159">Washington, D.C. Vacation</a></li>
	<li id="menu-item-181" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-181"><a href="/?page_id=161">Washington, D.C. &#038; New York Vacation</a></li>
	<li id="menu-item-179" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-179"><a href="/?page_id=165">Washington, D.C., Philadelphia &#038; New York Vacation</a></li>
</ul>
</li>
<li id="menu-item-54" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-54"><a href="/?page_id=24">Florida Vacations</a>
<ul class="sub-menu">
	<li id="menu-item-178" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-178"><a href="/?page_id=167">Orlando Theme Parks Vacation</a></li>
	<li id="menu-item-177" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-177"><a href="/?page_id=169">Orlando Theme Parks &#038; Golf Vacation</a></li>
	<li id="menu-item-176" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-176"><a href="/?page_id=171">Orlando Theme Parks &#038; Kennedy Space Center Vacation</a></li>
	<li id="menu-item-175" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-175"><a href="/?page_id=173">Orlando Theme Parks &#038; Miami Vacation</a></li>
</ul>
</li>
<li id="menu-item-55" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-55"><a href="/?page_id=26">Hawaii Vacations</a>
<ul class="sub-menu">
	<li id="menu-item-189" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-189"><a href="/?page_id=145">Waikiki Beach Vacation</a></li>
	<li id="menu-item-188" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-188"><a href="/?page_id=147">Maui Vacation</a></li>
	<li id="menu-item-187" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-187"><a href="/?page_id=149">Big Island Vacation</a></li>
</ul>
</li>
</ul></div>		</div><!-- #access -->
	<div id='page_photo'>
	
	<img src='/wp-content/uploads/header_images/6.jpg' alt='california tours'></div> <!-- page_photo -->
	<div id='page_area'>
		<div id='share_links'>
			share: &nbsp; <a href="http://www.facebook.com/share.php?u=http%3A%2F%2Fwww.california-tour.com%2Fwordpress%2F%3Fpage_id%3D40&title=Contact+Us" target='_blank'><img src='/wp-content/themes/catours/images/facebook-tiny.gif' ><span>facebook</span></a> &nbsp; <a href="javascript:;" onclick="panel_in('popup',this)" id='email_link'><img src='/wp-content/themes/catours/images/email-tiny.gif' ><span>email</span></a>
		</div>
		<div id='page_content'>
			

				<div id="post-40" class="post-40 page type-page status-publish hentry">