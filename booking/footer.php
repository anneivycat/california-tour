				</div><!-- #post-## -->

				
			<div id="comments">


	<p class="nocomments">Comments are closed.</p>


								
</div><!-- #comments -->

		</div> <!-- page_content -->
	
<div id='sidebar'>

		<div id="primary" class="widget-area" role="complementary">
			<ul class="xoxo">

<li id="text-3" class="widget-container widget_text"><h3 class="widget-title">L2 L3 Side Nav</h3>			<div class="textwidget"></div>
		</li>			</ul>
		</div><!-- #primary .widget-area -->


		<div id="secondary" class="widget-area" role="complementary">
			<ul class="xoxo">
				<li id="nav_menu-6" class="widget-container widget_nav_menu"><div class="menu-common-side-menu-container"><ul id="menu-common-side-menu" class="menu"><li id="menu-item-271" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-271"><a href="/?page_id=28">Group Travel | Private Tours</a></li>
<li id="menu-item-270" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-270"><a href="/?page_id=30">Destination Management Company</a></li>
<li id="menu-item-273" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-273"><a href="/?page_id=32">Educational Tours</a></li>
<li id="menu-item-272" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-272"><a href="/?page_id=34">Corporate Incentive Travel</a></li>
<li id="menu-item-275" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-275"><a href="/?page_id=36">Holiday Tours</a></li>
<li id="menu-item-274" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-274"><a href="/?page_id=38">Guided Bus Tours &#038; Packages</a></li>
</ul></div></li><li id="nav_menu-7" class="widget-container widget_nav_menu"><div class="menu-specials-arrow-container"><ul id="menu-specials-arrow" class="menu"><li id="menu-item-280" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-280"><a href="/?page_id=278">List of current specials</a></li>
</ul></div></li>			</ul>
		</div><!-- #secondary .widget-area -->

</div>
	<div class='clear'></div>
	</div> <!-- page_area -->

	<div id="footer" role="contentinfo">



			<div id="footer-widget-area" role="complementary">

				<div id="first" class="widget-area">
					<ul class="xoxo">
						<li id="nav_menu-4" class="widget-container widget_nav_menu"><div class="menu-bottom-navigation-container"><ul id="menu-bottom-navigation" class="menu"><li id="menu-item-65" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-65"><a href="/?page_id=28">Group Travel<br />Private Tours</a>
<ul class="sub-menu">
	<li id="menu-item-258" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-258"><a href="/?page_id=224">California Group Travel &#8211; Private Tours</a></li>
	<li id="menu-item-257" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-257"><a href="/?page_id=226">San Francisco Group Travel &#8211; Private Tours</a></li>
	<li id="menu-item-256" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-256"><a href="/?page_id=228">Group Travel from San Francisco &#8211; Private Tours</a></li>
	<li id="menu-item-255" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-255"><a href="/?page_id=230">Los Angeles Group Travel &#8211; Private Tours</a></li>
	<li id="menu-item-254" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-254"><a href="/?page_id=232">Group Travel from Los Angeles &#8211; Private Tours</a></li>
	<li id="menu-item-253" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-253"><a href="/?page_id=234">Napa Valley Group Travel &#8211; Private Tours</a></li>
	<li id="menu-item-252" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-252"><a href="/?page_id=236">Las Vegas Group Travel &#8211; Private Tours</a></li>
	<li id="menu-item-251" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-251"><a href="/?page_id=239">New York Group Travel &#8211; Private Tours</a></li>
	<li id="menu-item-942" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-942"><a href="/?page_id=396">Group Travel from New York &#8211; Private Tours</a></li>
	<li id="menu-item-250" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-250"><a href="/?page_id=241">Washington, D.C. Group Travel &#8211; Private Tours</a></li>
	<li id="menu-item-941" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-941"><a href="/?page_id=245">Group Travel from Washington D.C. &#8211; Private Tours</a></li>
	<li id="menu-item-249" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-249"><a href="/?page_id=243">Group Travel from Boston &#8211; Private Tours</a></li>
</ul>
</li>
<li id="menu-item-63" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-63"><a href="/?page_id=30">Destination<br />Management Company</a>
<ul class="sub-menu">
	<li id="menu-item-561" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-561"><a href="/?page_id=436">San Francisco Destination Management Company</a></li>
	<li id="menu-item-558" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-558"><a href="/?page_id=438">Los Angeles Destination Management Company</a></li>
	<li id="menu-item-560" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-560"><a href="/?page_id=440">San Diego Destination Management Company</a></li>
	<li id="menu-item-557" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-557"><a href="/?page_id=443">Las Vegas Destination Management Company</a></li>
	<li id="menu-item-563" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-563"><a href="/?page_id=388">Boston Destination Management Company</a></li>
	<li id="menu-item-559" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-559"><a href="/?page_id=444">New York Destination Management Company</a></li>
	<li id="menu-item-562" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-562"><a href="/?page_id=446">Washington D.C. Destination Management Company</a></li>
</ul>
</li>
<li id="menu-item-64" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-64"><a href="/?page_id=32">Educational<br />Tours</a>
<ul class="sub-menu">
	<li id="menu-item-573" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-573"><a href="/?page_id=470">Nutrition and Culinary Program</a></li>
	<li id="menu-item-572" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-572"><a href="/?page_id=454">San Francisco School Trips</a></li>
	<li id="menu-item-575" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-575"><a href="/?page_id=456">Los Angeles School Trips</a></li>
	<li id="menu-item-574" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-574"><a href="/?page_id=459">Las Vegas School Trips</a></li>
	<li id="menu-item-571" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-571"><a href="/?page_id=486">New York School Trips</a></li>
	<li id="menu-item-576" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-576"><a href="/?page_id=465">Washington D.C. School Trips</a></li>
	<li id="menu-item-577" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-577"><a href="/?page_id=482">Florida School Trips</a></li>
</ul>
</li>
<li id="menu-item-62" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-62"><a href="/?page_id=34">Corporate<br />Incentive Travel</a></li>
<li id="menu-item-67" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-67"><a href="/?page_id=36">Holiday Tours</a>
<ul class="sub-menu">
	<li id="menu-item-587" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-587"><a href="/?page_id=506">San Francisco Holiday Tours</a></li>
	<li id="menu-item-585" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-585"><a href="/?page_id=514">Los Angeles Holiday Tours</a></li>
	<li id="menu-item-584" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-584"><a href="/?page_id=509">Boston Holiday Tours</a></li>
	<li id="menu-item-586" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-586"><a href="/?page_id=512">San Diego Holiday Tours</a></li>
</ul>
</li>
<li id="menu-item-66" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-66"><a href="/?page_id=38">Guided Bus Tours<br />&#038; Packages</a>
<ul class="sub-menu">
	<li id="menu-item-591" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-591"><a href="/?page_id=310">Tours &#038; Packages from Boston</a></li>
	<li id="menu-item-589" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-589"><a href="/?page_id=522">Tours &#038; Packages from San Francisco</a></li>
	<li id="menu-item-597" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-597"><a href="/?page_id=520">Tours &#038; Packages from Los Angeles</a></li>
	<li id="menu-item-588" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-588"><a href="/?page_id=524">Tours &#038; Packages from San Diego</a></li>
</ul>
</li>
<li id="menu-item-68" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-68"><a href="/booking/">BOOK NOW</a></li>
</ul></div></li>					</ul>
				</div><!-- #first .widget-area -->

				<div id="second" class="widget-area">
					<ul class="xoxo">
						<li id="nav_menu-3" class="widget-container widget_nav_menu"><div class="menu-foot-navigation-container"><ul id="menu-foot-navigation" class="menu"><li id="menu-item-87" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-87"><a title="YouTube" href="http://www.youtube.com/user/CaliforniaToursInc"><img src='/wp-content/themes/catours/images/youtube-icon.gif' /></a></li>
<li id="menu-item-88" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-88"><a title="facebook" href="http://facebook.com"><img src='/wp-content/themes/catours/images/facebook-icon.gif' /></a></li>
<li id="menu-item-71" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-71"><a href="/?page_id=46">Site Map</a></li>
<li id="menu-item-3078" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-3078"><a href="http://privacy-policy.truste.com/verified-policy/www.california-tour.com">Policy</a></li>
<li id="menu-item-73" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-73"><a href="/?page_id=42">About Us</a></li>
<li id="menu-item-74" class="menu-item menu-item-type-post_type menu-item-object-page current-menu-item page_item page-item-40 current_page_item menu-item-74"><a href="/?page_id=40">Contact Us</a></li>
</ul></div></li>					</ul>
				</div><!-- #second .widget-area -->



			</div><!-- #footer-widget-area -->

	</div><!-- #footer -->
<ul id='bottom_logos'>
	<li><img src="/wp-content/themes/catours/images/foot-logo-OiSF.gif" style='width:93px;height:23px' alt='Only in San Francisco'></li>
	<li><img src="/wp-content/themes/catours/images/foot-logo-TIA.gif" style='width:52px;height:24px' alt='TIA'></li>
	<li><img src="/wp-content/themes/catours/images/foot-logo-LA.gif" style='width:75px;height:24px' alt='Los Angeles'></li>
	<li><img src="/wp-content/themes/catours/images/foot-logo-ECMTA.gif" style='width:70px;height:28px' alt='Only in San Francisco'></li>
	<li><img src="/wp-content/themes/catours/images/foot-logo-etrust.gif" style='width:22px;height:26px' alt='eTrust'></li>
</ul>

<div class='clear'></div>

</div><!-- #wrapper -->
<div id='page_bottom'>
</div>
<div id='site_credits'>
<script language='javascript' type='text/javascript'>
<!--
	var today = new Date();
	var thisyear = today.getFullYear();
	document.write("&copy; "+thisyear);
//-->
</script>
<noscript>
	&copy; 2011 
</noscript> California Tours. All rights reserved. California Seller of Travel #2065568-40<br />
<a href='http://almost-everything.com/portfolio' target='_blank'>Website design by Almost Everything Communications</a>
</div>
<div id='popup'>
	<div class='pop_close'><a href='javascript:;'onclick="panel_out('popup');reset_friend_form();">x</a></div>
	<div id='popup_panel'>
	<div id='pop_content'>
		<div id='err_field'></div>
		<form action='javascript:;' onsubmit="return tell_friend()">
				<input type='hidden' name='cat_this_page' id='cat_this_page' value='40'>
				<input type='hidden' name='cat_this_title' id='cat_this_title' value="Contact Us">
			<div class='element'>
				<label for="cat_send_email">Your Email:</label>
				<input type='text' name='cat_send_email' id='cat_send_email'>
			</div>
			<div class='element'>
				<label for="cat_frnd_email">Your Friend's Email:</label>
				<input type='text' name='cat_frnd_email' id='cat_frnd_email'>
			</div>
				<input type='submit' value='Share this page'>
		</form>
	</div>
	</div>
</div>
<script type="text/javascript">
var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
</script>
<script type="text/javascript">
var pageTracker = _gat._getTracker("UA-4235877-1");
pageTracker._initData();
pageTracker._trackPageview();
</script>

</body>
</html>
