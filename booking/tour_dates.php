<?php
include ("sqlfuncs.php");
include ("../wp-content/plugins/catours_features/catours_functions.php");
include ("../wp-content/plugins/catours_features/catours_config.php");

$l = connect_to_db();
$l = select_db($l);

$year=date("Y");
$month=date("m");
$day=date("d");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
   "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
<title>Select Tour Date</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<Meta name="Description" CONTENT="Gateway to the Pacific Rim, San Francisco will amaze and delight you while you travel." />
<Meta name="Keywords" CONTENT="travel San Francisco, Los Angeles, Disneyland, Santa Barbara, San Franicso Chinatown, San Francisco cablecar, Coit tower, Twin Peaks, American city, California sunset, San Francisco photographs, photos of San Francisco" />

<?php
include "header.php";
	
	if(isset($_REQUEST["city"]) && strlen($_REQUEST["city"])==2){
		$city = $_REQUEST["city"];
	}else{
		$city = "";
	}
	if(isset($_REQUEST["tour"])){
		$tour = $_REQUEST["tour"];
	}else{
		$tour = "";
	}
	if(isset($_REQUEST["tourtype"]) && strlen($_REQUEST["tourtype"])==4){
		$tourtype = $_REQUEST["tourtype"];
	}else{
		$tourtype = "";
	}

	$tour_list = "";
	
	if($tour != ""){
	
	 $query = "SELECT SCHEDULEDTOUR.SCHEDULEDTOUR_CODE,
  				TOUR.TOUR_ID,
				SCHEDULEDTOUR.SCHEDULEDTOUR_DEPART_DATE,
				CONCAT('20',SUBSTRING(SCHEDULEDTOUR.SCHEDULEDTOUR_DEPART_DATE,7)) AS year,
				TOUR.TOUR_NAME FROM SCHEDULEDTOUR,
				TOUR WHERE SCHEDULEDTOUR.TOUR_ID = TOUR.TOUR_ID
				AND SCHEDULEDTOUR.SCHEDULEDTOUR_CODE like '$city%'
				AND TOUR.TOUR_NAME like '%$tour%' 
				ORDER BY year,SCHEDULEDTOUR.SCHEDULEDTOUR_DEPART_DATE";
		}elseif($tourtype != ""){
			$query = "SELECT SCHEDULEDTOUR.SCHEDULEDTOUR_CODE, TOUR.TOUR_ID, SCHEDULEDTOUR.SCHEDULEDTOUR_DEPART_DATE, TOUR.TOUR_NAME FROM SCHEDULEDTOUR, TOUR WHERE SCHEDULEDTOUR.TOUR_ID = TOUR.TOUR_ID AND SCHEDULEDTOUR.SCHEDULEDTOUR_CODE like '$city%' AND TOUR.TOUR_TYPE like '%$tourtype%' ORDER BY SCHEDULEDTOUR.SCHEDULEDTOUR_DEPART_DATE";
		}
		
	  $mysql_result = mysql_query($query);
	  $totalnumrows = mysql_num_rows($mysql_result);
	  if ($totalnumrows>0) {
		$tour_list .= "<ul>";
		while ($row = mysql_fetch_array($mysql_result)) {
		   $DepartureDate = $row['SCHEDULEDTOUR_DEPART_DATE'];
		   $this_tour = ucwords(strtolower($row['TOUR_NAME']));
		   $tourday = substr($DepartureDate,3,2);
		   $tourmonth = substr($DepartureDate,0,2);
		   $touryear = "20" . substr($DepartureDate,6,2);
		   
		   $tour_date_num = strtotime($DepartureDate);
		   $show_after = time() - 86000;
       
		   if ($tour_date_num > $show_after) {
				 $ScheduledTourCode = strtoupper($row['SCHEDULEDTOUR_CODE']);
				 $TourName = $row['TOUR_NAME'];
				 $DepartureDate = $row['SCHEDULEDTOUR_DEPART_DATE'];
			 $TimeStamp = mktime(0,0,0,$tourmonth,$tourday,$touryear);
			 $PrettyDate = date("l, F j, Y", $TimeStamp);
				 $SchedCodeLength = strlen($ScheduledTourCode);
				 $ScheduledTourShortCode = substr($ScheduledTourCode,0,$SchedCodeLength - 2);
				 $myURL="/booking/reservations/checkcalsign.php?ActivityCode=$ScheduledTourShortCode".substr($touryear,-2);
				 $tour_list .= "\n<li><a href=\"$myURL\">$PrettyDate</a></li>\n";
		}
		  }
		  $tour_list .= "</ul>";
		}
		
		if($tour_list == "" || $tour_list == "<ul></ul>")
			$tour_list = "
			<p><em>No tours currently scheduled. Please <a href='/?page_id=40'>contact us</a> to arrange your tour.</em></p>";
		
?>

<h1>Book a Tour</h1>
<h3><?php echo $this_tour ?></h3>

<p>Please select a date from the list below to begin booking your tour.</p>

<?php
echo "$tour_list";

?>

<br clear="right" />
<br clear="left" />
<?php
include "footer.php";
?>
