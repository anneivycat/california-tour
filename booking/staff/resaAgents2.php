<html>
<head><title>Users Management Area</title>
<style type="text/css">
BODY {
	font: 400 11px Verdana, Arial, sans-serif;
}

TABLE {
	border: 1px silver solid;
	width: 100%;
}

TR {
	font: 400 11px Verdana, Arial, sans-serif;
	border: 0px silver dotted;
	background-color: #FFF;
}

TD {
	border: 0px;
	border-left: 1px silver solid;
	margin-left: 0px;
	font: 400 100% Verdana, Arial, sans-serif;
	background-color: #FFF;
	padding: 2px;
	margin: 0px;
}

.amount {
	font: 400 100% Verdana, Arial, sans-serif;
	text-align: right;
	padding: 2px;
}

.subtotal {
	font: bold 100% Verdana, Arial, sans-serif;
	text-align: right;
	padding: 2px;
}

.underline {
	border: 0px silver dotted;
	border-bottom: 2px silver solid;
	font: bold 100% Verdana, Arial, sans-serif;
	background-color: #eee;
	text-align: center;
	padding: 2px;
}

.underlineRight {
	border: 0px silver dotted;
	border-bottom: 2px silver solid;
	font: bold 100% Verdana, Arial, sans-serif;
	background-color: #eee;
	text-align: right;
	padding: 2px;
}

</style>
</head>
<body>


<?php
extract($_POST);

function formatDigits($d) {
	$e=$d+0;
	return(substr("00".$e,-2));
}

require_once("../sqlfuncs.php");
$l=connect_to_db();
select_db($l);

$dd=date("Y-m-d H:i:s");
?>
<h1>CALIFORNIA TOURS, LLC</h3><hr>
<span class="unObstrusive">2015 Center Street, 1st Floor Berkeley, CA 94704 Tel: 877/338-3883 Fax: 510/549-4210</span><div style="float: right;"><a href="mailto:tours@california-tour.com">tours@california-tour.com</a></div><br clear="all" />

<center><h2>Reservation List</h2></center>
<center><h4>(-- Created <?php echo $dd; ?> --)</h4></center>

<?php
echo "<h3>Date range: $StartMonth/$StartDay/$StartYear ~$EndMonth/$EndDay/$EndYear ($j reservations)</h3>\n";

$EndMonth=formatDigits($EndMonth);
$EndDay=formatDigits($EndDay);
$EndYear=formatDigits($EndYear);
$StartMonth=formatDigits($StartMonth);
$StartDay=formatDigits($StartDay);
$StartYear=formatDigits($StartYear);

$q="SELECT * FROM `SCHEDULEDTOUR`;";
$r=mysql_query($q);
$j=mysql_num_rows($r);
for($i=0;$i<$j;$i++) {
	$line=mysql_fetch_array($r);
	$tour[$line['SCHEDULEDTOUR_ID']]=$line['SCHEDULEDTOUR_CODE'];
}

$q="SELECT * FROM `AGENT` WHERE AGENT_ID=$AGENT_ID;";
$r=mysql_query($q);
//$j=mysql_num_rows($r);
//for($i=0;$i<$j;$i++) {
	$line=mysql_fetch_array($r);
	$agent[$line['AGENT_ID']]=$line['AGENT_CODE'];
	$agentComm[$line['AGENT_ID']]=$line['AGENT_COMMISIONTYPE'];
//}

$s="SELECT ";
$liste="SCHEDULEDTOURBOOK_TIMESTAMP	AGENT_ID	SCHEDULEDTOUR_ID	SCHEDULEDTOURBOOK_ID	LAST_NAME 	FIRST_NAME 	TOTALPAYTODLT	TOTALPAYTOAGENT	TOTALPAYCC	TOTALCHARGE";
$liste2="Date	Agent	Tour ID	Conf#	Last Name	First Name	Pd CLT	Pd Agent	Pd CC	Total<br />Charge";

$l=explode("\t",$liste);
$l2=explode("\t",$liste2);

$i=0;
$posSCHEDULEDTOURBOOK_TIMESTAMP=$i++;
$posAGENT_ID=$i++;
$posSCHEDULEDTOUR_ID=$i++;
$posSCHEDULEDTOURBOOK_ID=$i++;
$posLAST_NAME=$i++;
$posFIRST_NAME=$i++;
$posTOTALPAYTODLT=$i++;
$posTOTALPAYTOAGENT=$i++;
$posTOTALPAYCC=$i++;
$posTOTALCHARGE=$i++;

$nr=count($l);
echo "<table border='0'><tr>";
for($i=0;$i<$nr;$i++) {
	$fieldName[$i]=$l[$i];
	$filedRealName[$i]=$l2[$i];
	echo "<td class='underline'>".$l2[$i]."</td>\n";
	$s.="SCHEDULEDTOURBOOK.".$l[$i].", ";
}
echo "<td class='underline'>Web Disc</td>
<td class='underline'>Comm</td>
<td class='underline'>Bal&nbsp;Due</td></tr>\n";

$s=substr($s,0,strlen($s)-2)." 
FROM SCHEDULEDTOURBOOK
LEFT JOIN SCHEDULEDTOUR ON SCHEDULEDTOURBOOK.SCHEDULEDTOUR_ID = SCHEDULEDTOUR.SCHEDULEDTOUR_ID
WHERE SCHEDULEDTOURBOOK.`SCHEDULEDTOURBOOK_TIMESTAMP` <= '20$EndYear-$EndMonth-".($EndDay+1)."'
AND SCHEDULEDTOURBOOK.`SCHEDULEDTOURBOOK_TIMESTAMP` >= '20$StartYear-$StartMonth-$StartDay'
AND (SCHEDULEDTOURBOOK.`SCHEDULEDTOURBOOK_TIMESTAMP` LIKE  '20$StartYear-%'
OR SCHEDULEDTOURBOOK.`SCHEDULEDTOURBOOK_TIMESTAMP` LIKE  '20$EndYear-%')\n";

if($CITY!="-1") {
	$s.="AND SCHEDULEDTOUR.`SCHEDULEDTOUR_CODE` LIKE '$CITY%'\n";
}

if($AGENT_ID!="-1") {
	$s.="AND SCHEDULEDTOURBOOK.`AGENT_ID` = $AGENT_ID \n";
}

$s.="order by 
SCHEDULEDTOURBOOK.`AGENT_ID`, 
SCHEDULEDTOURBOOK.`SCHEDULEDTOUR_ID`, 
SCHEDULEDTOURBOOK.`LAST_NAME`;";

// SEE QUERY
//echo $s;
$r=mysql_query($s);
$j=mysql_num_rows($r);
$currentAgent="";

$counter=0;
for($i=0;$i<$j;$i++) {
	$ln=mysql_fetch_row($r);
	if($currentAgent!=$agent[$ln[$posAGENT_ID]]) {
		// first test whether currentAgent is empty: start!
		if($currentAgent!="") {
			$totalPdCLT+=$PdCLT[$currentAgent];
			$totalPdAgent+=$PdAgent[$currentAgent];
			$totalPdCC+=$PdCC[$currentAgent];
			$totalTotalCharge+=$ttlCharge[$currentAgent];
			echo "<tr>\n";
			for($ii=0;$ii<$nr;$ii++) {
				if($ii==$posTOTALPAYTODLT) {
					echo "<td class='subtotal'>$".number_format($PdCLT[$currentAgent], 2, '.', ",")."</td>\n";
				} elseif($ii==$posTOTALPAYTOAGENT) {
					echo "<td class='subtotal'>$".number_format($PdAgent[$currentAgent], 2, '.', ",")."</td>\n";
				} elseif($ii==$posTOTALCHARGE) {
					echo "<td class='subtotal'>$".number_format($ttlCharge[$currentAgent], 2, '.', ",")."</td>\n";
				} elseif($ii==$posTOTALPAYCC) {
					echo "<td class='subtotal'>$".number_format($PdCC[$currentAgent], 2, '.', ",")."</td>\n";
				} else {
					echo "<td>&nbsp;</td>\n";
				}
			}
			echo "<td class='subtotal'>$".number_format($zeDiscs[$currentAgent], 2, '.', ",")."</td>\n";
			$totalzeDiscs+=$zeDiscs[$currentAgent];
			echo "<td class='subtotal'>$".number_format($zeComms[$currentAgent], 2, '.', ",")."</td>\n";
			$totalzeComms+=$zeComms[$currentAgent];
			echo "<td class='subtotal'>$".number_format($Balance[$currentAgent], 2, '.', ",")."</td>\n";
			$totalBalance+=$Balance[$currentAgent];
			$counter=0;
			echo "<tr>\n";
			for($ii=0;$ii<$nr;$ii++) {
				echo "<td>&nbsp;</td>\n";
			}
			echo "</tr>\n";
		}
		// set in all cases the current agent's code
		$currentAgent=$agent[$ln[$posAGENT_ID]];
		// set their commission type
		$currentComm=$agentComm[$ln[$posAGENT_ID]];
	}
	echo "<tr>";
	$ln[$posLAST_NAME]=ucwords(strtolower($ln[$posLAST_NAME]));
	$ln[$posFIRST_NAME]=ucwords(strtolower($ln[$posFIRST_NAME]));
	for($ii=0;$ii<$nr;$ii++) {
		if($ii==$posAGENT_ID) {
			echo "<td>".$agent[$ln[$posAGENT_ID]]."</td>";
		} elseif($ii==$posSCHEDULEDTOURBOOK_TIMESTAMP) {
			echo "<td>".substr($ln[$posSCHEDULEDTOURBOOK_TIMESTAMP],0,10)."</td>";
		} elseif($ii==$posSCHEDULEDTOUR_ID) {
			echo "<td>".$tour[$ln[$posSCHEDULEDTOUR_ID]]."</td>";
		} elseif($ii==$posTOTALPAYTODLT || $ii==$posTOTALPAYTOAGENT || $ii==$posTOTALPAYCC || $ii==$posTOTALCHARGE) {
			echo "<td class='amount'>$".number_format($ln[$ii], 2, '.', ",")."</td>\n";
		} else {
			echo "<td>".$ln[$ii]."</td>";
		}
	}

	$tt=substr($tour[$ln[$posSCHEDULEDTOUR_ID]],0,4);
	echo "<!-- \$tt = [$tt] -->\n";
	// get the web discount on this tour
	if($TourWeb[$tt][$currentComm]=="") {
		// we don't have it yet (tour id 4 letters + commission type)
		// let's get it and CACHE it!
		$q="SELECT `TOUR_WEB$currentComm` FROM `TOUR` WHERE `TOUR_TYPE` ='$tt';";
		$rr=mysql_query($q);
		$lln=mysql_fetch_row($rr);
		$TourWeb[$tt][$currentComm]=($lln[0]+0.0);
		echo "<!-- \$TourWeb[$tt][$currentComm] = [".($lln[0]+0.0)."] -->\n";
	}
	// get the commission on this tour
	if($commission[$tt][$currentComm]=="") {
		// we don't have it yet (tour id 4 letters + commission type)
		// let's get it and CACHE it!
		$q="SELECT `TOUR_COMM$currentComm` FROM `TOUR` WHERE `TOUR_TYPE` ='$tt';";
		$rr=mysql_query($q);
		$lln=mysql_fetch_row($rr);
		$commission[$tt][$currentComm]=($lln[0]+0.0);
	}
	echo "<td class='amount'>$".number_format($TourWeb[$tt][$currentComm], 2, '.', ",")."</td>\n";
	echo "<td class='amount'>$".number_format($commission[$tt][$currentComm], 2, '.', ",")."</td>\n";
	echo "<td class='amount'>$".number_format(($ln[$posTOTALPAYTODLT]+$ln[$posTOTALPAYTOAGENT])-$commission[$tt][$currentComm], 2, '.', ",")."</td>\n";
	echo "</tr>\n";
	$PdCLT[$currentAgent]+=$ln[$posTOTALPAYTODLT];
	$PdAgent[$currentAgent]+=$ln[$posTOTALPAYTOAGENT];
	$PdCC[$currentAgent]+=$ln[$posTOTALPAYCC];
	$ttlCharge[$currentAgent]+=$ln[$posTOTALCHARGE];
	// The WebDiscounts
	$zeDiscs[$currentAgent]+=$TourWeb[$tt][$currentComm];
	// The Commissions
	$zeComms[$currentAgent]+=$commission[$tt][$currentComm];
	// The Balance: Pd CLT minus commission
	$Balance[$currentAgent]+=(($ln[$posTOTALPAYTODLT]+$ln[$posTOTALPAYTOAGENT])-$commission[$tt][$currentComm]);
	$counter+=1;
	if($counter==5) {
		$counter=0;
		echo "<tr>\n";
		for($ii=0;$ii<$nr+3;$ii++) {
			echo "<td>&nbsp;</td>\n";
		}
		echo "</tr><tr>\n";
	}
}
echo "<tr>\n";
			for($ii=0;$ii<$nr;$ii++) {
				if($ii==$posTOTALPAYTODLT) {
					echo "<td class='subtotal'>$".number_format($PdCLT[$currentAgent], 2, '.', ",")."</td>\n";
				} elseif($ii==$posTOTALPAYTOAGENT) {
					echo "<td class='subtotal'>$".number_format($PdAgent[$currentAgent], 2, '.', ",")."</td>\n";
				} elseif($ii==$posTOTALCHARGE) {
					echo "<td class='subtotal'>$".number_format($ttlCharge[$currentAgent], 2, '.', ",")."</td>\n";
				} elseif($ii==$posTOTALPAYCC) {
					echo "<td class='subtotal'>$".number_format($PdCC[$currentAgent], 2, '.', ",")."</td>\n";
				} else {
					echo "<td>&nbsp;</td>\n";
				}
			}
			echo "<td class='subtotal'>$".number_format($zeDiscs[$currentAgent], 2, '.', ",")."</td>\n";
			$totalzeDiscs+=$zeDiscs[$currentAgent];
			echo "<td class='subtotal'>$".number_format($zeComms[$currentAgent], 2, '.', ",")."</td>\n";
			$totalzeComms+=$zeComms[$currentAgent];
			echo "<td class='subtotal'>$".number_format($Balance[$currentAgent], 2, '.', ",")."</td>\n";
			$totalPdCLT+=$PdCLT[$currentAgent];
			$totalPdAgent+=$PdAgent[$currentAgent];
			$totalPdCC+=$PdCC[$currentAgent];
			$totalTotalCharge+=$ttlCharge[$currentAgent];
			$totalBalance+=$Balance[$currentAgent];

echo "<tr><td>&nbsp;</td></tr><tr>\n";
for($ii=0;$ii<$nr;$ii++) {
	if($ii==$posTOTALPAYTODLT || $ii==$posTOTALPAYTOAGENT || $ii==$posTOTALPAYCC || $ii==$posTOTALCHARGE) {
		echo "<td class='underline'>".$l2[$ii]."</td>";
	} else {
		echo "<td>&nbsp;</td>\n";
	}
}
echo "<td class='underline'>Web Disc</td>
<td class='underline'>Comm</td>
<td class='underline'>Bal Due</td></tr>\n";

echo "<tr>\n";
for($ii=0;$ii<$nr;$ii++) {
	if($ii==$posTOTALPAYTODLT) {
		echo "<td class='underlineRight'>$".number_format($totalPdCLT, 2, '.', ",")."</td>\n";
	} elseif($ii==$posTOTALPAYTOAGENT) {
		echo "<td class='underlineRight'>$".number_format($totalPdAgent, 2, '.', ",")."</td>\n";
	} elseif($ii==$posTOTALPAYCC) {
		echo "<td class='underlineRight'>$".number_format($totalPdCC, 2, '.', ",")."</td>\n";
	} elseif($ii==$posTOTALCHARGE) {
		echo "<td class='underlineRight'>$".number_format($totalTotalCharge, 2, '.', ",")."</td>\n";
	} else {
		echo "<td>&nbsp;</td>\n";
	}
}

echo "<td class='underlineRight'>$".number_format($totalzeDiscs, 2, '.', ",")."</td>\n";
echo "<td class='underlineRight'>$".number_format($totalzeComms, 2, '.', ",")."</td>\n";
echo "<td class='underlineRight'>$".number_format($totalBalance, 2, '.', ",")."</td></tr>\n";
?>
