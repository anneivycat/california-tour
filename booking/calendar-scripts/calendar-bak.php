<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=x-sjis">
<Script Language='JavaScript' src='translate.js'>
</script>

<title>California Tours, Inc.</title>
</head>

<body bgcolor="white" text="black">

<?

	include ("../sqlfuncs.php");

	// open a connection to mysql
	$mysql_link = connect_to_db();
	$mysql_result = select_db($mysql_link);

/*
	Copyright 2003
	Andrew George Stanton
	Stanton Web Applications
	649 Jones Street, #407
	San Francisco, CA 94102
	tel: (415)-929-8033, ext. 407
	fax: (415)-441-8033
	email: andy@stantonweb.com
	web: http://www.stantonweb.com

	Permission is given to use this PHP code
	for any purpose with the sole proviso that
	the address and copyright notice indicated above
	be retained in the source code
*/

	$dtToday = getdate(time());
/*
	print($city);
	print("<br>");
	print($year);
	print("<br>");
	print($month);
	print("<br>");
	print($day);
	print("<br>");
	print($languageUsed);
*/
	if ($year)
	{
		$dtCurrentDate = sprintf("%s/%s/%s",$month,$day,$year);
//		print("Year = $year");
//		print("<br>");
//		print("Month = $month");
//		print("<br>");
//		print("Day = $day");
//		print("<br>");

		$CurrentYear = $year;
		$CurrentMonth = $month;
		$CurrentDay = $day;

	}
	else
	{
		$dtCurrentDate = $dtToday;
	}





	print("<table ALIGN='CENTER' BORDER='0' bgcolor= '#0000ff' CELLSPACING='1' WIDTH='80%'>");

//	**************************************************
//	 Display a calendar based on the current date
//	 or date inserted as a variable
//	 *************************************************

//	 first, we we have to get create a function to display the
//	  the name of the current month


	function getMonthName($iMonth)
	{
		$strMonthName = "INVALID MONTH";
		if ($iMonth == 1) $strMonthName = "January";
		if ($iMonth == 2) $strMonthName = "February";
		if ($iMonth == 3) $strMonthName = "March";
		if ($iMonth == 4) $strMonthName = "April";
		if ($iMonth == 5) $strMonthName = "May";
		if ($iMonth == 6) $strMonthName = "June";
		if ($iMonth == 7) $strMonthName = "July";
		if ($iMonth == 8) $strMonthName = "August";
		if ($iMonth == 9) $strMonthName = "September";
		if ($iMonth == 10) $strMonthName = "October";
		if ($iMonth == 11) $strMonthName = "November";
		if ($iMonth == 12) $strMonthName = "December";

		return ($strMonthName);
	}

//	print(getMonthName($CurrentMonth));
//	print("<br>");


	function getDaysInMonth($CurrentYear,$CurrentMonth)
	{
		$iDaysInMonth = 31;


		if (getMonthName($CurrentMonth) == "January") $iDaysInMonth = 31;
		if (getMonthName($CurrentMonth) == "March") $iDaysInMonth = 31;
		if (getMonthName($CurrentMonth) == "April") $iDaysInMonth = 30;
		if (getMonthName($CurrentMonth) == "May") $iDaysInMonth = 31;
		if (getMonthName($CurrentMonth) == "June") $iDaysInMonth = 30;
		if (getMonthName($CurrentMonth) == "July") $iDaysInMonth = 31;
		if (getMonthName($CurrentMonth) == "August") $iDaysInMonth = 31;
		if (getMonthName($CurrentMonth) == "September") $iDaysInMonth = 30;
		if (getMonthName($CurrentMonth) == "October") $iDaysInMonth = 31;
		if (getMonthName($CurrentMonth) == "November") $iDaysInMonth = 30;
		if (getMonthName($CurrentMonth) == "December") $iDaysInMonth = 31;

		if (getMonthName($CurrentMonth) == "February")
		{

			$strCurrentYear = sprintf("%s",$CurrentYear);
			if (bcmod($CurrentYear,"4") == 0)
			{
				$iDaysInMonth = 29;
			}
			else
			{
				$iDaysInMonth = 28;
			}
		}
		return ($iDaysInMonth);
	}


//	print(getDaysInMonth($CurrentYear,$CurrentMonth));
//	print("<br>");

	function getFirstWeekDay($CurrentYear,$CurrentMonth,$CurrentDay)
	{
		$jdc = gregoriantojd($CurrentMonth,$CurrentDay,$CurrentYear);

		$iFirstWeekDay = jddayofweek($jdc,0);

		return ($iFirstWeekDay + 1);
	}

//	print(getFirstWeekDay($CurrentYear,$CurrentMonth,$CurrentDay));
//	print("<br>");


	// now, write the date as a header to the table for the current date



	//	 Now, we need to display a calender.  At most, a calendar can
	//	 have 6 weeks.  Since a week is 7 days,  we will show, at most
	//	 42 calendar cells.  We will use a TABLE to generate this calendar

	//	 we will create an array that will store the 42 possible days of the
	//	 month.

		$aCalendarDays[42];

	//	into aCalendarDays will will place the days of the current month.
	//	we will use the DatePart function to determine
	//	when the first day of the month is.

		$iFirstWeekDay = getFirstWeekDay($year,$month,$day);

//		print($iFirstWeekDay);
//		print("<br>");

	//	Now, we want to loop from 1 to the number of days in the
	//	 current month, populating the array aCalendarDays

		$iDaysInMonth = getDaysInMonth($year,$month);

//		print($iDaysInMonth);
//		print("<br>");

		for ($iLoop = 1; $iLoop <= $iDaysInMonth; $iLoop++)
		{
			$aCalendarDays[$iLoop + $iFirstWeekDay - 1] = $iLoop;
	// DEBUG		document.write(aCalendarDays[iLoop]);
		}

	//	 now that we have our populated array, we need to display
	//	 the array in calendar form.  We will create a table of 7 columns,
	//	 one for each day of the week.  The number of rows we will use
	//	 will be 6, the total number of possible rows, minus 42 (the upper
	//	 bound of the aCalendarDays array) minus the last array position
	//	 used (iFirstWeekDay + iDaysInMonth) divided by seven, the number
	//	 of days in the week! simple eh!!

		$iColumns;
		$iRows;

		$iColumns = 7;
		$iRows = 6 - floor((42 - ($iFirstWeekDay + $iDaysInMonth)) / 7);

	// DEBUG	document.write(iColumns);
	// DEBUG	document.write(iRows);


	//	Now, loop through 1 through 7, for the days of the week
	//	as a header for each column

		print("<TR bgcolor='#0000ff'>");

		for ($iDayBanner = 1; $iDayBanner <= $iColumns; $iDayBanner++)
		{
			print("<TD VALIGN='TOP' ALIGN='CENTER' WIDTH='14%'>");
			if ($languageUsed == "ENG")
			{
				if ($iDayBanner == 1) print("<font face='arial' size='+1' color='white'><b>Sun</b></font>");
				if ($iDayBanner == 2) print("<font face='arial' size='+1' color='white'><b>Mon</b></font>");
				if ($iDayBanner == 3) print("<font face='arial' size='+1' color='white'><b>Tues</b></font>");
				if ($iDayBanner == 4) print("<font face='arial' size='+1' color='white'><b>Wed</b></font>");
				if ($iDayBanner == 5) print("<font face='arial' size='+1' color='white'><b>Thur</b></font>");
				if ($iDayBanner == 6) print("<font face='arial' size='+1' color='white'><b>Fri</b></font>");
				if ($iDayBanner == 7) print("<font face='arial' size='+1' color='white'><b>Sat</b></font>");
			}
			else
			{
				if ($iDayBanner == 1) print("<font face='arial' size='+1' color='white'><b><script language = 'JavaScript'>translate(\"Sun\");</script></b></font>");
				if ($iDayBanner == 2) print("<font face='arial' size='+1' color='white'><b><script language = 'JavaScript'>translate(\"Mon\");</script></b></font>");
				if ($iDayBanner == 3) print("<font face='arial' size='+1' color='white'><b><script language = 'JavaScript'>translate(\"Tues\");</script></b></font>");
				if ($iDayBanner == 4) print("<font face='arial' size='+1' color='white'><b><script language = 'JavaScript'>translate(\"Wed\");</script></b></font>");
				if ($iDayBanner == 5) print("<font face='arial' size='+1' color='white'><b><script language = 'JavaScript'>translate(\"Thur\");</script></b></font>");
				if ($iDayBanner == 6) print("<font face='arial' size='+1' color='white'><b><script language = 'JavaScript'>translate(\"Fri\");</script></b></font>");
				if ($iDayBanner == 7) print("<font face='arial' size='+1' color='white'><b><script language = 'JavaScript'>translate(\"Sat\");</script></b></font>");
			}
			print("</TD>");
		}

		print("</TR>");

	//	Now, loop through 1 through iRows, then 1 through iColumns

		$strCalendarDate;
		$iCalendarDay;
		$dtCalendarDate;

		for ($iRowsLoop = 1; $iRowsLoop <= $iRows; $iRowsLoop++)
		{
	//		 create a new row
			print("<TR>");
			for ($iColumnsLoop = 1; $iColumnsLoop <= $iColumns; $iColumnsLoop++)
			{
	//			create a new column
	//			if there is a day there, display it, else black out the call
				if ($aCalendarDays[($iRowsLoop-1)*$iColumns + $iColumnsLoop] > 0)
				{
	//	display the date
					print("<TD BGCOLOR='WHITE' VALIGN= 'TOP' ALIGN='Left' WIDTH='14%'>");
					$iCalendarDay = $aCalendarDays[($iRowsLoop-1)*$iColumns + $iColumnsLoop];
					print("<font face='arial'>");
					print($iCalendarDay);

/* print any matching tours */

					$MonthName = getMonthName($month);
					$ShortYear = substr($year,2,2);

					if ($iCalendarDay < 10)
					{
						$query = "SELECT SCHEDULEDTOUR_CODE,TOUR_ID FROM SCHEDULEDTOUR WHERE SCHEDULEDTOUR_DEPART_DATE like '%$month/0$iCalendarDay/$ShortYear' AND SCHEDULEDTOUR_CODE like '$city%'";
					}
					else
					{
						$query = "SELECT SCHEDULEDTOUR_CODE,TOUR_ID FROM SCHEDULEDTOUR WHERE SCHEDULEDTOUR_DEPART_DATE like '%$month/$iCalendarDay/$ShortYear' AND SCHEDULEDTOUR_CODE like '$city%'";
					}

					// print($query);
					$mysql_result = mysql_query($query, $mysql_link);
					$totalnumrows = mysql_num_rows($mysql_result);

					if ($totalnumrows == 0)
					{
						print("<br><br><br>");
					}
					else
					{


						while ($row = mysql_fetch_array($mysql_result))
						{
							print("<br>");
							$ScheduledTourCode = strtoupper($row[0]);
							$TourId = strtoupper($row[1]);
							$querytype = "SELECT TOUR_NAME FROM TOUR WHERE TOUR_ID = '$TourId'";
							$mysql_result_type = mysql_query($querytype, $mysql_link);
							$rowtype = mysql_fetch_array($mysql_result_type);
							$TourName = $rowtype[0];

							// for the scheduled tour code - use the short version

							$SchedCodeLength = strlen($ScheduledTourCode);
							$ScheduledTourShortCode = substr($ScheduledTourCode,0,$SchedCodeLength - 2);


							if ($month < 10)
							{
								if ($city == "LA")
								{
									$strURL = sprintf("imgpopup.html?args=%s_%s-Los_Angeles,Tour_Name,%s0%s,%s,jpg,Jan_4th,185,%s,%s,%s,%s,%s",$MonthName,$year,$year,$month,$ScheduledTourShortCode,$languageUsed,$city,$year,$month,$day);
									print("<a href=\"$strURL\">$TourName</a><br>");
									$strImgPath = sprintf("%s0%s/%s.jpg",$year,$month,$ScheduledTourShortCode);
									if ($languageUsed == "JP")
									{
										print("<img border='0' width='40 height='60' src='http://www.california-tour.com/booking/japanlafliers/$strImgPath'>");
									}
									else
									{
										print("<img border='0' width='40 height='60' src='http://www.california-tour.com/booking/lafliers/$strImgPath'>");
									}
								}
								if ($city == "SF")
								{
									$strURL = sprintf("imgpopup.html?args=%s_%s-San_Francisco,Tour_Name,%s0%s,%s,jpg,Jan_4th,185,%s,%s,%s,%s,%s",$MonthName,$year,$year,$month,$ScheduledTourShortCode,$languageUsed,$city,$year,$month,$day);
									print("<a href=\"$strURL\">$TourName</a><br>");
									$strImgPath = sprintf("%s0%s/%s.jpg",$year,$month,$ScheduledTourShortCode);
									if ($languageUsed == "JP")
									{
										print("<img border='0' width='40 height='60' src='http://www.california-tour.com/booking/japansffliers/$strImgPath'>");
									}
									else
									{
										print("<img border='0' width='40 height='60' src='http://www.california-tour.com/booking/sffliers/$strImgPath'>");
									}

								}
								if ($city == "SD")
								{
									$strURL = sprintf("imgpopup.html?args=%s_%s-San_Diego,Tour_Name,%s0%s,%s,jpg,Jan_4th,185,%s,%s,%s,%s,%s",$MonthName,$year,$year,$month,$ScheduledTourShortCode,$languageUsed,$city,$year,$month,$day);
									print("<a href=\"$strURL\">$TourName</a><br>");
									$strImgPath = sprintf("%s0%s/%s.jpg",$year,$month,$ScheduledTourShortCode);
									if ($languageUsed == "JP")
									{
										print("<img border='0' width='40 height='60' src='http://www.california-tour.com/booking/japansdfliers/$strImgPath'>");
									}
									else
									{
										print("<img border='0' width='40 height='60' src='http://www.california-tour.com/booking/sdfliers/$strImgPath'>");
									}
								}

								if ($city == "SB")
								{
									$strURL = sprintf("imgpopup.html?args=%s_%s-Santa_Barbara,Tour_Name,%s0%s,%s,jpg,Jan_4th,185,%s,%s,%s,%s,%s",$MonthName,$year,$year,$month,$ScheduledTourShortCode,$languageUsed,$city,$year,$month,$day);
									print("<a href=\"$strURL\">$TourName</a><br>");
									$strImgPath = sprintf("%s0%s/%s.jpg",$year,$month,$ScheduledTourShortCode);
									if ($languageUsed == "JP")
									{
										print("<img border='0' width='40 height='60' src='http://www.california-tour.com/booking/japansbfliers/$strImgPath'>");
									}
									else
									{
										print("<img border='0' width='40 height='60' src='http://www.california-tour.com/booking/sffliers/$strImgPath'>");
									}
								}

							}
							else
							{
								if ($city == "LA")
								{
									$strURL = sprintf("imgpopup.html?args=%s_%s-Los_Angeles,Tour_Name,%s%s,%s,jpg,Jan_4th,185,%s,%s,%s,%s,%s",$MonthName,$year,$year,$month,$ScheduledTourShortCode,$languageUsed,$city,$year,$month,$day);
									print("<a href=\"$strURL\">$TourName</a><br>");
									$strImgPath = sprintf("%s%s/%s.jpg",$year,$month,$ScheduledTourShortCode);
									if ($languageUsed == "JP")
									{
										print("<img border='0' width='40 height='60' src='http://www.california-tour.com/booking/japanlafliers/$strImgPath'>");
									}
									else
									{
										print("<img border='0' width='40 height='60' src='http://www.california-tour.com/booking/lafliers/$strImgPath'>");
									}
								}
								if ($city == "SF")
								{
									$strURL = sprintf("imgpopup.html?args=%s_%s-San_Francisco,Tour_Name,%s%s,%s,jpg,Jan_4th,185,%s,%s,%s,%s,%s",$MonthName,$year,$year,$month,$ScheduledTourShortCode,$language,$city,$year,$month,$day);
									print("<a href=\"$strURL\">$TourName</a><br>");
									$strImgPath = sprintf("%s%s/%s.jpg",$year,$month,$ScheduledTourShortCode);
									if ($languageUsed == "JP")
									{
										print("<img border='0' width='40 height='60' src='http://www.california-tour.com/booking/japansffliers/$strImgPath'>");
									}
									else
									{
										print("<img border='0' width='40 height='60' src='http://www.california-tour.com/booking/sffliers/$strImgPath'>");
									}
								}
								if ($city == "SD")
								{
									$strURL = sprintf("imgpopup.html?args=%s_%s-San_Diego,Tour_Name,%s%s,%s,jpg,Jan_4th,185,%s,%s,%s,%s,%s",$MonthName,$year,$year,$month,$ScheduledTourShortCode,$languageUsed,$city,$year,$month,$day);
									print("<a href=\"$strURL\">$TourName</a><br>");
									$strImgPath = sprintf("%s%s/%s.jpg",$year,$month,$ScheduledTourShortCode);
									if ($languageUsed == "JP")
									{
										print("<img border='0' width='40 height='60' src='http://www.california-tour.com/booking/japansdfliers/$strImgPath'>");
									}
									else
									{
										print("<img border='0' width='40 height='60' src='http://www.california-tour.com/booking/sdfliers/$strImgPath'>");
									}
								}

								if ($city == "SB")
								{
									$strURL = sprintf("imgpopup.html?args=%s_%s-Santa_Barbara,Tour_Name,%s%s,%s,jpg,Jan_4th,185,%s,%s,%s,%s,%s",$MonthName,$year,$year,$month,$ScheduledTourShortCode,$languageUsed,$city,$year,$month,$day);
									print("<a href=\"$strURL\">$TourName</a><br>");
									$strImgPath = sprintf("%s%s/%s.jpg",$year,$month,$ScheduledTourShortCode);
									if ($languageUsed == "JP")
									{
										print("<img border='0' width='40 height='60' src='http://www.california-tour.com/booking/japansbfliers/$strImgPath'>");
									}
									else
									{
										print("<img border='0' width='40 height='60' src='http://www.california-tour.com/booking/sffliers/$strImgPath'>");
									}
								}

							}
						}
					}
					print("</font></TD>");
				}
				else
				{
	//				black out the cell
					print("<TD BGCOLOR='WHITE'></TD>");
				}
			}
	//		close the row
			print("</TR>");
		}

		print("</table><center>");
		print("<br>");


?>


</body>
</html>

