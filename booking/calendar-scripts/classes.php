<?


class SearchClass
{
	var $indexname;
	var $tablename;
	var $querystring;
	var $resultstring1;
	var $resultstring2;
	var $resultstring3;
	var $mysql_link;

	function init($tableindex,$tblname,$mysqllink)
	{
		$this->indexname = $tableindex;
		$this->tablename = $tblname;
		$this->mysql_link = $mysqllink;
		$this->resultstring1 = "";
		$this->resultstring2 = "";
		$this->resultstring3 = "";
	}

	function getresult1()
	{
		return($this->resultstring1);
	}

	function getresult2()
	{
		return($this->resultstring2);
	}

	function getresult3()
	{
		return($this->resultstring3);
	}

	function getquerystring()
	{
		return($this->querystring);
	}


	function reverse_compare($querystr)
	{
		$oldquerystr = sprintf("AND %s >=",$this->indexname);
		$replacestr = sprintf("AND %s <=",$this->indexname);
		$newquerystr = ereg_replace($oldquerystr,$replacestr, $querystr);
		return($newquerystr);
	}

	function restore_compare($querystr)
	{
		$oldquerystr = sprintf("AND %s <=",$this->indexname);
		$replacestr = sprintf("AND %s >=",$this->indexname);
		$newquerystr = ereg_replace($oldquerystr,$replacestr, $querystr);
		return($newquerystr);
	}


	function add_next_id($querystr,$oldrowid, $newrowid)
	{
		$oldquerystr = sprintf("AND %s >= '%d' ORDER BY %s ASC",$this->indexname,intval($oldrowid),$this->indexname);
		$replacestr = sprintf("AND %s >= '%d' ORDER BY %s ASC",$this->indexname,intval($newrowid),$this->indexname);
		$newquerystr = ereg_replace($oldquerystr,$replacestr, $querystr);
		//print($oldquerystr);
		//print("<br>");
		//print($replacestr);
		//print("<br>");
		//print($newquerystr);
		if ($querystr == $newquerystr)
		{
			$tempstr = sprintf("ORDER BY %s ASC",$this->indexname);
			$newquerystr = ereg_replace($tempstr, $replacestr, $querystr);
		}
		//print("<br>");
		//print($newquerystr);
		return ($newquerystr);
	}

	function add_less_id($querystr,$oldrowid,$newrowid)
	{
		$oldquerystr = sprintf("AND %s <= '%d' ORDER BY %s DESC",$this->indexname,intval($oldrowid),$this->indexname);
		$replacestr = sprintf("AND %s <= '%d' ORDER BY %s DESC",$this->indexname,intval($newrowid),$this->indexname);
		$newquerystr = ereg_replace($oldquerystr,$replacestr, $querystr);
		//print($oldquerystr);
		//print("<br>");
		//print($replacestr);
		//print("<br>");
		//print($newquerystr);
		if ($querystr == $newquerystr)
		{
			$tempstr = sprintf("ORDER BY %s DESC", $this->indexname);
			$newquerystr = ereg_replace($tempstr, $replacestr, $querystr);
		}
		//print("<br>");
		//print($newquerystr);
		return($newquerystr);
	}

	function reverse_order($querystr)
	{
		$oldquerystr = "ASC";
		$replacestr = "DESC";
		$newquerystr = ereg_replace($oldquerystr,$replacestr, $querystr);
		return($newquerystr);
	}

	function restore_order($querystr)
	{
		$oldquerystr = "DESC";
		$replacestr = "ASC";
		$newquerystr = ereg_replace($oldquerystr,$replacestr, $querystr);
		return($newquerystr);
	}


	function remove_next_id($querystr,$rowid)
	{
		if ($rowid == 0)
		{

			$oldquerystr = sprintf("AND %s >=",$this->indexname);
			$replacestr = "+";
			$tempquerystr = ereg_replace($oldquerystr,$replacestr, $querystr);
			if ($querystr != $tempquerystr)
			{
				$firstindex = strpos($tempquerystr,"+");
				//print("first index = $firstindex");
				$lastindex = strpos($tempquerystr,"ORDER BY");
				//print("last index = $lastindex");
				$newquerystr = sprintf("%s %s",substr($tempquerystr,0,$firstindex),substr($tempquerystr,$lastindex,strlen($tempquerystr)));
				//print($newquerystr);
			}
			else
			{
				$newquerystr = $querystr;
			}
		}
		else
		{
			$oldquerystr = sprintf("AND %s >= '%d' ORDER BY %s ASC",$this->indexname,intval($rowid),$this->indexname);
			$replacestr = sprintf("ORDER BY %s ASC",$this->indexname);
			$newquerystr = ereg_replace($oldquerystr,$replacestr, $querystr);
		}
		//print($oldquerystr);
		//print("<br>");
		//print($replacestr);
		//print("<br>");
		//print($newquerystr);
		return ($newquerystr);
	}

	function prev_record($querystr)
	{
		$querystring = stripslashes($querystr);
		$querystring = $this->reverse_order($querystring);
		$querystring = $this->reverse_compare($querystring);
		if (strlen($querystring) > 0)
		{
			$mysql_result = mysql_query($querystring, $this->mysql_link);
			$numrows = mysql_num_rows($mysql_result);
			if ($numrows == 1)
			{
				$row = mysql_fetch_array($mysql_result);
				$this->resultstring1 = "<b><font color='red'>First Record in Search.</font></b><br>";
				$this->resultstring2 = "<b><font color='blue'>Search returned </font><font color='red'>$numrows</font><font color='blue'> records</font></b><br>";
				$this->resultstring3 =  print_tb_size($this->tablename, $this->mysql_link);
			}
			else
			{
				if ($numrows == 0)
				{
					$this->resultstring1 = "<b><font color='red'>Database has no records</font></b>";
				}
				else
				{
					$row = mysql_fetch_array($mysql_result);
					$oldrowid = $row[0];
					$row = mysql_fetch_array($mysql_result);
					$newrowid = $row[0];
				}
			}
			if ($numrows > 0)
			{
				if ($newrowid != 0)
				{
					//print("Old row = $oldrowid, New row = $newrowid<br>");
					$newquerystr = $this->add_less_id($querystring,$oldrowid, $newrowid);
					//print($newquerystr);
					// print total number of records in search
					$startquerystr = $this->restore_order($newquerystr);
					$startquerystr = $this->restore_compare($startquerystr);
					$startquerystr = $this->remove_next_id($startquerystr, 0);
					//print($startquerystr);
					$mysql_result = mysql_query($startquerystr, $this->mysql_link);
					$numrows0 = mysql_num_rows($mysql_result);
					$recordindex = intval($numrows) - 1;
					if ($recordindex == 1)
					{
						$this->resultstring1 = sprintf("<b><font color='red'>Record %d of %d in search (First Record)</font></b><br>",$recordindex,intval($numrows0));
					}
					else
					{
						$this->resultstring1 = sprintf("<b><font color='red'>Record %d of %d in search</font></b><br>",$recordindex,intval($numrows0));
					}
					$this->resultstring2 = "<b><font color='blue'>Search returned </font><font color='red'>$numrows0</font><font color='blue'> records</font></b><br>";
					$this->resultstring3 =  print_tb_size($this->tablename, $this->mysql_link);
				}
				else
				{
					$newquerystr = $querystring;
					// print total number of records in search
					$startquerystr = $this->restore_order($newquerystr);
					$startquerystr = $this->restore_compare($startquerystr);
					$startquerystr = $this->remove_next_id($startquerystr, 0);
					//print($startquerystr);
					$mysql_result = mysql_query($startquerystr, $this->mysql_link);
					$numrows0 = mysql_num_rows($mysql_result);
					$recordindex = 1;
					if ($recordindex == 1)
					{
						$this->resultstring1 = sprintf("<b><font color='red'>Record %d of %d in search (First Record)</font></b><br>",$recordindex,intval($numrows0));
					}
					else
					{
						$this->resultstring1 = sprintf("<b><font color='red'>Record %d of %d in search</font></b><br>",$recordindex,intval($numrows0));
					}
					$this->resultstring2 = "<b><font color='blue'>Search returned </font><font color='red'>$numrows0</font><font color='blue'> records</font></b><br>";
					$this->resultstring3 =  print_tb_size($this->tablename, $this->mysql_link);
				}
			}
		}
		$this->querystring = $newquerystr;
		return ($row);
	}

	function next_record($querystr)
	{
		$querystring = stripslashes($querystr);
		$querystring = $this->restore_order($querystring);
		$querystring = $this->restore_compare($querystring);

		if (strlen($querystring) > 0)
		{
			$mysql_result = mysql_query($querystring, $this->mysql_link);
			$numrows = mysql_num_rows($mysql_result);
			if ($numrows == 1)
			{
				$row = mysql_fetch_array($mysql_result);
				$this->resultstring1 = "<b><font color='red'>Last Record in Search</font></b><br>";
				$this->resultstring2 = "<b><font color='blue'>Search returned </font><font color='red'>$numrows</font><font color='blue'> records</font></b><br>";
				$this->resultstring3 =  print_tb_size($this->tablename, $this->mysql_link);
			}
			else
			{
				if ($numrows == 0)
				{
					$this->resultstring1 = "<b><font color='red'>Database has no records</font></b>";
				}
				else
				{
					$row = mysql_fetch_array($mysql_result);
					$oldrowid = $row[0];
					$row = mysql_fetch_array($mysql_result);
					$newrowid = $row[0];
				}
			}
			if ($numrows > 0)
			{
				if ($newrowid != 0)
				{
					$newquerystr = $this->add_next_id($querystring, $oldrowid, $newrowid);
					// print total number of records in search
					$startquerystr = $this->remove_next_id($newquerystr, $newrowid);
					$mysql_result = mysql_query($startquerystr, $this->mysql_link);
					$numrows0 = mysql_num_rows($mysql_result);
					$recordindex = intval($numrows0) - intval($numrows) + 2;
					if ($recordindex == $numrows0)
					{
						$this->resultstring1 = sprintf("<b><font color='red'>Record %d of %d in search (Last Record)</font></b><br>",$recordindex,intval($numrows0));
					}
					else
					{
						$this->resultstring1 = sprintf("<b><font color='red'>Record %d of %d in search</font></b><br>",$recordindex,intval($numrows0));
					}
					$this->resultstring2 = "<b><font color='blue'>Search returned </font><font color='red'>$numrows0</font><font color='blue'> records</font></b><br>";
					$this->resultstring3 =  print_tb_size($this->tablename, $this->mysql_link);
				}
				else
				{
					$newquerystr = $querystring;
					// print total number of records in search
					$startquerystr = $this->remove_next_id($newquerystr, 0);
					$mysql_result = mysql_query($startquerystr, $this->mysql_link);
					$numrows0 = mysql_num_rows($mysql_result);
					//print($startquerystr);
					$recordindex = intval($numrows0);
					if ($recordindex == $numrows0)
					{
						$this->resultstring1 = sprintf("<b><font color='red'>Record %d of %d in search (Last Record)</font></b><br>",$recordindex,intval($numrows0));
					}
					else
					{
						$this->resultstring1 = sprintf("<b><font color='red'>Record %d of %d in search</font></b><br>",$recordindex,intval($numrows0));
					}
					$this->resultstring2 = "<b><font color='blue'>Search returned </font><font color='red'>$numrows0</font><font color='blue'> records</font></b><br>";
					$this->resultstring3 =  print_tb_size($this->tablename, $this->mysql_link);
				}
			}
		}
		$this->querystring = $newquerystr;
		return ($row);
	}

	function first_record($querystr)
	{
		$querystring = stripslashes($querystr);
		$querystring = $this->restore_order($querystring);
		$querystring = $this->restore_compare($querystring);
		$querystring = $this->remove_next_id($querystring, 0);

		if (strlen($querystring) > 0)
		{
			$mysql_result = mysql_query($querystring, $this->mysql_link);
			$numrows = mysql_num_rows($mysql_result);
			$row = mysql_fetch_array($mysql_result);
			$this->resultstring1 = "<b><font color='red'>First Record in Search.</font></b><br>";
			$this->resultstring2 = "<b><font color='blue'>Search returned </font><font color='red'>$numrows</font><font color='blue'> records</font></b><br>";
			$this->resultstring3 =  print_tb_size($this->tablename, $this->mysql_link);
		}
		$this->querystring = $querystring;
		return ($row);
	}

	function last_record($querystr)
	{
		$querystring = stripslashes($querystr);
		$querystring = $this->restore_order($querystring);
		$querystring = $this->restore_compare($querystring);
		$querystring = $this->remove_next_id($querystring, 0);
		$querystring = $this->reverse_order($querystring);
		$querystring = $this->reverse_compare($querystring);

		if (strlen($querystring) > 0)
		{
			$mysql_result = mysql_query($querystring, $this->mysql_link);
			$numrows = mysql_num_rows($mysql_result);
			$row = mysql_fetch_array($mysql_result);
			$this->resultstring1 = "<b><font color='red'>Last Record in Search.</font></b><br>";
			$this->resultstring2 = "<b><font color='blue'>Search returned </font><font color='red'>$numrows</font><font color='blue'> records</font></b><br>";
			$this->resultstring3 =  print_tb_size($this->tablename, $this->mysql_link);
		}
		$this->querystring = $querystring;
		return ($row);
	}

	function list_records($querystr)
	{
		$querystring = stripslashes($querystr);
		$querystring = $this->restore_order($querystring);
		$querystring = $this->restore_compare($querystring);
		$querystring = $this->remove_next_id($querystring, 0);
		if (strlen($querystring) > 0)
		{
			$mysql_result = mysql_query($querystring, $this->mysql_link);
			$numrows = mysql_num_rows($mysql_result);
			if ($numrows > 0)
			{
				$this->resultstring1 = "<b><font color='blue'>Search returned </font><font color='red'>$numrows</font><font color='blue'> records</font></b><br>";
				$this->resultstring2 =  print_tb_size($this->tablename, $this->mysql_link);
				$this->resultstring3 = "";
			}
		}
		$this->querystring = $querystring;
		return($mysql_result);
	}
}

class ReportSearchForm
{
	var $mainheader;
	var $printtofile;
	var $printtoscreen;
	var $search1;
	var $search2;
	var $search3;
	var $search4;
	var $search5;

	function init($searchheader,$printtoscreen,$printtofile,$headertext,$search1, $search2,$search3,$search4,$search5)
	{
		$this->printtofile 		= $printtofile;
		$this->printtoscreen 	= $printtoscreen;

		$this->searchheader 	= $searchheader;
		$this->headertext 		= $headertext;
		$this->search1 			= $search1;
		$this->search2 			= $search2;
		$this->search3 			= $search3;
		$this->search4 			= $search4;
		$this->search5 			= $search5;

	}

	function mainexitcmd()
	{
		print("<SCRIPT LANGUAGE = 'JavaScript'>\n");
		print("document.location = \"main.php?args=report,\" + strLevel + \",\" + strUserName;");
		print("</SCRIPT>\n");
	}

	function acctexitcmd()
	{
		print("<SCRIPT LANGUAGE = 'JavaScript'>\n");
		print("document.location = \"main.php?args=acct,\" + strLevel + \",\" + strUserName;");
		print("</SCRIPT>\n");
	}

	function showmain()
	{
		print("<b><center>$this->headertext</center></b>");
		print("<SCRIPT LANGUAGE = 'JavaScript'>\n");
		print("if (strLevel == \"1\")\n");
			print("{");
			print("document.write(\"<form action='$this->searchheader?args=1,\")");
			print("+");
			print("document.write(strUserName)");
			print("+");
			print("document.write(\"'  name='touredit' method='post'>\");");
			print("}");
		print("if (strLevel == \"2\")\n");
			print("{");
			print("document.write(\"<form action='$this->searchheader?args=2,\")");
			print("+");
			print("document.write(strUserName)");
			print("+");
			print("document.write(\"'  name='touredit' method='post'>\");");
			print("}");
		print("if (strLevel == \"4\")\n");
			print("{");
			print("document.write(\"<form action='$this->searchheader?args=4,\")");
			print("+");
			print("document.write(strUserName)");
			print("+");
			print("document.write(\"'  name='touredit' method='post'>\");");
			print("}");
		print("</SCRIPT>\n");
		print("");
		print("<TABLE ALIGN='CENTER' WIDTH='60%'>\n");
		if ($this->search1)
		{
			$this->search1->showitem();
		}
		if ($this->search2)
		{
			$this->search2->showitem();
		}
		if ($this->search3)
		{
			$this->search3->showitem();
		}
		if ($this->search4)
		{
			$this->search4->showitem();
		}
		if ($this->search5)
		{
			$this->search5->showitem();
		}


		print("<TR>\n");
		print("<TD ALIGN='LEFT' valign='top'><input type='submit' name='PrintScreenBtn'  value='Print To Screen'><br>");
		print("<TD ALIGN='LEFT' valign='top'><input type='submit' name='PrintFileBtn'  value='Print To File'><br>");
		print("<TD ALIGN='LEFT' valign='top'><input type='reset' name='ClearBtn'  value='Clear'><br>");
		print("<TD ALIGN='LEFT' valign='top'><input type='submit' name='ExitBtn'  value='Exit'><br>");
		print("</TR>\n");
		print("</TABLE>\n");
	}
}


class ReportTextBoxItem
{
	var $textvalue;
	var $textname;
	function init($Value,$Name)
	{
		$this->textvalue = $Value;
		$this->textname = $Name;
	}

	function showitem()
	{
		print("<TR>\n");
		print("<TD ALIGN='LEFT' valign='top'><Font color='blue'>$this->textvalue</font></TD>\n");
		print("<TD ALIGN='LEFT' valign='top'><input type='text' name='$this->textname'>\n");
		print("</TR>\n");

	}

}

class ReportRadioItem
{
	var $opt1;
	var $opt2;
	var $radioname;
	function init($Opt1text,$Opt2text,$Name)
	{
		$this->opt1 = $Opt1text;
		$this->opt2 = $Opt2text;
		$this->radioname = $Name;
	}

	function showitem()
	{
		print("<TR>\n");
		print("<TD ALIGN='LEFT' valign='top'><input type='radio' name='$this->radioname' value='$this->opt1' checked>$this->opt1\n");
		print("<TD ALIGN='LEFT' valign='top'><input type='radio' name='$this->radioname' value='$this->opt2'>$this->opt2\n");
		print("</TR>\n");

	}

}

class ReportSearchItem
{
	var $mysql_link;
	var $mainheader;
	var $printtofile;
	var $printtoscreen;
	var $searchbytext;
	var $searchindex;
	var $sortfield;
	var $searchtable;
	var $searchindexformname;

	function init($searchtable, $searchindex, $sortfield, $searchbytext,$searchindexformname,$mysqllink)
	{
		$this->mysql_link 		= $mysqllink;
		$this->searchbytext 	= $searchbytext;
		$this->searchindex 		= $searchindex;
		$this->sortfield 		= $sortfield;
		$this->searchtable 		= $searchtable;
		$this->searchindexformname	= $searchindexformname;
	}

	function showitem()
	{
		print("<TR>\n");
		print("<TD ALIGN='LEFT' valign='top'><Font color='blue'>$this->searchbytext</font></TD>\n");
		print("<TD ALIGN='LEFT' valign='top'><SELECT name = '$this->searchindexformname' size='1'>\n");

		print("<OPTION value=''>ALL</OPTION>\n");
		$query = sprintf("SELECT %s, %s FROM %s ORDER BY %s ASC",$this->searchindex,$this->sortfield,$this->searchtable,$this->sortfield);
		$mysql_result = mysql_query($query, $this->mysql_link);
		$numrows = mysql_num_rows($mysql_result);
		if ($numrows > 0)
		{
			$OldTempDisplayField = "";
			while ($row = mysql_fetch_array($mysql_result))
			{
				// get row result
				$TempIndexId = $row[0];
				$TempDisplayField = strtoupper($row[1]);
				if ($TempDisplayField <> $OldTempDisplayField)
					print("<OPTION value='$TempIndexId'>$TempDisplayField</OPTION>\n");
				$OldTempDisplayField = $TempDisplayField;
			}
		}
		print("</SELECT>\n");
		print("</TD>\n");
		print("</TR>\n");
	}

}


?>