<?


class SearchClass
{
	var $indexname;
	var $tablename;
	var $querystring;
	var $resultstring1;
	var $resultstring2;
	var $resultstring3;
	var $mysql_link;
	var $searchfield1;
	var $searchfield2;
	var $searchfield3;
	var $searchfield4;
	var $searchfield5;
	var $searchfield6;
	var $sortfield;
	var $FieldName = array( "","","","","",
							"","","","","",
							"","","","","",
							"","","","","",
							"","","","","");


	function init($tableindex,
					$tblname,
					$searchfield1,
					$searchfield2,
					$searchfield3,
					$searchfield4,
					$searchfield5,
					$searchfield6,
					$sortfield,
					$fieldname1,
					$fieldname2,
					$fieldname3,
					$fieldname4,
					$fieldname5,
					$fieldname6,
					$fieldname7,
					$fieldname8,
					$fieldname9,
					$fieldname10,
					$fieldname11,
					$fieldname12,
					$fieldname13,
					$fieldname14,
					$fieldname15,
					$fieldname16,
					$fieldname17,
					$fieldname18,
					$fieldname19,
					$fieldname20,
					$fieldname21,
					$fieldname22,
					$fieldname23,
					$fieldname24,
					$fieldname25,
					$mysqllink)
	{
		$this->indexname = $tableindex;
		$this->tablename = $tblname;
		$this->mysql_link = $mysqllink;
		$this->resultstring1 = "";
		$this->resultstring2 = "";
		$this->resultstring3 = "";
		$this->searchfield1 = $searchfield1;
		$this->searchfield2 = $searchfield2;
		$this->searchfield3 = $searchfield3;
		$this->searchfield4 = $searchfield4;
		$this->searchfield5 = $searchfield5;
		$this->searchfield6 = $searchfield6;
		$this->sortfield	= $sortfield;
		for ($index = 0;$index<25;$index++)
		{
			if ($index == 0) $this->FieldName[$index] = $fieldname1;
			if ($index == 1) $this->FieldName[$index] = $fieldname2;
			if ($index == 2) $this->FieldName[$index] = $fieldname3;
			if ($index == 3) $this->FieldName[$index] = $fieldname4;
			if ($index == 4) $this->FieldName[$index] = $fieldname5;
			if ($index == 5) $this->FieldName[$index] = $fieldname6;
			if ($index == 6) $this->FieldName[$index] = $fieldname7;
			if ($index == 7) $this->FieldName[$index] = $fieldname8;
			if ($index == 8) $this->FieldName[$index] = $fieldname9;
			if ($index == 9) $this->FieldName[$index] = $fieldname10;
			if ($index == 10) $this->FieldName[$index] = $fieldname11;
			if ($index == 11) $this->FieldName[$index] = $fieldname12;
			if ($index == 12) $this->FieldName[$index] = $fieldname13;
			if ($index == 13) $this->FieldName[$index] = $fieldname14;
			if ($index == 14) $this->FieldName[$index] = $fieldname15;
			if ($index == 15) $this->FieldName[$index] = $fieldname16;
			if ($index == 16) $this->FieldName[$index] = $fieldname17;
			if ($index == 17) $this->FieldName[$index] = $fieldname18;
			if ($index == 18) $this->FieldName[$index] = $fieldname19;
			if ($index == 19) $this->FieldName[$index] = $fieldname20;
			if ($index == 20) $this->FieldName[$index] = $fieldname21;
			if ($index == 21) $this->FieldName[$index] = $fieldname22;
			if ($index == 22) $this->FieldName[$index] = $fieldname23;
			if ($index == 23) $this->FieldName[$index] = $fieldname24;
			if ($index == 24) $this->FieldName[$index] = $fieldname25;
		}
	}

	function getresult1()
	{
		return($this->resultstring1);
	}

	function getresult2()
	{
		return($this->resultstring2);
	}

	function getresult3()
	{
		return($this->resultstring3);
	}

	function getquerystring()
	{
		return($this->querystring);
	}


	function reverse_compare($querystr)
	{
		$oldquerystr = sprintf("AND %s >=",$this->indexname);
		$replacestr = sprintf("AND %s <=",$this->indexname);
		$newquerystr = ereg_replace($oldquerystr,$replacestr, $querystr);
		return($newquerystr);
	}

	function restore_compare($querystr)
	{
		$oldquerystr = sprintf("AND %s <=",$this->indexname);
		$replacestr = sprintf("AND %s >=",$this->indexname);
		$newquerystr = ereg_replace($oldquerystr,$replacestr, $querystr);
		return($newquerystr);
	}


	function add_next_id($querystr,$oldrowid, $newrowid)
	{
		$oldquerystr = sprintf("AND %s >= '%d' ORDER BY %s ASC",$this->indexname,intval($oldrowid),$this->indexname);
		$replacestr = sprintf("AND %s >= '%d' ORDER BY %s ASC",$this->indexname,intval($newrowid),$this->indexname);
		$newquerystr = ereg_replace($oldquerystr,$replacestr, $querystr);
		//print($oldquerystr);
		//print("<br>");
		//print($replacestr);
		//print("<br>");
		//print($newquerystr);
		if ($querystr == $newquerystr)
		{
			$tempstr = sprintf("ORDER BY %s ASC",$this->indexname);
			$newquerystr = ereg_replace($tempstr, $replacestr, $querystr);
		}
		//print("<br>");
		//print($newquerystr);
		return ($newquerystr);
	}

	function add_less_id($querystr,$oldrowid,$newrowid)
	{
		$oldquerystr = sprintf("AND %s <= '%d' ORDER BY %s DESC",$this->indexname,intval($oldrowid),$this->indexname);
		$replacestr = sprintf("AND %s <= '%d' ORDER BY %s DESC",$this->indexname,intval($newrowid),$this->indexname);
		$newquerystr = ereg_replace($oldquerystr,$replacestr, $querystr);
		//print($oldquerystr);
		//print("<br>");
		//print($replacestr);
		//print("<br>");
		//print($newquerystr);
		if ($querystr == $newquerystr)
		{
			$tempstr = sprintf("ORDER BY %s DESC", $this->indexname);
			$newquerystr = ereg_replace($tempstr, $replacestr, $querystr);
		}
		//print("<br>");
		//print($newquerystr);
		return($newquerystr);
	}

	function reverse_order($querystr)
	{
		$oldquerystr = "ASC";
		$replacestr = "DESC";
		$newquerystr = ereg_replace($oldquerystr,$replacestr, $querystr);
		return($newquerystr);
	}

	function restore_order($querystr)
	{
		$oldquerystr = "DESC";
		$replacestr = "ASC";
		$newquerystr = ereg_replace($oldquerystr,$replacestr, $querystr);
		return($newquerystr);
	}


	function remove_next_id($querystr,$rowid)
	{
		if ($rowid == 0)
		{

			$oldquerystr = sprintf("AND %s >=",$this->indexname);
			$replacestr = "+";
			$tempquerystr = ereg_replace($oldquerystr,$replacestr, $querystr);
			if ($querystr != $tempquerystr)
			{
				$firstindex = strpos($tempquerystr,"+");
				//print("first index = $firstindex");
				$lastindex = strpos($tempquerystr,"ORDER BY");
				//print("last index = $lastindex");
				$newquerystr = sprintf("%s %s",substr($tempquerystr,0,$firstindex),substr($tempquerystr,$lastindex,strlen($tempquerystr)));
				//print($newquerystr);
			}
			else
			{
				$newquerystr = $querystr;
			}
		}
		else
		{
			$oldquerystr = sprintf("AND %s >= '%d' ORDER BY %s ASC",$this->indexname,intval($rowid),$this->indexname);
			$replacestr = sprintf("ORDER BY %s ASC",$this->indexname);
			$newquerystr = ereg_replace($oldquerystr,$replacestr, $querystr);
		}
		//print($oldquerystr);
		//print("<br>");
		//print($replacestr);
		//print("<br>");
		//print($newquerystr);
		return ($newquerystr);
	}

	function prev_record($querystr)
	{
		$querystring = stripslashes($querystr);
		$querystring = $this->reverse_order($querystring);
		$querystring = $this->reverse_compare($querystring);
		if (strlen($querystring) > 0)
		{
			$mysql_result = mysql_query($querystring, $this->mysql_link);
			$numrows = mysql_num_rows($mysql_result);
			if ($numrows == 1)
			{
				$row = mysql_fetch_array($mysql_result);
				$this->resultstring1 = "<b><font color='red'>First Record in Search.</font></b><br>";
				$this->resultstring2 = "<b><font color='blue'>Search returned </font><font color='red'>$numrows</font><font color='blue'> records</font></b><br>";
				$this->resultstring3 =  $this->print_tb_size($this->tablename, $this->mysql_link);
			}
			else
			{
				if ($numrows == 0)
				{
					$this->resultstring1 = "<b><font color='red'>Database has no records</font></b>";
				}
				else
				{
					$row = mysql_fetch_array($mysql_result);
					$oldrowid = $row[0];
					$row = mysql_fetch_array($mysql_result);
					$newrowid = $row[0];
				}
			}
			if ($numrows > 0)
			{
				if ($newrowid != 0)
				{
					//print("Old row = $oldrowid, New row = $newrowid<br>");

					$newquerystr = $this->add_less_id($querystring,$oldrowid, $newrowid);
					//print($newquerystr);
					// print total number of records in search
					$startquerystr = $this->restore_order($newquerystr);
					$startquerystr = $this->restore_compare($startquerystr);
					$startquerystr = $this->remove_next_id($startquerystr, 0);
					//print($startquerystr);
					$mysql_result = mysql_query($startquerystr, $this->mysql_link);
					$numrows0 = mysql_num_rows($mysql_result);
					$recordindex = intval($numrows) - 1;
					if ($recordindex == 1)
					{
						$this->resultstring1 = sprintf("<b><font color='red'>Record %d of %d in search (First Record)</font></b><br>",$recordindex,intval($numrows0));
					}
					else
					{
						$this->resultstring1 = sprintf("<b><font color='red'>Record %d of %d in search</font></b><br>",$recordindex,intval($numrows0));
					}
					$this->resultstring2 = "<b><font color='blue'>Search returned </font><font color='red'>$numrows0</font><font color='blue'> records</font></b><br>";
					$this->resultstring3 =  $this->print_tb_size();
				}
				else
				{
					$newquerystr = $querystring;
					// print total number of records in search
					$startquerystr = $this->restore_order($newquerystr);
					$startquerystr = $this->restore_compare($startquerystr);
					$startquerystr = $this->remove_next_id($startquerystr, 0);
					//print($startquerystr);
					$mysql_result = mysql_query($startquerystr, $this->mysql_link);
					$numrows0 = mysql_num_rows($mysql_result);
					$recordindex = 1;
					if ($recordindex == 1)
					{
						$this->resultstring1 = sprintf("<b><font color='red'>Record %d of %d in search (First Record)</font></b><br>",$recordindex,intval($numrows0));
					}
					else
					{
						$this->resultstring1 = sprintf("<b><font color='red'>Record %d of %d in search</font></b><br>",$recordindex,intval($numrows0));
					}
					$this->resultstring2 = "<b><font color='blue'>Search returned </font><font color='red'>$numrows0</font><font color='blue'> records</font></b><br>";
					$this->resultstring3 =  $this->print_tb_size();
				}
			}
		}
		$this->querystring = $newquerystr;
		return ($row);
	}

	function next_record($querystr)
	{
		$querystring = stripslashes($querystr);
		$querystring = $this->restore_order($querystring);
		$querystring = $this->restore_compare($querystring);

		if (strlen($querystring) > 0)
		{
			$mysql_result = mysql_query($querystring, $this->mysql_link);
			$numrows = mysql_num_rows($mysql_result);
			if ($numrows == 1)
			{
				$row = mysql_fetch_array($mysql_result);
				$this->resultstring1 = "<b><font color='red'>Last Record in Search</font></b><br>";
				$this->resultstring2 = "<b><font color='blue'>Search returned </font><font color='red'>$numrows</font><font color='blue'> records</font></b><br>";
				$this->resultstring3 =  $this->print_tb_size();
			}
			else
			{
				if ($numrows == 0)
				{
					$this->resultstring1 = "<b><font color='red'>Database has no records</font></b>";
				}
				else
				{
					$row = mysql_fetch_array($mysql_result);
					$oldrowid = $row[0];
					$row = mysql_fetch_array($mysql_result);
					$newrowid = $row[0];
				}
			}
			if ($numrows > 0)
			{
				if ($newrowid != 0)
				{
					$newquerystr = $this->add_next_id($querystring, $oldrowid, $newrowid);
					// print total number of records in search
					$startquerystr = $this->remove_next_id($newquerystr, $newrowid);
					$mysql_result = mysql_query($startquerystr, $this->mysql_link);
					$numrows0 = mysql_num_rows($mysql_result);
					$recordindex = intval($numrows0) - intval($numrows) + 2;
					if ($recordindex == $numrows0)
					{
						$this->resultstring1 = sprintf("<b><font color='red'>Record %d of %d in search (Last Record)</font></b><br>",$recordindex,intval($numrows0));
					}
					else
					{
						$this->resultstring1 = sprintf("<b><font color='red'>Record %d of %d in search</font></b><br>",$recordindex,intval($numrows0));
					}
					$this->resultstring2 = "<b><font color='blue'>Search returned </font><font color='red'>$numrows0</font><font color='blue'> records</font></b><br>";
					$this->resultstring3 =  $this->print_tb_size();
				}
				else
				{
					$newquerystr = $querystring;
					// print total number of records in search
					$startquerystr = $this->remove_next_id($newquerystr, 0);
					$mysql_result = mysql_query($startquerystr, $this->mysql_link);
					$numrows0 = mysql_num_rows($mysql_result);
					//print($startquerystr);
					$recordindex = intval($numrows0);
					if ($recordindex == $numrows0)
					{
						$this->resultstring1 = sprintf("<b><font color='red'>Record %d of %d in search (Last Record)</font></b><br>",$recordindex,intval($numrows0));
					}
					else
					{
						$this->resultstring1 = sprintf("<b><font color='red'>Record %d of %d in search</font></b><br>",$recordindex,intval($numrows0));
					}
					$this->resultstring2 = "<b><font color='blue'>Search returned </font><font color='red'>$numrows0</font><font color='blue'> records</font></b><br>";
					$this->resultstring3 =  $this->print_tb_size();
				}
			}
		}
		$this->querystring = $newquerystr;
		return ($row);
	}

	function first_record($querystr)
	{
		$querystring = stripslashes($querystr);
		$querystring = $this->restore_order($querystring);
		$querystring = $this->restore_compare($querystring);

		$querystring = $this->remove_next_id($querystring, 0);

		if (strlen($querystring) > 0)
		{
			$mysql_result = mysql_query($querystring, $this->mysql_link);
			$numrows = mysql_num_rows($mysql_result);
			$row = mysql_fetch_array($mysql_result);
			$this->resultstring1 = "<b><font color='red'>First Record in Search.</font></b><br>";
			$this->resultstring2 = "<b><font color='blue'>Search returned </font><font color='red'>$numrows</font><font color='blue'> records</font></b><br>";
			$this->resultstring3 =  $this->print_tb_size();
		}
		$this->querystring = $querystring;
		return ($row);
	}

	function last_record($querystr)
	{
		$querystring = stripslashes($querystr);
		$querystring = $this->restore_order($querystring);
		$querystring = $this->restore_compare($querystring);
		$querystring = $this->remove_next_id($querystring, 0);
		$querystring = $this->reverse_order($querystring);
		$querystring = $this->reverse_compare($querystring);

		if (strlen($querystring) > 0)
		{
			$mysql_result = mysql_query($querystring, $this->mysql_link);
			$numrows = mysql_num_rows($mysql_result);
			$row = mysql_fetch_array($mysql_result);
			$this->resultstring1 = "<b><font color='red'>Last Record in Search.</font></b><br>";
			$this->resultstring2 = "<b><font color='blue'>Search returned </font><font color='red'>$numrows</font><font color='blue'> records</font></b><br>";
			$this->resultstring3 =  $this->print_tb_size();
		}
		$this->querystring = $querystring;
		return ($row);
	}

	function list_records($querystr)
	{
		$mysql_result = 0;
		$querystring = stripslashes($querystr);
		$querystring = $this->restore_order($querystring);
		$querystring = $this->restore_compare($querystring);
		$querystring = $this->remove_next_id($querystring, 0);
		if (strlen($querystring) > 0)
		{
			$mysql_result = mysql_query($querystring, $this->mysql_link);
			$numrows = mysql_num_rows($mysql_result);
			if ($numrows > 0)
			{
				$this->resultstring1 = "<b><font color='blue'>Search returned </font><font color='red'>$numrows</font><font color='blue'> records</font></b><br>";
				$this->resultstring2 =  $this->print_tb_size();
				$this->resultstring3 = "";
			}
		}
		$this->querystring = $querystring;
		return($mysql_result);
	}

	function get_record($SearchFieldValue1,
						$SearchFieldValue2,
						$SearchFieldValue3,
						$SearchFieldValue4,
						$SearchFieldValue5,
						$SearchFieldValue6,
						$mysql_link)
	{

		$query = $this->get_query(	$SearchFieldValue1,
									$SearchFieldValue2,
									$SearchFieldValue3,
									$SearchFieldValue4,
									$SearchFieldValue5,
									$SearchFieldValue6,


									$mysql_link);

		//print($query);

		if ($this->mysql_link != 0)
		{
			$mysql_result = mysql_query($query, $this->mysql_link);
			if ($mysql_result)
			{
				$numrows = mysql_num_rows($mysql_result);
				if ($numrows > 0)
				{
					$row = mysql_fetch_array($mysql_result);
				}
				else
				{
					$row[0] = 0;
				}
			}
			else
			{
				$row[0] = 0;
			}
		}
		else
		{
			$row[0] = 0;
		}
		return ($row);
	}

	function get_query(		$SearchFieldValue1,
							$SearchFieldValue2,
							$SearchFieldValue3,
							$SearchFieldValue4,
							$SearchFieldValue5,
							$SearchFieldValue6,
							$mysql_link)
	{
		$index = 0;
		if ($SearchFieldValue1)
		{
			$where1 = sprintf("%s = '$SearchFieldValue1'", $this->searchfield1);
			$WhereArray[$index] = $where1;
			$index = $index + 1;
		}
		if ($SearchFieldValue2)
		{
			$where2 = sprintf("%s = '$SearchFieldValue2'", $this->searchfield2);
			$WhereArray[$index] = $where2;
			$index = $index + 1;
		}
		if ($SearchFieldValue3)
		{
			$where3 = sprintf("%s = '$SearchFieldValue3'", $this->searchfield3);
			$WhereArray[$index] = $where3;
			$index = $index + 1;
		}
		if ($SearchFieldValue4)
		{
			$where4 = sprintf("%s = '$SearchFieldValue4'", $this->searchfield4);
			$WhereArray[$index] = $where4;
			$index = $index + 1;
		}
		if ($SearchFieldValue5)
		{
			$where5 = sprintf("%s = '$SearchFieldValue5'", $this->searchfield5);
			$WhereArray[$index] = $where5;
			$index = $index + 1;
		}
		if ($SearchFieldValue6)
		{
			$where6 = sprintf("%s = '$SearchFieldValue6'", $this->searchfield6);
			$WhereArray[$index] = $where6;
			$index = $index + 1;
		}

		$indexlimit = $index;

		if ($indexlimit == 1)
		{
			$query = sprintf("SELECT * FROM %s WHERE %s ORDER BY %s ASC",$this->tablename, $WhereArray[0], $this->indexname);
		}
		if ($indexlimit == 2)
		{
			$query = sprintf("SELECT * FROM %s WHERE %s AND %s ORDER BY %s ASC",$this->tablename,$WhereArray[0],$WhereArray[1],$this->indexname);
		}
		if ($indexlimit == 3)
		{
			$query = sprintf("SELECT * FROM %s WHERE %s AND %s AND %s ORDER BY %s ASC",$this->tablename,$WhereArray[0],$WhereArray[1],$WhereArray[2],$this->indexname);
		}
		if ($indexlimit == 4)
		{
			$query = sprintf("SELECT * FROM %s WHERE %s AND %s AND %s AND %s ORDER BY %s ASC",$this->tablename,$WhereArray[0],$WhereArray[1],$WhereArray[2],$WhereArray[3],$this->indexname);
		}
		if ($indexlimit == 5)
		{
			$query = sprintf("SELECT * FROM %s WHERE %s AND %s AND %s AND %s AND %s ORDER BY %s ASC",$this->tablename,$WhereArray[0],$WhereArray[1],$WhereArray[2],$WhereArray[3],$WhereArray[4],$this->indexname);
		}
		if ($indexlimit == 6)
		{
			$query = sprintf("SELECT * FROM %s WHERE %s AND %s AND %s AND %s AND %s AND %s ORDER BY %s ASC",$this->tablename,$WhereArray[0],$WhereArray[1],$WhereArray[2],$WhereArray[3],$WhereArray[4],$WhereArray[5],$this->indexname);
		}

		return ($query);
	}

	function get_new_id()
	{
		$newrowid = 1;
		$query = sprintf("SELECT * FROM %s ORDER BY %s ASC", $this->tablename,$this->indexname);
		$mysql_result = mysql_query($query, $this->mysql_link);
		$numrows = mysql_num_rows($mysql_result);
		if ($numrows > 0)
		{
			while ($row = mysql_fetch_array($mysql_result))
			{
				$rowid = intval($row[0]);
				if ($rowid == $newrowid)
				{
					$newrowid++;
				}
			}
		}
		return ($newrowid);
	}

	function insert_new_record(	$SearchField1,
								$SearchField2,
								$SearchField3,
								$SearchField4,
								$SearchField5,
								$SearchField6,
								$FieldValue1,
								$FieldValue2,
								$FieldValue3,
								$FieldValue4,
								$FieldValue5,
								$FieldValue6,
								$FieldValue7,
								$FieldValue8,
								$FieldValue9,
								$FieldValue10,
								$FieldValue11,
								$FieldValue12,
								$FieldValue13,
								$FieldValue14,
								$FieldValue15,
								$FieldValue16,
								$FieldValue17,
								$FieldValue18,
								$FieldValue19,
								$FieldValue20,
								$FieldValue21,
								$FieldValue22,
								$FieldValue23,
								$FieldValue24,
								$FieldValue25,
							   	$mysql_link)
	{
		$mysql_result = 0;
		$row = $this->get_record(	$SearchField1,
									$SearchField1,
									$SearchField3,
									$SearchField4,
									$SearchField5,
									$SearchField6,
								   	$mysql_link);

		if ($row[0] == 0)
		{

			$fieldinserstr = "";
			$fieldvaluestr = "";

			for ($index=0;$index<25;$index++)
			{
				//print($this->FieldName[$index]);
				//print("<br>\n");
				if ($this->FieldName[$index])
				{
					if ($index == 0)
					{
						$fieldinsertstr = sprintf("%s",$this->FieldName[$index]);
					}
					else
					{
						$fieldinsertstr = sprintf("%s,%s",$fieldinsertstr,$this->FieldName[$index]);
					}
					if ($index == 0) $fieldvaluestr = sprintf("'%s'", $FieldValue1);
					if ($index == 1) $fieldvaluestr = sprintf("%s,'%s'", $fieldvaluestr, $FieldValue2);
					if ($index == 2) $fieldvaluestr = sprintf("%s,'%s'", $fieldvaluestr, $FieldValue3);
					if ($index == 3) $fieldvaluestr = sprintf("%s,'%s'", $fieldvaluestr, $FieldValue4);
					if ($index == 4) $fieldvaluestr = sprintf("%s,'%s'", $fieldvaluestr, $FieldValue5);
					if ($index == 5) $fieldvaluestr = sprintf("%s,'%s'", $fieldvaluestr, $FieldValue6);
					if ($index == 6) $fieldvaluestr = sprintf("%s,'%s'", $fieldvaluestr, $FieldValue7);
					if ($index == 7) $fieldvaluestr = sprintf("%s,'%s'", $fieldvaluestr, $FieldValue8);
					if ($index == 8) $fieldvaluestr = sprintf("%s,'%s'", $fieldvaluestr, $FieldValue9);
					if ($index == 9) $fieldvaluestr = sprintf("%s,'%s'", $fieldvaluestr, $FieldValue10);
					if ($index == 10) $fieldvaluestr = sprintf("%s,'%s'", $fieldvaluestr, $FieldValue11);
					if ($index == 11) $fieldvaluestr = sprintf("%s,'%s'", $fieldvaluestr, $FieldValue12);
					if ($index == 12) $fieldvaluestr = sprintf("%s,'%s'", $fieldvaluestr, $FieldValue13);
					if ($index == 13) $fieldvaluestr = sprintf("%s,'%s'", $fieldvaluestr, $FieldValue14);
					if ($index == 14) $fieldvaluestr = sprintf("%s,'%s'", $fieldvaluestr, $FieldValue15);
					if ($index == 15) $fieldvaluestr = sprintf("%s,'%s'", $fieldvaluestr, $FieldValue16);
					if ($index == 16) $fieldvaluestr = sprintf("%s,'%s'", $fieldvaluestr, $FieldValue17);
					if ($index == 17) $fieldvaluestr = sprintf("%s,'%s'", $fieldvaluestr, $FieldValue18);
					if ($index == 18) $fieldvaluestr = sprintf("%s,'%s'", $fieldvaluestr, $FieldValue19);
					if ($index == 19) $fieldvaluestr = sprintf("%s,'%s'", $fieldvaluestr, $FieldValue20);
					if ($index == 20) $fieldvaluestr = sprintf("%s,'%s'", $fieldvaluestr, $FieldValue21);
					if ($index == 21) $fieldvaluestr = sprintf("%s,'%s'", $fieldvaluestr, $FieldValue22);
					if ($index == 22) $fieldvaluestr = sprintf("%s,'%s'", $fieldvaluestr, $FieldValue23);
					if ($index == 23) $fieldvaluestr = sprintf("%s,'%s'", $fieldvaluestr, $FieldValue24);
					if ($index == 24) $fieldvaluestr = sprintf("%s,'%s'", $fieldvaluestr, $FieldValue25);
//					print($fieldvaluestr);
//					print("<br>\n");
				}
			}

			$query = sprintf("INSERT INTO %s (%s) VALUES (%s)",$this->tablename,$fieldinsertstr,$fieldvaluestr);

			//print($query);


			$mysql_result = mysql_query($query, $mysql_link); //or die("problem: " . mysql_error());
			//print("result = $mysql_result");
		}
		return ($mysql_result);
	}

	function print_tb_size()
	{
		$query = sprintf("SELECT * FROM %s",$this->tablename);
		$mysql_result = mysql_query($query, $this->mysql_link);

		$totalnumrows = mysql_num_rows($mysql_result);

		if ($totalnumrows > 0)
		{
			if ($totalnumrows == 1)
			{
				$ResultString = "<b><font color='blue'>Database has </font><font color='red'> $totalnumrows </font><font color='blue'> record</font></b>";
			}
			else
			{
				$ResultString = "<b><font color='blue'>Database has </font><font color='red'> $totalnumrows </font><font color='blue'> records</font></b>";
			}

		}
		else
		{
			$ResultString = "<b><font color='red'>Database has no records</font></b>";
		}

		return($ResultString);
	}

}


class ReportSearchForm
{
	var $mainheader;
	var $printtofile;
	var $printtoscreen;
	var $search1;
	var $search2;
	var $search3;
	var $search4;
	var $search5;

	function init($searchheader,$printtoscreen,$printtofile,$headertext,$search1, $search2,$search3,$search4,$search5)
	{
		$this->printtofile 		= $printtofile;
		$this->printtoscreen 	= $printtoscreen;
		$this->searchheader 	= $searchheader;
		$this->headertext 		= $headertext;
		$this->search1 			= $search1;
		$this->search2 			= $search2;
		$this->search3 			= $search3;
		$this->search4 			= $search4;
		$this->search5 			= $search5;

	}

	function exitcmd()
	{
		print("<SCRIPT LANGUAGE = 'JavaScript'>\n");
		print("if (strLevel == \"1\")\n");
		print("document.location='main.php?args=report,1';\n");
		print("if (strLevel == \"2\")\n");
		print("document.location='main.php?args=report,2';\n");
		print("if (strLevel == \"4\")\n");
		print("document.location='main.php?args=report,4';\n");
		print("</SCRIPT>\n");
	}

	function showmain()
	{
		print("<b><center>$this->headertext</center></b>");
		print("<SCRIPT LANGUAGE = 'JavaScript'>\n");
		print("if (strLevel == \"1\")\n");
		print("document.write(\"<form action='$this->searchheader?args=1' name='touredit' method='post'>\");");
		print("if (strLevel == \"2\")\n");
		print("document.write(\"<form action='$this->searchheader?args=2' name='touredit' method='post'>\");");
		print("if (strLevel == \"4\")\n");
		print("document.write(\"<form action='$this->searchheader?args=4' name='touredit' method='post'>\");");
		print("</SCRIPT>\n");
		print("");
		print("<TABLE ALIGN='CENTER' WIDTH='60%'>\n");
		if ($this->search1)
		{
			$this->search1->showitem();
		}
		if ($this->search2)
		{
			$this->search2->showitem();
		}
		if ($this->search3)
		{
			$this->search3->showitem();
		}
		if ($this->search4)
		{
			$this->search4->showitem();
		}
		if ($this->search5)
		{
			$this->search5->showitem();
		}


		print("<TR>\n");
		print("<TD ALIGN='LEFT' valign='top'><input type='submit' name='PrintScreenBtn'  value='Print To Screen'><br>");
		print("<TD ALIGN='LEFT' valign='top'><input type='submit' name='PrintFileBtn'  value='Print To File'><br>");
		print("<TD ALIGN='LEFT' valign='top'><input type='reset' name='ClearBtn'  value='Clear'><br>");
		print("<TD ALIGN='LEFT' valign='top'><input type='submit' name='ExitBtn'  value='Exit'><br>");
		print("</TR>\n");
		print("</TABLE>\n");
	}
}


class ReportRadioItem
{
	var $opt1;
	var $opt2;
	var $radioname;
	function init($Opt1text,$Opt2text,$Name)
	{
		$this->opt1 = $Opt1text;
		$this->opt2 = $Opt2text;
		$this->radioname = $Name;
	}

	function showitem()
	{
		print("<TR>\n");
		print("<TD ALIGN='LEFT' valign='top'><input type='radio' name='$this->radioname' value='$this->opt1' checked>$this->opt1\n");
		print("<TD ALIGN='LEFT' valign='top'><input type='radio' name='$this->radioname' value='$this->opt2'>$this->opt2\n");
		print("</TR>\n");

	}

}

class ReportSearchItem
{
	var $mysql_link;
	var $mainheader;
	var $printtofile;
	var $printtoscreen;
	var $searchbytext;
	var $searchindex;
	var $sortfield;
	var $searchtable;
	var $searchindexformname;

	function init($searchtable, $searchindex, $sortfield, $searchbytext,$searchindexformname,$mysqllink)
	{
		$this->mysql_link 		= $mysqllink;
		$this->searchbytext 	= $searchbytext;
		$this->searchindex 		= $searchindex;
		$this->sortfield 		= $sortfield;
		$this->searchtable 		= $searchtable;
		$this->searchindexformname	= $searchindexformname;
	}

	function showitem()
	{
		print("<TR>\n");
		print("<TD ALIGN='LEFT' valign='top'><Font color='blue'>$this->searchbytext</font></TD>\n");
		print("<TD ALIGN='LEFT' valign='top'><SELECT name = '$this->searchindexformname' size='1'>\n");

		print("<OPTION value=''>ALL</OPTION>\n");
		$query = sprintf("SELECT %s,%s FROM %s ORDER BY %s ASC",$this->searchindex,$this->sortfield,$this->searchtable,$this->sortfield);
		$mysql_result = mysql_query($query, $this->mysql_link);
		$numrows = mysql_num_rows($mysql_result);
		if ($numrows > 0)
		{
			while ($row = mysql_fetch_array($mysql_result))
			{
				// get row result
				$TempIndexId = $row[0];
				$TempDisplayField = strtoupper($row[1]);
				print("<OPTION value='$TempIndexId'>$TempDisplayField</OPTION>\n");
			}
		}
		print("</SELECT>\n");
		print("</TD>\n");
		print("</TR>\n");
	}

}


?>