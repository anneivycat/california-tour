<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the id=main div and all content
 * after.  Calls sidebar-footer.php for bottom widgets.
 *
 * @package WordPress
 * @subpackage Twenty_Ten
 * @since Twenty Ten 1.0
 */
?>

	<div id="footer" role="contentinfo">
		
<div style='clear:both;'></div>

<?php
	/* A sidebar in the footer? Yep. You can can customize
	 * your footer with four columns of widgets.
	 */
	get_sidebar( 'footer' );
?>

<div style='clear:both;'></div>

		<div id='very_bottom_left' style='display:inline-block;position:relative;margin-top:-15px;float:left;width:350px;margin-left:0px;padding:20px 0px 5px 10px ;height:20px;border-right:solid 1px #797878'>
			<p id='copyright'>&copy;2012 California Tours All rights reserved. California Seller of Travel #2065568&minus;40</p>
		</div>
		<div id="footer-very_bottom_right" role="complementary">
				<div id="very_bottom" class="widget-area">
					<ul class="xoxo">
						<li id="nav_menu-3" class="widget-container widget_nav_menu"><div class="menu-foot-navigation-container"><ul id="menu-foot-navigation" class="menu">
						<li id="menu-item-73" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-73"><a title="About Us" href="http://www.california-tour.com/about-us/">About Us</a></li>
						<li id="menu-item-3078" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-3078"><a title="Policy" href="http://privacy-policy.truste.com/verified-policy/www.california-tour.com">Policy</a></li>
						<li id="menu-item-74" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-74"><a title="Contact Us" href="http://www.california-tour.com/contact-us/">Contact Us</a></li>
						<li id="menu-item-4381" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-4381"><a title="blog" href="http://www.california-tour.com/blog">Blog</a></li>
					</ul>

				</div><!-- #very_bottom .widget-area -->
		</div><!-- #footer-very_bottom_right .widget-area -->
		<div style='clear:both;'></div>
	</div><!-- #footer -->


<div style='clear:both;'></div>
	<ul id='bottom_logos'>
	<li><img src="/wp-content/themes/catours/images/foot-logo-TIA.gif" style='width:52px;height:24px' alt='TIA'></li>
	<li><img src="/wp-content/themes/catours/images/foot-logo-OiSF.gif" style='width:93px;height:23px' alt='Only in San Francisco'></li>
	<li><img src="/wp-content/themes/catours/images/foot-logo-LA.gif" style='width:75px;height:24px' alt='Los Angeles'></li>
	<li><img src="/wp-content/themes/catours/images/foot-logo-asta.gif" style='width:48px;height:24px' alt='ASTA'></li>
	<li><img src="/wp-content/themes/catours/images/foot-logo-etrust.gif" style='width:22px;height:26px' alt='eTrust'></li>
	<li><img src="/wp-content/themes/catours/images/foot-logo-ECMTA.gif" style='width:70px;height:28px' alt='ECMTA'></li>
</ul>

</div><!-- #wrapper -->
<div id='page_bottom'>
</div>
<?php
	/* Always have wp_footer() just before the closing </body>
	 * tag of your theme, or you will break many plugins, which
	 * generally use this hook to reference JavaScript files.
	 */

	wp_footer();
?>
<script type="text/javascript">
var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
</script>
<script type="text/javascript">
var pageTracker = _gat._getTracker("UA-4235877-1");
pageTracker._initData();
pageTracker._trackPageview();
</script>
</body>
</html>


