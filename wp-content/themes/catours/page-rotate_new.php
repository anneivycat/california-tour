<?php
/**
 * Template Name: New Home page w/ rotating photos
 *
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package WordPress
 * @subpackage Twenty_Ten
 * @since Twenty Ten 1.0
 */

get_header("new_home"); ?>
<body <?php body_class(); ?>>
<div id="wrapper">
	<div id="header">
		<a href='/'><img src='/wp-content/themes/catours/images/ca-tours-logo-lrger.gif' id='logo'></a>
		
		<div id='head_box'>
			<div id='top_links'><?php languages_list_footer() ?> <a href='/book-a-tour-from-los-angeles-san-francisco-san-diego-or-boston/' title='Book a tour from Los Angeles, San Francisco, San Diego, or Boston' id='make_reservation'>MAKE A RESERVATION</a></div>
		</div>
		
		<div id="access" role="navigation">
		  <?php /*  Allow screen readers / text browsers to skip the navigation menu and get right to the good stuff */ ?>
			<div class="skip-link screen-reader-text"><a href="#content" title="<?php esc_attr_e( 'Skip to content', 'twentyten' ); ?>"><?php _e( 'Skip to content', 'twentyten' ); ?></a></div>
			<?php /* Our navigation menu.  If one isn't filled out, wp_nav_menu falls back to wp_page_menu.  The menu assiged to the primary position is the one used.  If none is assigned, the menu with the lowest ID is used.  */ ?>
			<?php wp_nav_menu( array( 'container_class' => 'menu-header', 'theme_location' => 'primary' ) ); ?>
		</div><!-- #access -->
		
	</div><!-- #header -->

	<div id='page_photo_home_rotate' style='background-image:none;'>
		<script type="text/javascript">

document.write("<img id='slide_1' src='/wp-content/themes/catours/images/home_images/home2.jpg' alt='California Tours Vacation Packages'><br/>");
document.write("<img id='slide_2' src='/wp-content/themes/catours/images/home_images/home1.jpg' alt='California Tours Vacation Packages'>");

</script>
<noscript>
  <img src="/wp-content/themes/catours/images/home_images/home2.jpg" width="900" height="500" id='fade_back'>
</noscript>
	</div> <!-- page_photo_home -->
	<script language='javascript'>
	change_img();
	</script>
	<div id='home_content'>
		</div> <!-- home_content -->
<?php get_footer('new_home'); ?>
