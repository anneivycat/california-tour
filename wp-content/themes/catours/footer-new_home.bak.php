<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the id=main div and all content
 * after.  Calls sidebar-footer.php for bottom widgets.
 *
 * @package WordPress
 * @subpackage Twenty_Ten
 * @since Twenty Ten 1.0
 */
?>

	<div id="footer" role="contentinfo">
		
		<div id='home_foot_col_left' style='display:block;position:relative;margin-top:20px;float:left;width:350px;margin-left:10px;padding-left:10px;height:140px;'>
		
			<?php
			/* Run the loop to output the page.
			 * If you want to overload this in a child theme then include a file
			 * called loop-page.php and that will be used instead.
			 */
			get_template_part( 'loop', 'page' );
			?>

		</div>
		<div id='home_foot_col_center' style='display:block;position:relative;margin-top:20px;width:310px;min-height:150px;float:left;border-left:solid 1px #797878;'>	<div id="footer-widget-area" role="complementary">

				<div id="first" class="widget-area">
					<ul class="xoxo">
						<li id="nav_menu-8" class="widget-container widget_nav_menu"><div class="menu-bottom-navigation-new-container"><ul id="menu-bottom-navigation-new" class="menu"><li id="menu-item-8337" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-8337"><a title="Destination Management Company" href="http://www.california-tour.com/usa_holiday/usa-destination-management-company/">Destination Management Company</a></li>
<li id="menu-item-8338" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-8338"><a title="Group Travel Private Tours" href="/group-travel-private-tours/">Group Travel Private Tours</a></li>
<li id="menu-item-8339" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-8339"><a title="Corporate Incentive Travel" href="/corporate-incentive-travel/">Corporate Incentive Travel</a></li>
<li id="menu-item-8340" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-8340"><a title="Educational Tours" href="/educational-tours/">Educational Tours</a></li>
<li id="menu-item-8341" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-8341"><a title="Holiday Travels" href="/holiday-tours/">Holiday Travels</a></li>
</ul></div></li>					</ul>
				</div><!-- #first .widget-area -->
				</div>
		</div>
		<div id='home_foot_col_right' style='display:block;position:relative;float:right;margin-top:20px;width:300px;padding-right:10px;height:180px;background-image:url(/wp-content/themes/catours/images/home-bottom-splash.gif);background-repeat:no-repeat;background-position:bottom right;'>
		

<form role="search" method="get" id="searchform" action="/">
				<input type='text' name='s' id='search_field' value='Search' onfocus="if(this.value=='Search'){this.value='';}" onblur="if(this.value==''){this.value='Search';}">
				<input type='image' src='/wp-content/themes/catours/images/search-go-new.gif' onmouseover="this.src='/wp-content/themes/catours/images/search-go-new-on.gif'" onmouseout="this.src='/wp-content/themes/catours/images/search-go-new.gif'" id='searchsubmit'>
			</form>
			<ul id='soc_icons'>
			<li><a title="YouTube" href="http://www.youtube.com/user/CaliforniaToursInc"><img src='/wp-content/themes/catours/images/youtube-icon.gif' /></a></li>
<li id="menu-item-88" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-88"><a title="facebook" href="http://facebook.com/CaliforniaToursInc"><img src='/wp-content/themes/catours/images/facebook-icon.gif' /></a></li>
			</ul>
			
		</div> <!-- home_foot_col_right -->
		
		<div id='home_foot_bot_col_left' style='display:block;position:relative;margin-top:0px;float:left;width:360px;margin-left:0px;padding-left:10px;padding-bottom:5px;height:20px;'>
		<p id='copyright'>&copy;2012 California Tours All rights reserved. California Seller of Travel #2065568&minus;40</p>
		</div>
		<div id='home_foot_bot_col_center' style='display:block;position:relative;margin-top:0px;width:310px;height:20px;float:left;border-left:solid 1px #797878;border:'>	<div id="footer-widget-area" role="complementary">
				<div id="second" class="widget-area">
				<ul class="xoxo">
						<li id="nav_menu-3" class="widget-container widget_nav_menu"><div class="menu-foot-navigation-container"><ul id="menu-foot-navigation" class="menu">
<li id="menu-item-4381" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-4381"><a title="blog" href="http://www.california-tour.com/blog">Blog</a></li>
<li id="menu-item-71" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-71"><a title="Site Map" href="http://www.california-tour.com/site-map/">Site Map</a></li>
<li id="menu-item-3078" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-3078"><a title="Policy" href="http://privacy-policy.truste.com/verified-policy/www.california-tour.com">Policy</a></li>
<li id="menu-item-73" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-73"><a title="About Us" href="http://www.california-tour.com/about-us/">About Us</a></li>
<li id="menu-item-74" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-74"><a title="Contact Us" href="http://www.california-tour.com/contact-us/">Contact Us</a></li>
</ul></div></li>					</ul>
				
				
				</div><!-- #second .widget-area -->
				</div>
		</div>
<?php
	/* A sidebar in the footer? Yep. You can can customize
	 * your footer with four columns of widgets.
	 */
?>

	</div><!-- #footer -->

<div class='clear'></div>

</div><!-- #wrapper -->
<div id='page_bottom'>
</div>
<?php
	/* Always have wp_footer() just before the closing </body>
	 * tag of your theme, or you will break many plugins, which
	 * generally use this hook to reference JavaScript files.
	 */

	wp_footer();
?>
<script type="text/javascript">
var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
</script>
<script type="text/javascript">
var pageTracker = _gat._getTracker("UA-4235877-1");
pageTracker._initData();
pageTracker._trackPageview();
</script>
</body>
</html>


