<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the id=main div and all content
 * after.  Calls sidebar-footer.php for bottom widgets.
 *
 * @package WordPress
 * @subpackage Twenty_Ten
 * @since Twenty Ten 1.0
 */
?>

	<div id="footer" role="contentinfo">

<?php
	/* A sidebar in the footer? Yep. You can can customize
	 * your footer with four columns of widgets.
	 */
	get_sidebar( 'footer' );
?>

	</div><!-- #footer -->
<ul id='bottom_logos'>
	<li><img src="/wp-content/themes/catours/images/foot-logo-TIA.gif" style='width:52px;height:24px' alt='TIA'></li>
	<li><img src="/wp-content/themes/catours/images/foot-logo-OiSF.gif" style='width:93px;height:23px' alt='Only in San Francisco'></li>
	<li><img src="/wp-content/themes/catours/images/foot-logo-LA.gif" style='width:75px;height:24px' alt='Los Angeles'></li>
	<li><img src="/wp-content/themes/catours/images/foot-logo-asta.gif" style='width:48px;height:24px' alt='ASTA'></li>
	<li><img src="/wp-content/themes/catours/images/foot-logo-etrust.gif" style='width:22px;height:26px' alt='eTrust'></li>
	<li><img src="/wp-content/themes/catours/images/foot-logo-ECMTA.gif" style='width:70px;height:28px' alt='ECMTA'></li>
</ul>

<div class='clear'></div>

</div><!-- #wrapper -->
<div id='page_bottom'>
</div>
<div id='site_credits'>

&copy; 2013 California Tours. All rights reserved. California Seller of Travel #2065568-40</div>
<?php
	/* Always have wp_footer() just before the closing </body>
	 * tag of your theme, or you will break many plugins, which
	 * generally use this hook to reference JavaScript files.
	 */

	wp_footer();
?>
<script type="text/javascript">
var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
</script>
<script type="text/javascript">
var pageTracker = _gat._getTracker("UA-4235877-1");
pageTracker._initData();
pageTracker._trackPageview();
</script>
</body>
</html>
