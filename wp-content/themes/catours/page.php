<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package WordPress
 * @subpackage Twenty_Ten
 * @since Twenty Ten 1.0
 */

get_header(); ?>

	<div id='page_photo'>
	
	<?php
                    // Check if this is a post or page, if it has a thumbnail, and if it's a big one
                    
                    $head_img_alt = str_replace("'","\\'",$GLOBALS['post']->post_title);
                    
                    if ( is_singular() && current_theme_supports( 'post-thumbnails' ) &&
                            has_post_thumbnail( $post->ID ) &&
                            ( /* $src, $width, $height */ $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'post-file' ) )) :
                        // Houston, we have a new header image!
                        
                        echo "<img src='".$image[0]."' width='1000' alt='$head_img_alt' />";
                    else : 
                    	$head_dir = $_SERVER["DOCUMENT_ROOT"]."/wp-content/uploads/header_images/";
                    	$num_imgs = count(scandir($head_dir)) - 2;
                    	
                    	$this_img = rand(1,$num_imgs).".jpg";
                    	
                    	if(file_exists($head_dir.$this_img))
                    		echo "<img src='/wp-content/uploads/header_images/$this_img' alt='$head_img_alt' />";
                    	else
                    		echo "<img src='/wp-content/uploads/header_images/1.jpg' alt='$head_img_alt' />";
                    	
                    endif; ?></div> <!-- page_photo -->
	<div id='page_area'>
		<div id='share_links'>
<?php
	$this_share_ttl = $GLOBALS['post']->post_title;
	$this_share_guid = $GLOBALS['post']->guid;
	$fb_u = urlencode($this_share_guid);
	$fb_ttl = str_replace("%2B","+",urlencode($this_share_ttl));
	$fb_link = "http://www.facebook.com/share.php?u=$fb_u&title=$fb_ttl";
?>
			share: &nbsp; <a href="<?php echo $fb_link; ?>" target='_blank'><img src='/wp-content/themes/catours/images/facebook-tiny.gif' ><span>facebook</span></a> &nbsp; <a href="javascript:;" onclick="panel_in('popup',this)" id='email_link'><img src='/wp-content/themes/catours/images/email-tiny.gif' ><span>email</span></a>
		</div>
		<div id='page_content'>
			<?php
			/* Run the loop to output the page.
			 * If you want to overload this in a child theme then include a file
			 * called loop-page.php and that will be used instead.
			 */
			get_template_part( 'loop', 'page' );
			?>
		</div> <!-- page_content -->
	
<div id='sidebar'>
<?php get_sidebar(); ?>
</div>
	<div class='clear'></div>
	</div> <!-- page_area -->
<?php get_footer(); ?>
<div id='popup'>
	<div class='pop_close'><a href='javascript:;'onclick="panel_out('popup');reset_friend_form();">x</a></div>
	<div id='popup_panel'>
	<div id='pop_content'>
		<div id='err_field'></div>
		<form action='javascript:;' onsubmit="return tell_friend()">
				<input type='hidden' name='cat_this_page' id='cat_this_page' value='<?php echo $post->ID; ?>'>
				<input type='hidden' name='cat_this_title' id='cat_this_title' value="<?php echo $post->post_title; ?>">
			<div class='element'>
				<label for="cat_send_email">Your Email:</label>
				<input type='text' name='cat_send_email' id='cat_send_email'>
			</div>
			<div class='element'>
				<label for="cat_frnd_email">Your Friend's Email:</label>
				<input type='text' name='cat_frnd_email' id='cat_frnd_email'>
			</div>
				<input type='submit' value='Share this page'>
		</form>
	</div>
	</div>
</div>
