<?php
/**
 * Template Name: Home page
 *
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package WordPress
 * @subpackage Twenty_Ten
 * @since Twenty Ten 1.0
 */

get_header(); ?>

	<div id='page_photo_home'>
		<div id='home_content'>
			<div id='home_box'>
			<?php
			/* Run the loop to output the page.
			 * If you want to overload this in a child theme then include a file
			 * called loop-page.php and that will be used instead.
			 */
			get_template_part( 'loop', 'page' );
			?>

			</div> <!-- home_box -->
			<div id='home_content_foot'>
			</div>
		</div> <!-- home_content -->
	</div> <!-- page_photo_home -->
	
<?php get_footer(); ?>
