<?php
/**
 * The Header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="main">
 *
 * @package WordPress
 * @subpackage Twenty_Ten
 * @since Twenty Ten 1.0
 */
?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>" />
<title><?php
	/*
	 * Print the <title> tag based on what is being viewed.
	 */
	global $page, $paged;

	wp_title( '|', true, 'right' );

	// Add the blog name.
	bloginfo( 'name' );

	// Add the blog description for the home/front page.
	$site_description = get_bloginfo( 'description', 'display' );
	if ( $site_description && ( is_home() || is_front_page() ) )
		echo " | $site_description";

	// Add a page number if necessary:
	if ( $paged >= 2 || $page >= 2 )
		echo ' | ' . sprintf( __( 'Page %s', 'twentyten' ), max( $paged, $page ) );

	?></title>
	
<link rel="shortcut icon" href="http://www.california-tour.com/favicon.ico" />
<link rel="icon" type="image/vnd.microsoft.icon" href="http://www.california-tour.com/favicon.ico" />
<link rel="profile" href="http://gmpg.org/xfn/11" />
<link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo( 'stylesheet_url' ); ?>" />
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
	<style type="text/css">
<!--
	#page_photo_home_rotate{
		display:block;
		position:relative;
		width:1000px;
		height:578px;
		z-index:10;
		margin:0px auto;
		background-color:#fff;
	/*	border: solid 1px #9900FF */
	}
	#slide_1, #slide_2{
		display:block;
		position:relative;
		width:1000px;
		height:578px;
		opacity:0;
		filter: alpha(opacity=0);
		color:#fff;
	}
	#slide_1{
		z-index:10;
		left:0px;
		float:left;
	}
	#slide_2{
		z-index:11;
		top:-578px;
		left:0px;
	}
	div#home_content{
		display:block;
		top:-515px;
		left:4px;
		z-index:99;
		margin-bottom:-500px;
	}
	#footer ul.xoxo{
		display:block;
		position:relative;
		float:left;
		top:-39px;
	}
-->
    </style>
	<script src='/wp-content/themes/catours/scripts-rotator.js' type='text/javascript' language='javascript'>
	</script>
<?php
	/* We add some JavaScript to pages with the comment form
	 * to support sites with threaded comments (when in use).
	 */
	if ( is_singular() && get_option( 'thread_comments' ) )
		wp_enqueue_script( 'comment-reply' );

	/* Always have wp_head() just before the closing </head>
	 * tag of your theme, or you will break many plugins, which
	 * generally use this hook to add elements to <head> such
	 * as styles, scripts, and meta tags.
	 */
	wp_head();
?>
</head>

<body <?php body_class(); ?>>
<div id="wrapper">
	<div id="header">
		<a href='/'><img src='/wp-content/themes/catours/images/ca-tours_logo.gif' id='logo'></a>
		<div id='head_box'>
			<div id='top_phone'>877 338 3883</div>
			<div id='second_line'><form role="search" method="get" id="searchform" action="/">
				<input type='text' name='s' id='search_field' value='Search' onfocus="if(this.value=='Search'){this.value='';}" onblur="if(this.value==''){this.value='Search';}">
				<input type='image' src='wp-content/themes/catours/images/search-go.gif' id='searchsubmit'>
			</form>
			| <a href="/booking/index.html">LOG IN</a> | <a href="/contact-us/">CONTACT US</a></div>
			<div id='third_line' style='margin:6px 10px 0px 0px;clear:right;display:block;position:relative;'><a href="http://california-tour.com/california/es/index.php"><img src="/california/images/sp.gif" width="46" height="14" alt="Español" /></a>&nbsp;&nbsp;&nbsp;<a href="http://california-tour.com/california/ja/index2.php"><img src="/california/images/jp.gif" width="46" height="14" alt="Japanese" /></a></div>
		</div>
	</div><!-- #header -->
		<div id="access" role="navigation">
		  <?php /*  Allow screen readers / text browsers to skip the navigation menu and get right to the good stuff */ ?>
			<div class="skip-link screen-reader-text"><a href="#content" title="<?php esc_attr_e( 'Skip to content', 'twentyten' ); ?>"><?php _e( 'Skip to content', 'twentyten' ); ?></a></div>
			<?php /* Our navigation menu.  If one isn't filled out, wp_nav_menu falls back to wp_page_menu.  The menu assiged to the primary position is the one used.  If none is assigned, the menu with the lowest ID is used.  */ ?>
			<?php wp_nav_menu( array( 'container_class' => 'menu-header', 'theme_location' => 'primary' ) ); ?>
		</div><!-- #access -->