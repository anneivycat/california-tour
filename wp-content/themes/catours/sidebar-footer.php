<?php
/**
 * The Footer widget areas.
 *
 * @package WordPress
 * @subpackage Twenty_Ten
 * @since Twenty Ten 1.0
 */

?>

<?php
	/* The footer widget area is triggered if any of the areas
	 * have widgets. So let's check that first.
	 *
	 * If none of the sidebars have widgets, then let's bail early.
	 */
	if (   ! is_active_sidebar( 'first-footer-widget-area'  )
		&& ! is_active_sidebar( 'second-footer-widget-area' )
		&& ! is_active_sidebar( 'third-footer-widget-area'  )
		&& ! is_active_sidebar( 'fourth-footer-widget-area' )
	)
		return;
global $wp_query;
	// If we get this far, we have widgets. Let do this.
?>

			<div id="footer-widget-area" role="complementary">
<?php 
if( 8333 == $wp_query->query_vars['p'] ):
	$first = get_post( '8663 ' );
	$fourth = get_post( '8665' ); ?>
	<div id="first" class="widget-area">
		<ul class="xoxo">
			<li><h3 class="widget-title"><?php echo $first->post_title; ?></h3></li>
			<li>
				<div class="textwidget">
					<?php echo $first->post_content; ?>
				</div>
			</li>
		</ul>
	</div>
	<div id="second" class="widget-area">
		<ul class="xoxo">
			<li>
				<?php wp_nav_menu( array( 'menu' => 35, 'container_class' => 'menu-bottom-navigation-new-container' ) ); ?>
			</li>
		</ul>
	</div>
	<div id="third" class="widget-area">
		<ul class="xoxo">
			<li>
				<?php wp_nav_menu( array( 'menu' => 37, 'container_class' => 'menu-bottom-navigation-new-container' ) ); ?>
			</li>
		</ul>
	</div>
	<div id="fourth" class="widget-area">
		<ul class="xoxo">
			<li>
				<div class="textwidget">
					<?php echo $fourth->post_content; ?>
				</div>
			</li>
		</ul>
	</div><?php
	
	dynamic_sidebar( 'page-sidebar-8333' );
else:

if ( is_active_sidebar( 'first-footer-widget-area' ) ) : ?>
				<div id="first" class="widget-area">
					<ul class="xoxo">
						<?php dynamic_sidebar( 'first-footer-widget-area' ); ?>
					</ul>
				</div><!-- #first .widget-area -->
<?php endif; ?>

<?php if ( is_active_sidebar( 'second-footer-widget-area1' ) ) : ?>
				<div id="second" class="widget-area">
					<ul class="xoxo">
						<?php dynamic_sidebar( 'second-footer-widget-area' ); ?>
					</ul>
				</div><!-- #second .widget-area -->
<?php endif; ?>

<?php if ( is_active_sidebar( 'third-footer-widget-area1' ) ) : ?>
				<div id="third" class="widget-area">
					<ul class="xoxo">
						<?php dynamic_sidebar( 'third-footer-widget-area' ); ?>
					</ul>
				</div><!-- #third .widget-area -->
<?php endif; ?>

<?php if ( is_active_sidebar( 'fourth-footer-widget-area1' ) ) : ?>
				<div id="fourth" class="widget-area">
					<ul class="xoxo">
						<?php dynamic_sidebar( 'fourth-footer-widget-area' ); ?>
					</ul>
				</div><!-- #fourth .widget-area -->
<?php endif; ?>

<?php if ( is_active_sidebar( 'fourth-footer-widget-area1' ) ) : ?>
				<div id="fourth" class="widget-area">
					<ul class="xoxo">
						<?php dynamic_sidebar( 'fourth-footer-widget-area' ); ?>
					</ul>
				</div><!-- #fourth .widget-area -->
<?php endif; ?><?php
 endif;?>

			</div><!-- #footer-widget-area -->
