<?php
/**
 * Template Name: Home page w/ rotating photos
 *
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package WordPress
 * @subpackage Twenty_Ten
 * @since Twenty Ten 1.0
 */

get_header("rotate"); ?>

	<div id='page_photo_home_rotate' style='background-image:none;'>
		<script type="text/javascript">

document.write("<img id='slide_1' src='/wp-content/themes/catours/images/home_images/home2.jpg' alt='California Tours Vacation Packages'><br/>");
document.write("<img id='slide_2' src='/wp-content/themes/catours/images/home_images/home1.jpg' alt='California Tours Vacation Packages'>");

</script>
<noscript>
  <img src="/wp-content/themes/catours/images/home_images/home2.jpg" width="900" height="500" id='fade_back'>
</noscript>
	</div> <!-- page_photo_home -->
	<script language='javascript'>
	change_img();
	</script>
	<div id='home_content'>
			<div id='home_box'>
			<?php
			/* Run the loop to output the page.
			 * If you want to overload this in a child theme then include a file
			 * called loop-page.php and that will be used instead.
			 */
			get_template_part( 'loop', 'page' );
			?>

			</div> <!-- home_box -->
			<div id='home_content_foot'>
			</div>
		</div> <!-- home_content -->
<?php get_footer(); ?>
