<?php
/**
 * Template Name: New Home page
 *
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package WordPress
 * @subpackage Twenty_Ten
 * @since Twenty Ten 1.0
 */

get_header('new_home'); ?>

	<div id='page_photo_home'>
		<div id='home_content'>
			<div id='home_content_foot'>
			</div>
		</div> <!-- home_content -->
	</div> <!-- page_photo_home -->
	
<?php get_footer('new_home'); ?>
