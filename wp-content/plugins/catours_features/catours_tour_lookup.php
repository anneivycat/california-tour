<?php
	include "catours_functions.php";
	include "catours_config.php";

function catour_get_tours($city,$tour,$tourtype){
	global $catour_year, $catour_day, $catour_month,$bookdates;
	$tour_list = "";
	
	if(isset($_REQUEST["bookdates"]))
		$bookdates = 1;
	else
		$bookdates = 0;
	
	if($tour != ""){
	
	 $query = "SELECT SCHEDULEDTOUR.SCHEDULEDTOUR_CODE,
  				TOUR.TOUR_ID,
				SCHEDULEDTOUR.SCHEDULEDTOUR_DEPART_DATE,
				CONCAT('20',SUBSTRING(SCHEDULEDTOUR.SCHEDULEDTOUR_DEPART_DATE,7)) AS year,
				TOUR.TOUR_NAME FROM SCHEDULEDTOUR,
				TOUR WHERE SCHEDULEDTOUR.TOUR_ID = TOUR.TOUR_ID
				AND SCHEDULEDTOUR.SCHEDULEDTOUR_CODE like '$city%'
				AND TOUR.TOUR_NAME like '%$tour%' 
				ORDER BY year,SCHEDULEDTOUR.SCHEDULEDTOUR_DEPART_DATE";
		}elseif($tourtype != ""){
			$query = "SELECT SCHEDULEDTOUR.SCHEDULEDTOUR_CODE, TOUR.TOUR_ID, SCHEDULEDTOUR.SCHEDULEDTOUR_DEPART_DATE, TOUR.TOUR_NAME FROM SCHEDULEDTOUR, TOUR WHERE SCHEDULEDTOUR.TOUR_ID = TOUR.TOUR_ID AND SCHEDULEDTOUR.SCHEDULEDTOUR_CODE like '$city%' AND TOUR.TOUR_TYPE like '%$tourtype%' ORDER BY SCHEDULEDTOUR.SCHEDULEDTOUR_DEPART_DATE";
		}
//	echo "$query";
  $mysql_result = mysql_query($query);
  $totalnumrows = mysql_num_rows($mysql_result);
  if ($totalnumrows>0) {
    $tour_list .= "<ul>";
    while ($row = mysql_fetch_array($mysql_result)) {
       $DepartureDate = $row['SCHEDULEDTOUR_DEPART_DATE'];
       $tourday = substr($DepartureDate,3,2);
       $tourmonth = substr($DepartureDate,0,2);
       $touryear = "20" . substr($DepartureDate,6,2);
       
       $tour_date_num = strtotime($DepartureDate);
       $show_after = time() - 86000;
       
       if ($tour_date_num > $show_after) {
             $ScheduledTourCode = strtoupper($row['SCHEDULEDTOUR_CODE']);
             $TourName = $row['TOUR_NAME'];
             $DepartureDate = $row['SCHEDULEDTOUR_DEPART_DATE'];
	     $TimeStamp = mktime(0,0,0,$tourmonth,$tourday,$touryear);
	     $PrettyDate = date("l, F j, Y", $TimeStamp);
             $SchedCodeLength = strlen($ScheduledTourCode);
             $ScheduledTourShortCode = substr($ScheduledTourCode,0,$SchedCodeLength - 2);
             $myURL="http://california-tour.com/booking/reservations/checkcalsign.php?ActivityCode=$ScheduledTourShortCode".substr($touryear,-2);
             $tour_list .= "\n<li><a href=\"$myURL\">$PrettyDate</a></li>\n";
	}
      }
      $tour_list .= "</ul>";
    }
	
	if($tour_list == "" || $tour_list == "<ul></ul>")
		$tour_list = "
		<p><em>No tours currently scheduled. Please <a href='/?page_id=40'>contact us</a> to arrange your tour.</em></p>";
	if($bookdates == 1)
		$tour_list .= "
	<script language='javascript' type='text/javascript'>
		show_tab('booking',4)
	</script>
	";
	
	return $tour_list;
}


?>