<?php
	include "catours_functions.php";
	include "catours_config.php";

	if(isset($_POST["f"]))
		$f_name = prep_email_input($_POST["f"]);
	else
		$f_name = "";
	
	if(isset($_POST["l"]))
		$l_name = prep_email_input($_POST["l"]);
	else
		$l_name = "";
	
	if(isset($_POST["i"])){
		$initial = prep_email_input($_POST["i"]);
			if(strlen($initial) > 3)
				$initial = substr($initial,0,2);
	}else{
		$initial = "";
	}
	if(isset($_POST["cp"]) && $_POST["cp"] == "phone")
		$cont_pref = "phone";
	else
		$cont_pref = "email";
	
	if(isset($_POST["e"]))
		$email = str_replace('\n','',str_replace(',','',str_replace(' ','',prep_email_input($_POST["e"]))));
	else
		$email = "";
	
	if(isset($_POST["t"]))
		$cust_title = prep_email_input($_POST["t"]);
	else
		$cust_title = "";
	
	if(isset($_POST["c"]))
		$country = prep_email_input($_POST["c"]);
	else
		$country = "";
	
	if(isset($_POST["p"]))
		$phone = prep_email_input($_POST["p"]);
	else
		$phone = "";
	
	if(isset($_POST["r"]) && $_POST["r"] == "contact"){
		$redir = "contact";
		$mail_subject = $contact_mail_subject;
	}else{
		$redir = "question";
		$mail_subject = $question_mail_subject;
	}
	if(isset($_POST["x"]))
		$ext = prep_email_input($_POST["x"]);
	else
		$ext = "";
	
	if(isset($_POST["q"]))
		$question = str_replace("\\n","\n",prep_email_input($_POST["q"]));
	else
		$question = "";
		
	
	$err_mess = "";
	$err_fields = "";
	
	if($f_name == ""){
		$err_mess .= "<li>Please provide your first name</li>";
		$err_fields .= "	f_name";
	}
	
	if($l_name == ""){
		$err_mess .= "<li>Please provide your last name</li>";
		$err_fields .= "	l_name";
	}
	
	if($email == "" || !check_email($email)){
		$err_mess .= "<li>Please provide a valid email address</li>";
		$err_fields .= "	email";
	}
	
	if($err_mess != ""){
		echo "error	$err_mess$err_fields";
	}else{
	
		$message = "The following question was submitted on the California Tours Website:

Name: ";

		if($cust_title != "")
			$message .= "$cust_title ";
			
$message .= "$f_name ";

		if($initial != "")
			$message .= "$initial";
			
$message .= " $l_name
Email: $email";
		if($phone != ""){
			$message .= "
Phone: $phone";
			if($ext != "")
				$message .= ", ext: $ext";
		}

		if($country != "")
			$message .= "
Country: $country ";

		if($question != "")
			$message .= "
			
Question:
$question
";
			$mailYes = mail($questions_email, $mail_subject, $message,
				"From: ".$email."\r\n");
		
		
		echo "success\t$redir\t";
	
	
	}


?>