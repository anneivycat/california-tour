<?php

/*
Plugin Name: CATour Features
Plugin URI: http://www.california-tours.com
Description: This custom plugin adds various features to the California Tours site 
Version: 0.1
Author: Tod Abbott
Author URI: http://www.almost-everything.com
*/

include "catours_question_form.php";
include "catours_tour_lookup.php";
include "catours_side_menu.php";

$_GLOBAL['questions_string'] = "[catours question form]";
$_GLOBAL['menu_string'] = "[catours sidebar sub menu]";
$_GLOBAL['friend_string'] = "[catours email friend link]";
$_GLOBAL['hidden_field_string'] = "***";  // for stripping "hidden" layout code from search results

$catour_post = $post->ID;
//echo "p: $catour_post";
function check_content($the_stuff){
	global $questions_string, $catour_post, $post,$friend_string,$hidden_field_string;
	

	if(strpos($the_stuff,$questions_string) >= 0){
		$the_content = catours_question_form();	
		$the_stuff = str_replace($questions_string,$the_content,$the_stuff);
	}
	if(strpos($the_stuff,$friend_string) >= 0){
		$friend_link = "<a href='javascript:;' onclick=\"panel_in('popup',this,)\"><img src='wp-content/themes/catours/images/email-tiny.gif' ><span>email</span></a>";
		$the_stuff = str_replace($friend_string,$friend_link,$the_stuff);
	}
	if(strpos($the_stuff,$hidden_field_string) >= 0){ // strips "hidden" layout code from search results
		$the_stuff = str_replace("***Overview content below***","",$the_stuff);
	}
	
	
	
	return $the_stuff;
}

function catour_booking_func( $atts ) {
	extract( shortcode_atts( array(
		'city' => '',
		'tour' => '',
		'tourtype' => '',
	), $atts ) );
	$the_tours = catour_get_tours($city,$tour,$tourtype);
	return $the_tours;
}
add_shortcode( 'catour_booking', 'catour_booking_func' );


function check_widget($the_stuff){
	global $menu_string, $catour_post, $post;

	if(strpos($the_stuff,$menu_string) >= 0){
		$the_menu = get_catours_menu();
		$the_stuff = str_replace($menu_string,$the_menu,$the_stuff);
	}
	return $the_stuff;
}


add_filter('the_content','check_content');
add_action('widget_text', 'check_widget');

?>