<?php

function catours_question_form(){

	$this_page_ttl = wp_title(" ",false);
	
	if(strpos($this_page_ttl,"Contact") > -1){
		$redir = "contact";
		$re_message = "";
	}else{
		$redir = "question";
		$re_message = "Regarding: $this_page_ttl";
	}
	
	
	
	$response = <<<END
	<p>For a question and quote request, please fill out following questionnaire. For a quote, please include number of travelers, travel dates, group's background and any special travel requests.</p>
<form action="javascript:;" id='question_form' onsubmit="return send_question()"> 
<div id='err_field'></div>
<br />
<em>Required *</em><br />
<input id="act" name="act" type="hidden" value="send" />
<input id="redir" name="redir" type="hidden" value="$redir" />
<div class="form_field">
<div class="form_label">How do you prefer to be contacted?</div><div class="radio_options_single"><label><input id="cont_pref" name="cont_pref" type="radio" value="email" checked /> Email</label> &nbsp;&nbsp;&nbsp; <label><input id="cont_pref" name="cont_pref" type="radio" value="phone" /> Daytime Phone</label></div><div style='clear:both'></div>
</div>
<div class="form_field"><label class="form_label">Email*</label><input id="email" name="email" type="text" /><div style='clear:both'></div></div>

<div class="form_field"><label class="form_label">Daytime Phone</label><input id="phone" name="phone" type="text" /><div style='clear:both'></div></div>

<div class="form_field"><label class="form_label">Extension</label><input id="ext" name="ext" type="text" style='width:60px;' /><div style='clear:both'></div></div>

<br />
<div class="form_field"><label class="form_label">Title</label><select id="cust_title" name="cust_title"> <option>Title</option> <option>Mr.</option> <option>Ms.</option> <option>Mrs.</option> <option>Dr.</option> </select><div style='clear:both'></div></div>

<div class="form_field"><label class="form_label">First Name*</label><input id="f_name" name="f_name" type="text" value=""> <div class='field_note'>(as printed on Passport)</div><div style='clear:both'></div></div>

<div class="form_field"><label class="form_label">Middle Initial</label><input id="initial" name="initial" type="text" style='width:30px;' /><div style='clear:both'></div></div>

<div class="form_field"><label class="form_label">Last Name*</label><input id="l_name" name="l_name" type="text" value="" /><div style='clear:both'></div></div>

<div class="form_field"><label class="form_label">Country</label><input id="country" name="country" type="text" /><div style='clear:both'></div></div>
<div class="form_field"><label class="form_label">Your Question:</label><textarea id="question_field" style="vertical-align: top;" name="question">$re_message
</textarea><div style='clear:both'></div></div>

<div class="form_field"><div class='field_note'><input type="submit" value="SEND YOUR QUESTION" /></div></div>
</form>
END;
	

	
	return $response;
}


?>