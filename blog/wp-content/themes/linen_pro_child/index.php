<?php get_header(); ?>
	<?php if ( is_front_page() || $wp_query->is_posts_page ) { $linen->front_page_posts(); } ?>
	<?php if (have_posts()) : ?>
	<?php while (have_posts()) : the_post(); ?>
		<div id="post-<?php the_ID(); ?>" <?php post_class('clear'); ?>>
			<div class="post-date-box <?php if ( !has_post_thumbnail() ) echo 'no-thumb'; ?>">
				<div class="post-date">
					<p><?php the_time( __( 'M j', 'linen' ) ); ?></p>
				</div>
				<?php if ( has_post_thumbnail() ) {
					the_post_thumbnail( 'index-thumb' );
				} ?>
				<div class="post-comments">
					
					
					<?php comments_popup_link( '',  __( '1 Comment', 'linen' ), sprintf( __( '%s Comments', 'linen' ), get_comments_number ()), '', ''); ?>
					
					
					
				</div>
			</div>
			<div class="entry">
				<h2 class="title">
					<a href="<?php the_permalink(); ?>" rel="bookmark" title="<?php esc_attr( sprintf( __( 'Permanent Link to %s', 'linen' ), the_title_attribute( 'echo=false' ) ) ); ?>"><?php the_title(); ?></a>
				</h2>
				
				<div class="single-post-front">
					<?php printf( __( 'Written by %s on', 'linen' ), get_the_author() ); ?> <?php the_date(); ?>
				</div>
			
			
				<?php the_excerpt(__('read more...', 'linen')); ?>
<p><span class="read_more">
<a title="Read more about: <?php the_title(); ?>" href="<?php the_permalink() ?>">Read more &raquo;</a></span></p>
				
				
				
				<?php edit_post_link( __( 'Edit this', 'linen' ), '<p>', '</p>' ); ?>
			</div><!--end entry-->
		</div><!--end post-->
	<?php endwhile; /* rewind or continue if all posts have been fetched */ ?>
	<?php if ( is_front_page() || $wp_query->is_posts_page ) { wp_reset_query(); } ?>
		<div class="navigation index">
			<?php if (function_exists( 'wp_pagenavi' )) : wp_pagenavi(); ?>
			<?php else : ?>
				<div class="alignleft"><?php next_posts_link( __( '&larr; Older Entries', 'linen' )); ?></div>
				<div class="alignright"><?php previous_posts_link( __( 'Newer Entries &rarr;', 'linen' )); ?></div>
			<?php endif; ?>
		</div><!--end navigation-->
	<?php else : ?>
	<?php endif; ?>
</div><!--end content-->
<?php get_sidebar(); ?>
<?php get_footer(); ?>