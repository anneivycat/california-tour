<!DOCTYPE html>
<html dir="ltr" lang="en-US">
<head>
<meta charset="UTF-8" />
<title>California Tours2</title>
<?php


?>
<link rel="stylesheet" href="http://www.california-tour.com/wp-content/plugins/sitepress-multilingual-cms/res/css/language-selector.css?v=2.0.4.1" type="text/css" media="all" />
	
<link rel="shortcut icon" href="http://www.california-tour.com/favicon.ico" />
<link rel="icon" type="image/vnd.microsoft.icon" href="http://www.california-tour.com/favicon.ico" />
<link rel="profile" href="http://gmpg.org/xfn/11" />
<link rel="stylesheet" type="text/css" media="all" href="http://www.california-tour.com/wp-content/themes/catours/style2.css" />
<link rel="pingback" href="http://www.california-tour.com/xmlrpc.php" />
	<style type="text/css">
<!--
	body{
		background-color:#ffffff !important;
	}
	div#wrapper,div#page_bottom{
		background-image:none !important;
	}
	#top_links a{
		font-weight:bold;
	}
	#page_photo_home_rotate{
		display:block;
		position:relative;
		width:980px;
		height:567px;
		z-index:10;
		margin:0px auto;
		background-color:#fff;
	/*	border: solid 1px #9900FF */
	}
	#slide_1, #slide_2{
		display:block;
		position:relative;
		width:980px;
		height:567px;
		opacity:0;
		filter: alpha(opacity=0);
		color:#fff;
	}
	#slide_1{
		z-index:10;
		left:0px;
		float:left;
	}
	#slide_2{
		z-index:11;
		top:-567px;
		left:0px;
	}
	div#home_content{
		display:block;
		top:-505px;
		left:4px;
		z-index:99;
		margin-bottom:-500px;
	}
-->
    </style>

	<script src='/wp-content/themes/catours/scripts-rotator.js' type='text/javascript' language='javascript'>
	</script>
<link rel="alternate" type="application/rss+xml" title="California Tours &raquo; Feed" href="http://www.california-tour.com/feed/" />
<link rel="alternate" type="application/rss+xml" title="California Tours &raquo; Comments Feed" href="http://www.california-tour.com/comments/feed/" />
<link rel="alternate" type="application/rss+xml" title="California Tours &raquo; Home Comments Feed" href="http://www.california-tour.com/new-home/feed/" />
<link rel='stylesheet' id='page-list-style-css'  href='http://www.california-tour.com/wp-content/plugins/sitemap/css/page-list.css?ver=3.8' type='text/css' media='all' />
<script type='text/javascript' src='http://www.california-tour.com/wp-content/uploads/jw-player-plugin-for-wordpress/player/jwplayer.js?ver=3.4.2'></script>
<script type='text/javascript' src='http://www.california-tour.com/wp-includes/js/comment-reply.js?ver=3.4.2'></script>
<link rel="EditURI" type="application/rsd+xml" title="RSD" href="http://www.california-tour.com/xmlrpc.php?rsd" />
<link rel="wlwmanifest" type="application/wlwmanifest+xml" href="http://www.california-tour.com/wp-includes/wlwmanifest.xml" /> 
<meta name="generator" content="WordPress 3.5" />

<!-- All in One SEO Pack 1.6.15 by Michael Torbert of Semper Fi Web Designob_start_detected [-1,-1] -->
<link rel="canonical" href="http://www.california-tour.com/" />

<!-- /all in one seo pack -->
<script type="text/javascript">var icl_lang = 'en';var icl_home = 'http://www.california-tour.com';</script>
<script type="text/javascript" src="http://www.california-tour.com/wp-content/plugins/sitepress-multilingual-cms/res/js/sitepress.js"></script>
<meta name="generator" content="WPML ver:2.0.4.1 stt:1,28,2;0;0;0" />
</head>

<body class="home page page-id-8333 page-template page-template-page-rotate_new-php">
<div id="wrapper">
	<div id="header">
		<a href='/'><img src='/wp-content/themes/catours/images/ca-tours-logo-lrger.gif' id='logo'></a>
		
		<div id='head_box'>
			<div id='top_links'>&nbsp;&nbsp;&nbsp;<a href="http://www.california-tour.com?lang=es"><a href="http://www.california-tour.com?lang=es">Español</a>&nbsp;&nbsp;&nbsp;<a href="http://www.california-tour.com?lang=ja"><a href="http://www.california-tour.com?lang=ja">日本語</a> <a href='/book-a-tour-from-los-angeles-san-francisco-san-diego-or-boston/' title='Book a tour from Los Angeles, San Francisco, San Diego, or Boston' id='make_reservation'>MAKE A RESERVATION</a></div>

		</div>
		
		<div id="access" role="navigation">
		  			<div class="skip-link screen-reader-text"><a href="#content" title="Skip to content">Skip to content</a></div>
						<div class="menu-header"><ul id="menu-top-navigation" class="menu"><li id="menu-item-53" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-53"><a title="California Vacation Packages" href="http://www.california-tour.com/california-vacation-packages/">California <span class='smaller'>Vacations</span></a>
<ul class="sub-menu">
	<li id="menu-item-3416" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-3416"><a title="California Vacation Packages Los Angeles &#8211; San Francisco" href="http://www.california-tour.com/california-vacation-packages/california-lax-sfo-vacation-packages/">California Vacation Packages Los Angeles &#8211; San Francisco</a></li>

	<li id="menu-item-218" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-218"><a title="California Vacation Packages San Francisco &#8211; Los Angeles" href="http://www.california-tour.com/california-vacation-packages/california-beyond-sfo-lax-vacation-packages/">California Vacation Package San Francisco &#8211; Los Angeles</a></li>
</ul>
</li>
<li id="menu-item-60" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-60"><a title="San Francisco Vacation Packages" href="http://www.california-tour.com/san-francisco-vacation-packages/">San&nbsp;Francisco <span class='smaller'>Vacations</span></a>
<ul class="sub-menu">
	<li id="menu-item-193" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-193"><a title="San Francisco &amp; Yosemite Vacation Packages" href="http://www.california-tour.com/san-francisco-vacation-packages/san-francisco-yosemite-vacation-packages/">San Francisco &#038; Yosemite Vacation Packages</a></li>
	<li id="menu-item-192" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-192"><a title="San Francisco &amp; Los Angeles by Coach Vacation Packages" href="http://www.california-tour.com/san-francisco-vacation-packages/san-francisco-los-angeles-by-coach-vacation-packages/">San Francisco &#038; Los Angeles by Coach Vacation</a></li>

	<li id="menu-item-191" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-191"><a title="San Francisco &amp; Los Angeles by Air Vacation Packages" href="http://www.california-tour.com/san-francisco-vacation-packages/san-francisco-los-angeles-by-air-vacation-packages/">San Francisco &#038; Los Angeles by Air Vacation</a></li>
	<li id="menu-item-190" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-190"><a title="San Francisco &amp; Las Vegas Vacation Packages" href="http://www.california-tour.com/san-francisco-vacation-packages/san-francisco-las-vegas-vacation-packages/">San Francisco &#038; Las Vegas Vacation</a></li>
	<li id="menu-item-210" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-210"><a title="San Francisco &amp; California Coast Vacation Packages" href="http://www.california-tour.com/san-francisco-vacation-packages/san-francisco-las-vegas-los-angeles-vacation-packages/">San Francisco &#038; California Coast Vacation</a></li>
</ul>
</li>

<li id="menu-item-59" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-59"><a title="New York Vacation Packages" href="http://www.california-tour.com/new-york-vacation-packages-2/">New York <span class='smaller'>Vacations</span></a>
<ul class="sub-menu">
	<li id="menu-item-186" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-186"><a title="New York Vacation Packages" href="http://www.california-tour.com/new-york-vacation-packages-2/new-york-vacation/">New York Vacation</a></li>
	<li id="menu-item-185" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-185"><a href="http://www.california-tour.com/new-york-vacation-packages-2/new-york-washington-d-c-vacation-packages/">New York &#038; Washington D.C. Vacation</a></li>
	<li id="menu-item-184" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-184"><a title="New York, Amish Country &amp; Philadelphia Vacation Packages" href="http://www.california-tour.com/new-york-vacation-packages-2/new-york-amish-country-philadelphia-vacation-packages/">New York, Amish Country &#038; Philadelphia Vacation</a></li>

	<li id="menu-item-183" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-183"><a title="New York, Boston &amp; Washington D.C. Vacation Packages" href="http://www.california-tour.com/new-york-vacation-packages-2/new-york-boston-washington-d-c-vacation-packages/">New York, Boston &#038; Washington D.C. Vacation</a></li>
</ul>
</li>
<li id="menu-item-58" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-58"><a title="Napa Valley Vacation Packages" href="http://www.california-tour.com/napa-valley-vacation-packages/">Napa Valley <span class='smaller'>Vacations</span></a>
<ul class="sub-menu">
	<li id="menu-item-215" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-215"><a title="Napa Valley &amp; San Francisco Vacation Packages" href="http://www.california-tour.com/napa-valley-vacation-packages/napa-valley-san-francisco-vacation-packages/">Napa Valley &#038; San Francisco Vacation</a></li>
	<li id="menu-item-214" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-214"><a title="Napa Valley &amp; San Francisco Connoisseur Vacation Packages" href="http://www.california-tour.com/napa-valley-vacation-packages/napa-valley-san-francisco-connoisseur-vacation/">Napa Valley &#038; San Francisco Connoisseur Vacation</a></li>

	<li id="menu-item-213" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-213"><a title="Napa Valley &amp; San Francisco Calistoga Spa Vacation Packages" href="http://www.california-tour.com/napa-valley-vacation-packages/napa-valley-san-francisco-calistoga-spa-vacation-packages/">Napa Valley &#038; San Francisco Calistoga Spa Vacation</a></li>
	<li id="menu-item-212" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-212"><a title="Napa Valley &amp; San Francisco Calistoga Resort Vacation Packages" href="http://www.california-tour.com/napa-valley-vacation-packages/napa-valley-san-francisco-calistoga-resort-vacation-packages/">Napa Valley &#038; San Francisco Calistoga Resort Vacation</a></li>
</ul>
</li>
<li id="menu-item-56" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-56"><a title="Las Vegas Vacation Packages" href="http://www.california-tour.com/las-vegas-vacation-packages/">Las Vegas <span class='smaller'>Vacations</span></a>
<ul class="sub-menu">
	<li id="menu-item-219" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-219"><a title="Las Vegas &amp; Grand Canyon Vacation Packages" href="http://www.california-tour.com/las-vegas-vacation-packages/las-vegas-grand-canyon-vacation-packages/">Las Vegas &#038; Grand Canyon Vacation</a></li>

	<li id="menu-item-223" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-223"><a title="Las Vegas, Zion National Park &amp; Grand Canyon Vacation Packages" href="http://www.california-tour.com/las-vegas-vacation-packages/las-vegas-zion-national-park-grand-canyon-vacation-packages/">Las Vegas, Zion National Park &#038; Grand Canyon Vacation</a></li>
	<li id="menu-item-222" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-222"><a title="Las Vegas &amp; Southwest National Parks Vacation Packages" href="http://www.california-tour.com/las-vegas-vacation-packages/las-vegas-southwest-national-park-vacation-packages/">Las Vegas &#038; Southwest National Parks Vacation</a></li>
	<li id="menu-item-221" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-221"><a title="Las Vegas &amp; San Francisco Vacation Packages" href="http://www.california-tour.com/las-vegas-vacation-packages/las-vegas-san-francisco-vacation-packages/">Las Vegas &#038; San Francisco Vacation</a></li>
	<li id="menu-item-220" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-220"><a title="Las Vegas &amp; Los Angeles Vacation Packages" href="http://www.california-tour.com/las-vegas-vacation-packages/las-vegas-los-angeles-vacation-packages/">Las Vegas &#038; Los Angeles Vacation</a></li>

</ul>
</li>
<li id="menu-item-57" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-57"><a title="Los Angeles Vacation Packages" href="http://www.california-tour.com/los-angeles-vacation-packages-2/">Los Angeles <span class='smaller'>Vacations</span></a>
<ul class="sub-menu">
	<li id="menu-item-106" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-106"><a title="Los Angeles Vacation Packages" href="http://www.california-tour.com/los-angeles-vacation-packages-2/los-angeles-vacation-packages/">Los Angeles Vacations</a></li>
	<li id="menu-item-105" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-105"><a title="Los Angeles &amp; Las Vegas by Coach Vacation Packages" href="http://www.california-tour.com/los-angeles-vacation-packages-2/los-angeles-las-vegas-by-coach-vacation-packages/">Los Angeles &#038; Las Vegas by Coach Vacation</a></li>
	<li id="menu-item-104" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-104"><a title="Los Angeles &amp; Las Vegas by Air Vacation Packages" href="http://www.california-tour.com/los-angeles-vacation-packages-2/los-angeles-las-vegas-by-air-vacation-packages/">Los Angeles &#038; Las Vegas by Air Vacation</a></li>

	<li id="menu-item-103" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-103"><a title="Los Angeles &amp; San Francisco by Coach Vacation Packages" href="http://www.california-tour.com/los-angeles-vacation-packages-2/los-angeles-san-francisco-by-coach-vacation-packages/">Los Angeles &#038; San Francisco by Coach Vacation</a></li>
	<li id="menu-item-7930" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-7930"><a href="http://www.california-tour.com/los-angeles-vacation-packages-2/los-angeles-san-francisco-by-air-vacation-packages/">Los Angeles &#038; San Francisco by Air Vacation</a></li>
	<li id="menu-item-101" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-101"><a title="Los Angeles, San Francisco &amp; Las Vegas by Air Vacation Packages" href="http://www.california-tour.com/los-angeles-vacation-packages-2/los-angeles-san-francisco-las-vegas-by-air-vacation-packages/">Los Angeles, San Francisco &#038; Las Vegas by Air Vacation</a></li>
</ul>
</li>

<li id="menu-item-55" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-55"><a title="Hawaii Vacation Packages" href="http://www.california-tour.com/hawaii-vacation-packages/">Hawaii<br /><span class='smaller'>Vacations</span></a>
<ul class="sub-menu">
	<li id="menu-item-189" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-189"><a title="Waikiki Beach Vacation Packages" href="http://www.california-tour.com/hawaii-vacation-packages/waikiki-beach-vacation-packages/">Waikiki Beach Vacation</a></li>
	<li id="menu-item-188" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-188"><a title="Maui Vacation Packages" href="http://www.california-tour.com/hawaii-vacation-packages/maui-vacation-packages/">Maui Vacation</a></li>
	<li id="menu-item-187" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-187"><a title="Big Island Vacation Packages" href="http://www.california-tour.com/hawaii-vacation-packages/big-island-vacation-packages/">Big Island Vacation</a></li>
</ul>
</li>
<li id="menu-item-61" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-61"><a title="Washington Vacation Packages" href="http://www.california-tour.com/washington-vacation-packages/">Washington <span class='smaller'>Vacations</span></a>

<ul class="sub-menu">
	<li id="menu-item-182" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-182"><a title="Washington, D.C. Vacation Packages" href="http://www.california-tour.com/washington-vacation-packages/washington-d-c-vacation-packages/">Washington D.C. Vacation</a></li>
	<li id="menu-item-181" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-181"><a title="Washington, D.C. &amp; New York Vacation Packages" href="http://www.california-tour.com/washington-vacation-packages/washington-d-c-new-york-vacation-packages/">Washington D.C. &#038; New York Vacation</a></li>
	<li id="menu-item-179" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-179"><a title="Washington, D.C., Philadelphia &amp; New York Vacation Packages" href="http://www.california-tour.com/washington-vacation-packages/washington-d-c-philadelphia-new-york-vacation-packages/">Washington D.C., Philadelphia &#038; New York Vacation</a></li>
</ul>
</li>
<li id="menu-item-54" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-54"><a title="Florida Vacation Packages" href="http://www.california-tour.com/florida-vacation-packages/">Florida <span class='smaller'>Vacations</span></a>

<ul class="sub-menu">
	<li id="menu-item-178" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-178"><a title="Orlando Theme Parks Vacation Packages" href="http://www.california-tour.com/florida-vacation-packages/orlando-theme-parks-vacation-packages/">Orlando Theme Parks Vacation</a></li>
	<li id="menu-item-176" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-176"><a title="Orlando Theme Parks &amp; Kennedy Space Center Vacation Packages" href="http://www.california-tour.com/florida-vacation-packages/orlando-theme-parks-kennedy-space-center-vacation-packages/">Orlando Theme Parks &#038; Kennedy Space Center Vacation</a></li>
	<li id="menu-item-175" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-175"><a title="Orlando Theme Parks &amp; Miami Vacation Packages" href="http://www.california-tour.com/florida-vacation-packages/orlando-theme-parks-miami-vacation-packages/">Orlando Theme Parks &#038; Miami Vacation</a></li>
</ul></li>
<li id="menu-item-54" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-54"><a title="Florida Vacation Packages" href="http://www.california-tour.com/florida-vacation-packages/">Vancouver <span class='smaller'>Vacations</span></a>
</li>
</ul></div>		</div><!-- #access -->

		
	</div><!-- #header -->

	<div id='page_photo_home_rotate' style='background-image:none;'>
		<script type="text/javascript">

document.write("<img id='slide_1' src='/wp-content/themes/catours/images/home_images/home2.jpg' alt='California Tours Vacation Packages'><br/>");
document.write("<img id='slide_2' src='/wp-content/themes/catours/images/home_images/home1.jpg' alt='California Tours Vacation Packages'>");

</script>
<noscript>
  <img src="/wp-content/themes/catours/images/home_images/home2.jpg" width="900" height="500" id='fade_back'>
</noscript>
	</div> <!-- page_photo_home -->
	<script language='javascript'>
	change_img();
	</script>
	<div id='home_content'>

		</div> <!-- home_content -->

	<div id="footer" role="contentinfo">
		
<div style='clear:both;'></div>



			<div id="footer-widget-area" role="complementary">

				<div id="first" class="widget-area">
					<ul class="xoxo">

						<li id="text-4" class="widget-container widget_text"><h3 class="widget-title">Limited Time Only</h3>			<div class="textwidget"><p><a title="Yosemite National Park" href="http://www.california-tour.com/group-travel-private-tours/group-travel-from-san-francisco-private-tours/national-park/">Yosemite National park from Los Angeles</a></p>
<p><a title="Napa Valley &amp; San Francisco Holiday" href="http://www.california-tour.com/usa_holiday/napa-valley-holiday-packages/napa-valley-san-francisco-holiday-packages/">Napa Valley &amp; San Francisco Vacation Special</a></p></div>
		</li>					</ul>
				</div><!-- #first .widget-area -->

				<div id="second" class="widget-area">

					<ul class="xoxo">
						<li id="nav_menu-8" class="widget-container widget_nav_menu"><div class="menu-bottom-navigation-new-container"><ul id="menu-bottom-navigation-new" class="menu"><li id="menu-item-8337" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-8337"><a title="Destination Management Company" href="http://www.california-tour.com/usa_holiday/usa-destination-management-company/">Destination Management Company</a></li>
<li id="menu-item-8338" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-8338"><a title="Group Travel Private Tours" href="/group-travel-private-tours/">Group Travel Private Tours</a></li>
<li id="menu-item-8339" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-8339"><a title="Corporate Incentive Travel" href="/corporate-incentive-travel/">Corporate Incentive Travel</a></li>
<li id="menu-item-8340" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-8340"><a title="Educational Tours" href="/educational-tours/">Educational Tours</a></li>
<li id="menu-item-8341" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-8341"><a title="Holiday Travels" href="/holiday-tours/">Holiday Travels</a></li>
</ul></div></li>					</ul>
				</div><!-- #second .widget-area -->

				<div id="third" class="widget-area">
					<ul class="xoxo">
						<li id="nav_menu-10" class="widget-container widget_nav_menu"><div class="menu-bottom-bus-tours-new-container"><ul id="menu-bottom-bus-tours-new" class="menu"><li id="menu-item-8362" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-8362"><a href="http://www.california-tour.com/usa_holiday/usa-guided-bus-tours-packages/">Guided Bus Tours &#038; Packages</a>
<ul class="sub-menu">
	<li id="menu-item-8363" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-8363"><a title="Tours &amp; Packages from Boston" href="http://www.california-tour.com/usa_holiday/usa-guided-bus-tours-packages/usa-tours-from-boston/">from Boston</a></li>
	<li id="menu-item-8365" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-8365"><a title="Tours &amp; Packages from San Francisco " href="http://www.california-tour.com/usa_holiday/usa-guided-bus-tours-packages/usa-tours-from-san-francisco-2/">from San Francisco</a></li>

	<li id="menu-item-8364" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-8364"><a title="Tours &amp; Packages from Los Angeles " href="http://www.california-tour.com/usa_holiday/usa-guided-bus-tours-packages/usa-tours-from-los-angeles/">from Los Angeles</a></li>
	<li id="menu-item-8366" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-8366"><a title="Tours &amp; Packages from San Diego" href="http://www.california-tour.com/usa_holiday/usa-guided-bus-tours-packages/usa-tours-from-san-diego/">from San Diego</a></li>
</ul>
</li>
</ul></div></li>					</ul>
				</div><!-- #third .widget-area -->

				<div id="fourth" class="widget-area">
					<ul class="xoxo">

						<li id="text-5" class="widget-container widget_text">			<div class="textwidget"><form role="search" method="get" id="searchform" action="/">
				<input type='text' name='s' id='search_field' value='Search' onfocus="if(this.value=='Search'){this.value='';}" onblur="if(this.value==''){this.value='Search';}">
				<input type='image' src='/wp-content/themes/catours/images/search-go-new.gif' onmouseover="this.src='/wp-content/themes/catours/images/search-go-new-on.gif'" onmouseout="this.src='/wp-content/themes/catours/images/search-go-new.gif'" id='searchsubmit'>
			</form>
			<ul id='soc_icons'>
			<li><a title="YouTube" href="http://www.youtube.com/user/CaliforniaToursInc"><img src='/wp-content/themes/catours/images/youtube-icon.gif' /></a></li>
<li id="menu-item-88" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-88"><a title="facebook" href="http://facebook.com/CaliforniaToursInc"><img src='/wp-content/themes/catours/images/facebook-icon.gif' /></a></li>
			<li><a title="Log In" href="http://www.california-tour.com/booking/index.html" style="margin-right:8px;display:inline-block;margin-top:3px;vertical-align:middle;">Log-In</a></li>

			</ul></div>
		</li>					</ul>
				</div><!-- #fourth .widget-area -->

			</div><!-- #footer-widget-area -->

<div style='clear:both;'></div>

		<div id='very_bottom_left' style='display:inline-block;position:relative;margin-top:-15px;float:left;width:350px;margin-left:0px;padding:20px 0px 5px 10px ;height:20px;border-right:solid 1px #797878'>
			<p id='copyright'>&copy;2012 California Tours All rights reserved. California Seller of Travel #2065568&minus;40</p>

		</div>
		<div id="footer-very_bottom_right" role="complementary">
				<div id="very_bottom" class="widget-area">
					<ul class="xoxo">
						<li id="nav_menu-3" class="widget-container widget_nav_menu"><div class="menu-foot-navigation-container"><ul id="menu-foot-navigation" class="menu">
						<li id="menu-item-73" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-73"><a title="About Us" href="http://www.california-tour.com/about-us/">About Us</a></li>
						<li id="menu-item-3078" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-3078"><a title="Policy" href="http://privacy-policy.truste.com/verified-policy/www.california-tour.com">Policy</a></li>
						<li id="menu-item-74" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-74"><a title="Contact Us" href="http://www.california-tour.com/contact-us/">Contact Us</a></li>

						<li id="menu-item-4381" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-4381"><a title="blog" href="http://www.california-tour.com/blog">Blog</a></li>
					</ul>

				</div><!-- #very_bottom .widget-area -->
		</div><!-- #footer-very_bottom_right .widget-area -->
		<div style='clear:both;'></div>
	</div><!-- #footer -->


<div style='clear:both;'></div>

	<ul id='bottom_logos'>
	<li><img src="/wp-content/themes/catours/images/foot-logo-TIA.gif" style='width:52px;height:24px' alt='TIA'></li>
	<li><img src="/wp-content/themes/catours/images/foot-logo-OiSF.gif" style='width:93px;height:23px' alt='Only in San Francisco'></li>
	<li><img src="/wp-content/themes/catours/images/foot-logo-LA.gif" style='width:75px;height:24px' alt='Los Angeles'></li>
	<li><img src="/wp-content/themes/catours/images/foot-logo-asta.gif" style='width:48px;height:24px' alt='ASTA'></li>
	<li><img src="/wp-content/themes/catours/images/foot-logo-etrust.gif" style='width:22px;height:26px' alt='eTrust'></li>
	<li><img src="/wp-content/themes/catours/images/foot-logo-ECMTA.gif" style='width:70px;height:28px' alt='ECMTA'></li>
</ul>

</div><!-- #wrapper -->

<div id='page_bottom'>
</div>
<p id="wpml_credit_footer"><a href="http://wpml.org/">Multilingual WordPress</a> by <a href="http://www.icanlocalize.com/site/">ICanLocalize</a></p><script type="text/javascript">
var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
</script>
<script type="text/javascript">
var pageTracker = _gat._getTracker("UA-4235877-1");
pageTracker._initData();
pageTracker._trackPageview();
</script>
</body>
</html>


